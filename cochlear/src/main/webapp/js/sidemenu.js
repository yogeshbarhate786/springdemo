// FUNCTION: 	traverse 
// DESC: 	traverses DOM nodes and calls a user specified function to determine actions to be taken on that node
// PARAMETERS:	node: the root node
//		visitor: the function that will be called
//		level: Only used internally. No need to call traverse with this parameter
// RETURNS:	true
// TESTED WITH:	IE 5.x, IE 6.x, Mozilla 1.6


function sidemenuSetupXstation( select )
{
	sidemenuSetup( "sidemenu", select );
}

// ***************************************
function sidemenuSelectXstation( id )
{
	sidemenuSelect( "sidemenu", id );
}


function traverse( node, visitor, level )
{
	if( !level ){ level = 0;}
	
	if( node && node.childNodes )
	{
		for( var i = 0; i < node.childNodes.length; i++ )
		{
			if( visitor(node.childNodes[i], level) )
			{
				traverse( node.childNodes[i], visitor, level + 1 );
			}
		}
	}
	
	return true;
}

// SIDEMENU SPECIFIC FUNCTIONS AND VARS

// map of all submenus in the page
var sidemenus = {};

// refers to the menu currently being setup
var curMenu = null;

// FUNCTION: 	sidemenuSelect 
// DESC: 	Called to select a sidemenu item. Usually when the item is clicked but also to programatically select an item.
// PARAMETERS:	name: name of the menu
//		id: id of the item you want to select
// RETURNS:	void
// TESTED WITH:	IE 5.x, IE 6.x, Mozilla 1.6
function sidemenuSelect( name, id )
{
	var menu = sidemenus[name];
	if( id === null && menu )
	{
		if( menu.selected ){
			menu.selected.className = "itemoff";}
	}
	else
	{
		var item = document.getElementById(id);
		if( item && menu )
		{
			if( menu.selected )
			{
				// TODO: should remove itemon and replace by itemoff instead since there can be more than one class
				menu.selected.className = "itemoff";
			}
			// select and make visible
			item.className = "itemon";
			parent.menuValueForLeft=''+id;
			menu.selected = item;
			var tmp = item.parentNode;
			while( tmp.tagName == "UL" )
			{
				tmp.style.display = "";
				tmp = tmp.parentNode.parentNode;
			}
		}
	}
}

// FUNCTION: 	sidemenuNodeSetup 
// DESC: 	Callback function for traverse. Will setup the DOM nodes for a valid sphinx sidemenu. 
// PARAMETERS:	node: node passed by traverse
//		level: current level in the DOM tree.
// RETURNS:	void
// TESTED WITH:	IE 5.x, IE 6.x, Mozilla 1.6
function sidemenuNodeSetup( node, level )
{
	if( node.tagName == "UL" && node.className.indexOf("nocollapse") == -1 )
	{
		var titleNode = node.previousSibling;
		while( titleNode )
		{
			if( ("" + titleNode.className).indexOf("title") != -1 ){ break;}
			titleNode = titleNode.previousSibling;
		}
		if( titleNode )
		{
			titleNode.style.cursor = "pointer";
			titleNode.onclick = function(){
				var parentClasses = ("" + this.parentNode.className).split(" ");
				var classes = "";
				for( var i = 0; i < parentClasses.length; i++ )
				{
					if( parentClasses[i] != "expanded" && parentClasses[i] != "collapsed" ){
						classes += parentClasses[i] + " ";}
				}
				
				if( node.style.display == "none" || ("" + this.parentNode.className).indexOf("collapsed") != -1)
				{
					this.parentNode.className = classes + "expanded";
					node.style.display = "";
				}
				else
				{
					this.parentNode.className = classes + "collapsed";
					node.style.display = "none";
				}
			};
		}

	}
	else if( node.tagName == "A" )
	{
		// exclude parent nodes that are not LI, that do not have an id and that do not have a class
		var par = node.parentNode;
		if( par.tagName == "LI" && par.id && par.className )
		{
			par.onclick = function() {
				sidemenuSelect( "" + curMenu, par.id );
			};
			if( par.className && par.className == "itemon" ){
				sidemenus[curMenu].selected = par;}
		}
		return false; // do not look further
	}
	return true;
}

// FUNCTION: 	sidemenuSetup 
// DESC: 	Will setup a valid sphinx sidemenu. This should be called right after the sidemenu is rendered.
// PARAMETERS:	name: name of the menu ( same as "id" attribute )
//		select: optional. Item that will be selected first
// RETURNS:	void
// TESTED WITH:	IE 5.x, IE 6.x, Mozilla 1.6
function sidemenuSetup( name, select )
{
	menu = document.getElementById( name );
	if( menu )
	{
		sidemenus[name] = {};
		sidemenus[name].selected = null;
		if( select ){
			sidemenuSelect( name, select );}
		curMenu = name;
		traverse( menu, sidemenuNodeSetup );
	}
}