function getSearch()
{
	var search = document.getElementById("txtSearch").value;	
	if(search != " "){
		Dashbord.doView(search);
	}	

}



function getState(){
	
	var countryid = document.getElementById("countryid").value;	
	if(countryid != " "){
		Doctor.getState(countryid,loadProcesslist2);
	}
	
}

function loadProcesslist2(stateList) {

	dwr.util.removeAllOptions("stateid");
	dwr.util.addOptions("stateid", [ {
		name : '-- Select --',
		id : ""
	} ], "id", "name");
	
	if (stateList != null && stateList.length != 0) {
		for (i = 0; i < stateList.length; i++) {
			dwr.util.addOptions("stateid", [ {
				name : stateList[i].stateName,
				id : stateList[i].id,
			} ], "id", "name");
		}
	}

}

function getCity(){
	
	var stateid = document.getElementById("stateid").value;	
	if(stateid != " "){
		Doctor.getCity(stateid,loadProcesslist3);
	}
	
}

function loadProcesslist3(cityList) {

	dwr.util.removeAllOptions("cityid");
	dwr.util.addOptions("cityid", [ {
		name : '-- Select --',
		id : ""
	} ], "id", "name");
	
	if (cityList != null && cityList.length != 0) {
		for (i = 0; i < cityList.length; i++) {
			dwr.util.addOptions("cityid", [ {
				name : cityList[i].cityName,
				id : cityList[i].id,
			} ], "id", "name");
		}
	}

}

function getCategory(){
	
	var maincategoryid = document.getElementById("maincategoryid").value;	
	if(maincategoryid != " "){
		/*document.getElementById("ij").style.display="block";		*/
		Product.getCategory(maincategoryid,loadProcesslist);
	}	
	}
function loadProcesslist(categorylist) //This method is get list. This parameter is same name as return java method list. 
{
	var categoryList = categorylist;	
	dwr.util.removeAllOptions("categoryid");
	dwr.util.addOptions("categoryid", [ {	name : '-- Select --',	id : ""	} ], "id", "name");
	
	if (categoryList != null && categoryList.length != 0) {
		for (i = 0; i < categoryList.length; i++) {
			dwr.util.addOptions("categoryid", [ {name : categoryList[i].name,id  : categoryList[i].id,} ], "id", "name");			
		}
	}
	
	$('#subcategoryid').val($("#subcategoryid option:first").val());//set first option as default for subcategory
}



function getSubCategory(){
	var categoryid = document.getElementById("categoryid").value;	
	if(categoryid != " "){
		/*document.getElementById("ij").style.display="block";		*/
		Product.getSubCategory(categoryid,loadProcesslist1);
	}	
	}
function loadProcesslist1(subcategorylist) //This method is get list. This parameter is same name as return java method list. 
{
	var subcategoryList = subcategorylist;	
	dwr.util.removeAllOptions("subcategoryid");
	dwr.util.addOptions("subcategoryid", [ {	name : '-- Select --',	id : ""	} ], "id", "name");
	
	if (subcategoryList != null && subcategoryList.length != 0) {
		for (i = 0; i < subcategoryList.length; i++) {
			dwr.util.addOptions("subcategoryid", [ {name : subcategoryList[i].name,id  : subcategoryList[i].id,} ], "id", "name");			
		}
	}
}

function ValidateMobileNumber(evt){  
	//var e = evt || window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode
	if (charCode != 46 && charCode > 31 
	&& (charCode < 48 || charCode > 57))
	    return false;
	    return true;
	}