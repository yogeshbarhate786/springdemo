
function RequiredInputFun(objValue,strError)
{
 var ret = true;
 var val = objValue.value;
 val = val.replace(/^\s+|\s+$/g,"");//trim
    if(eval(val.length) == 0) 
    { 
      	alert(strError)
		objValue.focus();
        ret=false; 
    }
return ret;
}


function InputTypeFun(objValue,strRegExp,strError)
{
   var ret = true;
    var charpos = objValue.value.search(strRegExp); 
    if(objValue.value.length > 0 &&  charpos >= 0) 
    { 
         alert(strError)
         ret = false; 
    }
 return ret;
}




function DontSelectChk(objValue,chkValue,strError)
{

	var pass = true;
	if(IsCheckSelected(objValue,chkValue)){
		pass=true;
	}
	else{
		pass=false
	}

	if(pass==false)
	{
     if(!strError || strError.length ==0) 
        { 
        	strError = "Can't Proceed as you selected "+objValue.name;  
        }//if			  
	alert(strError);
	  
	}
    return pass;
}


function IsCheckSelected(objValue,chkValue)
{
	var len=objValue.options.length;

	if(len){
		for(i=0;i<len;i++){

				if(objValue.options[i].selected==true){
					if(objValue.options[i].value==chkValue || objValue.options[i].value=="")
					{						
						return false;
					}
				}
		}
	}
		
	
	return true;
}

function EmailChk(objValue,strError)
{
var ret = true;
     if(objValue.value.length > 0 && !validateEmail(objValue.value)	 ) 
     { 
       alert(strError)
         ret = false; 
     }//if 
return ret;
}
