<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<body>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" valign="top" class="dashTitle" colspan="2">User
		Details</td>
	</tr>
	<tr>
		<td width="100%" align="left" valign="top" class="boxBg">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">

			<tr>
				<td width="50%" align="left" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="common.salutation" /></td>
						<td width="60%">${userData.person.salutation}</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="person.firstname" /></td>
						<td width="60%">${userData.person.firstName}</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="person.middlename" /></td>
						<td width="60%">${userData.person.middleName}</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="person.lastname" /></td>
						<td width="60%">${userData.person.lastName}</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="person.gender" /></td>
						<td width="60%">${userData.person.gender}</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="person.maritalstatus" /></td>
						<td width="60%">${userData.person.maritalStatus}</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="person.nickname" /></td>
						<td width="60%">${userData.person.shortName}</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="common.login" /> <fmt:message key="common.name" /></td>
						<td width="60%">${userData.login.loginName}</td>
					</tr>
				</table>
				</td>

				<td width="50%" align="left" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">


					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="person.address.fulladdress" /></td>
						<td width="60%">${userData.person.address.addressFull}</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="person.address.streetaddress" /></td>
						<td width="60%">${userData.person.address.addressStreet}</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="person.address.country" /></td>
						<td width="60%">${userData.person.address.country.countryName}</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="person.address.state" /></td>
						<td width="60%">${userData.person.address.state.stateName}</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="person.address.city" /></td>
						<td width="60%">${userData.person.address.city.cityName}</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="person.occupation" /></td>
						<td width="60%">${userData.person.occupation.occupationName}</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="person.birthdate" /></td>
						<td width="60%"><fmt:formatDate pattern="dd/MM/yyyy"
							value="${userData.person.birthDate}"></fmt:formatDate></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="person.nativelanguage" /></td>
						<td width="60%">${userData.person.nativeLanguage}</td>
					</tr>
					<tr>
						<td align="right" valign="top">&nbsp;</td>
						<td align="left" valign="top"><c:if
							test="${autho['USER_PROFILE_EDIT'] == 'Yes'}">
							<input type="button" class="buttonBg"
								name='<fmt:message
							key="edit" />'
								value='<fmt:message
							key="edit" />'
								onclick="window.open('profileEdit.do?id=<c:out value="${userData.id}"/>','_parent')" />
						</c:if> <input type="button" class="buttonBg"
							name='<fmt:message
							key="back" />'
							value='<fmt:message
							key="back" />'
							onclick="window.open('../../admin/dashboard/view.do','_parent')" />
						</td>
					</tr>

				</table>
				</td>

			</tr>

		</table>
		</td>
	</tr>
</table>

<input name="id" type="hidden" value="${userData.id}" />
</body>





