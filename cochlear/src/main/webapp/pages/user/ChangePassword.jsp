<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags"
	prefix="ajax"%>

<head>
<script language="javascript">

function Submit(form) {
var confirmPassword = form.confirmPassword.value;
var newPassword = form.newPassword.value;
var oldPassword = form.oldPassword.value;
if (confirmPassword && oldPassword &&  newPassword) 
	{
		if(!(confirmPassword==newPassword))
		{
			alert("New Password & Retype Password are not match");
			return false;
		}		
	}
}
</script>
</head>

<body>
<form name="loginForm" id="loginForm"
	action="<c:url value="/admin/user/changePassword.do"/>" method="post">


<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td width="60%" align="left" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" valign="top" class="dashTitle">Password
				Details</td>
			</tr>
			<tr>
				<td align="left" valign="top" class="boxBg">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="oldPassword" /><span class="mandtry">*</span></td>
						<td width="60%"><input id="oldPassword" name="oldPassword"
							type="password" size="11"></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="newPassword" /><span class="mandtry">*</span></td>
						<td width="60%"><input id="newPassword" name="newPassword"
							type="password" size="11"></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
							key="confirmPassword" /><span class="mandtry">*</span></td>
						<td width="60%"><input id="confirmPassword"
							name="confirmPassword" type="password" size="11"></td>
					</tr>
					<tr>
						<td align="right" valign="top">&nbsp;</td>
						<td align="left" valign="top"><input  type="submit" class="buttonBg" 
							name='<fmt:message
											key="save" />'
							value='<fmt:message
											key="save" />'
							onClick="return Submit(this.form)" /></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>

		<td width="40%" align="left" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">

			<tr>
				<td align="left" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">

				</table>
				</td>
			</tr>

		</table>
		</td>

	</tr>

</table>

<input type="hidden" name="loginId" value="${userData.login.id}">
</form>


<script language="JavaScript" type="text/javascript">

 var frmvalidator  = new Validator("loginForm");
 frmvalidator.addValidation("oldPassword","req","'Old Password' is mandatory");
 frmvalidator.addValidation("oldPassword","maxlen=20",	"Max length for 'Old Password' is 20");
 frmvalidator.addValidation("newPassword","req","'New Password' Field is mandatory");
 frmvalidator.addValidation("newPassword","maxlen=20",	"Max length for 'New Password' is 20");
 frmvalidator.addValidation("confirmPassword","req", "'Confirm Password' is mandatory"); 
</script>

</body>
