<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>

<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li class="active"><b>Company List</b></li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="description">Company Id</td>
							<td class="description">Company Name</td>
							<td class="">Phone No</td>
							<td class="description">Email</td>
							<td></td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					
					<c:forEach items="${CompanyList}" var="companylistvar">
						<tr>
							<td class="cart_description">
								<p>${companylistvar.id}</p>
							</td>
							<td class="cart_description">
								<p>${companylistvar.companyName}</p>
							</td>
							<td class="cart_price">
								<p> ${companylistvar.phone}</i></p>
							</td>
							<td class="cart_price">
								<p> ${companylistvar.email}</i></p>
							</td>
							
							<td class="cart_quantity">
								<a class="cart_quantity_up" href="#" onclick="window.open('../../admin/company/edit.do?id=<c:out value="${companylistvar.id}"/>','_parent')">
								Edit</a>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="#" onclick="window.open('../../admin/company/delete.do?id=<c:out value="${companylistvar.id}"/>','_parent')">
								<i class="fa fa-times"></i></a>
							</td>
						</tr>
					</c:forEach>
						

						
					</tbody>
				</table>
			</div>
		</div>
	</section>
</html>