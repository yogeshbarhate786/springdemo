<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Address.js'></script>	
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Doctor.js'></script>		
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
<script type="text/javascript">

function ValidateAlpha(evt)
{
    var keyCode = (evt.which) ? evt.which : evt.keyCode
    if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 122))
     
    return false;
       
    return true;
}

function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.charCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

   return true;
}

function ValidateAlphaAndNum(evt)
{
    var keyCode = (evt.which) ? evt.which : evt.keyCode
    		  if ((keyCode > 31 && (keyCode < 48 || keyCode > 57))&&((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 122) && keyCode!=32))
    		      return false;
    return true;
}
   
function addCompany(form) {
		
		if(form.companyname.value == "") 
		  {
		    alert("Company Name cannot be blank");
		    form.companyname.focus();
		    return false;
		  }
		
		if(form.details.value == "") 
		  {
		    alert("Details cannot be blank");
		    form.details.focus();
		    return false;
		  }
		
		if (document.companyForm.email.value == "")
	    {
	    alert("Please enter your Email!");
	    document.companyForm.email.focus();
	    return false;
	    }
	    else
	    {
	    var str=document.companyForm.email.value
	    var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([com net org]{3}(?:\.[a-z]{6})?)$/i
	    if (!filter.test(str))
	    {

	    alert("Please enter a valid email address!")
	     document.companyForm.email.focus();
	    return false;
	     }
	   }
	    
	    var a = document.companyForm.phone.value;
	    if(a=="")
	    {
	    alert("please Enter the Contact Number");
	    document.companyForm.phone.focus();
	    return false;
	    }
	    if(document.getElementById("phone").value.length !=10 )
	    {
	       alert("Mobileno must be 10 digit"); 
	       document.getElementById("phone").focus(); 
	       return false;
	    }
	    
	    var e = document.getElementById("countryid");
	    var strUser = e.options[e.selectedIndex].value;
	    if(strUser==0)
	    {
	    alert("Please select a Country");
	    return false;
	    }
	    
	    var e = document.getElementById("stateid");
	    var strUser = e.options[e.selectedIndex].value;
	    if(strUser==0)
	    {
	    alert("Please select a State");
	    return false;
	    }
	    
		var e = document.getElementById("cityid");
	    var strUser = e.options[e.selectedIndex].value;
	    if(strUser==0)
	    {
	    alert("Please select a City");
	    return false;
	    }
	    
	  }
 </script>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="login-form">
					<!--login form-->
					<form action="../../admin/company/create.do" method="post"
						id="companyForm" name="companyForm" onsubmit="return addCompany(this)" enctype="multipart/form-data" >

						<div id="form-textarea">
							<label class="required">Company Name<span>*</span></label> <input
								type="text" placeholder="companyname" name="companyname"  id="companyname" onkeypress="ValidateAlphaAndNum(event);"/>
						</div>
						<br>
						<div id="form-textarea">
							<label class="required">Details<span>*</span></label>
							<textarea id="question-details" cols="58" rows="8" name="details"></textarea>
						</div>
						<br>
						<div id="form-textarea">
							<label class="required">Phone<span>*</span></label> 
							<input type="text" placeholder="phone"  name="phone" id='phoneno'  maxlength="10"  onkeypress="return isNumberKey(event);" pattern="[789][0-9]{9}"/>
						</div>
						<br>
						<div id="form-textarea">
							<label class="required">Email<span>*</span></label> <input
								type="text" placeholder="email" name="email" id="email" />
						</div>
						<br>

						<div id="form-textarea">
							<label class="required">Country</label>
							<select id="countryid" name="countryid"
								class="form-control" onchange="getState()">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
								<c:forEach items="${countrylist}" var="country">
									<option value="${country.id}">${country.countryName}</option>
								</c:forEach>
							</select>
						</div>
						<br>
						<div id="form-textarea">
							<label class="required">State</label>
							<select id="stateid" name="stateid" class="form-control"
								onchange="getCity()">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
							</select>
						</div>
						<br>
						<div id="form-textarea">
							<label class="required">City</label>
							<select id="cityid" name="cityid"
								class="form-control">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
							</select>
						</div>
						<br>
						

						<div style="display: inline;" align="center">
							<div style="float: left;">
								<button type="submit" id="" value="Add" >ADD</button>
							</div>
							<div style="margin-left:20px; float: left;">
								<button type="button" name="LIST" onclick="window.open('list.do','_parent')" >LIST</button>
							</div>
						</div>
						<br style="clear:both;">

						
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>