<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
  <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">  
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">	
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
.vertical-menu {
    width: 200px;
     background-color: #FE980F;
}

.vertical-menu a {
    background-color: #eee;
    color: black;
    display: block;
    padding: 12px;
    text-decoration: none;
     width: 282px;
}

.vertical-menu a:hover {
    background-color: #ccc;
    
}

.vertical-menu a.active {
    background-color: #FE980F;
    color: white;
     width: 250px;
}
</style>
<style>
.button {
	background-color: #FE980F;
	border: none;
	color: black;
	padding: 10px 25px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 13px;
	margin: 4px 2px;
	cursor: pointer;
	border-radius: 5px;
}
.button1 {
	background-color: #FE980F;
	border: none;
	color: black;
	padding: 9px 15px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 13px;
	margin: 4px 2px;
	cursor: pointer;
	border-radius:5px;
	
}
</style>
<style>
#snackbar {
	visibility: hidden;
	min-width: 250px;
	margin-left: -125px;
	background-color: red;
	color: #fff;
	text-align: center;
	border-radius: 2px;
	padding: 16px;
	position: fixed;
	z-index: 1;
	left: 50%;
	bottom: 30px;
	font-size: 17px;
	
}

#snackbar2 {
	visibility: hidden;
	min-width: 250px;
	margin-left: -125px;
	background-color: green;
	color: #fff;
	text-align: center;
	border-radius: 2px;
	padding: 16px;
	position: fixed;
	z-index: 1;
	left: 50%;
	bottom: 30px;
	font-size: 17px;
}

#snackbar.show {
	visibility: visible;
	-webkit-animation: fadein 0.2s, fadeout 0.2s 1.5s;
	animation: fadein 0.2s, fadeout 0.2s 1.5s;
}

#snackbar2.show {
	visibility: visible;
	-webkit-animation: fadein 0.2s, fadeout 0.2s 1.5s;
	animation: fadein 0.2s, fadeout 0.2s 1.5s;
}

@
-webkit-keyframes fadein {
	from {bottom: 0;
	opacity: 0;
}

to {
	bottom: 30px;
	opacity: 1;
}

}
@
keyframes fadein {
	from {bottom: 0;
	opacity: 0;
}

to {
	bottom: 30px;
	opacity: 1;
}

}
@
-webkit-keyframes fadeout {
	from {bottom: 30px;
	opacity: 1;
}

to {
	bottom: 0;
	opacity: 0;
}

}
@
keyframes fadeout {
	from {bottom: 30px;
	opacity: 1;
}

to {
	bottom: 0;
	opacity: 0;
}
}
</style>
<style>
.slidecontainer {
    width: 100%;
}

.slider {
    -webkit-appearance: none;
    width: 100%;
    height: 25px;
    background: #d3d3d3;
    outline: none;
    opacity: 0.7;
    -webkit-transition: .2s;
    transition: opacity .2s;
}

.slider:hover {
    opacity: 1;
}

.slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 25px;
    height: 25px;
    background: #4CAF50;
    cursor: pointer;
}

.slider::-moz-range-thumb {
    width: 25px;
    height: 25px;
    background: #4CAF50;
    cursor: pointer;
}
</style>


<!-- <section id="slider">slider
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
							<div class="item active" style="padding-left: 5px">
								<div class="col-sm-12">
									<img src="../../template/images/home/girl1.jpg" class="girl img-responsive" alt="" />
								</div>
							</div>
							<div class="item" style="padding-left: 5px">
								<div class="col-sm-12">
									<img src="../../template/images/home/girl2.jpg" class="girl img-responsive" alt="" />
								</div>
							</div>
						</div>
						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>/slider -->
<!-- <div class="alert alert-success" id="success-alert" >
   Product have added to your wishlist.
   </div> -->
 <c:if test="${ROLE ne LOGINROLE_TELECALLER }">
<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="left-sidebar">
				<p>  
			       <div class="w3-container">
						  <h2>Filters</h2>
						<form action="../../admin/dashboard/view.do" onsubmit="return checkForm(this);">
							<br>
							 <div class="w3-dropdown-hover">
						     <label>Min Price</label>&nbsp;&nbsp;<i class="fa fa-rupee" style="font-size:15px"></i>
						    <select name="select1" id="select1" style="margin-bottom: 20px" >  
								  <c:forEach items="${PriceList}" var="list">
								  <option value="${list.price}">${list.price}</option>
								 </c:forEach>
								</select>
							 <label>Max Price</label>&nbsp;&nbsp;<i class="fa fa-rupee" style="font-size:15px"></i>
							 <select name="select2" id="select2" style="margin-bottom: 20px"> 
								<c:forEach items="${PriceList}" var="list">
								   <option value="${list.price}">${list.price}</option>
								 </c:forEach>
								</select>	
						  </div>
						 <button  class="button1"  onclick="Myfunction()" >OK</button>
					</form> 
				</div>  
				<div class="w3-container ">
						  <h2>Sort By</h2>
						<form action="../../admin/dashboard/view.do" onsubmit="return checkForm(this);">
							<br>
							<div class="vertical-menu " style="cursor:pointer">
							  <!-- <a href="#" class="active">Home</a> -->
							  <a onclick="window.open('../../admin/dashboard/view.do?sortid=lowtohigh','_parent')" >Popular</a>
							   <a onclick="window.open('../../admin/dashboard/view.do?sortid=lowtohigh','_parent')">Price-Low to High</a>
							  <a onclick="window.open('../../admin/dashboard/view.do?sortid=hightolow','_parent')">Price-High to Low</a>
							   <a onclick="window.open('../../admin/dashboard/view.do?sortid=newest','_parent')">Newest First</a>
							</div>
						
					</form> 
				</div>  
					<h2>Category</h2>
					<div class="panel-group category-products" id="accordian">
						<!--category-productsr-->
						<c:forEach items="${categoryList}" var="list">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian"
											href="#${list.id}"> <span class="badge pull-right"><i
												class="fa fa-plus"></i></span>${list.name}
										</a>
									</h4>
								</div>
								<div id="${list.id}" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<c:forEach items="${subCategoryList}" var="sublist">
												<c:if test="${sublist.maincategoryid.id==list.id }">
													<li><a
														href="../../admin/dashboard/list.do?id=<c:out value="${sublist.id}"/> ">${sublist.name}</a></li>
												</c:if>
											</c:forEach>
										</ul>
									</div>
								</div>
							</div>
						</c:forEach>
					</div>
					<!--/category-products-->

					<div class="brands_products">
						<!--brands_products-->
						<h2>Products</h2>
						<div class="brands-name">
							<ul class="nav nav-pills nav-stacked">
								<c:forEach items="${subCategoryList}" var="sublist">
									<li><a
										href="../../admin/dashboard/list.do?id=<c:out value="${sublist.id}"/> ">
											<span class="pull-right"></span>${sublist.name}
									</a></li>
								</c:forEach>
							</ul>
						</div>
					</div>
							
						


				</div>
			</div>
			<%--  <div class="col-sm-9 padding-left">
				<form action="../../admin/dashboard/view.do" method="post">
					<div class="row">
						<div class="col-xs-6 col-md-4">
							<div class="input-group">
								<input list="browsers" class="form-control" placeholder="Search"
									id="txtSearch" name="txtsearch"
									style="width: 700px; height: 40px" />
								<datalist id="browsers">
									<c:forEach items="${ProductsList}" var="products">
										<option value="${products.name}">
									</c:forEach>
								</datalist>

								<div class="input-group-btn">
									<button type="submit" style="width: 60px; height: 40px"
										onclick="getSearch()">
										<span class="glyphicon glyphicon-search"></span>
									</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div> 
 --%>

			<div class="col-sm-9 padding-right">
				<div class="features_items">
					<!--features_items-->
					<div class="boxedtitle page-title">
						<%-- <h2>
							<span style="font-size: 12px; color: green;">Found
								${fn:length(ProductsList)} products</span>
						</h2> --%>
					</div>
					<h2 class="title text-center">Features Items</h2>
					<c:if test="${empty ProductsList}">
						<div class="container text-center">
							<div class="content-404">
								<img src="../../template/images/404/404.png" class="img-responsive" alt="" />
								<h1><b>OPPS!</b> We Couldn’t Find this Product</h1>
								<p>Uh... you found something different.</p>
								
							</div>
						</div>
					
					</c:if>
					<c:if test="${not empty ProductsList}">
					<c:forEach items="${ProductsList}" var="products">
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										
										<a class="" href="#" onclick="window.open('../../admin/productdetails/prodetails.do?productid=<c:out value="${products.id}"/>','_parent')"> <img src="${products.picurl}" width="200" height="240" /></a>
										<%-- <h2>${ products.price}</h2> --%>
										<%-- <p>${fn:substring(products.name, 0, 20)}</p> --%>
										<p><a class="" href="#" onclick="window.open('../../admin/productdetails/prodetails.do?productid=<c:out value="${products.id}"/>','_parent')"> ${products.name}</a></p>
										<br>
										<p>
											<i class="fa fa-rupee"> ${products.price}</i>
										</p>
                                        <c:if test="${ROLE eq LOGINROLE_DOCTOR}">

										<button id="btnwish${products.id}" type="button" 
											class="btn btn-default" value="${products.id}" style="margin: 5px"
											onclick="loadDoc(this.value)">
											<span class="glyphicon glyphicon-heart"></span> Add To
											Wishlist 
										</button>
										<div id="snackbar"><span class="glyphicon glyphicon-heart"></span><space> </space>Product Added Into Wishlist..</div>
										<%-- <button type="button" class="button" value="${products.id}" style="margin-top:10px "
											onclick="loadCart(this.value)">
											<i class="fa fa-shopping-cart"></i> Add to cart
										</button>
										<div id="snackbar2"><span class="glyphicon glyphicon-ok"></span><space> </space>Product Added Into Cart..</div> --%>
									   </c:if> 
									</div>
									<!-- <div class="product-overlay">
										<div class="overlay-content">
											<h2>$56</h2>
											<p>Easy Polo Black Edition</p>
											<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
										</div>
									</div> -->
								</div>


							</div>
						</div>
					</c:forEach>
					</c:if>
				</div>

				<!--features_items-->

				<!-- recommended_items -->
				<!-- <div class="recommended_items">
						<h2 class="title text-center">recommended items</h2>
						
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<div class="item active">	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="../../template/images/home/recommend1.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="../../template/images/home/recommend2.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="../../template/images/home/recommend3.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
								</div>
								<div class="item">	
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="../../template/images/home/recommend1.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="../../template/images/home/recommend2.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="../../template/images/home/recommend3.jpg" alt="" />
													<h2>$56</h2>
													<p>Easy Polo Black Edition</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </a>
							  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </a>			
						</div>
					</div> -->
				<!--/recommended_items-->

			</div>
		</div>
	</div>
</section>
</c:if>
<script>
function checkForm(form)
{
var select1 = document.getElementById("select1").value;
 var select2 = document.getElementById("select2").value;
 
 if(form.select1.value == "") 
  {
    alert("Error: MinPrice cannot be blank!");
    form.select1.focus();
    return false;
  }
 if(form.select2.value == "") 
 {
   alert("Error: MaxPrice cannot be blank!");
   form.select2.focus();
   return false;
 }
if(Number(select2) < Number(select1))
 {
  
	   alert("Error: Maximum Price is less Than Minimum Price");
	    form.select2.focus();
	    return false;
 }
}
</script>	

 <script>
$(document).ready(function(){
	$('select[name="select1"]').change(function(){
		
		 $.ajax({
		      type: "post",
		      url:"../../admin/dashboard/view.do",
		  data : {
			   minprice : $(this).val(),
			    },
		      
				dataType: "json",
		      success: function(data){
		  /*   alert(data.cartlist);  */
		      	
		  
		      },
		      error:function(e)
		      {
		         
		      }
		  });
	    
	});
});
</script> 
<script>
	function loadDoc(productid) {
		var x = document.getElementById("snackbar")
		x.className = "show";
		setTimeout(function() {
			x.className = x.className.replace("show", "");
		}, 1500);

		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				myFunction(this);
			}
		};
		
		var buttonwish= document.getElementById("btnwish"+productid);
		buttonwish.style.backgroundColor = "#ff80d5"
		 document.getElementById("btnwish"+productid).innerHTML = "Added To Wishlist";
		 document.getElementById("btnwish"+productid).disabled = 'disabled';
		 
		xhttp.open("GET", "../../admin/order/addwishlist.do?productid="
				+ productid, true);
		xhttp.send();
	}
</script>




<script>
	function loadCart(productid) {
		var x = document.getElementById("snackbar2")
		x.className = "show";
		setTimeout(function() {
			x.className = x.className.replace("show", "");
		}, 1500);
		
		$.ajax({
	        type: "post",
	        url:"../../admin/cart/create.do",
	   data : {
	        	productid : productid,
	        },
	        
			dataType: "json",
	        success: function(data){
          /*   alert(data.cartlist);  */
	        
	        var value2 = document.getElementById("cartvalue").innerHTML;
	       document.getElementById("cartvalue").innerHTML = data.cartlist;
	        },
	        error:function(e)
	        {
	           
	        }
	    });
	}
	
</script>




