<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<body>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="left" valign="top" class="dashTitle" colspan="2">Role
		Details</td>
	</tr>
	<tr>
		<td width="60%" align="left" valign="top" class="boxBg">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
					key="common.name" /></td>
				<td width="60%">${roleData.roleName}</td>
			</tr>
			<tr>
				<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
					key="common.desc" /></td>
				<td width="60%">${roleData.roleDesc}</td>
			</tr>
			<tr>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top"><c:if
					test="${autho['ROLE_EDIT'] == 'Yes'}">
					<input  type="button" class="buttonBg"  name='<fmt:message
					key="edit" />'
						value='<fmt:message
					key="edit" />'
						onclick="window.open('edit.do?id=<c:out value="${roleData.id}"/>','_parent')" />
				</c:if> <c:if test="${autho['ROLE_LIST'] == 'Yes'}">
					<input  type="button" class="buttonBg"  name='<fmt:message
					key="goToList" />'
						value='<fmt:message
					key="goToList" />'
						onclick="window.open('list.do','_parent')" />
				</c:if></td>
			</tr>


		</table>
		</td>
		<td width="40%" align="left" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="40%" align="left" valign="top"></td>
				<td width="60%"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>

</body>
