<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<body>
<form name='roleForm' id='roleForm'
	action='<c:url value="/admin/role/create.do"/>' method="post">


<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="left" valign="top" class="dashTitle" colspan="2">Role
		Details</td>
	</tr>
	<tr>
		<td width="60%" align="left" valign="top" class="boxBg">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">

			<tr>
				<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
					key="common.name" /><span class="mandtry">*</span></td>
				<td width="60%"><input name="roleName" type="text" value="" /></td>
			</tr>
			<tr>
				<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
					key="common.desc" /></td>
				<td width="60%"><textarea name="roleDesc" rows="2" cols="25"
					maxlength="254" onkeyup="return ismaxlength(this)"></textarea></td>
			</tr>
			<tr>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top"><c:if
					test="${autho['ROLE_ADD'] == 'Yes'}">
					<input  type="submit" class="buttonBg"  name='<fmt:message
											key="save" />'
						value='<fmt:message
											key="save" />' />
				</c:if> <c:if test="${autho['ROLE_LIST'] == 'Yes'}">
					<input  type="button" class="buttonBg" 
						name='<fmt:message
											key="goToList" />'
						value='<fmt:message
											key="goToList" />'
						onclick="window.open('list.do','_parent')" />
				</c:if></td>
			</tr>
		</table>
		</td>
		<td width="40%" align="left" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="40%" align="left" valign="top"></td>
				<td width="60%"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>

</form>

<script language="JavaScript" type="text/javascript">	 
	 var frmvalidator  = new Validator("roleForm");
	  frmvalidator.addValidation("roleName","req","Role Name Is Mandatory");
	  frmvalidator.addValidation("roleName","maxlen=40","Max length For Role Name is 40");  
	  frmvalidator.addValidation("roleName","alphanumeric_space","Only Alphanumberic Characters With Space Are Allowed For Role Name");
	  frmvalidator.addValidation("roleDesc","maxlen=254","Max Length For Description Is 254");	  	
</script>
</body>