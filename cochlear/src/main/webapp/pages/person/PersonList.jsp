<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags" prefix="ajax"%>
<link href="../../css/AutoComplete.css" rel="stylesheet" type="text/css"></link>

<div align="right"><input  type="button" class="buttonBg"  name='<fmt:message key="add" />' 
	value='<fmt:message key="add" />' onclick="window.open('add.do','_parent')" />
</div>

<br>
<body>

<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center">
	<tr>
		<td >
			<fieldset>
			<legend class="dashTitle">Search Person</legend>
					<form name='personForm'
						action='<c:url value="/admin/person/list.do"/>' method="post">
						<table width="100%" border="0" cellspacing="0" cellpadding="5"
							align="center">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="5"
										align="center">
										<tr>
											<td align="left" class="leftColon" valign="top"><fmt:message
													key="person.firstname" />
											<td><input id="firstName" name="firstName1" value="${first}"
												type="text" style="width: 182px" maxlength="50"/></td>
										</tr>
												
																	
									</table>
									<div align="right">
										<input type="submit" class="buttonBg" name='Search'
											value='Search' /> <input type="button" class="buttonBg"
											name="Clear Search" value="Clear Search"
											onclick="window.open('list.do','_parent')" />
									</div>
								</td>
							</tr>
						</table>
					</form>
			</fieldset>
			
			<legend class="dashTitle">List </legend>

			<ajax:displayTag id="displayTagFrame">
				<display:table name="valueList" export="true" sort="list" pagesize="10"
					cellpadding="2" cellspacing="0" class="its" id="currentRowObject"
					requestURI="list.do?ajax=true">
			
					<display:column title="SNO"  class="colCenter"> ${currentRowObject_rowNum}</display:column>
					
					
					<display:column media="html" title="FIRST NAME" sortable="true"
						headerClass="sortable" sortProperty="firstName" class="colCenter">
						<a href='view.do?personID=${currentRowObject.id}'>${currentRowObject.firstName}</a>
					</display:column>
					
					<display:column media="html" title="LAST NAME" property="lastName" />
			       			        				
				    <display:column media="html" title="ADDRESS" property="address.id" />
				    
				    <display:column media="html" title="EDIT" class="colCenter">
						<a href="#" onclick="window.open('edit.do?person=<c:out value="${currentRowObject.id}"/>','_parent')">
						<img src='<c:url value="/img/edit-icon.gif"/>' /></a>
					</display:column>
				    
					<display:column media="html" title="DELETE" class="colCenter">
						<a href="#" onclick="window.open('delete.do?id=<c:out value="${currentRowObject.id}"/>','_parent')">
						<img src='<c:url value="/img/icn_delete.gif"/>' /></a>
					</display:column>					
							
				</display:table>
			</ajax:displayTag>
		
		</td>
	</tr>
</table>

</body>