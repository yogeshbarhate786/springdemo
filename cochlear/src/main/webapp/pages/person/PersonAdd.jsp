<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags" prefix="ajax"%>

<body>

	<form name='personForm' id='personForm'
		action='<c:url value="/admin/person/create.do"/>' method="post">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="100%" align="left" valign="top" class="boxBg">
					<fieldset>
						<legend class="dashTitle">Person Details</legend>
						<table width="100%" border="0" cellspacing="0" cellpadding="5">

							<tr>
								<td width="50%" align="left" valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
													key="common.salutation" /></td>
											<td width="60%"><select id="salutation" name="salutation" style="width: 182px">
													<option value=""> <fmt:message key="common.listbox.select" /> </option>
													<c:forEach items="${salutationList}" var="Salutation">
								                        <option value="${Salutation}">${Salutation}</option>
							                        </c:forEach>
											</select></td>
										</tr>
										<tr>
											<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
													key="person.firstname" /><span class="mandtry">*</span></td>
											<td width="60%"><input id="firstName" name="firstName"
												type="text" value="" maxlength="50" style="width: 182px" /></td>
										</tr>
										<tr>
											<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
													key="person.middlename" /></td>
											<td width="60%"><input id="middleName" name="middleName"
												type="text" value="" maxlength="50" style="width: 182px" />
											</td>
										</tr>
										<tr>
											<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
													key="person.lastname" /><span class="mandtry">*</span></td>
											<td width="60%"><input id="lastName" name="lastName"
												type="text" value="" maxlength="50" style="width: 182px" /></td>
										</tr>
										<tr>
											<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
													key="person.gender" /></td>
											<td width="60%"><select id="gender" name="gender" style="width: 182px">
													<option value=""> <fmt:message key="common.listbox.select" /> </option>
													<c:forEach items="${genderList}" var="Gender">
								                        <option value="${Gender}">${Gender}</option>
							                        </c:forEach>
											</select></td>
										</tr>
										<tr>
											<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
													key="person.maritalstatus" /></td>
											<td width="60%"><select id="maritalStatus" name="maritalStatus" style="width: 182px">
													<option value=""> <fmt:message key="common.listbox.select" />
													</option>
													<c:forEach items="${maritalStatusList}" var="MaritalStatus">
								                           <option value="${MaritalStatus}">${MaritalStatus}</option>
							                        </c:forEach>
											</select></td>
										</tr>

									</table>
								</td>

								<td width="50%" align="left" valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="5">


										<tr>
											<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
													key="person.address.fulladdress" /></td>
											<td width="60%"><input id="addressFull"
												name="address.addressFull" type="text" value=""
												maxlength="100" style="width: 182px" /></td>
										</tr>
										<tr>
											<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
													key="person.address.streetaddress" /></td>
											<td width="60%"><input id="addressStreet"
												name="address.addressStreet" type="text" value=""
												maxlength="100" style="width: 182px" /></td>
										</tr>

										<%-- <select id="personsalutation"
							name="person.salutation">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${salutationList}" var="Salutation">
								<c:if test="${userData.person.salutation == Salutation}">
									<option value="${Salutation}" selected="selected">
									${Salutation}</option>
								</c:if>
								<c:if test="${userData.person.salutation != Salutation}">
									<option value="${Salutation}">${Salutation}</option>
								</c:if>
							</c:forEach>
						</select> --%>

										<tr>
											<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
													key="person.address.country" /><span class="mandtry">*</span></td>
											<td width="60%"><select id="countryID"
												name="address.country.id" style="width: 182px">
													<option value=""> <fmt:message key="common.listbox.select" /> </option>
											<c:forEach items="${countryList}" var="Country">
										            <c:choose>
											           <c:when test="${DefaultCountry.id==Country.id}">
												            <option value="${Country.id}" selected="selected">${Country.countryName}</option>
											           </c:when>
											               <c:otherwise>
												               <option value="${Country.id}">${Country.countryName}</option>
											             </c:otherwise>
										            </c:choose>
									            </c:forEach>		
													
													
													
													<!--  
													<c:forEach items="${countryList}" var="Country">
														<option value="${Country.id}">${Country.countryName}</option>
													</c:forEach>
													-->
											</select></td>
										</tr>
										<tr>
											<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
													key="person.address.state" /><span class="mandtry">*</span></td>
											<td width="60%"><select id="stateID" name="address.state.id" style="width: 182px">
													<option value=""> <fmt:message key="common.listbox.select" /> </option>
													<c:forEach items="${StateList}" var="State">
														<option value="${State.id}">${State.stateName}</option>
													</c:forEach>
											</select></td>
										</tr>
										<tr>
											<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
													key="person.address.city" /></td>
													
									    		
											<td width="60%"><select id="cityID" name="address.city.id" style="width: 182px">
													<option value=""> <fmt:message key="common.listbox.select" /> </option>
													<option value=""><fmt:message key="common.listbox.select" /></option>
												  </select></td>
											</tr>

									</table>

								</td>

							</tr>

							<tr>
								<td align="left" valign="top">&nbsp;</td>
								<td align="right" valign="top"><input type="submit"
									class="buttonBg" name='<fmt:message key="save" />'
									value='<fmt:message key="save" />' /> <input type="button"
									class="buttonBg" name='<fmt:message key="goToList" />'
									value='<fmt:message key="goToList" />'
									onclick="window.open('list.do','_parent')" /></td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
		</table>

	</form>

	<script language="JavaScript" type="text/javascript">
		var frmvalidator = new Validator("personForm");
		frmvalidator.addValidation("firstName", "req", "'First Name' Is Mandatory");
		frmvalidator.addValidation("lastName", "req", "'Last Name' Is Mandatory");
		/**
		frmvalidator.addValidation("country", "req", "'country Name' Is Mandatory");
		frmvalidator.addValidation("state", "req", "'state Name' Is Mandatory");
		frmvalidator.addValidation("firstName","alphanumeric_space","Only Alphanumberic Characters With Space Are Allowed For First Name");
		frmvalidator.addValidation("lastName","alphanumeric_space","Only Alphanumberic Characters With Space Are Allowed For Last Name");
         */
		</script>
	
<c:url var="urlTogetStateList" value="/admin/person/getCountryWiseStateList.do" />
<ajax:select source="countryID" target="stateID" 
	baseUrl="${urlTogetStateList}" parameters="countryID={countryID}">
</ajax:select>

<c:url var="urlTogetCityList" value="/admin/person/getStateWiseCityList.do" />
<ajax:select source="stateID" target="cityID"
	baseUrl="${urlTogetCityList}" parameters="stateID={stateID}">
</ajax:select>


</body>
