<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
<script type="text/javascript">
	var file_count = 1;
	function createFileInput(fileboxid) {
		if (file_count > 9) {
			alert("You can attach maximum 10 Files per Artcle.");
		} else {
			file_count++;

			var p_tag = document.createElement("p");

			var label = document.createElement("label");
			label.innerHTML = "Attachment " + file_count;

			var fileDetails = document.createElement("span");
			fileDetails.id = "file" + file_count;
			fileDetails.innerHTML = "&nbsp;&nbsp;&nbsp;No File Selected";

			var inputFile = document.createElement("input");
			inputFile.type = "file";
			inputFile.className = "form-control";
			inputFile.id = "attachment" + file_count;
			inputFile.name = "attachment" + file_count;
			inputFile.setAttribute("onchange", "readFile(this, " + "'file"
					+ file_count + "')");
			inputFile.setAttribute("style",
					"border: 1px solid #f3f3f3;padding: 5px;");
			p_tag.append(label);
			p_tag.append(inputFile);
			p_tag.append(fileDetails);

			document.getElementById(fileboxid).append(p_tag);
		}
	}

	var sizeOfFile = 0;
	var submit = 0;
	function readFile(inputFile, fileShow) {
		var FileUploadPath = inputFile.value;
		var file_name = inputFile.value.substring(12);

		var s = inputFisRle.files[0].size;
		s = s / 1024;
		if (s > 1024) {
			s = s / 1024;
			sizeOfFile = Math.floor(s);
			if (sizeOfFile > 20) {
				inputFile.value = "";
				alert("Error exceed file size.\nFile Size - " + Math.floor(s)
						+ " MB.  \nNote : File size should be less than 20MB.");
			} else {
				document.getElementById(fileShow).innerHTML = " &nbsp;&nbsp;&nbsp; File Name : <b>"
						+ file_name
						+ "</b> &nbsp;&nbsp;&nbsp; File Size : <b>"
						+ Math.floor(s) + " MB.</b>";
			}
		} else {
			sizeOfFile = Math.floor(s);
			document.getElementById(fileShow).innerHTML = "&nbsp; &nbsp; &nbsp; File Name : <b>"
					+ file_name
					+ "</b> &nbsp;&nbsp;&nbsp; File Size : <b>"
					+ Math.floor(s) + " KB.</b>";

		}
	}

	function addProduct(form) {
		
		if(form.title.value == "") 
		  {
		    alert("Product Name cannot be blank");
		    form.title.focus();
		    return false;
		  }
		
		if(form.details.value == "") 
		  {
		    alert("Details cannot be blank");
		    form.details.focus();
		    return false;
		  }
		
		var e = document.getElementById("maincategoryid");
	    var strUser = e.options[e.selectedIndex].value;
	    if(strUser==0)
	    {
	    alert("Please select a Main Category");
	    return false;
	    }
	    
	    var e = document.getElementById("categoryid");
	    var strUser = e.options[e.selectedIndex].value;
	    if(strUser==0)
	    {
	    alert("Please select a Category");
	    return false;
	    }
	    
	    var e = document.getElementById("subcategoryid");
	    var strUser = e.options[e.selectedIndex].value;
	    if(strUser==0)
	    {
	    alert("Please select a Sub Category");
	    return false;
	    }
	    
	    if(form.price.value == "") 
		  {
		    alert("Price cannot be blank");
		    form.price.focus();
		    return false;
		  }
	    if(form.version.value == "") 
		  {
		    alert("Version cannot be blank");
		    form.version.focus();
		    return false;
		  }
	    
		if (form.warranty.value == "") {
			alert("Warranty cannot be blank");
			form.warranty.focus();
			return false;
		}
		

		form.submit();
	}
</script>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="login-form">
					<!--login form-->
					<form action="../../admin/productsku/update.do" method="post"
						id="productForm" name="productForm" enctype="multipart/form-data">

						<div id="form-textarea">
							<label class="required">Product Name<span>*</span></label> <input
								type="text" value="${ProductEditList.name}" name="title" />
						</div>
						<div id="form-textarea">
							<label class="required">Details<span>*</span></label>
							<textarea id="question-details" cols="58" rows="8" name="details">${ProductEditList.details}
							</textarea>
							
						</div>
						<br>

						<div id="form-textarea">
							<select id="maincategoryid" name="maincategoryid"
								class="form-control" onchange="getCategory()">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
								<c:forEach items="${maincategorylist}" var="Main">
									<c:choose>
										<c:when
											test="${Main.id eq ProductEditList.maincatid.id }">
											<option value="${Main.id}" selected="selected">${Main.name}</option>
										</c:when>
										<c:otherwise>
											<option value="${Main.id}">${Main.name}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
						<br>
						<div id="form-textarea">
							<select id="categoryid" name="categoryid" class="form-control"
								onchange="getSubCategory()">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
								<c:forEach items="${categorylist}" var="Category">
									<c:choose>
										<c:when
											test="${Category.id eq ProductEditList.catid.id }">
											<option value="${Category.id}" selected="selected">${Category.name}</option>
										</c:when>
										<c:otherwise>
											<option value="${Category.id}">${Category.name}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
						<br>
						<div id="form-textarea">
							<select id="subcategoryid" name="subcategoryid"
								class="form-control">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
								<c:forEach items="${subcategorylist}" var="Subcategory">
									<c:choose>
										<c:when
											test="${Subcategory.id eq ProductEditList.subcatid.id }">
											<option value="${Subcategory.id}" selected="selected">${Subcategory.name}</option>
										</c:when>
										<c:otherwise>
											<option value="${Subcategory.id}">${Subcategory.name}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
						<br>
						<div id="form-textarea">
							<label class="required">Price<span>*</span></label>
							<input type="number" placeholder="Price" value="${ProductEditList.price}" name="price"></input>
						</div>
						<div id="form-textarea">
							<label class="required">Version<span>*</span></label>
							<input type="number" placeholder="Version" value="${ProductEditList.version}" name="version"></input>
						</div>
						<div id="form-textarea">
							<label class="required">Warranty<span></span></label>
							<input type="text" placeholder="Warranty" value="${ProductEditList.warranty}" name="warranty"></input>
						</div>
						<div id="form-textarea">
							<label class="required">Product Image's<span>*</span></label> <input
								type="file" onchange="readFile(this,'file1')"
								style="border: 1px solid #f3f3f3; padding: 5px;" id="attachment"
								name="attachment"><span id="file1">&nbsp;&nbsp;&nbsp;No
								File Selected</span>
						</div>
						<div id="filebox"></div>
						<div>
							<p>
								<span class="form-description">Please choose a file(
									Image) less than 2 MB .</span>
							</p>
						</div>
						<!-- <button type="button" id="AddFile" value="Add File"
							onclick="createFileInput('filebox')" class="button color small"
							style="background: #2a3d95;">Add More File</button> -->
							
						<input id="productid" name="productid"
									type="hidden" value="${ProductEditList.id}" />	
									
						<div style="display: inline;" align="center">
							<div style="float: left;">
								<button type="button" id="" value="Update"
							 onclick="addProduct(this.form)">UPDATE</button>
							</div>
							<div style="margin-left:20px; float: left;">
								<button type="submit" id="" value="List" 
							 formaction="../../admin/productsku/list.do">LIST</button>
							</div>
						</div>
						<br style="clear:both;">
						
						<!-- <button type="button" id="" value="Update"
							class="button color small " onclick="addProduct(this.form)">UPDATE</button>
						
						<button type="submit" id="" value="List" 
							 formaction="../../admin/productsku/list.do">LIST</button> -->	
						
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>