<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
	<style>
.button {
    background-color: #FE980F;
    border: none;
    color: black;
    padding: 10px 13px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 13px;
    margin: 53px 2px;
    cursor: pointer;
    border-radius: 5px;
    height: 44px;
    width: 100px;
}
</style>
<script type="text/javascript">
	var file_count = 1;
	function createFileInput(fileboxid) {
		if (file_count > 9) {
			alert("You can attach maximum 10 Files per Artcle.");
		} else {
			file_count++;

			var p_tag = document.createElement("p");

			var label = document.createElement("label");
			label.innerHTML = "Attachment " + file_count;

			var fileDetails = document.createElement("span");
			fileDetails.id = "file" + file_count;
			fileDetails.innerHTML = "&nbsp;&nbsp;&nbsp;No File Selected";

			var inputFile = document.createElement("input");
			inputFile.type = "file";
			inputFile.className = "form-control";
			inputFile.id = "attachment" + file_count;
			inputFile.name = "attachment" + file_count;
			inputFile.setAttribute("onchange", "readFile(this, " + "'file"
					+ file_count + "')");
			inputFile.setAttribute("style",
					"border: 1px solid #f3f3f3;padding: 5px;");
			p_tag.append(label);
			p_tag.append(inputFile);
			p_tag.append(fileDetails);

			document.getElementById(fileboxid).append(p_tag);
		}
	}

	var sizeOfFile = 0;
	var submit = 0;
	function readFile(inputFile, fileShow) {
		var FileUploadPath = inputFile.value;
		var file_name = inputFile.value.substring(12);

		var s = inputFisRle.files[0].size;
		s = s / 1024;
		if (s > 1024) {
			s = s / 1024;
			sizeOfFile = Math.floor(s);
			if (sizeOfFile > 20) {
				inputFile.value = "";
				alert("Error exceed file size.\nFile Size - " + Math.floor(s)
						+ " MB.  \nNote : File size should be less than 20MB.");
			} else {
				document.getElementById(fileShow).innerHTML = " &nbsp;&nbsp;&nbsp; File Name : <b>"
						+ file_name
						+ "</b> &nbsp;&nbsp;&nbsp; File Size : <b>"
						+ Math.floor(s) + " MB.</b>";
			}
		} else {
			sizeOfFile = Math.floor(s);
			document.getElementById(fileShow).innerHTML = "&nbsp; &nbsp; &nbsp; File Name : <b>"
					+ file_name
					+ "</b> &nbsp;&nbsp;&nbsp; File Size : <b>"
					+ Math.floor(s) + " KB.</b>";

		}
	}

	
</script>
<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li class="active"><b>Product List</b></li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description">Product Name</td>
							<td class="price">Price</td>
							<td class="description">Warranty</td>
							<td></td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					
					<c:forEach items="${ProductList}" var="productlistvar">
						<tr>
							<td class="cart_description">
								<a href=""><img src=${productlistvar.picurl} height="150" width="150" alt=""></a>
							</td>
							<td class="cart_description">
								<p>${productlistvar.name}</p>
							</td>
							<td class="cart_price">
								<p><i class="fa fa-inr" style="font-size:18px">${productlistvar.price}</i></p>
							</td>
							<td class="cart_description">
								<p>${productlistvar.warranty}</p>
							</td>
							<td class="cart_quantity">
								
								<button class="button" type="button"
							 onclick="window.open('../../admin/productsku/edit.do?productid=<c:out value="${productlistvar.id}"/>','_parent')">
								Edit 
								</button>
							</td>
							<td class="cart_delete">
								
									<button class="button" type="button"
							onclick="window.open('../../admin/productsku/delete.do?productid=<c:out value="${productlistvar.id}"/>','_parent')">
								Remove
								</button>
							</td>
						</tr>
					</c:forEach>
						

						
					</tbody>
				</table>
			</div>
		</div>
	</section>
</html>