<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<!-- <link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<style>
a#mod {
	text-decoration: none !important;
	background-color: #2ECC71;
	color: #FFFFFF;
	font-weight: bold;
	padding: 5px;
	border-radius: 10%;
}
</style>

</head>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>

<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li class="active"><b>Doctor List</b></li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="price">Doctor First Name</td>
							<td class="price">Doctor Last Name</td>
							<td class="description">Email</td>
							<td class="description">User Name</td>
							<td class="price">Doctor Profile</td>
							<!-- <td></td> -->
							<td></td>
						</tr>
					</thead>
					<tbody>
					
					<c:forEach items="${DoctorList}" var="doctorlistvar" varStatus="loop">
						<tr>
							<td class="cart_price">
								<p>${doctorlistvar.firstName}</p>
							</td>
							<td class="cart_price">
								<p>${doctorlistvar.lastName}</p>
							</td>
							<td class="cart_description">
								<p>${doctorlistvar.email}</p>
							</td>
							<td class="cart_description">
								<p>${doctorlistvar.userName}</p>
							</td>
							<td class="cart_price"><a href='#'
						data-toggle="modal" data-target='#sss${loop.index}' id='mod'>Show</a></td>
							<%-- <td class="cart_quantity">
								<a class="cart_quantity_up" href="#" onclick="window.open('../../admin/productsku/edit.do?productid=<c:out value="${productlistvar.id}"/>','_parent')">
								Edit</a>
							</td> --%>
							<c:if test="${ROLE ne LOGINROLE_DOCTOR}">
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="#" onclick="window.open('../../admin/doctor/delete.do?doctorid=<c:out value="${doctorlistvar.id}"/>','_parent')">
								<i class="fa fa-times"></i></a>
							</td>
							</c:if>
						</tr>
					</c:forEach>
						

						
					</tbody>
				</table>

		<c:forEach items="${DoctorList}" var="doctorlistvar" varStatus="loop">
			<div class="modal fade" id="sss${loop.index}" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Profile</h4>
						</div>
						<div class="modal-body">
							<table width="100%"
								class="table table-striped table-bordered table-hover">

								<tr>
									<td width="40%" align="left" valign="top" class="leftColon">Gender</td>
									<td width="60%">${doctorlistvar.gender}</td>
								</tr>

								<tr>
									<td width="40%" align="left" valign="top" class="leftColon">Age</td>
									<td width="60%">${doctorlistvar.age}</td>
								</tr>

								<tr>
									<td width="40%" align="left" valign="top" class="leftColon">About Doctor</td>
									<td width="60%">${doctorlistvar.bio}</td>
								</tr>

								<tr>
									<td width="40%" align="left" valign="top" class="leftColon">Mobile Number</td>
									<td width="60%">${doctorlistvar.mobile}</td>
								</tr>

								<tr>
									<td width="40%" align="left" valign="top" class="leftColon">Full Address1</td>
									<td width="60%">${doctorlistvar.addressid.addressFull}, ${doctorlistvar.addressid.addressStreet}</td>
								</tr>
								
								<tr>
									<td width="40%" align="left" valign="top" class="leftColon">Full Address2</td>
									<td width="60%">${doctorlistvar.addressid.country.countryName}, ${doctorlistvar.addressid.state.stateName}, 
									${doctorlistvar.addressid.city.cityName}, ${doctorlistvar.addressid.zipCode}</td>
								</tr>

								<tr>
									<td width="40%" align="left" valign="top" class="leftColon">Specialist</td>
									<td width="60%">${doctorlistvar.specialist}</td>
								</tr>
								
								<tr>
									<td width="40%" align="left" valign="top" class="leftColon">Belong To Hospital</td>
									<td width="60%">${doctorlistvar.belongToHospital}</td>
								</tr>

							</table>
						</div>

					</div>

				</div>
			</div>
		</c:forEach>

	</div>
		</div>
	</section>
</html>