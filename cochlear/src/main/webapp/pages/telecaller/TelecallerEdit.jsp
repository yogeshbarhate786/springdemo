<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Doctor.js'></script>

</head>
	<style>
	#addbutton{
    width: 50%;
    height: 70px;
    margin: 0 auto;
    overflow: hidden;
     align-items: center;
    justify-content: space-around;
    display: flex;
    float: none;
  border-radius: 15px;
}
</style>

<script type="text/javascript">

	function updateTellecaller(form) {
		
		if(form.telecaller_first_name.value == "") 
		  {
		    alert("First Name cannot be blank");
		    form.telecaller_first_name.focus();
		    return false;
		  }
		
		if(form.telecaller_last_name.value == "") 
		  {
		    alert("Last Name cannot be blank");
		    form.telecaller_last_name.focus();
		    return false;
		  }
		
		if(form.telecaller_email_address.value == "") 
		  {
		    alert("Email Address cannot be blank");
		    form.telecaller_email_address.focus();
		    return false;
		  }
		
		if(form.telecaller_user_name.value == "") 
		  {
		    alert("User Name cannot be blank");
		    form.telecaller_user_name.focus();
		    return false;
		  }
		
		if(form.telecaller_gender.value == "") 
		  {
		    alert("Gender cannot be blank");
		    form.telecaller_gender.focus();
		    return false;
		  }
		
		if(form.telecaller_age.value == "") 
		  {
		    alert("Age cannot be blank");
		    form.tellecaller_age.focus();
		    return false;
		  }
		
		if(form.telecaller_bio.value == "") 
		  {
		    alert("About Yourself cannot be blank");
		    form.telecaller_bio.focus();
		    return false;
		  }
		
		if(form.telecaller_mobile_number.value == "") 
		  {
		    alert("Mobile Number cannot be blank");
		    form.telecaller_mobile_number.focus();
		    return false;
		  }
		
		if(form.telecaller_full_address.value == "") 
		  {
		    alert("Full Address cannot be blank");
		    form.telecaller_full_address.focus();
		    return false;
		  }
		
		if(form.telecaller_address_street.value == "") 
		  {
		    alert("Address Street cannot be blank");
		    form.telecaller_address_street.focus();
		    return false;
		  }
		  
		  if(form.telecaller_zip_code.value == "") 
		  {
		    alert("Zip Code cannot be blank");
		    form.telecaller_zip_code.focus();
		    return false;
		  }
		  
		 	var e = document.getElementById("countryid");
		    var strUser = e.options[e.selectedIndex].value;
		    if(strUser==0)
		    {
		    alert("Please select a Country");
		    return false;
		    }
		    
		    var e = document.getElementById("stateid");
		    var strUser = e.options[e.selectedIndex].value;
		    if(strUser==0)
		    {
		    alert("Please select a State");
		    return false;
		    }
		    
		    var e = document.getElementById("cityid");
		    var strUser = e.options[e.selectedIndex].value;
		    if(strUser==0)
		    {
		    alert("Please select a City");
		    return false;
		    }
		
	
		
		form.submit();
	}
</script>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="login-form">
					<!--login form-->
					<form action="../../admin/telecaller/update.do" method="post"
						id="telecallerForm" name="telecallerForm" enctype="multipart/form-data" onsubmit="return  updateTellecaller(this)">
						
						
						<div id="form-textarea">
							<label class="required">First Name<span>*</span></label> <input
								type="text" value="${TelecallerEditList.firstName}" name="telecaller_first_name" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Middle Name</label> <input
								type="text" value="${TelecallerEditList.middleName}" name="telecaller_middle_name" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Last Name<span>*</span></label> <input
								type="text" value="${TelecallerEditList.lastName}" name="telecaller_last_name" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Email Address<span>*</span></label> <input
								type="text" value="${TelecallerEditList.email}" name="telecaller_email_address" readonly/>
						</div>
						
						<div id="form-textarea">
							<label class="required">User Name</label> <input
								type="text" value="${TelecallerEditList.userName}" name="telecaller_user_name" readonly/>
						</div>
						
						<div id="form-textarea">
							<label class="required">Gender</label>
							<select id="genderid" name="genderid" class="form-control"
								onchange="">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
									
										<c:if
											test="${TelecallerEditList.gender eq 'Male'}">
											<option value="Male" selected="selected">Male</option>
											<option value="Female">Female</option>
										</c:if>
									<c:if
											test="${TelecallerEditList.gender eq 'Female'}">
											<option value="Male" >Male</option>
											<option value="Female" selected="selected">Female</option>
										</c:if>
										<c:if
											test="${empty TelecallerEditList.gender }">
											<option value="Male" selected="selected">Male</option>
											<option value="Female" >Female</option>
										</c:if>
									
									
								</select>
						</div>
						
						<div id="form-textarea">
							<label class="required">Age</label> <input type="text" value="${TelecallerEditList.age}"
								 onkeypress="return ValidateMobileNumber(event)" maxlength=3 name="telecaller_age" />
						</div>
						
						<div id="form-textarea">
							<label class="required">About Yourself</label> <input
								type="text" value="${TelecallerEditList.bio}" name="telecaller_bio" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Mobile Number</label> <input type="text" value="${TelecallerEditList.mobile}"
								 onkeypress="return ValidateMobileNumber(event)" maxlength=10 name="telecaller_mobile_number" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Full Address</label> <input
								type="text" value="${TelecallerEditList.addressid.addressFull}" name="telecaller_full_address" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Address Street</label> <input
								type="text" value="${TelecallerEditList.addressid.addressStreet}" name="telecaller_address_street" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Zip Code</label> <input
								type="text" value="${TelecallerEditList.addressid.zipCode}" name="telecaller_zip_code" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Country</label>
							<select id="countryid" name="countryid" class="form-control"
								onchange="getState()">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
								<c:forEach items="${CountryList}" var="countrylistobj">
									<c:choose>
										<c:when
											test="${countrylistobj.id eq TelecallerEditList.addressid.country.id}">
											<option value="${countrylistobj.id}" selected="selected">${countrylistobj.countryName}</option>
										</c:when>
										<c:otherwise>
											<option value="${countrylistobj.id}">${countrylistobj.countryName}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
						<br>
						<div id="form-textarea">
							<label class="required">State</label>
							<select id="stateid" name="stateid" class="form-control"
								onchange="getCity()">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
								<c:forEach items="${StateList}" var="statelistobj">
									<c:choose>
										<c:when
											test="${statelistobj.id eq TelecallerEditList.addressid.state.id }">
											<option value="${statelistobj.id}" selected="selected">${statelistobj.stateName}</option>
										</c:when>
										<c:otherwise>
											<option value="${statelistobj.id}">${statelistobj.stateName}</option>

										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
						<br>
						<div id="form-textarea">
							<label class="required">City</label>
							<select id="cityid" name="cityid" class="form-control">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
								<c:forEach items="${CityList}" var="citylistobj">
									<c:choose>
										<c:when test="${citylistobj.id eq TelecallerEditList.addressid.city.id}">
											<option value="${citylistobj.id}" selected="selected">${citylistobj.cityName}</option>
										</c:when>
										<c:otherwise>
											<option value="${citylistobj.id}">${citylistobj.cityName}</option>

										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
						<br>
						
						
					<input id="telecallerid" name="telecallerid"
									type="hidden" value="${TelecallerEditList.id}" />
									
							<div id="addbutton">
							<div style="float: left;">
								<button type="submit" id="" value="Update" 
							onclick="updateTellecaller(this.form)" style="position: static;border-radius:25px; ">UPDATE</button>
							</div>
							</div>
						<c:if test="${ROLE eq LOGINROLE_ADMINISTRATOR }">
							<div style="margin-left:20px; float: left;">
								<button type="submit" id="" value="List" 
							 formaction="../../admin/doctor/list.do">LIST</button>
							</div>
							</c:if>
						</div>
						<br style="clear:both;">
							
							<!-- <button type="button" id="" value="Update" 
							onclick="updateDoctor(this.form)">UPDATE</button>
							
							<button type="submit" id="" value="List" 
							 formaction="../../admin/doctor/list.do">LIST</button> -->
							
							
							
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>