<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
	<style>
.button {
    background-color: #FE980F;
    border: none;
    color: black;
    padding: 10px 13px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 13px;
    margin: 53px 2px;
    cursor: pointer;
    border-radius: 5px;
    height: 44px;
    width: 100px;
}
</style>
<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li class="active"><b>Varify Order List</b></li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Order Id</td>
							<td class="description">User Id</td>
							<td class="description">FirstName</td>
							<td class="description">LastName</td>
							<td class="price">Status</td>
							<td class="description">Varify Date</td>
							
							
						</tr>
					</thead>
					<tbody>
					
					<c:forEach items="${orderlist}" var="varifylist">
						<tr>
						
							<td class="cart_description">
								<p>${varifylist.orderid}</p>
							</td>
							<td class="cart_price">
								<p>${varifylist.doctorid.firstName}</p>
							</td>
								<td class="cart_price">
								<p>${varifylist.doctorid.lastName}</p>
							</td>
							<td class="cart_description">
								<p>${varifylist.approvestatus}</p>
							</td>
							<td class="cart_quantity">
								<p>${varifylist.approvedate}</p>
							
						
							
						</tr>
					</c:forEach>
						

						
					</tbody>
				</table>
			</div>
		</div>
	</section>
</html>