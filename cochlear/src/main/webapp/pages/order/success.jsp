<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script src="../../template/js/jquery.js"></script>
	<script src="../../template/js/bootstrap.min.js"></script>
	<script src="../../template/js/jquery.scrollUp.min.js"></script>
	<script src="../../template/js/price-range.js"></script>
    <script src="../../template/js/jquery.prettyPhoto.js"></script>
    <script src="../../template/js/main.js"></script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
/* CSS used here will be applied after bootstrap.css */
.qlt-confirmation {
    width: 30%;
    margin: 10px auto;
 }
  
.qlt-confirmation .panel-body {
  	width: 99%;
 	margin: 0 auto;      
        padding: 40px 10px;
 }
      .qlt-confirmation .panel-body .desc{
             	margin: 10px auto; 
            }

.qlt-confirmation .panel-body .notice {
       padding: 0px 20px; 
        margin-top: 50px;
        text-align: left;
        font-style:italic;
        color: gray;
      }


</style>
</head>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>


<body>
	<div class="qlt-confirmation">
  	<div class="panel panel-default">
      <div class="panel-body">
        <center>
        <img src="https://cdn4.iconfinder.com/data/icons/social-communication/142/open_mail_letter-512.png" style="width:30px; height: 30px;">
          <p class="desc">Thank you for Shopping with us!<br>Your Order Placed Successfuly.<br><img style="width: 40px; height: 40px" src="http://cdn.mysitemyway.com/etc-mysitemyway/icons/legacy-previews/icons/glossy-black-icons-business/080840-glossy-black-icon-business-mailbox.png">.</p>
        </center>
        
         
      </div>
	</div>
</div>
</body>
</html>