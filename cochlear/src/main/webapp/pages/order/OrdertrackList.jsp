<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
 <style>
.button {
    background-color: #FE980F;
    border: none;
    color: black;
    padding: 10px 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
.button1 {
    background-color: #FE980F;
    border: none;
    color: black;
    padding: 5px 15px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    margin: 4px 2px;
    cursor: pointer;
}
</style>
<section id="cart_items">
<div class="container">
	<div class="breadcrumbs">
		<ol class="breadcrumb">
			<li class="active"><b><h4>My Orders</h4></b></li>
		</ol>
	</div>
	<form action="../../admin/doctor/create.do" method="post"
		id="doctorForm" name="doctorForm" enctype="multipart/form-data">
		<div class="table-responsive cart_info">
			<table class="table table-condensed">
				<%--  <thead>
					<tr class="cart_menu">
						<td></td>
						<td class="description">Order Id</td>
						<td class="price" style="width: 50px;">Quantity</td>
						<td class="price" ><center>Total Bill</center></td>
							<td class="price" style="width: 50px;">Order Status</td>
						<td class="price"><center>Order Placed Date</center></td>
					</tr>
				</thead>  --%>
				<tbody>
				 <c:if test="${not empty CartList}">
                       
					<c:forEach items="${CartList}" var="cartlistvar">
				
                       <tr>
						<c:forEach items="${cartlistvar.cart}" var="cartobj">
								<c:if test="${cartlistvar.status eq 'Cancelled'}">
								<td class="cart_quantity_input"><center> <button class="button" disabled="disabled">${cartlistvar.orderid}</button> </center></td> 
								</c:if>
								<c:if test="${cartlistvar.status eq 'Success'}">
								<td class="cart_quantity_input"><center> <button class="button" >${cartlistvar.orderid}</button> </center></td> 
								</c:if>
		                   <td class="cart_product"><center><a href=""><img
									src=${cartobj.productid.picurl } height="150" width="150"
									alt=""></a></center></td>
							<td class="cart_description" align="center"><center>
								<p>${cartobj.productid.name}</p>
							<center>
							 <span> </span>Rs.${cartobj.productid.price}  </center>
							
							</td>
							<td><h6>Quantity</h6><input class="cart_quantity_input" type="text"
								name="quantity" value="${cartobj.quantity}" readonly
								onkeypress="return ValidateMobileNumber(event)" maxlength=2
								style="width: 50px;"></td>
							<td><center><h6>Total Bill</h6><i class="fa fa-rupee"></i>
							<c:out value="${cartobj.totalbill}"/>
								</center>
							</td>
								
							<c:if test="${cartlistvar.status eq 'Cancelled'}">
							<td><center>Order Status
							<h4><c:out value="${cartlistvar.status}"/></h4>
								</center>
							</td>
							</c:if>
							<c:if test="${cartlistvar.status eq 'Success'}">
							<td><center>
							<h6>Order</h6>	
							<button type="button"  class="button1"  onclick="window.open('trackorder.do?cartid=<c:out value="${cartlistvar.cartid}"/>','_parent')" >cancel </button>
								</center>
							</td>
							</c:if>
							
							<td><center><h6>Order Placed Date</h6>
							 <span> </span>${cartlistvar.addeddate}  </center>
								
							</td>
							<c:if test="${cartlistvar.status eq 'Success'}">
							<td><center><button type="button"  class="btn"  onclick="window.open('../../admin/ticket/add.do?cartid=<c:out value="${cartlistvar.cartid}"/>','_parent')" ><i class="glyphicon glyphicon-question-sign"></i> need help</button></center>
								
							</td>
							</c:if>
							<c:if test="${cartlistvar.status eq 'Cancelled'}">
							<td><center><button type="button"  class="btn"  disabled="disabled" ><i class="glyphicon glyphicon-question-sign"></i> need help</button></center>
								
							</td>
							</c:if>
						
						</c:forEach>
						 </tr>
						
					</c:forEach>
                       </c:if>
                        <c:if test="${empty CartList}">
                        <tr>
						
						<td colspan="2">&nbsp;</td>
						 <td colspan="2">
							<table class="table table-condensed total-result">
								<tr class="shipping-cost">
										<td>
										
											<h5>Oops their is not yet placed any Order</h5>
										</td>
								</tr>
								
							</table>
						</td> 
					
					</tr> 
                        </c:if>
					 
			

				</tbody>
			</table>
		</div>
	</form>
</div>
</section>
</html>