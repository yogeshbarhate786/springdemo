<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
	<style>
	#addbutton{
    width: 50%;
    height: 70px;
    margin: 0 auto;
    overflow: hidden;
     align-items: center;
    justify-content: space-around;
    display: flex;
    float: none;
}
.button {
    background-color: #FE980F;
    border: none;
    color: black;
    padding: 10px 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    border-radius: 5px;
}
	
	</style>
<script type="text/javascript">
	
	function addProduct(form) {
		
		if(form.title.value == "") 
		  {
		    alert("Enter Your product Name");
		    form.title.focus();
		    return false;
		  }
		var e = document.getElementById("maincategoryid");
	    var strUser = e.options[e.selectedIndex].value;
	    if(strUser==0)
	    {
	    alert("Please select a Main Category");
	    return false;
	    }
	    
	    var e = document.getElementById("categoryid");
	    var strUser = e.options[e.selectedIndex].value;
	    if(strUser==0)
	    {
	    alert("Please select a Category");
	    return false;
	    }
	    
	    var e = document.getElementById("subcategoryid");
	    var strUser = e.options[e.selectedIndex].value;
	    if(strUser==0)
	    {
	    alert("Please select a Sub Category");
	    return false;
	    }
		if(form.question.value == "") 
		  {
		    alert("Please Enter Your Query");
		    form.question.focus();
		    return false;
		  }
	   
	}
</script>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="login-form">
					<!--login form-->
					<form action="../../admin/ticket/create.do" method="post"
						id="ticketForm" name="ticketForm" onsubmit="return addProduct(this)" >

						<div id="form-textarea">
							<label class="required">Product Name<span>*</span></label> 
						    <select id="productid" name="product"
								class="form-control" onchange="">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
								<c:forEach items="${ProList}" var="productlist">
									<option value="${productlist.id}" selected="selected">${productlist.name}</option>
								</c:forEach>
							</select>
						</div>
					
						<br>

						<div id="form-textarea">
							<label class="required">Main Category<span>*</span></label>
							<select id="maincategoryid" name="maincategoryid"
								class="form-control" onchange="getCategory()">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
								<c:forEach items="${maincategorylist}" var="Main">
									<option value="${Main.id}">${Main.name}</option>
								</c:forEach>
							</select>
						</div>
						<br>
						<div id="form-textarea">
							<label class="required">Category<span>*</span></label>
							<select id="categoryid" name="categoryid" class="form-control"
								onchange="getSubCategory()">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
							</select>
						</div>
						<br>
						<div id="form-textarea">
							<label class="required">Sub Category<span>*</span></label>
							<select id="subcategoryid" name="subcategoryid"
								class="form-control">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
							</select>
						</div>
						<br>
							<div id="form-textarea">
							<label class="required">Your Query<span>*</span></label>
							<textarea id="question-details" cols="30" rows="3" name="question"></textarea>
						</div>
						
						<div id="addbutton">
							<div style="float: left;">
								<center><button type="submit" class="button" id="" value="Add"  >SUBMIT</button></center>
							</div>
					
						</div>
						<br style="clear:both;">
                          <input type="hidden" value="${cartid}" name="cartid">
					
							
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>