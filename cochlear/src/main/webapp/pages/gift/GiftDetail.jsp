<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="html" uri="http://jakarta.apache.org/struts/tags-html"%>
<%@ taglib prefix="bean" uri="http://jakarta.apache.org/struts/tags-bean"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags" prefix="ajax"%>
	
<body>


<table width="100%" border="0" cellspacing="0" cellpadding="0" class="boxBg">
	<tr>
		<td>
			<fieldset>
			<legend class="dashTitle">Gift Details</legend>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="55%" align="left" valign="top">
						<table width="100%" border="0" cellspacing="5" cellpadding="5">
							<tr>
								<td align="left" class="leftColon"><fmt:message key="gift.giftName" /><span class="mandtry">*</span></td>
								<td>${giftData.giftName} </td>
							</tr>
							<tr>
								<td align="left" class="leftColon"><fmt:message key="gift.giftPrice" /><span class="mandtry">*</span></td>
								<td>${giftData.giftPrice} </td>
							</tr>
						</table>
						</td>
						<td width="45%">
						<table width="100%" border="0" cellspacing="5" cellpadding="5">
							<tr>
								<td align="left" class="leftColon"><fmt:message key="gift.description" /><span class="mandtry">*</span></td>
								<td>${giftData.description}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top">&nbsp;</td>
						<td align="right" valign="top"><input type="button" class="buttonBg" name='<fmt:message key="edit" />'
							value='<fmt:message key="edit" />' onclick="window.open('edit.do?id=<c:out value="${giftData.id}"/>','_parent')" />
						<input type="button" class="buttonBg" name='<fmt:message key="goToList" />'
							value='<fmt:message key="goToList" />' onclick="window.open('list.do','_parent')" /></td>
					</tr>
				</table>
			</fieldset>
		</td>
	</tr>
</table>

</body>
