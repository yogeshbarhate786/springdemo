<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="html" uri="http://jakarta.apache.org/struts/tags-html"%>
<%@ taglib prefix="bean" uri="http://jakarta.apache.org/struts/tags-bean"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags" prefix="ajax"%>
	
<body>

<form id='giftForm' name='giftForm' action='<c:url value="/admin/gift/update.do"/>' method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="boxBg">
	<tr>
		<td>
			<fieldset>
			<legend class="dashTitle">Gift Details</legend>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="55%" align="left" valign="top">
						<table width="100%" border="0" cellspacing="5" cellpadding="5">
							<tr>
								<td align="left" class="leftColon"><fmt:message key="gift.giftName" /><span class="mandtry">*</span></td>
								<td><input id="giftName"  name="giftName" value="${giftData.giftName}" type="text" style="width: 182px" maxlength="50"/>
								</td>
							</tr>
							<tr>
								<td align="left" class="leftColon"><fmt:message key="gift.giftPrice" /><span class="mandtry">*</span></td>
								<td><input id="giftPrice"  name="giftPrice" value="${giftData.giftPrice}" type="text" style="width: 182px" maxlength="9"/>
								</td>
							</tr>
						</table>
						</td>
						<td width="45%">
						<table width="100%" border="0" cellspacing="5" cellpadding="5">
							<tr>
								<td align="left" class="leftColon"><fmt:message key="gift.description" /></td>
								<td><textarea id="description"  name="description"  style="width: 182px" rows="1" cols="20" maxlength="100">${giftData.description}</textarea>
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top"><input id="id" name="id" type="hidden" value="${giftData.id }" /></td>
				
						<td align="right" valign="top">
							<input type="submit" class="buttonBg" name='<fmt:message key="update" />' value='<fmt:message key="update" />' />   
							<input type="button" class="buttonBg" name='<fmt:message key="goToList" />'
								value='<fmt:message key="goToList" />' onclick="window.open('list.do','_parent')" /></td>
					</tr>
				</table>
			</fieldset>
		</td>
	</tr>
</table>
</form>
<script language="JavaScript" type="text/javascript">	 
	var frmvalidator  = new Validator("giftForm");
	frmvalidator.addValidation("giftName","req","Gift Name Is Mandatory");
	frmvalidator.addValidation("giftName","alphanumeric_space","Only Alphanumberic Characters With Space Are Allowed For Gift Name");
	frmvalidator.addValidation("giftPrice","dec","Only decimals are allowed for 'Gift Price'");
</script>
</body>
