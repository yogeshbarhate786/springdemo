<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags"
	prefix="ajax"%>

<c:if test="${autho['PROFILE_ADD'] == 'Yes'}">
	<div align="right"><input  type="button" class="buttonBg" 
		name='<fmt:message key="add" />' value='<fmt:message key="add" />'
		onclick="window.open('add.do','_parent')" /></div>
</c:if>
<br>
<body>
<ajax:displayTag id="displayTagFrame">

	<display:table name="valueList" export="true" sort="list" pagesize="10"
		cellpadding="2" cellspacing="0" class="its" id="currentRowObject"
		requestURI="list.do?ajax=true">

		<display:setProperty name="export.excel.filename"
			value="Profile List.xls" />
		<display:setProperty name="export.pdf.filename"
			value="Profile List.pdf" />
		<display:setProperty name="export.xml.filename"
			value="Profile List.xml" />
		<display:setProperty name="export.csv.filename"
			value="Profile List.csv" />
		<display:setProperty name="export.rtf.filename"
			value="Profile List.rtf" />

		<display:column media="html" title="ID" sortable="true"
			headerClass="sortable" sortProperty="id" class="colCenter">
			<a href='view.do?id=${currentRowObject.id}'>${currentRowObject.id}</a>
		</display:column>
		<display:column media="csv excel pdf xml rtf" property="id" title="ID"
			class="colCenter" />

		<display:column media="html" title="PROFILE NAME" sortable="true"
			headerClass="sortable" sortProperty="name">
			<a href='view.do?id=${currentRowObject.id}'>${currentRowObject.name}</a>
		</display:column>
		<display:column media="csv excel pdf xml rtf" property="name"
			title="PROFILE NAME" />

		<display:column media="html csv excel pdf xml rtf" title="ROLE"
			sortable="true" headerClass="sortable" property="role.roleName">
		</display:column>

		<display:column media="html csv excel pdf xml rtf"
			property="description" title="PROFILE DESC" />

		<display:column media="html" class="colCenter" title="EDIT">
			<a href="#"
				onclick="window.open('edit.do?id=<c:out value="${currentRowObject.id}"/>','_parent')"><img
				src='<c:url value="/img/edit-icon.gif"/>' /></a>
		</display:column>

		<display:column media="html" class="colCenter" title="DELETE">
			<a href="#"
				onclick="window.open('delete.do?id=<c:out value="${currentRowObject.id}"/>','_parent')"><img
				src='<c:url value="/img/icn_delete.gif"/>' /></a>
		</display:column>
	</display:table>

</ajax:displayTag>
<body>