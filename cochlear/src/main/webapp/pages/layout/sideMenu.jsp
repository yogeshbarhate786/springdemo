<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<HEAD>
<LINK href="../../css/sidemenu.css" rel=stylesheet>
<SCRIPT src="../../js/sidemenu.js" type=text/javascript>
</SCRIPT>

<SCRIPT type=text/javascript>
function test()
{
	alert('test');
}
</SCRIPT>
</HEAD>
<div class="sidemenu" id=sidemenu>
<DIV class="Text Display" id=implementationMenu><SPAN class=title>Main
Menu</SPAN>
<UL class=nocollapse>
	<LI class="itemoff Link" id=implementNewFirm><A
		href=<c:url value="/admin/dashboard/view.do"/>> Home</A></LI>
	

</UL>
</DIV>

<c:if test="${ROLE =='1'}">
<DIV class="Text Display" id=Work><SPAN class=title>Registration Mail</SPAN>
<UL class=nocollapse>
	
		<LI class="itemoff Link" id=taskTree><A
			href=<c:url value="/admin/customer/toSendRegistrationMail.do"/>>Send</A></LI>	
		
</UL>
</DIV>
</c:if>

<c:if test="${ROLE =='1'}">

<DIV class="Text Display" id=Work><SPAN class=title>Role</SPAN>
<UL class=nocollapse>
	
		<LI class="itemoff Link" id=taskTree><A
			href=<c:url value="/admin/role/list.do"/>>List</A></LI>
			
		<LI class="itemoff Link" id=task><A
			href=<c:url value="/admin/role/add.do"/>>Add</A></LI>		
</UL>
</DIV>

<DIV class="Text Display" id=Work><SPAN class=title>Profile</SPAN>
<UL class=nocollapse>
	
		<LI class="itemoff Link" id=taskTree><A
			href=<c:url value="/admin/profile/list.do"/>>List</A></LI>
			
		<LI class="itemoff Link" id=task><A
			href=<c:url value="/admin/profile/add.do"/>>Add</A></LI>		
</UL>
</DIV>

<DIV class="Text Display" id=Work><SPAN class=title>User</SPAN>
<UL class=nocollapse>
	
		<LI class="itemoff Link" id=taskTree><A
			href=<c:url value="/admin/user/list.do"/>>List</A></LI>
			
		<LI class="itemoff Link" id=task><A
			href=<c:url value="/admin/user/add.do"/>>Add</A></LI>		
</UL>
</DIV>

<DIV class="Text Display" id=Work><SPAN class=title>Customer Task</SPAN>
<UL class=nocollapse>
	
		<LI class="itemoff Link" id=taskTree><A
			href=<c:url value="/admin/customertask/tosearch.do"/>>Search</A></LI>
			
		<LI class="itemoff Link" id=task><A
			href=<c:url value="/admin/customertask/add.do"/>>Add</A></LI>	
			
		<LI class="itemoff Link" id=task><A
			href=<c:url value="/admin/customertask/list.do"/>>List</A></LI>
		
		<LI class="itemoff Link" id=task><A
			href=<c:url value="/admin/customertask/toCustomerSearch.do"/>>Customer Search To Assign</A></LI>		
		
</UL>
</DIV>


<DIV class="Text Display" id=Work><SPAN class=title>Dealer Task</SPAN>
<UL class=nocollapse>
	
		<LI class="itemoff Link" id=taskTree><A
			href=<c:url value="/admin/dealertask/tosearch.do"/>>Search</A></LI>
			
		<LI class="itemoff Link" id=task><A
			href=<c:url value="/admin/dealertask/add.do"/>>Add</A></LI>	
			
		<LI class="itemoff Link" id=task><A
			href=<c:url value="/admin/dealertask/list.do"/>>List</A></LI>
			
		<LI class="itemoff Link" id=task><A
			href=<c:url value="/admin/dealertask/toDealerSearch.do"/>>Dealer Search To Assign</A></LI>
</UL>
</DIV>

<DIV class="Text Display" id=Work><SPAN class=title>Customer Script</SPAN>
<UL class=nocollapse>
	
		<LI class="itemoff Link" id=CustomerTree><A
			href=<c:url value="/admin/customerscript/search.do"/>>Search</A></LI>
			
		<LI class="itemoff Link" id=customer><A
			href=<c:url value="/admin/customerscript/add.do"/>>Add</A></LI>
			
		<LI class="itemoff Link" id=customer><A
			href=<c:url value="/admin/customerscript/list.do"/>>List</A></LI>

</UL>
</DIV>



<DIV class="Text Display" id=Work><SPAN class=title>Dealer Script</SPAN>
<UL class=nocollapse>

			<LI class="itemoff Link" id=test><A
				href=<c:url value="/admin/dealerscript/search.do"/>>Search</A></LI>
		
			<LI class="itemoff Link" id=test><A
				href=<c:url value="/admin/dealerscript/add.do"/>>Add</A></LI>	
		
			<LI class="itemoff Link" id=test><A
				href=<c:url value="/admin/dealerscript/list.do"/>>List</A></LI>

</UL>
</DIV>

<DIV class="Text Display" id=Work><SPAN class=title>Customer Dispositions</SPAN>
<UL class=nocollapse>
	
		<LI class="itemoff Link" id=DispositionTree><A
			href=<c:url value="/admin/customerdisposition/search.do"/>>Search</A></LI>
			
		<LI class="itemoff Link" id=disposition><A
			href=<c:url value="/admin/customerdisposition/add.do"/>>Add</A></LI>	
			
		<LI class="itemoff Link" id=disposition><A
			href=<c:url value="/admin/customerdisposition/list.do"/>>List</A></LI>

</UL>
</DIV>
</c:if>


<DIV class="Text Display" id=Work><SPAN class=title>Customer</SPAN>
<UL class=nocollapse>
	
		<LI class="itemoff Link" id=customerTree><A
			href=<c:url value="/admin/customer/tosearch.do"/>>Search</A></LI>
			
		<LI class="itemoff Link" id=customer><A
			href=<c:url value="/admin/customer/add.do"/>>Add</A></LI>	
			
		<LI class="itemoff Link" id=customer><A
			href=<c:url value="/admin/customer/list.do"/>>List</A></LI>
			
		<LI class="itemoff Link" id=customer><A
			href=<c:url value="/admin/customer/cyclicsearch.do"/>>CyclicSearch</A></LI>
			

</UL>
</DIV>




<DIV class="Text Display" id=Work><SPAN class=title>Dealer</SPAN>
<UL class=nocollapse>
	
		<LI class="itemoff Link" id=dealerTree><A
			href=<c:url value="/admin/dealer/tosearch.do"/>>Search</A></LI>
			
		<LI class="itemoff Link" id=dealer><A
			href=<c:url value="/admin/dealer/add.do"/>>Add</A></LI>	
			
		<LI class="itemoff Link" id=dealer><A
			href=<c:url value="/admin/dealer/list.do"/>>List</A></LI>

</UL>
</DIV>


<DIV class="Text Display" id=Work><SPAN class=title>Complaint</SPAN>
<UL class=nocollapse>
	
		<LI class="itemoff Link" id=complaintTree><A
			href=<c:url value="/admin/complaint/tosearch.do"/>>Search</A></LI>
			
		<LI class="itemoff Link" id=complaint><A
			href=<c:url value="/admin/complaint/add.do"/>>Add</A></LI>	
			
		<LI class="itemoff Link" id=complaint><A
			href=<c:url value="/admin/complaint/list.do"/>>List</A></LI>

</UL>
</DIV>



<DIV class="Text Display" id=Work><SPAN class=title>Reports</SPAN>
<UL class=nocollapse>
	
		<LI class="itemoff Link" id=ReportTree><A
			href=<c:url value="/admin/customer/toDateSpanReport.do"/>>Date Span Report</A></LI>
			
		<LI class="itemoff Link" id=report><A
			href=<c:url value="/admin/customer/toDayFollowup.do"/>>Today's FollowUp</A></LI>	
			
		<LI class="itemoff Link" id=report><A
			href=<c:url value="/admin/customer/dailyReport.do"/>>Daily Report</A></LI>
					
		<LI class="itemoff Link" id=report><A
			href=<c:url value="/admin/complaint/toComplaintReport.do"/>>Complaint Report</A></LI>

</UL>
</DIV>










</DIV>


<SCRIPT type=text/javascript>
				var sideMenu;
				sideMenu = 'none';
				if (sideMenu != '') {
					sidemenuSetupXstation(sideMenu);
				} else {
					sideMenu = '';
					if (sideMenu != '') {
						sidemenuSetupXstation(sideMenu);
					}
				}
</SCRIPT>





