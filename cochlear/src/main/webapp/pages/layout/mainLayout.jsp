<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="en">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shopper</title>
    <link href="../../template/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../template/css/font-awesome.min.css" rel="stylesheet">
    <link href="../../template/css/prettyPhoto.css" rel="stylesheet">
    <link href="../../template/css/price-range.css" rel="stylesheet">
    <link href="../../template/css/animate.css" rel="stylesheet">
	<link href="../../template/css/main.css" rel="stylesheet">
	<link href="../../template/css/responsive.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <!-- <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png"> -->
</head><!--/head-->

<body>
	<header id="header" class="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> 020 6030 0505</a></li>
								<li><a href="http://www.solutionsline.com/home.html"><i class="fa fa-envelope"></i> www.solutionsline.com</a></li>
							</ul>
						</div>
					</div>
					  <c:if test="${ROLE eq LOGINROLE_DOCTOR}">
					<div class="col-sm-6">
						<form action="../../admin/dashboard/view.do" method="post">
					       <div class="input-group">
								<input list="browsers" class="form-control" placeholder="Search"
									id="txtSearch" name="txtsearch"
									style="width: 550px; height: 39px;border-radius:17px;margin-top: 2px"/>
								<datalist id="browsers">
									<c:forEach items="${ProductsList}" var="products">
										<option value="${products.name}">
									</c:forEach>
								</datalist>
								 <button type="submit" class="button123" 
										onclick="getSearch()">
								 <span class="glyphicon glyphicon-search"></span>
							   </button> 
							<!--     <div class="input-group-btn">
						        <button class="btn btn-primary" type="submit">
						        <span class="glyphicon glyphicon-search"></span>
						        </button>
						   </div> -->
						</div>
				       </form>
					</div>
					<div class="col-sm-3">
						
			   	  </div>	
					</c:if>
					 <c:if test="${ROLE ne LOGINROLE_DOCTOR}">
					 <div class="col-sm-6">
					 </div>
					 </c:if>
					 
					<div class="col-sm-3">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
								<li><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
								<li><a href="https://www.linkedin.com/"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="https://dribbble.com/"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="https://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle" ><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="../../admin/dashboard/view.do"><img src="../../template/images/home/logo.png" alt="" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<c:if test="${ROLE eq LOGINROLE_DOCTOR}">
							    <li><a href="../../admin/dashboard/view.do"><i class="fa fa-home"></i>Home</a></li>
								<li><a href="../../admin/doctor/edit.do"><i class="fa fa-user"></i>My Account</a></li>
								<li><a href="../../admin/order/wishlist.do"><i class="fa fa-star"></i> Wishlist</a></li>
								<li><a href="../../admin/order/trackorder.do"><i class="glyphicon glyphicon-map-marker"></i>Track Order</a></li>
								<li><a href="../../admin/ticket/add.do"><i class="glyphicon glyphicon-question-sign"></i>Need Help</a></li>
								<li><a href="../../admin/cart/list.do" id="cartcount" ><i class="fa fa-shopping-cart"></i> Cart (${CartListCount})</a></li>
							
								</c:if>
								
								<c:if test="${ROLE eq LOGINROLE_ADMINISTRATOR }">
								<li><a href="../../admin/dashboard/view.do"><i class="fa fa-home"></i>Home</a></li>
								<li><a href="#"><i class="fa fa-user"></i> Account</a></li>
								<li><a href="../../admin/company/add.do"><i class="fa fa-star"></i> Add Company</a></li>
								<li><a href="../../admin/productsku/add.do"><i class="fa fa-star"></i> Add Product</a></li>
								<li><a href="../../admin/doctor/add.do"><i class="fa fa-star"></i> Add Doctor</a></li>
								<li><a href="../../admin/telecaller/add.do"><i class="fa fa-star"></i> Add Telecaller</a></li>
							   </c:if>
							   <c:if test="${ROLE eq LOGINROLE_TELECALLER }">
							   <li><a href=""><i class="fa fa-star"></i> Dashboard</a></li>
							   <li><a href="../../admin/telecaller/getorderlist.do"><i class="fa fa-star"></i>OrderList(${ordercount})</a></li>
							    <li><a href="../../admin/ticket/list.do"><i class="fa fa-ticket"></i>TicketList(${ticketcount})</a></li>
							   <li><a href="../../admin/telecaller/edit.do"><i class="fa fa-user"></i>My Account</a></li>
							   
							   </c:if>
							   
								<!-- <li><a href="checkout.html"><i class="fa fa-crosshairs"></i> Checkout</a></li> -->
								<!-- <li><a href="../../admin/cart/list.do"><i class="fa fa-shopping-cart"></i> Cart</a></li> -->
							   <li><a href="../../admin/login/logout.do"><i class="fa fa-lock"></i> Logout</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<%-- <div class="container">
				<div class="row">
					<div class="col-sm-3">
						 
					</div> 
					<div class="col-sm-9">
						<form action="../../admin/dashboard/view.do" method="post">
					       <div class="input-group">
								<input list="browsers" class="form-control" placeholder="Search"
									id="txtSearch" name="txtsearch"
									style="width: 750px; height: 40px;border-radius:10px;"/>
								<datalist id="browsers">
									<c:forEach items="${ProductsList}" var="products">
										<option value="${products.name}">
									</c:forEach>
								</datalist>
								<button type="submit" class="button123" 
										onclick="getSearch()">
										<span class="glyphicon glyphicon-search"></span>
									</button>
							</div>
				       </form>
					</div>
					
				</div>
			</div> --%>
			</div>
		
	</header><!--/header-->
	
	<tiles:get name="content" />
	
 <footer id="footer">
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="companyinfo">
							<h2><span>e</span>-shopper</h2>
							
						</div>
					</div>
					<div class="col-sm-7">
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="../../template/images/home/image1.jpg" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="../../template/images/home/image2.jpg" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
							
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="../../template/images/home/image3.jpg" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="../../template/images/home/image5.jpg" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="address">
							<img src="../../template/images/home/map.png" alt="" />
							
						</div>
					</div>
				</div>
			</div>
		</div> 
		
<!-- 		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Service</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Online Help</a></li>
								<li><a href="#">Contact Us</a></li>
								<li><a href="#">Order Status</a></li>
								<li><a href="#">Change Location</a></li>
								<li><a href="#">FAQ’s</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Quock Shop</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">T-Shirt</a></li>
								<li><a href="#">Mens</a></li>
								<li><a href="#">Womens</a></li>
								<li><a href="#">Gift Cards</a></li>
								<li><a href="#">Shoes</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Policies</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Terms of Use</a></li>
								<li><a href="#">Privecy Policy</a></li>
								<li><a href="#">Refund Policy</a></li>
								<li><a href="#">Billing System</a></li>
								<li><a href="#">Ticket System</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>About Shopper</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Company Information</a></li>
								<li><a href="#">Careers</a></li>
								<li><a href="#">Store Location</a></li>
								<li><a href="#">Affillate Program</a></li>
								<li><a href="#">Copyright</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3 col-sm-offset-1">
						<div class="single-widget">
							<h2>About Shopper</h2>
							<form action="#" class="searchform">
								<input type="text" placeholder="Your email address" />
								<button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
								<p>Get the most recent updates from <br />our site and be updated your self...</p>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div> -->
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2010 Solutionsline SoftTech Pvt. Ltd. All rights reserved.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="http://www.solutionsline.com/">Solutionsline</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	

  
    <script src="../../template/js/jquery.js"></script>
	<script src="../../template/js/bootstrap.min.js"></script>
	<script src="../../template/js/jquery.scrollUp.min.js"></script>
	<script src="../../template/js/price-range.js"></script>
    <script src="../../template/js/jquery.prettyPhoto.js"></script>
    <script src="../../template/js/main.js"></script>
    <style>
.button123 {
	background-color: #FE980F;
	border: none;
	color: black;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 13px;
	margin: 4px 2px;
	cursor: pointer;
	padding-bottom: 8px;
    padding-top: 0px;
    width: 61px;
    border-radius: 17px;
    height: 40px;
    margin-top: 12px;
}

</style>

</body>
</html>
