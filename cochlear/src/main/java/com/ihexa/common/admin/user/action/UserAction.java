package com.ihexa.common.admin.user.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.validation.DataBinder;

import com.ihexa.common.admin.iHexaConstants.IHexaConstants;
import com.ihexa.common.admin.login.Login;
import com.ihexa.common.admin.login.service.LoginService;
import com.ihexa.common.admin.skill.Skill;
import com.ihexa.common.admin.skill.service.SkillService;
import com.ihexa.common.admin.user.Address;
import com.ihexa.common.admin.user.City;
import com.ihexa.common.admin.user.Country;
import com.ihexa.common.admin.user.Location;
import com.ihexa.common.admin.user.Occupation;
import com.ihexa.common.admin.user.Person;
import com.ihexa.common.admin.user.Role;
import com.ihexa.common.admin.user.State;
import com.ihexa.common.admin.user.User;
import com.ihexa.common.admin.user.service.UserService;
import com.ihexa.common.admin.userskill.UserSkill;
import com.ihexa.common.admin.userskill.service.UserSkillService;
import com.prounify.framework.base.action.AutoComplete;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.base.action.BindingActionForm;
import com.prounify.framework.constant.ProUnifyConstants;
import com.prounify.framework.context.Context;
import com.prounify.framework.context.UserContext;
import com.prounify.framework.util.MD5Util;

public class UserAction extends BaseCRUDAction {

	/**
	 * to allow other methods other than framework
	 * 
	 * @param httpMethod
	 * @param methodId
	 * @return
	 */
	protected boolean isHttpMethodAllowed(String httpMethod, String methodId) {

		if (httpMethod.equals(HTTP_METHOD_GET)) {

			if (methodId.equals("changePassword"))
				return true;
			if (methodId.equals("toChangePassword"))
				return true;
			if (methodId.equals("profileEdit"))
				return true;
			if (methodId.equals("profileUpdate"))
				return true;
			if (methodId.equals("profileView"))
				return true;
		}
		return super.isHttpMethodAllowed(httpMethod, methodId);
	}

	/**
	 * to update User Details
	 * 
	 * @return
	 */
	public Object doProfileUpdate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		String skills[] = request.getParameterValues("skill.id");
		BindingActionForm actionForm = (BindingActionForm) form;
		UserService userService = (UserService) getService(mapping);
		String ID = request.getParameter("id");
		User entity = (User) userService.findById(new Long(ID));

		entity = initializeUser(entity);

		Set<UserSkill> userSkillSet = new HashSet<UserSkill>();
		DataBinder binder = getBinder(entity, mapping, request);
		entity = checkUser(entity);

		ActionMessages messages = new ActionMessages();
		messages.clear();
		ActionMessage msg = null;

		try {

			if (skills != null) {
				for (String str : skills) {
					UserSkill userSkill = new UserSkill();
					userSkill.getUser().setId(entity.getId());
					userSkill.getSkill().setId(Long.valueOf(str));
					userSkillSet.add(userSkill);
				}
			}
			entity.setUserSkills(userSkillSet);
			userService.update(entity, binder.getBindingResult());
			if (entity != null && entity.getId() != null) {
				msg = new ActionMessage("user.UPDATE_SUCCESS");
				messages.add("success", msg);
			}
			saveMessages(request, messages);
			return RETURN_SUCCESS;
		} catch (Exception e) {
			// actionForm.expose(binder.getBindingResult(), request, e);
			msg = new ActionMessage("user.UPDATE_FAIL", e.getCause());
			messages.add("error", msg);
			saveMessages(request, messages);
			return ERROR;
		}

	}

	/**
	 * to redirect to user edit page with static data
	 * 
	 * @return
	 */
	public Object doProfileEdit(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		String ID = UserContext.getUserId() + "";

		SkillService skillService = (SkillService) Context.getInstance().getBean("SkillService");
		List<Skill> skillList = skillService.findAll();

		/*
		 * To get the list of the UserSkill
		 */
		UserSkillService service = (UserSkillService) Context.getInstance().getBean("UserSkillService");
		DetachedCriteria criteria = DetachedCriteria.forClass(UserSkill.class);
		criteria.add(Restrictions.eq(UserSkill.PROP_USER + "." + User.PROP_ID, Long.valueOf(ID)));
		List<UserSkill> userSkillList = service.findAll(criteria);

		/*
		 * Comparing the user's Skill with the All Skill & set the userSkill for displaying on the JSP
		 */
		for (Skill skill : skillList) {
			for (UserSkill userSkill : userSkillList) {
				if (userSkill.getSkill().getId().longValue() == skill.getId().longValue()) {
					skill.setSelected(true);
					break;
				}

			}
		}

		request.setAttribute("Skills", skillList);
		request.setAttribute("UserSkills", userSkillList);

		UserService userService = (UserService) getService(mapping);
		User entity = (User) userService.findById(new Long(ID));

		// SETTING STATIC LIST START

		request.setAttribute("roleList", userService.getRoleList());
		request.setAttribute("salutationList", getSalutationList());
		request.setAttribute("genderList", getGenderList());
		request.setAttribute("maritalStatusList", getMaritalStatusList());
		request.setAttribute("countryList", userService.getCountryList());
		request.setAttribute("occupationList", userService.getOccupationList());
		request.setAttribute("cityList", null);
		request.setAttribute("StateList", null);
		request.setAttribute("managerList", null);
		// SETTING STATIC LIST END

		User user = new User();
		if (entity instanceof User) {
			user = (User) entity;
		}
		// to get respective List
		if (user != null && user.getId() != null) {
			if (user.getPerson() != null) {
				if (user.getPerson().getAddress() != null) {
					if (user.getPerson().getAddress().getCountry() != null) {
						if (user.getPerson().getAddress().getCountry().getId() != null) {
							request.setAttribute("stateList", userService.getStateList(user.getPerson().getAddress().getCountry().getId()));
						}
					}
					if (user.getPerson().getAddress().getState() != null) {
						if (user.getPerson().getAddress().getState().getId() != null) {
							request.setAttribute("cityList", userService.getCityList(user.getPerson().getAddress().getState().getId()));
						}
					}
				}
			}
			if (user.getRole() != null && user.getRole().getId() != null) {
				request.setAttribute("managerList", userService.getManagerList(user.getRole().getId()));
			}
		}

		request.setAttribute(mapping.getDataAttribute(), entity);
		return SUCCESS;
	}

	/**
	 * to redirect to user edit page with static data
	 * 
	 * @return
	 */
	public Object doProfileView(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		String ID = UserContext.getUserId() + "";

		SkillService skillService = (SkillService) Context.getInstance().getBean("SkillService");
		List<Skill> skillList = skillService.findAll();

		/*
		 * To get the list of the UserSkill
		 */
		UserSkillService service = (UserSkillService) Context.getInstance().getBean("UserSkillService");
		DetachedCriteria criteria = DetachedCriteria.forClass(UserSkill.class);
		criteria.add(Restrictions.eq(UserSkill.PROP_USER + "." + User.PROP_ID, Long.valueOf(ID)));
		List<UserSkill> userSkillList = service.findAll(criteria);

		/*
		 * Comparing the user's Skill with the All Skill & set the userSkill for displaying on the JSP
		 */
		for (Skill skill : skillList) {
			for (UserSkill userSkill : userSkillList) {
				if (userSkill.getSkill().getId().longValue() == skill.getId().longValue()) {
					skill.setSelected(true);
					break;
				}

			}
		}

		request.setAttribute("Skills", skillList);
		request.setAttribute("UserSkills", userSkillList);

		UserService userService = (UserService) getService(mapping);
		User entity = (User) userService.findById(new Long(ID));

		// SETTING STATIC LIST START

		request.setAttribute("roleList", userService.getRoleList());
		request.setAttribute("salutationList", getSalutationList());
		request.setAttribute("genderList", getGenderList());
		request.setAttribute("maritalStatusList", getMaritalStatusList());
		request.setAttribute("countryList", userService.getCountryList());
		request.setAttribute("occupationList", userService.getOccupationList());
		request.setAttribute("cityList", null);
		request.setAttribute("StateList", null);
		request.setAttribute("managerList", null);
		// SETTING STATIC LIST END

		User user = new User();
		if (entity instanceof User) {
			user = (User) entity;
		}
		// to get respective List
		if (user != null && user.getId() != null) {
			if (user.getPerson() != null) {
				if (user.getPerson().getAddress() != null) {
					if (user.getPerson().getAddress().getCountry() != null) {
						if (user.getPerson().getAddress().getCountry().getId() != null) {
							request.setAttribute("stateList", userService.getStateList(user.getPerson().getAddress().getCountry().getId()));
						}
					}
					if (user.getPerson().getAddress().getState() != null) {
						if (user.getPerson().getAddress().getState().getId() != null) {
							request.setAttribute("cityList", userService.getCityList(user.getPerson().getAddress().getState().getId()));
						}
					}
				}
			}
			if (user.getRole() != null && user.getRole().getId() != null) {
				request.setAttribute("managerList", userService.getManagerList(user.getRole().getId()));
			}
		}

		request.setAttribute(mapping.getDataAttribute(), entity);
		return SUCCESS;
	}

	/**
	 * to Change user password
	 * 
	 * @return
	 */
	public Object doChangePassword(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		BindingActionForm actionForm = (BindingActionForm) form;
		MD5Util util = new MD5Util();

		LoginService loginService = (LoginService) Context.getInstance().getBean("LoginService");

		String ID = request.getParameter("loginId");
		String newPassword = request.getParameter("newPassword");
		String oldpassword = request.getParameter("oldPassword");

		Login login = (Login) loginService.findById(new Long(ID));

		oldpassword = util.hashPassword(oldpassword);

		ActionMessages messages = new ActionMessages();
		messages.clear();
		ActionMessage msg = null;

		// if old password matches with the existing password then only can change the password else
		// give the message "invalid password"

		try {
			if (oldpassword.equals(login.getPassword())) {
				login.setPassword(util.hashPassword(newPassword));
				loginService.update(login, null);

				msg = new ActionMessage("password.CHANGE_SUCCESS");
				messages.add("success", msg);
				saveMessages(request, messages);
				return RETURN_SUCCESS;
			} else {
				msg = new ActionMessage("password.WRONG_PASSWORD");
				messages.add("error", msg);
				saveMessages(request, messages);
				return RETURN_ERROR;
			}
		} catch (Exception e) {

			// TODO: handle exception
			// actionForm.expose(binder.getBindingResult(), request, e);
			msg = new ActionMessage("password.CHANGE_FAIL", e.getCause());
			messages.add("error", msg);
			saveMessages(request, messages);
			return ERROR;
		}

	}

	/**
	 * to redirect to user password change page Details
	 * 
	 * @return
	 */
	public Object doToChangePassword(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		UserService userService = (UserService) getService(mapping);
		User entity = (User) userService.findById(UserContext.getUserId());
		request.setAttribute(mapping.getDataAttribute(), entity);

		return RETURN_SUCCESS;
	}

	/**
	 * to delete user
	 * 
	 * @return
	 */
	@Override
	public Object doDelete(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		BindingActionForm actionForm = (BindingActionForm) form;

		ActionMessages messages = new ActionMessages();
		messages.clear();
		ActionMessage msg = null;
		try {
			super.doDelete(mapping, form, request, response);
			msg = new ActionMessage("user.DELETE_SUCCESS");
			messages.add("success", msg);
			saveMessages(request, messages);
			return RETURN_SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			// actionForm.expose(binder.getBindingResult(), request, e);
			msg = new ActionMessage("user.DELETE_FAIL", e.getCause());
			messages.add("error", msg);
			saveMessages(request, messages);
			return ERROR;
		}

	}

	/**
	 * to update User Details
	 * 
	 * @return
	 */
	public Object doUpdate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		String skills[] = request.getParameterValues("skill.id");
		BindingActionForm actionForm = (BindingActionForm) form;
		UserService userService = (UserService) getService(mapping);
		String ID = request.getParameter("id");
		User entity = (User) userService.findById(new Long(ID));

		entity = initializeUser(entity);

		Set<UserSkill> userSkillSet = new HashSet<UserSkill>();
		DataBinder binder = getBinder(entity, mapping, request);
		entity = checkUser(entity);

		ActionMessages messages = new ActionMessages();
		messages.clear();
		ActionMessage msg = null;

		try {

			if (skills != null) {
				for (String str : skills) {
					UserSkill userSkill = new UserSkill();
					userSkill.getUser().setId(entity.getId());
					userSkill.getSkill().setId(Long.valueOf(str));
					userSkillSet.add(userSkill);
				}
			}
			entity.setUserSkills(userSkillSet);
			userService.update(entity, binder.getBindingResult());
			if (entity != null && entity.getId() != null) {
				msg = new ActionMessage("user.UPDATE_SUCCESS");
				messages.add("success", msg);
			}
			saveMessages(request, messages);
			return RETURN_SUCCESS;
		} catch (Exception e) {
			// actionForm.expose(binder.getBindingResult(), request, e);
			msg = new ActionMessage("user.UPDATE_FAIL", e.getCause());
			messages.add("error", msg);
			saveMessages(request, messages);
			return ERROR;
		}

	}

	/**
	 * to redirect to user edit page with static data
	 * 
	 * @return
	 */
	public Object doEdit(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		String ID = request.getParameter("id");

		SkillService skillService = (SkillService) Context.getInstance().getBean("SkillService");
		List<Skill> skillList = skillService.findAll();

		/*
		 * To get the list of the UserSkill
		 */
		UserSkillService service = (UserSkillService) Context.getInstance().getBean("UserSkillService");
		DetachedCriteria criteria = DetachedCriteria.forClass(UserSkill.class);
		criteria.add(Restrictions.eq(UserSkill.PROP_USER + "." + User.PROP_ID, Long.valueOf(ID)));
		List<UserSkill> userSkillList = service.findAll(criteria);

		/*
		 * Comparing the user's Skill with the All Skill & set the userSkill for displaying on the JSP
		 */
		for (Skill skill : skillList) {
			for (UserSkill userSkill : userSkillList) {
				if (userSkill.getSkill().getId().longValue() == skill.getId().longValue()) {
					skill.setSelected(true);
					break;
				}

			}
		}

		request.setAttribute("Skills", skillList);
		request.setAttribute("UserSkills", userSkillList);

		UserService userService = (UserService) getService(mapping);
		User entity = (User) userService.findById(new Long(ID));

		// SETTING STATIC LIST START

		request.setAttribute("roleList", userService.getRoleList());
		request.setAttribute("salutationList", getSalutationList());
		request.setAttribute("genderList", getGenderList());
		request.setAttribute("maritalStatusList", getMaritalStatusList());
		request.setAttribute("countryList", userService.getCountryList());
		request.setAttribute("occupationList", userService.getOccupationList());
		request.setAttribute("cityList", null);
		request.setAttribute("StateList", null);
		request.setAttribute("managerList", null);
		// SETTING STATIC LIST END

		User user = new User();
		if (entity instanceof User) {
			user = (User) entity;
		}
		// to get respective List
		if (user != null && user.getId() != null) {
			if (user.getPerson() != null) {
				if (user.getPerson().getAddress() != null) {
					if (user.getPerson().getAddress().getCountry() != null) {
						if (user.getPerson().getAddress().getCountry().getId() != null) {
							request.setAttribute("stateList", userService.getStateList(user.getPerson().getAddress().getCountry().getId()));
						}
					}
					if (user.getPerson().getAddress().getState() != null) {
						if (user.getPerson().getAddress().getState().getId() != null) {
							request.setAttribute("cityList", userService.getCityList(user.getPerson().getAddress().getState().getId()));
						}
					}
				}
			}
			if (user.getRole() != null && user.getRole().getId() != null) {
				request.setAttribute("managerList", userService.getManagerList(user.getRole().getId()));
			}
		}

		request.setAttribute(mapping.getDataAttribute(), entity);
		return SUCCESS;
	}

	/**
	 * to save new User
	 * 
	 * @param entity
	 * @return
	 */
	public Object doCreate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		String skills[] = request.getParameterValues("skill.id");
		BindingActionForm actionForm = (BindingActionForm) form;
		UserService userService = (UserService) getService(mapping);

		User entity = (User) getBean(mapping.getBaseName());

		Set<UserSkill> userSkillSet = new HashSet<UserSkill>();
		entity.setManager(new User());
		DataBinder binder = getBinder(entity, mapping, request);

		// check user object values
		entity = checkUser(entity);

		ActionMessages messages = new ActionMessages();
		messages.clear();
		ActionMessage msg = null;

		try {
			userService.add(entity, binder.getErrors());

			/*
			 * for adding the Selected Skill on to the UserSkill table
			 */
			if (skills != null) {
				for (String str : skills) {
					UserSkill userSkill = new UserSkill();
					userSkill.getUser().setId(entity.getId());
					userSkill.getSkill().setId(Long.valueOf(str));
					userSkillSet.add(userSkill);
				}
			}

			entity.setUserSkills(userSkillSet);
			userService.update(entity, null);

			if (entity != null && entity.getId() != null) {
				msg = new ActionMessage("user.SAVE_SUCCESS");
				messages.add("success", msg);
			}
			saveMessages(request, messages);
			return RETURN_SUCCESS;
		} catch (Exception e) {
			// actionForm.expose(binder.getBindingResult(), request, e);
			e.printStackTrace();
			msg = new ActionMessage("user.SAVE_FAIL", e.getCause());
			messages.add("error", msg);
			saveMessages(request, messages);
			return ERROR;
		}
	}

	/**
	 * to redirect to user add page with static data
	 * 
	 * @return
	 */
	public Object doAdd(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		SkillService skillService = (SkillService) Context.getInstance().getBean("SkillService");
		UserService userService = (UserService) getService(mapping);

		// SETTING STATIC LIST START

		request.setAttribute("roleList", userService.getRoleList());
		request.setAttribute("salutationList", getSalutationList());
		request.setAttribute("genderList", getGenderList());
		request.setAttribute("maritalStatusList", getMaritalStatusList());
		request.setAttribute("countryList", userService.getCountryList());
		request.setAttribute("occupationList", userService.getOccupationList());
		request.setAttribute("Skills", skillService.findAll());
		request.setAttribute("managerList", null);
		// SETTING STATIC LIST END

		return super.doAdd(mapping, form, request, response);
	}

	/* AJAX METHOD'S */

	/**
	 * AJAX CALL to get selected role managers. Usually means nothing gets written in the database.
	 * 
	 * <p>
	 * <b>Note:</b> If the operation shouldn't be allowed for this action, a subclass can override this method and throw a NotImplementedException.
	 * </p>
	 * 
	 * @param mapping
	 *            The ActionMapping used to select this action instance
	 * @param form
	 *            The optional ActionForm bean for this request (if any)
	 * @param request
	 *            The HTTP request we are processing
	 * @param response
	 *            The HTTP response we are creating
	 * @return The forward to which control should be transferred, or <code>null</code> if the response has been completed. It can either be an ActionForward or any other object
	 *         for which the toString() method will return the name of a forward.
	 * @throws Exception
	 *             if the application business logic throws an exception. The default implementation returns a ServletException as it is an application error to call this method
	 *             without having implemented it.
	 * 
	 */
	public Object doGetManagerList(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		Long roleId = new Long(-1);
		List<User> managerList = new ArrayList<User>();
		String roleIdStr = request.getParameter("roleId").toString();

		if (roleIdStr != null && !"".equals(roleIdStr)) {
			roleId = Long.parseLong(roleIdStr);
			UserService customerService = (UserService) getService(mapping);
			managerList = customerService.getManagerList(roleId);
		}
		System.out.println("roleID ::: " + roleId);
		AutoComplete autoComplete = new AutoComplete();
		autoComplete.addItem(getProperty("common.listbox.select"), "");
		for (User user : managerList) {
			autoComplete.addItem(user.getPerson().getFirstName() + " " + user.getPerson().getLastName() + "-" + user.getRole().getRoleName(), user.getId().toString());
		}
		return autoComplete.doAjax(request, response);
	}

	/**
	 * to check User Object null values for list box
	 * 
	 * @param entity
	 * @return
	 */
	public User checkUser(User entity) {
		if (entity != null) {

			// Checking Manager(User) Object
			if (entity.getManager() != null && entity.getManager().getId() == null) {
				entity.setManager(null);
			}
			// Checking UserSkills Object
			if (entity.getUserSkills() != null && entity.getUserSkills().size() > 0) {
				entity.setUserSkills(null);
			}
			// Checking Role Object
			if (entity.getRole() != null && entity.getRole().getId() == null) {
				entity.setRole(null);
			}

			// Checking Person Object
			if (entity.getPerson() != null) {
				// Checking Occupation Object
				if (entity.getPerson().getOccupation() != null && entity.getPerson().getOccupation().getId() == null) {
					entity.getPerson().setOccupation(null);
				}

				// Checking Address Object
				if (entity.getPerson().getAddress() != null) {
					if (entity.getPerson().getAddress().getCountry() != null && entity.getPerson().getAddress().getCountry().getId() == null) {
						entity.getPerson().getAddress().setCountry(null);
					}
					if (entity.getPerson().getAddress().getState() != null && entity.getPerson().getAddress().getState().getId() == null) {
						entity.getPerson().getAddress().setState(null);
					}
					if (entity.getPerson().getAddress().getCity() != null && entity.getPerson().getAddress().getCity().getId() == null) {
						entity.getPerson().getAddress().setCity(null);
					}
					/*if (entity.getPerson().getAddress().getLocation() != null && entity.getPerson().getAddress().getLocation().getId() == null) {
						entity.getPerson().getAddress().setLocation(null);
					}*/
				}
			}
		}
		return entity;
	}

	/**
	 * to create new Objects if Object is null for list box
	 * 
	 * @param entity
	 * @return
	 */
	public User initializeUser(User entity) {
		if (entity != null) {

			// Checking Occupation Object
			if (entity.getPerson().getOccupation() == null) {
				entity.getPerson().setOccupation(new Occupation());
			}

			// Checking Manager(User) Object
			if (entity.getManager() == null) {
				entity.setManager(new User());
			}

			// Checking UserSkills Object
			if (entity.getUserSkills() == null) {
				entity.setUserSkills(new HashSet<UserSkill>());
			}
			// Checking Person Object
			if (entity.getPerson() == null) {
				entity.setPerson(new Person());
			} else {
				if (entity.getPerson().getOccupation() == null) {
					entity.getPerson().setOccupation(new Occupation());
				}
				if (entity.getPerson().getAddress() == null) {
					entity.getPerson().setAddress(new Address());
				} else {
					if (entity.getPerson().getAddress().getCountry() == null) {
						entity.getPerson().getAddress().setCountry(new Country());
					}
					if (entity.getPerson().getAddress().getState() == null) {
						entity.getPerson().getAddress().setState(new State());
					}
					if (entity.getPerson().getAddress().getCity() == null) {
						entity.getPerson().getAddress().setCity(new City());
					}
					/*if (entity.getPerson().getAddress().getLocation() == null) {
						entity.getPerson().getAddress().setLocation(new Location());
					}*/
				}
			}
		}
		return entity;
	}

	/**
	 * to get role list
	 * 
	 * @return
	 */
	private List<Role> getRoles() {

		List<Role> list = new ArrayList<Role>();

		switch (UserContext.getRole()) {

			// Administrator
			case 7 :
				list.add(new Role(ProUnifyConstants.LOGINROLE_ADMINISTRATOR, "Administrator"));
				list.add(new Role(ProUnifyConstants.LOGINROLE_CUSTOMER, "Customer"));
				list.add(new Role(ProUnifyConstants.LOGINROLE_FREELANCER, "Freelancer"));
				list.add(new Role(ProUnifyConstants.LOGINROLE_GUEST, "Guest"));
				list.add(new Role(ProUnifyConstants.LOGINROLE_RESELLER, "Reseller"));
				list.add(new Role(ProUnifyConstants.LOGINROLE_SERVICE_PROVIDER, "Service Provider"));
				list.add(new Role(ProUnifyConstants.LOGINROLE_VENDOR, "Vendor"));
				list.add(new Role(ProUnifyConstants.LOGINROLE_CPA, "CPA"));
				list.add(new Role(ProUnifyConstants.LOGINROLE_CPA_ASSISTANT, "Assistant"));

				break;

			// CPA
			case 1 :
				list.add(new Role(ProUnifyConstants.LOGINROLE_CPA, "CPA"));
				list.add(new Role(ProUnifyConstants.LOGINROLE_CPA_ASSISTANT, "Assistant"));
				list.add(new Role(ProUnifyConstants.LOGINROLE_SERVICE_PROVIDER, "Service Provider"));

				break;

			// Service Provider
			case 4 :
				list.add(new Role(ProUnifyConstants.LOGINROLE_TEAM_MEMBER, "Team Member"));

				break;

			default : // do nothing
		}

		return list;
	}
	// to get static list section start

	/**
	 * to get source list from IHexaConstants
	 * 
	 * @return List
	 */
	public List getSourceList() {
		List sourceList = new ArrayList();

		for (int i = 0; i < IHexaConstants.SOURCE.length; i++) {
			sourceList.add(IHexaConstants.SOURCE[i]);
		}
		return sourceList;
	}

	/**
	 * to get salutation list from IHexaConstants
	 * 
	 * @return List
	 */
	public List getSalutationList() {
		List salutationList = new ArrayList();

		for (int i = 0; i < IHexaConstants.SALUTATION.length; i++) {
			salutationList.add(IHexaConstants.SALUTATION[i]);
		}
		return salutationList;
	}

	/**
	 * to get gender list from IHexaConstants
	 * 
	 * @return List
	 */
	public List getGenderList() {
		List genderList = new ArrayList();

		for (int i = 0; i < IHexaConstants.GENDER.length; i++) {
			genderList.add(IHexaConstants.GENDER[i]);
		}
		return genderList;
	}

	/**
	 * to get maritalStatus list from IHexaConstants
	 * 
	 * @return List
	 */
	public List getMaritalStatusList() {
		List maritalStatusList = new ArrayList();

		for (int i = 0; i < IHexaConstants.MARITALSTATUS.length; i++) {
			maritalStatusList.add(IHexaConstants.MARITALSTATUS[i]);
		}
		return maritalStatusList;
	}

	/**
	 * to get media list from IHexaConstants
	 * 
	 * @return List
	 */
	public List getMediaList() {
		List mediaList = new ArrayList();

		for (int i = 0; i < IHexaConstants.MEDIA.length; i++) {
			mediaList.add(IHexaConstants.MEDIA[i]);
		}
		return mediaList;
	}

	/**
	 * to get status list from IHexaConstants
	 * 
	 * @return List
	 */
	public List getStatusList() {
		List statusList = new ArrayList();

		for (int i = 0; i < IHexaConstants.STATUS.length; i++) {
			statusList.add(IHexaConstants.STATUS[i]);
		}
		return statusList;
	}

	/**
	 * to get YESNO list from IHexaConstants
	 * 
	 * @return List
	 */
	public List getYesNoList() {
		List yesNoList = new ArrayList();

		for (int i = 0; i < IHexaConstants.YESNO.length; i++) {
			yesNoList.add(IHexaConstants.YESNO[i]);
		}
		return yesNoList;
	}

	// to get static list section end

	/**
	 * to get Property value from .properties file
	 * 
	 * @param key
	 * @return value of that key
	 */
	public String getProperty(String key) {
		return ResourceBundle.getBundle("IHexaResources", Locale.getDefault()).getString(key);
	}
}
