package com.ihexa.common.admin.dashboard;

import com.ihexa.common.admin.dashboard.base.BasePricelist;



public class Pricelist extends BasePricelist {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Pricelist () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Pricelist (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}