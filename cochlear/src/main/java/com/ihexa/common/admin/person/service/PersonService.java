
package com.ihexa.common.admin.person.service;

import com.ihexa.common.admin.person.City;
import com.ihexa.common.admin.person.State;
import com.ihexa.common.admin.person.Country;
import com.prounify.framework.base.service.Service;

import java.util.List;


public interface PersonService<T> extends Service<T> {
   
	public List<Country> getCountryList();

   // public List getStateList();
	 public Country getCountryListByID(Long countryID);
	 public List<State> getStateList(Long countryID);
	 public List<City> getCityList(Long stateID);
  
    }
    
  


