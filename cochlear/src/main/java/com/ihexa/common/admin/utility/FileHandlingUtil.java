package com.ihexa.common.admin.utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import org.apache.struts.upload.FormFile;

public class FileHandlingUtil {

	public static synchronized String saveToTemp(FormFile theFile, String prevParam[], Date presentDate)
	throws FileNotFoundException, IOException {
		String newName = null;
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newName;
	}
	
	public static boolean moveFile(String inPath,String outPath,String fileName){
		
		try {
			File file = new File(inPath + fileName);
			File dir = new File(outPath);
			boolean success = file.renameTo(new File(dir, file.getName()));
			if (success) {
				System.out.println("File was successfully moved");
			}else{
				new File(outPath + fileName);
				System.out.println("File was successfully created in "+outPath);
				boolean checkFileDelete = file.delete() ;
				if(checkFileDelete){
					System.out.println("File was successfully deleted from "+inPath);
				}else{
					System.out.println("File could not be deleted from "+inPath);
				}
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static String convertDatetoString(Date date,DateFormat formatter) throws ParseException {		
		String sdate=null;
		if(date!=null)
		{
			sdate = formatter.format(date); 	
		}
			  
		return sdate;
	}
	
	/*public static String nameEdifile(String propFileDate,String ediType,String portalName) throws ParseException 
	{
		//String stringPropFile=stringFrmPropFile;	
		SimpleDateFormat formatter=null;
		String newFileName=null;
		
		System.out.println("propFileDate "+propFileDate);
		if(propFileDate!=null && propFileDate.length()>0)		
			{
			    if(propFileDate!=null)
			    {
			    	*//** For date format yyyy *//*
			    	
			    	if(propFileDate.equalsIgnoreCase("MMDDYYYY"))
			    	{
			    		 formatter = new SimpleDateFormat("MMddyyyy");
			    	}				    	
			    	if(propFileDate.equalsIgnoreCase("DDMMYYYY"))
			    	{
			    		 formatter = new SimpleDateFormat("ddMMyyyy");
			    	}	
			    	if(propFileDate.equalsIgnoreCase("YYYYDDMM"))
			    	{
			    		 formatter = new SimpleDateFormat("yyyyddmm");
			    	}	
			    	if(propFileDate.equalsIgnoreCase("YYYYMMDD"))
			    	{
			    		 formatter = new SimpleDateFormat("yyyymmdd");
			    	}
			    	
			    	*//** For date format yy **//*
			    	
			    	if(propFileDate.equalsIgnoreCase("MMDDYY"))
			    	{
			    		 formatter = new SimpleDateFormat("MMddyy");
			    	}				    	
			    	if(propFileDate.equalsIgnoreCase("DDMMYY"))
			    	{
			    		 formatter = new SimpleDateFormat("ddMMyy");
			    	}	
			    	if(propFileDate.equalsIgnoreCase("YYDDMM"))
			    	{
			    		 formatter = new SimpleDateFormat("yyddmm");
			    	}	
			    	if(propFileDate.equalsIgnoreCase("YYMMDD"))
			    	{
			    		 formatter = new SimpleDateFormat("yymmdd");
			    	}				    	
			    }	
			    else
			    {
			    	formatter = new SimpleDateFormat("yyyymmdd");
			    }	
			    Date today=new Date();
			    String dateString =convertDatetoString(today,formatter);				
			   	System.out.println("New File Name "+dateString+ediType+portalName);	  
			    newFileName=dateString+"-"+ediType+"-"+portalName;
			   	
		    }
		return newFileName;
	}*/
	
	/*public static void renameEdiFile(String ediType,String portalName) throws ParseException
	{		
		String edifilename=null;			
		
		System.out.println("portalName "+edifilename);
		try {
			Properties props = new Properties();	
	       props.load(new FileInputStream("D://textrade.properties"));
	       if(portalName.equalsIgnoreCase("Target"))
			   edifilename = props .getProperty("edifiledateformatTarget");
	       if(portalName.equalsIgnoreCase("Kohls"))
			   edifilename = props .getProperty("edifiledateformatKohl");
	       if(portalName.equalsIgnoreCase("Sears"))
			edifilename = props .getProperty("edifiledateformatSears");
		       if(edifilename!=null)
		       {
		    	   System.out.println("Portal Name ="+nameEdifile(edifilename,ediType,portalName));
		       }
          } 
	      catch(IOException e)
	      {
	             e.printStackTrace();
	      }
	}*/
	
	public static String getUniqueFileName() {
		String uniqueName = "";
		try {
			Date date = new Date();
			String dateStr = getDateString(date);
			String timeStr = getTimeString(date);
			uniqueName = dateStr+timeStr;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return uniqueName;
	}
	
	@SuppressWarnings("deprecation")
	public static String getDateString(Date date) {
		Date dt = date;
		int year , month , day ;
		String monthStr="",dayStr="";

		year =  dt.getYear() + 1900;

		month = dt.getMonth() + 1;
		if(month>0 && month<10){
			monthStr = "0"+month ;
		}else{
			monthStr = "" + month ;
		}

		day = dt.getDate();
		if(day>0 && day<10){
			dayStr = "0"+day ;
		}else{
			dayStr = "" + day ;
		}

		return ""+year+""+monthStr+""+dayStr;
	}
	
	@SuppressWarnings("deprecation")
	public static String getInterchangeDateString(Date date) {
		Date dt = date;
		int year , month , day ;
		String yearStr,monthStr="",dayStr="";

		year =  (dt.getYear() + 1900)- 2000;

		month = dt.getMonth() + 1;
		
		if(year>0 && year<10){
			yearStr = "0"+year ;
		}else{
			yearStr = "" + year ;
		}
		
		if(month>0 && month<10){
			monthStr = "0"+month ;
		}else{
			monthStr = "" + month ;
		}

		day = dt.getDate();
		if(day>0 && day<10){
			dayStr = "0"+day ;
		}else{
			dayStr = "" + day ;
		}

		return ""+yearStr+""+monthStr+""+dayStr;
	}

	@SuppressWarnings("deprecation")
	public static String getTimeString(Date date) {

		int hr , min , sec;
		String hrStr="", minStr="", secStr="" ;

		hr = date.getHours();
		if(hr>0 && hr<10){
			hrStr = "0"+hr ;
		}else{
			hrStr = "" + hr ;
		}

		min = date.getMinutes();
		if(min>0 && min<10){
			minStr = "0"+min ;
		}else{
			minStr = "" + min ;
		}

		sec = date.getSeconds();
		if(sec>0 && sec<10){
			secStr = "0"+sec ;
		}else{
			secStr = "" + sec ;
		}

		return ""+hrStr+""+minStr+""+secStr;
	}
	
	@SuppressWarnings("deprecation")
	public static String getInterchangeTimeString(Date date) {

		int hr , min ;
		String hrStr="", minStr="" ;

		hr = date.getHours();
		if(hr>0 && hr<10){
			hrStr = "0"+hr ;
		}else{
			hrStr = "" + hr ;
		}

		min = date.getMinutes();
		if(min>0 && min<10){
			minStr = "0"+min ;
		}else{
			minStr = "" + min ;
		}

		return ""+hrStr+""+minStr;
	}
	
	public static String getFileName(String fullFileName){
		int lastIndex=0;
		if(fullFileName != null && (lastIndex=fullFileName.lastIndexOf('.')+1)>1){			
			return fullFileName.substring(0,lastIndex-1);
		}else{
			return fullFileName;
		}
	}
	public static String getFileExtention(String fileName){
		int lastIndex=0;
		if(fileName != null && (lastIndex=fileName.lastIndexOf('.'))>0){			
			return fileName.substring(lastIndex);
		}else{
			return "";
		}		
	}
}
	
