package com.ihexa.common.admin.user.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the person table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="person"
 */

public abstract class BasePerson  implements Serializable {

	public static String REF = "Person";
	public static String PROP_EMAIL = "email";
	public static String PROP_FAX_AREA_CODE = "faxAreaCode";
	public static String PROP_STD_NUMBER2 = "stdNumber2";
	public static String PROP_ADDRESS = "address";
	public static String PROP_STD_NUMBER1 = "stdNumber1";
	public static String PROP_STD_NUMBER = "stdNumber";
	public static String PROP_SHORT_NAME = "shortName";
	public static String PROP_GENDER = "gender";
	public static String PROP_SALUTATION = "salutation";
	public static String PROP_FAX_NUMBER = "faxNumber";
	public static String PROP_EMAIL1 = "email1";
	public static String PROP_EMAIL2 = "email2";
	public static String PROP_NATIVE_LANGUAGE = "nativeLanguage";
	public static String PROP_FIRST_NAME = "firstName";
	public static String PROP_PHONE_NUMBER2 = "phoneNumber2";
	public static String PROP_PHONE_NUMBER1 = "phoneNumber1";
	public static String PROP_MOBILE_NUMBER1 = "mobileNumber1";
	public static String PROP_MOBILE_NUMBER2 = "mobileNumber2";
	public static String PROP_MIDDLE_NAME = "middleName";
	public static String PROP_MARITAL_STATUS = "maritalStatus";
	public static String PROP_OCCUPATION = "occupation";
	public static String PROP_MOBILE_NUMBER = "mobileNumber";
	public static String PROP_PHONE_NUMBER = "phoneNumber";
	public static String PROP_LAST_NAME = "lastName";
	public static String PROP_ID = "id";
	public static String PROP_BIRTH_DATE = "birthDate";


	// constructors
	public BasePerson () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BasePerson (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String salutation;
	private java.lang.String firstName;
	private java.lang.String middleName;
	private java.lang.String lastName;
	private java.lang.String shortName;
	private java.lang.String gender;
	private java.util.Date birthDate;
	private java.lang.String maritalStatus;
	private java.lang.String nativeLanguage;
	private java.lang.String email;
	private java.lang.String email1;
	private java.lang.String email2;
	private java.lang.Long stdNumber;
	private java.lang.Long stdNumber1;
	private java.lang.Long stdNumber2;
	private java.lang.Long phoneNumber;
	private java.lang.Long phoneNumber1;
	private java.lang.Long phoneNumber2;
	private java.lang.String mobileNumber;
	private java.lang.String mobileNumber1;
	private java.lang.String mobileNumber2;
	private java.lang.Long faxAreaCode;
	private java.lang.Long faxNumber;

	// many to one
	private com.ihexa.common.admin.user.Address address;
	private com.ihexa.common.admin.user.Occupation occupation;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: Salutation
	 */
	public java.lang.String getSalutation () {
		return salutation;
	}

	/**
	 * Set the value related to the column: Salutation
	 * @param salutation the Salutation value
	 */
	public void setSalutation (java.lang.String salutation) {
		this.salutation = salutation;
	}



	/**
	 * Return the value associated with the column: FirstName
	 */
	public java.lang.String getFirstName () {
		return firstName;
	}

	/**
	 * Set the value related to the column: FirstName
	 * @param firstName the FirstName value
	 */
	public void setFirstName (java.lang.String firstName) {
		this.firstName = firstName;
	}



	/**
	 * Return the value associated with the column: MiddleName
	 */
	public java.lang.String getMiddleName () {
		return middleName;
	}

	/**
	 * Set the value related to the column: MiddleName
	 * @param middleName the MiddleName value
	 */
	public void setMiddleName (java.lang.String middleName) {
		this.middleName = middleName;
	}



	/**
	 * Return the value associated with the column: LastName
	 */
	public java.lang.String getLastName () {
		return lastName;
	}

	/**
	 * Set the value related to the column: LastName
	 * @param lastName the LastName value
	 */
	public void setLastName (java.lang.String lastName) {
		this.lastName = lastName;
	}



	/**
	 * Return the value associated with the column: ShortName
	 */
	public java.lang.String getShortName () {
		return shortName;
	}

	/**
	 * Set the value related to the column: ShortName
	 * @param shortName the ShortName value
	 */
	public void setShortName (java.lang.String shortName) {
		this.shortName = shortName;
	}



	/**
	 * Return the value associated with the column: Gender
	 */
	public java.lang.String getGender () {
		return gender;
	}

	/**
	 * Set the value related to the column: Gender
	 * @param gender the Gender value
	 */
	public void setGender (java.lang.String gender) {
		this.gender = gender;
	}



	/**
	 * Return the value associated with the column: BirthDate
	 */
	public java.util.Date getBirthDate () {
		return birthDate;
	}

	/**
	 * Set the value related to the column: BirthDate
	 * @param birthDate the BirthDate value
	 */
	public void setBirthDate (java.util.Date birthDate) {
		this.birthDate = birthDate;
	}



	/**
	 * Return the value associated with the column: MaritalStatus
	 */
	public java.lang.String getMaritalStatus () {
		return maritalStatus;
	}

	/**
	 * Set the value related to the column: MaritalStatus
	 * @param maritalStatus the MaritalStatus value
	 */
	public void setMaritalStatus (java.lang.String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}



	/**
	 * Return the value associated with the column: NativeLanguage
	 */
	public java.lang.String getNativeLanguage () {
		return nativeLanguage;
	}

	/**
	 * Set the value related to the column: NativeLanguage
	 * @param nativeLanguage the NativeLanguage value
	 */
	public void setNativeLanguage (java.lang.String nativeLanguage) {
		this.nativeLanguage = nativeLanguage;
	}



	/**
	 * Return the value associated with the column: Email
	 */
	public java.lang.String getEmail () {
		return email;
	}

	/**
	 * Set the value related to the column: Email
	 * @param email the Email value
	 */
	public void setEmail (java.lang.String email) {
		this.email = email;
	}



	/**
	 * Return the value associated with the column: Email1
	 */
	public java.lang.String getEmail1 () {
		return email1;
	}

	/**
	 * Set the value related to the column: Email1
	 * @param email1 the Email1 value
	 */
	public void setEmail1 (java.lang.String email1) {
		this.email1 = email1;
	}



	/**
	 * Return the value associated with the column: Email2
	 */
	public java.lang.String getEmail2 () {
		return email2;
	}

	/**
	 * Set the value related to the column: Email2
	 * @param email2 the Email2 value
	 */
	public void setEmail2 (java.lang.String email2) {
		this.email2 = email2;
	}



	/**
	 * Return the value associated with the column: StdNumber
	 */
	public java.lang.Long getStdNumber () {
		return stdNumber;
	}

	/**
	 * Set the value related to the column: StdNumber
	 * @param stdNumber the StdNumber value
	 */
	public void setStdNumber (java.lang.Long stdNumber) {
		this.stdNumber = stdNumber;
	}



	/**
	 * Return the value associated with the column: StdNumber1
	 */
	public java.lang.Long getStdNumber1 () {
		return stdNumber1;
	}

	/**
	 * Set the value related to the column: StdNumber1
	 * @param stdNumber1 the StdNumber1 value
	 */
	public void setStdNumber1 (java.lang.Long stdNumber1) {
		this.stdNumber1 = stdNumber1;
	}



	/**
	 * Return the value associated with the column: StdNumber2
	 */
	public java.lang.Long getStdNumber2 () {
		return stdNumber2;
	}

	/**
	 * Set the value related to the column: StdNumber2
	 * @param stdNumber2 the StdNumber2 value
	 */
	public void setStdNumber2 (java.lang.Long stdNumber2) {
		this.stdNumber2 = stdNumber2;
	}



	/**
	 * Return the value associated with the column: PhoneNumber
	 */
	public java.lang.Long getPhoneNumber () {
		return phoneNumber;
	}

	/**
	 * Set the value related to the column: PhoneNumber
	 * @param phoneNumber the PhoneNumber value
	 */
	public void setPhoneNumber (java.lang.Long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}



	/**
	 * Return the value associated with the column: PhoneNumber1
	 */
	public java.lang.Long getPhoneNumber1 () {
		return phoneNumber1;
	}

	/**
	 * Set the value related to the column: PhoneNumber1
	 * @param phoneNumber1 the PhoneNumber1 value
	 */
	public void setPhoneNumber1 (java.lang.Long phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}



	/**
	 * Return the value associated with the column: PhoneNumber2
	 */
	public java.lang.Long getPhoneNumber2 () {
		return phoneNumber2;
	}

	/**
	 * Set the value related to the column: PhoneNumber2
	 * @param phoneNumber2 the PhoneNumber2 value
	 */
	public void setPhoneNumber2 (java.lang.Long phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}



	/**
	 * Return the value associated with the column: MobileNumber
	 */
	public java.lang.String getMobileNumber () {
		return mobileNumber;
	}

	/**
	 * Set the value related to the column: MobileNumber
	 * @param mobileNumber the MobileNumber value
	 */
	public void setMobileNumber (java.lang.String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}



	/**
	 * Return the value associated with the column: MobileNumber1
	 */
	public java.lang.String getMobileNumber1 () {
		return mobileNumber1;
	}

	/**
	 * Set the value related to the column: MobileNumber1
	 * @param mobileNumber1 the MobileNumber1 value
	 */
	public void setMobileNumber1 (java.lang.String mobileNumber1) {
		this.mobileNumber1 = mobileNumber1;
	}



	/**
	 * Return the value associated with the column: MobileNumber2
	 */
	public java.lang.String getMobileNumber2 () {
		return mobileNumber2;
	}

	/**
	 * Set the value related to the column: MobileNumber2
	 * @param mobileNumber2 the MobileNumber2 value
	 */
	public void setMobileNumber2 (java.lang.String mobileNumber2) {
		this.mobileNumber2 = mobileNumber2;
	}



	/**
	 * Return the value associated with the column: FaxAreaCode
	 */
	public java.lang.Long getFaxAreaCode () {
		return faxAreaCode;
	}

	/**
	 * Set the value related to the column: FaxAreaCode
	 * @param faxAreaCode the FaxAreaCode value
	 */
	public void setFaxAreaCode (java.lang.Long faxAreaCode) {
		this.faxAreaCode = faxAreaCode;
	}



	/**
	 * Return the value associated with the column: FaxNumber
	 */
	public java.lang.Long getFaxNumber () {
		return faxNumber;
	}

	/**
	 * Set the value related to the column: FaxNumber
	 * @param faxNumber the FaxNumber value
	 */
	public void setFaxNumber (java.lang.Long faxNumber) {
		this.faxNumber = faxNumber;
	}



	/**
	 * Return the value associated with the column: AddressID
	 */
	public com.ihexa.common.admin.user.Address getAddress () {
		return address;
	}

	/**
	 * Set the value related to the column: AddressID
	 * @param address the AddressID value
	 */
	public void setAddress (com.ihexa.common.admin.user.Address address) {
		this.address = address;
	}



	/**
	 * Return the value associated with the column: OccupationID
	 */
	public com.ihexa.common.admin.user.Occupation getOccupation () {
		return occupation;
	}

	/**
	 * Set the value related to the column: OccupationID
	 * @param occupation the OccupationID value
	 */
	public void setOccupation (com.ihexa.common.admin.user.Occupation occupation) {
		this.occupation = occupation;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.user.Person)) return false;
		else {
			com.ihexa.common.admin.user.Person person = (com.ihexa.common.admin.user.Person) obj;
			if (null == this.getId() || null == person.getId()) return false;
			else return (this.getId().equals(person.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}