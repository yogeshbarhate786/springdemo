package com.ihexa.common.admin.gift.action;

import com.ihexa.common.admin.gift.Gift;
import com.ihexa.common.admin.gift.service.GiftService;
import com.ihexa.common.admin.iHexaConstants.IHexaConstants;

import com.prounify.framework.base.action.AutoComplete;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.context.Context;

import org.apache.commons.lang.StringUtils;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import org.springframework.validation.DataBinder;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class GiftAction extends BaseCRUDAction {
    /* (non-Javadoc)
    * @see com.prounify.framework.base.action.BaseCRUDAction#isHttpMethodAllowed(java.lang.String, java.lang.String)
    */
    @Override
    protected boolean isHttpMethodAllowed(String httpMethod, String methodId) {
        if (httpMethod.equals(HTTP_METHOD_GET)) {
            if (methodId.equals("changeStatus")) {
                return true;
            }
        }

        return super.isHttpMethodAllowed(httpMethod, methodId);
    }

    @SuppressWarnings("unchecked")
    public Object doAdd(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        try {
            super.doAdd(mapping, form, request, response);

            return "ADD_PAGE";
        } catch (Exception e) {
            return "GO_TO_LIST";
        }
    }

    @Override
    public Object doCreate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        GiftService giftService = (GiftService) Context.getInstance().getBean("GiftService");
        DetachedCriteria criteria = DetachedCriteria.forClass(Gift.class);
        ActionMessages messages = new ActionMessages();
        messages.clear();

        ActionMessage msg = null;

        try {
            Gift gift = (Gift) getBean(mapping.getBaseName());
            DataBinder binder = getBinder(gift, mapping, request);

            gift.setStatus(IHexaConstants.STATUS_ACTIVE);

            giftService.add(gift, null);

            msg = new ActionMessage("gift.SAVE_SUCCESS");
            messages.add("success", msg);
            saveMessages(request, messages);

            request.setAttribute("valueList", giftService.findAll(criteria));

            return SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            msg = new ActionMessage("gift.SAVE_FAIL", e.getCause());
            messages.add("error", msg);
            saveMessages(request, messages);

            return ERROR;
        }
    }

    public Object doView(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        GiftService giftService = (GiftService) Context.getInstance().getBean("GiftService");
        String giftID = request.getParameter("giftID");
        Gift gift = new Gift();

        try {
            if (StringUtils.isNotBlank(giftID)) {
                gift = (Gift) giftService.findById(new Long(giftID));
            }

            request.setAttribute("giftData", gift);

            return "DETAIL_PAGE";
        } catch (Exception e) {
            return "GO_TO_LIST";
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object doEdit(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        GiftService giftService = (GiftService) Context.getInstance().getBean("GiftService");
        String id = request.getParameter("id");
        Gift gift = new Gift();

        try {
            if (StringUtils.isNotBlank(id)) {
                gift = (Gift) giftService.findById(new Long(id));
            }

            request.setAttribute("giftData", gift);

            return "EDIT_PAGE";
        } catch (Exception e) {
            return "GO_TO_LIST";
        }
    }

    /**
     * to update Gift
     *
     * @param
     * @return
     */
    @Override
    public Object doUpdate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        GiftService giftService = (GiftService) Context.getInstance().getBean("GiftService");
        DetachedCriteria criteria = DetachedCriteria.forClass(Gift.class);
        ActionMessages messages = new ActionMessages();
        messages.clear();

        ActionMessage msg = null;

        try {
            String id = request.getParameter("id");

            if (StringUtils.isNotBlank(id)) {
                Gift gift = (Gift) giftService.findById(new Long(id));

                DataBinder binder = getBinder(gift, mapping, request);

                giftService.update(gift, binder.getBindingResult());
            }

            msg = new ActionMessage("gift.UPDATE_SUCCESS");
            messages.add("success", msg);
            saveMessages(request, messages);

            request.setAttribute("valueList", giftService.findAll(criteria));

            return "SUCCESS";
        } catch (Exception e) {
            e.printStackTrace();
            msg = new ActionMessage("gift.UPDATE_FAIL", e.getCause());
            messages.add("error", msg);
            saveMessages(request, messages);

            return "GO_TO_LIST";
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object doList(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        GiftService giftService = (GiftService) Context.getInstance().getBean("GiftService");
        DetachedCriteria criteria = DetachedCriteria.forClass(Gift.class);

        String gift = request.getParameter("gift");

        try {
            if (StringUtils.isNotBlank(gift)) {
                criteria.add(Restrictions.like(Gift.PROP_GIFT_NAME,
                        ((gift != null) ? gift : "") + "%"));
                request.setAttribute("gift", gift);
            }

            request.setAttribute("valueList", giftService.findAll(criteria));

            if ("true".equals(request.getParameter("ajax"))) {
                return RETURN_SUCCESS_AJAX;
            } else {
                return RETURN_SUCCESS;
            }
        } catch (Exception e) {
            e.printStackTrace();

            return RETURN_ERROR;
        }
    }

    /**
     * to delete Gift
     *
     * @param
     * @return
     */
    @Override
    public Object doDelete(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        GiftService service = (GiftService) Context.getInstance().getBean("GiftService");
        String ID = request.getParameter("id");
        Gift gift = null;

        ActionMessages messages = new ActionMessages();
        messages.clear();

        ActionMessage msg = null;

        try {
            super.doDelete(mapping, form, request, response);

            msg = new ActionMessage("gift.DELETE_SUCCESS");
            messages.add("save-fail", msg);
            saveMessages(request, messages);

            return SUCCESS;
        } catch (Exception e) {
            msg = new ActionMessage("gift.DELETE_FAIL", e.getCause());
            messages.add("save-fail", msg);
            saveMessages(request, messages);

            return ERROR;
        }
    }

    public Object doChangeStatus(BaseActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        GiftService service = (GiftService) Context.getInstance().getBean("GiftService");
        String ID = request.getParameter("id");

        ActionMessages messages = new ActionMessages();
        messages.clear();

        ActionMessage msg = null;

        try {
            if (StringUtils.isNotBlank(ID)) {
                Gift gift = (Gift) service.findById(getLongValue(ID));

                if ((gift != null) && (gift.getId() != null)) {
                    if (new Byte("1").equals(gift.getStatus())) {
                        gift.setStatus(new Byte("0"));
                    } else {
                        gift.setStatus(new Byte("1"));
                    }

                    service.update(gift, null);

                    if (new Byte("1").equals(gift.getStatus())) {
                        msg = new ActionMessage("gift.ACTIVE_SUCCESS");
                        messages.add("success", msg);
                        saveMessages(request, messages);
                    } else {
                        msg = new ActionMessage("gift.INACTIVE_SUCCESS");
                        messages.add("success", msg);
                        saveMessages(request, messages);
                    }
                }
            }

            return SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            msg = new ActionMessage("gift.CHANGE_STATUS_FAIL", e.getCause());
            messages.add("save-fail", msg);
            saveMessages(request, messages);

            return ERROR;
        }
    }

    public Object doGetGiftName(BaseActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        List<Gift> giftList = new ArrayList<Gift>();
        String gift = request.getParameter("gift");

        if (StringUtils.isNotBlank(gift)) {
            GiftService service = (GiftService) Context.getInstance().getBean("GiftService");

            DetachedCriteria criteria = DetachedCriteria.forClass(Gift.class);
            criteria.add(Restrictions.like(Gift.PROP_GIFT_NAME, ((gift != null) ? gift : "") + "%"));

            giftList = service.findAll(criteria);
        }

        AutoComplete autoComplete = new AutoComplete();

        for (Gift giftobj : giftList) {
            autoComplete.addItem(giftobj.getGiftName().toString(), giftobj.getGiftName().toString());
        }

        return autoComplete.doAjax(request, response);
    }
    
    public Object doGetGiftName1(BaseActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
            List<Gift> giftList = new ArrayList<Gift>();
            String gift = request.getParameter("gift");

            if (StringUtils.isNotBlank(gift)) {
                GiftService service = (GiftService) Context.getInstance().getBean("GiftService");

                DetachedCriteria criteria = DetachedCriteria.forClass(Gift.class);
                criteria.add(Restrictions.like(Gift.PROP_GIFT_NAME, gift+"%"));
               // criteria.add(Restrictions.like(Gift.PROP_GIFT_NAME, ((gift != null) ? gift : "") + "%"));

                giftList = service.findAll(criteria);
            }
           AutoComplete autoComplete = new AutoComplete();

            
            for (Gift giftobj : giftList) {
                autoComplete.addItem(giftobj.getGiftName().toString(), giftobj.getGiftName().toString());
            }

            return autoComplete.doAjax(request, response);
        }
    

    private Long getLongValue(String parameter) {
        try {
            if ((parameter != null) && !"".equals(parameter)) {
                return Long.parseLong(parameter);
            }
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

        return null;
    }
}
