package com.ihexa.common.admin.productdetails;

import com.ihexa.common.admin.productdetails.base.BaseReview;



public class Review extends BaseReview {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Review () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Review (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}