package com.ihexa.common.admin.utility;

import java.util.ArrayList;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

public class SendMail {

	final static String userName = "ssplpune33@gmail.com";
	final static String password = "Solutionsline";

	public static String sendEmail(String email, String subject, String content, ArrayList<String> cc) {
		Properties properties = new Properties();
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.auth", "true");

		Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, password);
			}
		});

		try {
			Message message = new MimeMessage(session);

			message.setFrom(new InternetAddress(userName));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject(subject);

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Now set the actual message
			messageBodyPart.setContent(content, "text/html");

			// Create a multipart message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);
			// Part two is attachment
			messageBodyPart = new MimeBodyPart();
			String filename = System.getProperty("catalina.base") + java.io.File.separator + "webapps"
					+ java.io.File.separator + "Files" + java.io.File.separator + "logo" + java.io.File.separator
					+ "logo.jpg";
			DataSource source = new FileDataSource(filename);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(filename);
			multipart.addBodyPart(messageBodyPart);

			// Send the complete message parts
			message.setContent(multipart);

			for (String string : cc) {
				message.addRecipient(RecipientType.CC, new InternetAddress(string));
			}
			Transport.send(message);
			return "success";
		} catch (MessagingException messageException) {
			messageException.printStackTrace();
			return "failed";
		}

	}

	public static String getResultOfMail(String email, String subject, String content) {

		Properties properties = new Properties();
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.auth", "true");

		Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, password);
			}
		});

		try {
			Message message = new MimeMessage(session);

			message.setFrom(new InternetAddress(userName));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject(subject);

			// Now set the actual message
			message.setContent(content, "text/html");

			Transport.send(message);
			return "success";
		} catch (MessagingException messageException) {
			messageException.printStackTrace();
			return "failed";
		}

	}

	public static String sendEmailAttachment(String email, String subject, String content,
			ArrayList<String> attachments, ArrayList<String> cc) {
		Properties properties = new Properties();
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.auth", "true");

		Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, password);
			}
		});

		try {
			Message message = new MimeMessage(session);

			message.setFrom(new InternetAddress("rtmsspl@gmail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject(subject);

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Now set the actual message
			messageBodyPart.setText(content);

			// Create a multipart message
			Multipart multipart = new MimeMultipart();

			for (String string : attachments) {
				// Set text message part
				multipart.addBodyPart(messageBodyPart);
				// Part two is attachment
				messageBodyPart = new MimeBodyPart();
				String filename = System.getProperty("catalina.base") + java.io.File.separator + "webapps" + string;
				DataSource source = new FileDataSource(filename);
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName(filename);
				multipart.addBodyPart(messageBodyPart);

			}

			// Set the complete message parts
			message.setContent(multipart);

			// message.addRecipient(RecipientType.BCC, new
			// InternetAddress("realtor.sspl2@gmail.com"));
			for (String string : cc) {
				message.addRecipient(RecipientType.CC, new InternetAddress(string));
			}

			// Send message
			Transport.send(message);
			return "success";
		} catch (MessagingException messageException) {
			return "failed";
		}
	}
	
	public static class SendSimpleMail {

		public  static String sendSimpleEmail(String emailID, String content, String subject) {
			final String username = "ssplpune33@gmail.com";
			final String password = "Solutionsline";

			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");

			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});

			try {
				Message message = new MimeMessage(session);

				message.setFrom(new InternetAddress(username));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailID));
				message.setSubject(subject);
				message.setContent(content, "text/html; charset=utf-8");

				Transport.send(message);

				return "success";
			} catch (MessagingException e) {
				e.printStackTrace();
				return "failed";
			}
		}
	}

}