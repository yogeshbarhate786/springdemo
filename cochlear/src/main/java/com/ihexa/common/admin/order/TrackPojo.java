package com.ihexa.common.admin.order;

import java.util.List;

import com.ihexa.common.admin.cart.Cart;

public class TrackPojo
{
	private String orderid;
	private String status;
	private List<Cart> list;
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Cart> getList() {
		return list;
	}
	public void setList(List<Cart> list) {
		this.list = list;
	}

}
