package com.ihexa.common.admin.dashboard.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the pricelist table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="pricelist"
 */

public abstract class BasePricelist  implements Serializable {

	public static String REF = "Pricelist";
	public static String PROP_PRICE = "Price";
	public static String PROP_ID = "Id";


	// constructors
	public BasePricelist () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BasePricelist (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.Integer price;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="id"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: price
	 */
	public java.lang.Integer getPrice () {
		return price;
	}

	/**
	 * Set the value related to the column: price
	 * @param price the price value
	 */
	public void setPrice (java.lang.Integer price) {
		this.price = price;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.dashboard.Pricelist)) return false;
		else {
			com.ihexa.common.admin.dashboard.Pricelist pricelist = (com.ihexa.common.admin.dashboard.Pricelist) obj;
			if (null == this.getId() || null == pricelist.getId()) return false;
			else return (this.getId().equals(pricelist.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}