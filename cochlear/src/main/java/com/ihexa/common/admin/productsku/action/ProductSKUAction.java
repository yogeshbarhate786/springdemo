package com.ihexa.common.admin.productsku.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.MultipartRequestWrapper;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.hql.ast.tree.RestrictableStatement;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ihexa.common.admin.gift.service.GiftService;
import com.ihexa.common.admin.productsku.Category;
import com.ihexa.common.admin.productsku.Maincategory;
import com.ihexa.common.admin.productsku.Product;
import com.ihexa.common.admin.productsku.Producthistroy;
import com.ihexa.common.admin.productsku.Subcategory;
import com.ihexa.common.admin.utility.FileUpload;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.context.Context;

public class ProductSKUAction extends BaseCRUDAction {
	
	GiftService service = (GiftService) Context.getInstance().getBean("GiftService");
	
	@Override
	public Object doAdd(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		try {
			DetachedCriteria criteria = DetachedCriteria.forClass(Maincategory.class);
			List<Maincategory> maincategorylist = service.findAll(criteria);
			request.setAttribute("maincategorylist", maincategorylist);
			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}
	}
	
	@Override
	public Object doCreate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		try {
			String title = request.getParameter("title");
			String details = request.getParameter("details");
			String price = request.getParameter("price");
			String warranty = request.getParameter("warranty");
           String maincatid = request.getParameter("maincategoryid");
			String catid = request.getParameter("categoryid");
			String subcatid = request.getParameter("subcategoryid");
			String path = null;
			if (request instanceof MultipartRequestWrapper) {
				MultipartRequestWrapper multipart = (MultipartRequestWrapper) request;
				MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) multipart.getRequest();
				Map files = multiRequest.getFileMap();
				for (Iterator iterator = files.values().iterator(); iterator.hasNext();) {
					MultipartFile multipartFile = (MultipartFile) iterator.next();
					if (!(multipartFile.isEmpty())) {
						path = FileUpload.uploadAttachment(multipartFile);
						String[] originalFileName = multipartFile.getOriginalFilename().split("\\.");
						String extension = originalFileName[originalFileName.length - 1];
					} else {
					}
				}
				Product product = new Product();
				if(StringUtils.isNotBlank(title)) {
					product.setName(title);
				} else {
					product.setName(null);
				}
				
				if (StringUtils.isNotBlank(details)) {
					product.setDetails(details);
				} else {
					product.setDetails(null);
				}
				
				if(StringUtils.isNotBlank(price))
				{
					product.setPrice(Integer.valueOf(price));
				}
				else
				{
					product.setPrice(null);
				}
				
				if(StringUtils.isNotBlank(warranty))
				{
					product.setWarranty(warranty+"year");
				}
				else
				{
					product.setWarranty(null);
				}
				
				if(StringUtils.isNotBlank(maincatid))
				{
					product.setMaincatid(new Maincategory(new Long(maincatid)));
				}
				else
				{
					product.setMaincatid(null);
				}
				
				if(StringUtils.isNotBlank(catid))
				{
					product.setCatid(new Category(new Long(catid)));
				}
				else
				{
					product.setCatid(null);
				}
				
				if(StringUtils.isNotBlank(subcatid))
				{
					product.setSubcatid(new Subcategory(new Long(subcatid)));
				}
				else
				{
					product.setSubcatid(null);
				}
				
				product.setVersion(new BigDecimal(1));
			    product.setPicurl(path);
				product.setStatus("Active");
				
				product.setCreateddate(new Date());
				
				
				service.add(product, null);
				
				Producthistroy prohisobj=new Producthistroy();
				prohisobj.setAddeddate(new Date());
				prohisobj.setStopdate(null);
				prohisobj.setVersion(new BigDecimal(1));
			    prohisobj.setProductprice(new BigDecimal(price));
			    prohisobj.setProducturl(path);
				prohisobj.setStatus("Active");
				
				prohisobj.setProductid(product);
				service.add(prohisobj,null);
				}
			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}
	}
	
	@Override
	public Object doEdit(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			
			String productid = request.getParameter("productid");
			DetachedCriteria criteriaobj = DetachedCriteria.forClass(Product.class);
			criteriaobj.add(Restrictions.eq(Product.PROP_ID, new Long(productid)));
			List<Product> listobj = service.findAll(criteriaobj);
			request.setAttribute("ProductEditList", listobj.get(0));
			
			
			
			DetachedCriteria criteria = DetachedCriteria.forClass(Maincategory.class);
			List<Maincategory> maincategorylist = service.findAll(criteria);
			request.setAttribute("maincategorylist", maincategorylist);
			
			DetachedCriteria criteria2 = DetachedCriteria.forClass(Category.class);
			criteria2.add(Restrictions.eq(Category.PROP_MAINCATEGORYID+"."+Maincategory.PROP_ID, listobj.get(0).getMaincatid().getId()));
			List<Category> categorylist = service.findAll(criteria2);
			request.setAttribute("categorylist", categorylist);
			
			DetachedCriteria criteria1 = DetachedCriteria.forClass(Subcategory.class);
			criteria1.add(Restrictions.eq(Subcategory.PROP_CATEGORYID+"."+Category.PROP_ID, listobj.get(0).getCatid().getId()));
			List<Subcategory> subcategorylist = service.findAll(criteria1);
			request.setAttribute("subcategorylist", subcategorylist);
			
			
			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}
	}

	@Override
	public Object doUpdate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			
			String productid = request.getParameter("productid");
			DetachedCriteria criteriaobj = DetachedCriteria.forClass(Product.class);
			criteriaobj.add(Restrictions.eq(Product.PROP_ID, new Long(productid)));
			List<Product> listobj = service.findAll(criteriaobj);

			for (Product productlist : listobj) {

				String title = request.getParameter("title");
				String details = request.getParameter("details");
				String price = request.getParameter("price");
				String warranty = request.getParameter("warranty");
				String version = request.getParameter("version");
				String maincatid = request.getParameter("maincategoryid");
				String catid = request.getParameter("categoryid");
				String subcatid = request.getParameter("subcategoryid");
				String path = null;
				if (request instanceof MultipartRequestWrapper) {
					MultipartRequestWrapper multipart = (MultipartRequestWrapper) request;
					MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) multipart.getRequest();
					Map files = multiRequest.getFileMap();
					for (Iterator iterator = files.values().iterator(); iterator.hasNext();) {
						MultipartFile multipartFile = (MultipartFile) iterator.next();
						if (!(multipartFile.isEmpty())) {
							path = FileUpload.uploadAttachment(multipartFile);
							String[] originalFileName = multipartFile.getOriginalFilename().split("\\.");
							String extension = originalFileName[originalFileName.length - 1];
						} else {
						}
					}
					
				}
				
				if(StringUtils.isNotBlank(title))
				{
					productlist.setName(title);
				}
				else
				{
					productlist.setName(null);
				}
				
				if(StringUtils.isNotBlank(details))
				{
					productlist.setDetails(details.trim());
				}
				else
				{
					productlist.setDetails(null);
				}
				
				if(StringUtils.isNotBlank(price))
				{
					productlist.setPrice(Integer.valueOf(price));
				}
				else
				{
					productlist.setPrice(null);
				}
				if(StringUtils.isNotBlank(version))
				{
					productlist.setVersion(new BigDecimal(version));
				}
				else
				{
					productlist.setVersion(new BigDecimal(1));
				}
				
				if(StringUtils.isNotBlank(warranty))
				{
					productlist.setWarranty(warranty);
				}
				else
				{
					productlist.setWarranty(null);
				}
				
				if(StringUtils.isNotBlank(maincatid))
				{
					productlist.setMaincatid(new Maincategory(new Long(maincatid)));
				}
				else
				{
					productlist.setMaincatid(null);
				}
				
				if(StringUtils.isNotBlank(catid))
				{
					productlist.setCatid(new Category(new Long(catid)));
				}
				else
				{
					productlist.setCatid(null);
				}
				
				if(StringUtils.isNotBlank(subcatid))
				{
					productlist.setSubcatid(new Subcategory(new Long(subcatid)));
				}
				else
				{
					productlist.setSubcatid(null);
				}
				
				if (StringUtils.isNotBlank(path)) {
					productlist.setPicurl(path);
				} else {
					// not set null. if we set null then previous image path update by null in DB
				}
				
				productlist.setStatus("Active");
				productlist.setUpdateddate(new Date());
				service.add(productlist, null);
				

				DetachedCriteria criteriaobj1 = DetachedCriteria.forClass(Producthistroy.class);
			    criteriaobj1.add(Restrictions.eq(Producthistroy.PROP_PRODUCTID+"."+	Product.PROP_ID,new Long(productlist.getId())));
			    criteriaobj1.add(Restrictions.isNull(Producthistroy.PROP_STOPDATE));
			   /* criteriaobj1.setProjection(Projections.max(Producthistroy.PROP_ADDEDDATE));*/
				List<Producthistroy> listobj1 = service.findAll(criteriaobj1);
				
				for (Producthistroy producthistroy : listobj1) 
				{
					producthistroy.setStopdate(new Date());
					service.add(producthistroy, null);
				}
				
				Producthistroy productobj=new Producthistroy();
				productobj.setAddeddate(new Date());
				if (StringUtils.isNotBlank(version)) {
					productobj.setVersion(new BigDecimal(version));
				}
				else
				{
					productobj.setVersion(new BigDecimal(1));	
				}
				productobj.setStatus("Active");
				productobj.setStopdate(null);
				productobj.setProductprice(new BigDecimal(price));
				productobj.setProducturl(path);
				productobj.setProductid(new Product(productlist.getId()));
				service.add(productobj, null); 
			}
		 return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}
	}
	

	@Override
	public Object doList(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		try {
			
			DetachedCriteria criteriaobj = DetachedCriteria.forClass(Product.class);
			criteriaobj.add(Restrictions.eq(Product.PROP_STATUS, "Active"));
			List<Product> listobj = service.findAll(criteriaobj);

			request.setAttribute("ProductList", listobj);

			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
		
	}
	
	@Override
	public Object doDelete(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			String productid = request.getParameter("productid");
			DetachedCriteria criteriaobj = DetachedCriteria.forClass(Product.class);
			criteriaobj.add(Restrictions.eq(Product.PROP_ID, new Long(productid)));
			List<Product> listobj = service.findAll(criteriaobj);

			for (Product productlist : listobj) {

				productlist.setUpdateddate(new Date());
				productlist.setStatus("InActive");

				service.add(productlist, null);

			}

			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}



	public List<Category> getCategory(String maincategoryid)
	{
		List<Category>categorylist=new ArrayList<Category>();
		try{
			if (StringUtils.isNotBlank(maincategoryid) && GenericValidator.isLong(maincategoryid)) {
				GiftService service = (GiftService) Context.getInstance().getBean("GiftService");
				DetachedCriteria criteria=DetachedCriteria.forClass(Category.class);
				criteria.add(Restrictions.eq(Category.PROP_MAINCATEGORYID+"."+Maincategory.PROP_ID, new Long(maincategoryid)));
				categorylist = service.findAll(criteria);
			}			
		}catch(Exception e)
		{
			e.printStackTrace();
		}		
		return categorylist;
	}
	
	
	
	public List<Subcategory> getSubCategory(String categoryid)
	{
		List<Subcategory>subcategorylist=new ArrayList<Subcategory>();
		try{
			if (StringUtils.isNotBlank(categoryid) && GenericValidator.isLong(categoryid)) {
				GiftService service = (GiftService) Context.getInstance().getBean("GiftService");
				DetachedCriteria criteria=DetachedCriteria.forClass(Subcategory.class);
				criteria.add(Restrictions.eq(Subcategory.PROP_CATEGORYID+"."+Category.PROP_ID, new Long(categoryid)));
				subcategorylist = service.findAll(criteria);
			}			
		}catch(Exception e)
		{
			e.printStackTrace();
		}		
		return subcategorylist;
	}
}
