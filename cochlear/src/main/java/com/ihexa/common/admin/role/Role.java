package com.ihexa.common.admin.role;

import com.ihexa.common.admin.role.base.BaseRole;



public class Role extends BaseRole {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Role () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Role (java.lang.Long id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public Role (
		java.lang.Long id,
		java.lang.String roleName) {

		super (
			id,
			roleName);
	}

/*[CONSTRUCTOR MARKER END]*/


}