package com.ihexa.common.admin.order;

import java.util.Date;
import java.util.List;

import com.ihexa.common.admin.cart.Cart;

public class MyorderPojo {
	private String orderid;
	private Date addeddate;
	private List<TrackPojo> cart;
	private String status;
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public Date getAddeddate() {
		return addeddate;
	}
	public void setAddeddate(Date addeddate) {
		this.addeddate = addeddate;
	}
	public List<TrackPojo> getCart() {
		return cart;
	}
	public void setCart(List<TrackPojo> cart) {
		this.cart = cart;
	}
	
}
