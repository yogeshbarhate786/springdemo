package com.ihexa.common.admin.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import jxl.CellView;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.struts.upload.FormFile;


/**
 * @author
 */
public abstract class ExcelUtil {
    /**
     * This method is internally used by <br>
     * class methods to generate unique <br>
     * file name by appending timeStamp
     *
     * @param fileName
     * @param timeStamp
     * @return
     */

    // ///////////DbToExcel
    protected WritableCellFormat timesBoldUnderline;
    protected WritableCellFormat times;
    protected String inputFile;
    public int rowCount;
    

    // ////////////
    private String getNewName(String fileName, String timeStamp) {
        // fileName = fileName.toLowerCase();
        if (fileName.endsWith(".doc")) {
            return fileName.replace(".doc", "_" + timeStamp + ".doc");
        } else if (fileName.endsWith(".dot")) {
            return fileName.replace(".dot", "_" + timeStamp + ".doc");
        } else if (fileName.endsWith(".txt")) {
            return fileName.replace(".txt", "_" + timeStamp + ".txt");
        } else if (fileName.endsWith(".xlsx")) {
            return fileName.replace(".xlsx", "_" + timeStamp + ".xls");
        } else if (fileName.endsWith(".xl")) {
            return fileName.replace(".xl", "_" + timeStamp + ".xls");
        } else if (fileName.endsWith(".xls")) {
            return fileName.replace(".xls", "_" + timeStamp + ".xls");
        } else if (fileName.endsWith(".xlv")) {
            return fileName.replace(".xlv", "_" + timeStamp + ".xls");
        } else if (fileName.endsWith(".xla")) {
            return fileName.replace(".xla", "_" + timeStamp + ".xls");
        } else if (fileName.endsWith(".xlb")) {
            return fileName.replace(".xlb", "_" + timeStamp + ".xls");
        } else if (fileName.endsWith(".xlt")) {
            return fileName.replace(".xlt", "_" + timeStamp + ".xls");
        } else if (fileName.endsWith(".xlm")) {
            return fileName.replace(".xlm", "_" + timeStamp + ".xls");
        } else if (fileName.endsWith(".xlk")) {
            return fileName.replace(".xlk", "_" + timeStamp + ".xls");
        } else if (fileName.endsWith(".xml")) {
            return fileName.replace(".xml", "_" + timeStamp + ".xml");
        } else if (fileName.endsWith(".svg")) {
            return fileName.replace(".svg", "_" + timeStamp + ".svg");
        } else if (fileName.endsWith(".mp3")) {
            return fileName.replace(".mp3", "_" + timeStamp + ".mp3");
        } else {
            return fileName + "_" + timeStamp + ".xls";
        }
    }

    public String saveToDirectory(FormFile formFile, final String rootDirectory)
        throws FileNotFoundException, IOException {
        String newName = null;
        String fileLocation = null;

        newName = formFile.getFileName();

        if (StringUtils.isNotBlank(newName)) {
            newName = newName.replaceAll(" ", "_");
        }

        if (StringUtils.isNotBlank(rootDirectory)) {
            new File(rootDirectory).mkdirs();
            fileLocation = rootDirectory + "/" + newName;
        }

        newName = newName.replaceAll("\\u005c", "-");
        newName = newName.replaceAll("\\?", "-");
        newName = newName.replaceAll("\\:", "-");
        newName = newName.replaceAll("\\|", "-");
        newName = newName.replaceAll("\\<", "-");
        newName = newName.replaceAll("\\>", "-");
        newName = newName.replaceAll("\\*", "-");
        newName = newName.replaceAll("/", "-");

        File file = new File(fileLocation);
        FileOutputStream output = new FileOutputStream(file);
        output.write(formFile.getFileData());
        output.close();

        return newName;
    }

    public void loadFromDirectory(final String rootDirectory, final String fileName,
        final int columnCount, final boolean isHeaders, final boolean isBlankRow)
        throws Exception {
        String fileLocation = null;

        if (StringUtils.isNotBlank(rootDirectory)) {
            new File(rootDirectory).mkdirs();
            fileLocation = rootDirectory + "/" + fileName;
        } else {
            fileLocation = "/" + fileName;
        }

        POIFSFileSystem fileSystem = new POIFSFileSystem(new FileInputStream(fileLocation));
        HSSFWorkbook workBook = new HSSFWorkbook(fileSystem);
        HSSFSheet sheet = workBook.getSheetAt(0);
        Iterator<HSSFRow> rows = sheet.rowIterator();

        // No need to read headers
        if (isHeaders) {
            if (rows.hasNext()) {
                rows.next();
            }
        }

        // No need to read blank line after headers
        if (isBlankRow) {
            if (rows.hasNext()) {
                rows.next();
            }
        }

        while (rows.hasNext()) {
            String[] COLUMN_NAMES = new String[columnCount];

            HSSFRow row = rows.next();
            setRowCount(row.getRowNum());

            // once get a row its time to iterate through cells.
            Iterator<HSSFCell> cells = row.cellIterator();
            int innerVarColumnCount = 0;

            while (cells.hasNext() && (innerVarColumnCount < columnCount)) {
                HSSFCell cell = cells.next();

                /*
                 * Now we will get the cell type and use the values accordingly.
                 */
                switch (cell.getCellType()) {
                case HSSFCell.CELL_TYPE_NUMERIC: {
                    if (HSSFDateUtil.isCellDateFormatted(cell)) {
                        COLUMN_NAMES[innerVarColumnCount] = "" + cell.getDateCellValue();
                    } else {
                        // cell type numeric.
                        COLUMN_NAMES[innerVarColumnCount] = "" + cell.getNumericCellValue();
                        COLUMN_NAMES[innerVarColumnCount] = formatnumber(COLUMN_NAMES[innerVarColumnCount]);
                    }

                    break;
                }

                case HSSFCell.CELL_TYPE_STRING: {
                    // cell type string.
                    COLUMN_NAMES[innerVarColumnCount] = cell.getStringCellValue();

                    break;
                }

                case HSSFCell.CELL_TYPE_BLANK: {
                    // cell type string.
                    COLUMN_NAMES[innerVarColumnCount] = cell.getStringCellValue();

                    break;
                }

                case HSSFCell.CELL_TYPE_FORMULA: {
                    // cell type string.
                    COLUMN_NAMES[innerVarColumnCount] = cell.getCellFormula();

                    break;
                }

                default: {
                    // types other than String and Numeric.
                    COLUMN_NAMES[innerVarColumnCount] = "";

                    break;
                }
                }

                innerVarColumnCount++;

                if (innerVarColumnCount > columnCount) {
                    break;
                }
            }

            for (int index = 0; index < COLUMN_NAMES.length; index++) {
                if (StringUtils.isNotBlank(COLUMN_NAMES[index])) {
                    COLUMN_NAMES[index] = COLUMN_NAMES[index].replace("'", "\\'");
                    COLUMN_NAMES[index] = COLUMN_NAMES[index].trim();
                    COLUMN_NAMES[index] = formatImportedString(COLUMN_NAMES[index]);
                }
            }

            // Here you can use 'COLUMN_NAMES' array. 'COLUMN_NAMES' array contains data in one row.
            useRowData(COLUMN_NAMES);
        }
       
    }

    /**
     * User should implement this abstract method <br>
     * from FileHandlingUtil.java. <br>
     * User can process each row data in this method. <br>
     * Row data is in String Array. <br>
     * Length of String Array is equal to number of <br>
     * column's user want to read.
     *
     * @param rowData
     */
    public abstract void useRowData(String[] rowData) throws Exception;

    /**
     * Used to remove scientific conversion and decimal <br>
     * zeros from number string.
     *
     * @param number
     * @return String
     * @throws NumberFormatException
     */
    private String formatnumber(String number) throws NumberFormatException {
        String returnNumber = "";

        int dotCount = -1;

        if (number != null) {
            number = number.trim().replace(" ", "");
        } else {
            return null;
        }

        for (int count = 0; count < number.length(); count++) {
            if (((number.charAt(count) <= 57) && (number.charAt(count) >= 48)) ||
                    (number.charAt(count) == '/')) {
                returnNumber = returnNumber + number.charAt(count);
            } else if (number.charAt(count) == '.') {
                if (number.length() == (count + 2)) {
                    break;
                }

                dotCount = count;
            } else if (number.charAt(count) == 'E') {
                if (!"".equals(number.charAt(count + 1))) {
                    Integer expo = new Integer("" + number.charAt(count + 1));

                    // System.out.println("expo:" + expo);
                    String temp = null;

                    if (dotCount != -1) {
                        temp = number.subSequence(dotCount + 1, count).toString();
                    }

                    if (temp != null) {
                        for (int cnt = temp.length(); cnt < expo; cnt++)
                            returnNumber = returnNumber + "0";
                    }

                    count++;
                }
            }
        }

        return returnNumber;
    }

    /**
     * Used to remove special charater
     *
     * @param importedString
     * @return String
     */
    private String formatImportedString(String importedString) {
        importedString = importedString.trim();
        importedString = importedString.replaceAll("\\u00C2", "");
        importedString = importedString.replaceAll("�", "");

        StringBuffer outputString = new StringBuffer(importedString);
        boolean startFlag = true;
        boolean endFlag = true;

        while (startFlag || endFlag) {
            if (((outputString.charAt(0) <= 57) && (outputString.charAt(0) >= 48)) ||
                    ((outputString.charAt(0) <= 90) && (outputString.charAt(0) >= 65)) ||
                    ((outputString.charAt(0) <= 122) && (outputString.charAt(0) >= 97)) ||
                    (outputString.charAt(0) == 46)||(outputString.charAt(0) == 36)){
                startFlag = false;
            } else {
                outputString = outputString.deleteCharAt(0);
            }

            int end = outputString.length() - 1;

            if (((outputString.charAt(end) <= 57) && (outputString.charAt(end) >= 48)) ||
                    ((outputString.charAt(end) <= 90) && (outputString.charAt(end) >= 65)) ||
                    ((outputString.charAt(end) <= 122) && (outputString.charAt(end) >= 97)) ||
                    (outputString.charAt(end) == 46)||(outputString.charAt(end) == 36)){
                endFlag = false;
            } else {
                outputString = outputString.deleteCharAt(end);
            }
        }

        return outputString.toString();
    }

    /*
     *
     */

    // /////////////DbToExcel
    public void setOutputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    public abstract void write(String sheetName, String[] headerList)
        throws IOException, WriteException;

    public void createLabel(WritableSheet sheet, String[] headerList)
        throws WriteException {
        // create a times font
        WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
        // define the cell format
        times = new WritableCellFormat(times10pt);
        // automatically wrap the cells
        times.setWrap(true);

        // create a bold font with unterlines
        WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.TIMES, 10,
                WritableFont.BOLD, false, UnderlineStyle.SINGLE);
        timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
        // automatically wrap the cells

        // timesBoldUnderline.setBackground(c);
        timesBoldUnderline.setWrap(true);

        CellView cv = new CellView();
        cv.setFormat(times);
        cv.setFormat(timesBoldUnderline);

        // cv.setAutosize(true);

        // add column header
        for (int ctr = 0; ctr < headerList.length; ctr++) {
            // System.out.println(headerList[ctr]);
            addCaption(sheet, ctr, 0, headerList[ctr]);
        }
    }

    public abstract void createContent(WritableSheet sheet)
        throws WriteException, RowsExceededException;

    private void addCaption(WritableSheet sheet, int column, int row, String s)
        throws RowsExceededException, WriteException {
        Label label;
        label = new Label(column, row, s, timesBoldUnderline);
        sheet.addCell(label);
    }

    public void addLabel(WritableSheet sheet, int column, int row, String s)
        throws WriteException, RowsExceededException {
        Label label;
        label = new Label(column, row, s, times);
        sheet.addCell(label);

        // System.out.println("column: "+column+" row: "+row+" Value: "+s);
    }



	/**
	 * @return the rowCount
	 */
	public int getRowCount() {
		return rowCount;
	}

	/**
	 * @param rowCount the rowCount to set
	 */
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

}
