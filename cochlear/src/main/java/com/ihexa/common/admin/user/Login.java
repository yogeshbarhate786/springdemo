package com.ihexa.common.admin.user;

import com.ihexa.common.admin.user.base.BaseLogin;

public class Login extends BaseLogin {
	private static final long serialVersionUID = 1L;

	/* [CONSTRUCTOR MARKER BEGIN] */
	public Login() {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Login(java.lang.Long id) {
		super(id);
	}

	public Login(com.ihexa.common.admin.user.User user) {
		setUser(user);
	}
	/* [CONSTRUCTOR MARKER END] */

}