package com.ihexa.common.admin.scheduler;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.ee.servlet.QuartzInitializerServlet;
import org.quartz.impl.StdSchedulerFactory;

import com.ihexa.common.admin.iHexaConstants.IHexaConstants;

public class IHexaScheduler extends QuartzInitializerServlet {

	private static final long serialVersionUID = 1L;

	public void init(ServletConfig cfg) throws ServletException {
		try {

			System.out.println("In init() metnod of scheduler 'iHexaTrigger'");
			SchedulerFactory schedulerFactory = new StdSchedulerFactory();
			Scheduler scheduler = schedulerFactory.getScheduler();

			JobDetail iHexaJobDetail = new JobDetail("iHexaJobDetail", scheduler.DEFAULT_GROUP, IHexaJob.class);
			CronTrigger iHexaTrigger = new CronTrigger("iHexaTrigger", scheduler.DEFAULT_GROUP, IHexaConstants.SCHEDULER_CRON_EXPRESSION);
			scheduler.scheduleJob(iHexaJobDetail, iHexaTrigger);
			System.out.println("'iHexaJobDetail' Added in scheduler");

			System.out.println(" Starting 'IHexaScheduler'");
			scheduler.start();
			System.out.println("'IHexaScheduler' started");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		super.init(cfg);
	}

}
