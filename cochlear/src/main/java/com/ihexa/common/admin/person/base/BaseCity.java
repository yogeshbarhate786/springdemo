package com.ihexa.common.admin.person.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the city table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="city"
 */

public abstract class BaseCity  implements Serializable {

	public static String REF = "City";
	public static String PROP_STATE = "state";
	public static String PROP_CITY_NAME = "cityName";
	public static String PROP_ID = "id";
	public static String PROP_COUNTRY = "country";


	// constructors
	public BaseCity () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseCity (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String cityName;

	// many to one
	private com.ihexa.common.admin.person.State state;
	private com.ihexa.common.admin.person.Country country;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="sequence"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: CityName
	 */
	public java.lang.String getCityName () {
		return cityName;
	}

	/**
	 * Set the value related to the column: CityName
	 * @param cityName the CityName value
	 */
	public void setCityName (java.lang.String cityName) {
		this.cityName = cityName;
	}



	/**
	 * Return the value associated with the column: StateID
	 */
	public com.ihexa.common.admin.person.State getState () {
		return state;
	}

	/**
	 * Set the value related to the column: StateID
	 * @param state the StateID value
	 */
	public void setState (com.ihexa.common.admin.person.State state) {
		this.state = state;
	}



	/**
	 * Return the value associated with the column: CountryID
	 */
	public com.ihexa.common.admin.person.Country getCountry () {
		return country;
	}

	/**
	 * Set the value related to the column: CountryID
	 * @param country the CountryID value
	 */
	public void setCountry (com.ihexa.common.admin.person.Country country) {
		this.country = country;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.person.City)) return false;
		else {
			com.ihexa.common.admin.person.City city = (com.ihexa.common.admin.person.City) obj;
			if (null == this.getId() || null == city.getId()) return false;
			else return (this.getId().equals(city.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}