package com.ihexa.common.admin.authorization;

import com.ihexa.common.admin.authorization.base.BaseAuthorization;

public class Authorization extends BaseAuthorization {
	private static final long serialVersionUID = 1L;

	private boolean checked = false;

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

}
