package com.ihexa.common.admin.company.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.ActionForm;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.ihexa.common.admin.company.Company;
import com.ihexa.common.admin.gift.service.GiftService;
import com.ihexa.common.admin.productsku.Category;
import com.ihexa.common.admin.productsku.Maincategory;
import com.ihexa.common.admin.productsku.Product;
import com.ihexa.common.admin.productsku.Subcategory;
import com.ihexa.common.admin.user.Address;
import com.ihexa.common.admin.user.City;
import com.ihexa.common.admin.user.Country;
import com.ihexa.common.admin.user.State;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.context.Context;
import com.sun.corba.se.impl.javax.rmi.CORBA.Util;

public class CompanyAction extends BaseCRUDAction 
{
	
	GiftService service = (GiftService) Context.getInstance().getBean("GiftService");

	@Override
	public Object doAdd(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
               DetachedCriteria criteriaobj=DetachedCriteria.forClass(Country.class);
               List<Country> list=service.findAll(criteriaobj);
               request.setAttribute("countrylist",list);
			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}
	}
	
	
	
	
	@Override
	public Object doCreate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		try {
			String title = request.getParameter("companyname");
			String details = request.getParameter("details");
			String phone = request.getParameter("phone");
		   String email = request.getParameter("email");
			String countryid = request.getParameter("countryid");
			String stateid = request.getParameter("stateid");
			String cityid = request.getParameter("cityid");
			
			Company company=new Company();
			
			if(title == null)
			{
				company.setCompanyName(null);
			}
			else
			{
				company.setCompanyName(title);	
			}
			if(details == null)
			{
				company.setDescription(null);
			}
			else
			{
				company.setDescription(details);
			}
			if(phone == null)
			{
				company.setPhone(null);
			}
			else
			{
				company.setPhone(Long.parseLong(phone));
			}
			if(email == null)
			{
				company.setEmail(null);
			}
			else
			{
				company.setEmail(email);
			}
			Address address=new Address();
		
			if(countryid == null)
			{
				address.setCountry(null);
			}
			else
			{
				address.setCountry(new Country(new Long(countryid)));	
			}
			if(stateid == null)
			{
				address.setState(null);
			}
			else
			{
				address.setState(new State(new Long(stateid)));
			}
			if(cityid == null)
			{
				address.setCity(null);
			}
			else
			{
				address.setCity((new City(new Long(cityid))));	
			}
			service.add(address, null);
			company.setAddressid(address);
			company.setStatus("active");
			service.add(company, null);
			
			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}
	}
	@Override
	public Object doList(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		try {
			
			DetachedCriteria criteriaobj = DetachedCriteria.forClass(Company.class);
			criteriaobj.add(Restrictions.eq(Company.PROP_STATUS, "active"));
			List<Company> listobj = service.findAll(criteriaobj);

			request.setAttribute("CompanyList", listobj);

			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
		
	}

@Override
	public Object doDelete(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception 
			{
		String id=request.getParameter("id");
		
		DetachedCriteria crobj=DetachedCriteria.forClass(Company.class);
	    crobj.add(Restrictions.eq(Company.PROP_ID, new Long(id)));
	    List<Company> list=service.findAll(crobj);
	    
	    for (Company company : list)
	    {
	    	company.setStatus("Deactive");	
	    service.add(company, null);
	   }
	return SUCCESS;
	}



@Override
public Object doEdit(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
		HttpServletResponse response) throws Exception 
	{
    String id=request.getParameter("id");
    
    if(id!=null)
    {
    
    DetachedCriteria criteria=DetachedCriteria.forClass(Company.class);
    criteria.add(Restrictions.eq(Company.PROP_ID, new Long(id)));
    List<Company> list=service.findAll(criteria);
    
    request.setAttribute("compobj", list.get(0));
    
     DetachedCriteria criteria1=DetachedCriteria.forClass(Country.class);
     List<Country> countrylist=service.findAll(criteria1);
     
     request.setAttribute("Countrylist", countrylist);
     
     Company comobj = list.get(0);
     
     if(comobj.getAddressid()!=null)
     {
    DetachedCriteria criteria2=DetachedCriteria.forClass(State.class);
     criteria2.add(Restrictions.eq(State.PROP_COUNTRY+"."+Country.PROP_ID,comobj.getAddressid().getState()
				.getCountry().getId()));
     List<State> statelist=service.findAll(criteria2);
     
     request.setAttribute("statelist",statelist);
     
     
     DetachedCriteria criteria3=DetachedCriteria.forClass(City.class);
     criteria3.add(Restrictions.eq(City.PROP_STATE+"."+State.PROP_ID,comobj.getAddressid().getCity()
				.getState().getId()));
     List<City> citylist=service.findAll(criteria3);
     
     request.setAttribute("citylist", citylist);
      }
    }
     return SUCCESS;
}




@Override
public Object doUpdate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
		HttpServletResponse response) throws Exception {
	
	String id=request.getParameter("id");
	
	String title = request.getParameter("companyname");
	String details = request.getParameter("details");
	String phone = request.getParameter("phone");
   String email = request.getParameter("email");
	
	
	DetachedCriteria criteria=DetachedCriteria.forClass(Company.class);
	criteria.add(Restrictions.eq(Company.PROP_ID,  new Long(id)));
	List<Company> list=service.findAll(criteria);

	for (Company company : list) 
	{
		
       if(title == null)
		{
			company.setCompanyName(null);
		}
		else
		{
			company.setCompanyName(title);	
		}
		if(details == null)
		{
			company.setDescription(null);
		}
		else
		{
			company.setDescription(details);
		}
		if(phone == null)
		{
			company.setPhone(null);
		}
		else
		{
			company.setPhone(Long.parseLong(phone));
		}
		if(email == null)
		{
			company.setEmail(null);
		}
		else
		{
			company.setEmail(email);
		}
		
	
		DetachedCriteria criteriaobj1 = DetachedCriteria.forClass(Address.class);
		criteriaobj1.add(Restrictions.eq(Address.PROP_ID, new Long(company.getAddressid().getId())));
		List<Address> listobj1 = service.findAll(criteriaobj1);

		for (Address address : listobj1) {
			
			String countryid = request.getParameter("countryid");
			String stateid = request.getParameter("stateid");
			String cityid = request.getParameter("cityid");

			
			address.setCountry(new Country(new Long(countryid)));
			address.setState(new State(new Long(stateid)));
			address.setCity(new City(new Long(cityid)));
			service.add(address, null);
			company.setAddressid(address);
		}
		company.setStatus("active");
		service.add(company, null);
	    }
	return SUCCESS;
}




public List<State> getState(String countryid)
	{
		List<State> statelist=new ArrayList<State>();
		try{
			if (StringUtils.isNotBlank(countryid) && GenericValidator.isLong(countryid)) {
				GiftService service = (GiftService) Context.getInstance().getBean("GiftService");
				DetachedCriteria criteria=DetachedCriteria.forClass(State.class);
				criteria.add(Restrictions.eq(State.PROP_COUNTRY+"."+Country.PROP_ID,new Long(countryid)));
				
				statelist = service.findAll(criteria);
			}			
		}catch(Exception e)
		{
			e.printStackTrace();
		}		
		return statelist;
	}
	
	
	
	public List<City> getCity(String stateid)
	{
		List<City> citylist=new ArrayList<City>();
		try{
			if (StringUtils.isNotBlank(stateid) && GenericValidator.isLong(stateid)) {
				GiftService service = (GiftService) Context.getInstance().getBean("GiftService");
				DetachedCriteria criteria=DetachedCriteria.forClass(City.class);
				criteria.add(Restrictions.eq(City.PROP_STATE+"."+State.PROP_ID, new Long(stateid)));
				citylist = service.findAll(criteria);
			}			
		}catch(Exception e)
		{
			e.printStackTrace();
		}		
		return citylist;
	}



}
