package com.ihexa.common.admin.user.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the country table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="country"
 */

public abstract class BaseCountry  implements Serializable {

	public static String REF = "Country";
	public static String PROP_COUNTRY_NAME = "countryName";
	public static String PROP_ID = "id";


	// constructors
	public BaseCountry () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseCountry (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String countryName;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: CountryName
	 */
	public java.lang.String getCountryName () {
		return countryName;
	}

	/**
	 * Set the value related to the column: CountryName
	 * @param countryName the CountryName value
	 */
	public void setCountryName (java.lang.String countryName) {
		this.countryName = countryName;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.user.Country)) return false;
		else {
			com.ihexa.common.admin.user.Country country = (com.ihexa.common.admin.user.Country) obj;
			if (null == this.getId() || null == country.getId()) return false;
			else return (this.getId().equals(country.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}