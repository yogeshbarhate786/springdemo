package com.ihexa.common.admin.person.action;

import com.ihexa.common.admin.iHexaConstants.IHexaConstants;
import com.ihexa.common.admin.person.Address;
import com.ihexa.common.admin.person.City;
import com.ihexa.common.admin.person.Country;
import com.ihexa.common.admin.person.Person;
import com.ihexa.common.admin.person.State;
import com.ihexa.common.admin.person.service.PersonService;

import com.prounify.framework.base.action.AutoComplete;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.context.Context;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import org.springframework.validation.DataBinder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * doAdd method called from main layout.. getCountryList();getStateList();
 * method declared in PersonService interface... and definition to this given in
 * PersonServiceImpl class here we set attribute for both method to get list
 * data in PersonAdd.jsp
 * 
 */
public class PersonAction extends BaseCRUDAction {
	public Object doAdd(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		PersonService personService = (PersonService) Context.getInstance().getBean("PersonService");

		try {
			request.setAttribute("maritalStatusList", Arrays.asList(IHexaConstants.MARITALSTATUS));
			request.setAttribute("genderList", Arrays.asList(IHexaConstants.GENDER));
			request.setAttribute("salutationList", Arrays.asList(IHexaConstants.SALUTATION));
			request.setAttribute("countryList", personService.getCountryList());
			request.setAttribute("DefaultCountry", personService.getCountryListByID(new Long("88")));
			request.setAttribute("StateList", personService.getStateList(new Long("88")));

			// request.setAttribute("stateList", personService.getStateList());
			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	public String getProperty(String key) {
		return ResourceBundle.getBundle("IHexaResources", Locale.getDefault()).getString(key);
	}

	@Override
	public Object doCreate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		PersonService personService = (PersonService) Context.getInstance().getBean("PersonService");

		ActionMessages messages = new ActionMessages();
		messages.clear();

		ActionMessage msg = null;

		try {
			Person person = (Person) getBean(mapping.getBaseName());
			DataBinder binder = getBinder(person, mapping, request);

			Address address = person.getAddress();
			person = setNullEntity(person);
			/**
			 * if(address.getCountry()==null || address.getCountry().getId()==
			 * null){ address.setCountry(null); } if(address.getState()== null
			 * || address.getState().getId()==null){ address.setState(null); }
			 */
			personService.add(address, null);
			personService.add(person, null);

			msg = new ActionMessage("person.SAVE_SUCCESS");
			messages.add("success", msg);
			saveMessages(request, messages);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			return ERROR;
		}
	}

	@Override
	public Object doList(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		PersonService personService = (PersonService) Context.getInstance().getBean("PersonService");

		String firstName = request.getParameter("firstName1");

		DetachedCriteria criteria = DetachedCriteria.forClass(Person.class);

		try {
			if (StringUtils.isNotBlank(firstName)) {
				criteria.add(Restrictions.like(Person.PROP_FIRST_NAME, firstName));
				request.setAttribute("first", firstName);
			}

			request.setAttribute("valueList", personService.findAll(criteria));

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			return ERROR;
		}
	}
	
	   @Override
	    public Object doEdit(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
	        HttpServletResponse response) throws Exception {
		   PersonService personService = (PersonService) Context.getInstance()
	                                                                   .getBean("PersonService");
	          request.setAttribute("CountryList", personService.getCountryList());

	          String id = request.getParameter("person");

	        try {
	            if (StringUtils.isNotBlank(id) && GenericValidator.isLong(id)) {
	            	Person person = (Person) personService.findById(new Long(id));

	                if ((person != null) && (person.getId() != null)) {
	                    
	                        Address address = person.getAddress();
	                        if(address !=null && address.getId()!=null){

	                        if ((address.getCountry() != null) &&
	                                (address.getCountry().getId() != null)) {
	                            request.setAttribute("StateList",
	                            		personService.getStateList(address.getCountry().getId()));
	                        }

	                        if ((address.getState() != null) && (address.getState().getId() != null)) {
	                            request.setAttribute("CityList",
	                            		personService.getCityList(address.getState().getId()));
	                        }
	                     
	                     }
	                    request.setAttribute("personData", person);
	                    request.setAttribute("GenderList", Arrays.asList(IHexaConstants.GENDER));
	                    request.setAttribute("SalutationList", Arrays.asList(IHexaConstants.SALUTATION));
	                    request.setAttribute("MaritalStatusList",
	                        Arrays.asList(IHexaConstants.MARITALSTATUS));
	                     
	                }else{
	                	return ERROR;
	                	
	                }
	                
	                } else {
	                    return ERROR;
	                }
	           return SUCCESS;
	        } catch (Exception e) {
	            e.printStackTrace();

	            return ERROR;
	        }
	    }
/*
	@Override
	public Object doEdit(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		PersonService personService = (PersonService) Context.getInstance().getBean("PersonService");
		Person person = (Person) getBean(mapping.getBaseName());
		
		request.setAttribute("CountryList", personService.getCountryList());
		
		String id = request.getParameter("person");
			
		try {
			if(StringUtils.isNotBlank(id)&& GenericValidator.isLong(id)){
				person=(Person)personService.findById(new Long(id));
			
			if ((person != null) && (person.getId() != null)) {
				Address address = person.getAddress();

				if ((address != null) && (address.getId() != null)) {
					
					if ((address.getCountry() != null) && (address.getCountry().getId() != null)) {
						request.setAttribute("StateList", personService.getStateList(address.getCountry().getId()));
					}

					if ((address.getState() != null) && (address.getState().getId() != null)) {
						request.setAttribute("CityList", personService.getStateList(address.getState().getId()));
					}
				}
					request.setAttribute(mapping.getDataAttribute(), person);//this mtd belongs to BaseActionMapping.java.......it return string "baseclassname+"Data"
					
					request.setAttribute("GenderList", Arrays.asList(IHexaConstants.GENDER));
					request.setAttribute("SalutationList", Arrays.asList(IHexaConstants.SALUTATION));
					request.setAttribute("MaritalStatusList", Arrays.asList(IHexaConstants.MARITALSTATUS));
				} else {
					    return ERROR;
				       }
			
			} else {
				       return ERROR;
			       }

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			return ERROR;
		}
	}
 **/
	@Override
	public Object doUpdate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		PersonService personService = (PersonService) Context.getInstance().getBean("PersonService");
		ActionMessages messages = new ActionMessages();
		messages.clear();

		ActionMessage msg = null;
		String id = request.getParameter("id");

		try {
			if (StringUtils.isNotBlank(id) && GenericValidator.isLong(id)) {
				Person person = (Person) personService.findById(new Long(id));
				
				person=initializePerson(person);
				
				/*
				Address address = person.getAddress();

				if ((address == null) || (address.getId() == null)) {
					person.setAddress(new Address());
				}

				if ((address.getCountry() == null) || (address.getCountry().getId() == null)) {
					address.setCountry(new Country());
				}

				if ((address.getState() == null) || (address.getState().getId() == null)) {
					address.setState(new State());
				}
				**/

				DataBinder binder = getBinder(person, mapping, request);
				person=setNullEntity(person);
				
				Address address=person.getAddress();
                /*
				if ((address.getCountry() == null) || (address.getCountry().getId() == null)) {
					address.setCountry(null);
				}

				if ((address.getState() == null) || (address.getState().getId() == null)) {
					address.setState(null);
				}
                 **/
				personService.update(address, binder.getBindingResult());
				personService.update(person, binder.getBindingResult());
			}

			msg = new ActionMessage("person.UPDATE_SUCCESS");
			messages.add("success", msg);
			saveMessages(request, messages);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			msg = new ActionMessage("person.UPDATE_FAIL", e.getCause());
			messages.add("error", msg);
			saveMessages(request, messages);

			return ERROR;
		}
	}

	@Override
	public Object doView(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		PersonService personService = (PersonService) Context.getInstance().getBean("PersonService");
		String id = request.getParameter("personID");

		try {
			if (StringUtils.isNotBlank(id) && GenericValidator.isLong(id)) {
				Person person = (Person) personService.findById(new Long(id));
				request.setAttribute("personData", person);
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			return ERROR;
		}
	}

	public Object doGetCountryWiseStateList(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		List<State> stateList = new ArrayList<State>();
		String countryID = request.getParameter("countryID");

		if (StringUtils.isNotBlank(countryID) && GenericValidator.isLong(countryID)) {
			PersonService personService = (PersonService) getService(mapping);

			stateList = personService.getStateList(new Long(countryID));
		}

		AutoComplete autoComplete = new AutoComplete();
		autoComplete.addItem(getProperty("common.listbox.select"), "");

		for (State state : stateList) {
			autoComplete.addItem(state.getStateName(), state.getId().toString());
		}

		return autoComplete.doAjax(request, response);
	}

	public Object doGetStateWiseCityList(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		List<City> cityList = new ArrayList<City>();
		String stateID = request.getParameter("stateID");

		if (StringUtils.isNotBlank(stateID) && GenericValidator.isLong(stateID)) {
			PersonService personService = (PersonService) getService(mapping);
			cityList = personService.getCityList(new Long(stateID));
		}

		AutoComplete autoComplete = new AutoComplete();
		autoComplete.addItem(getProperty("common.listbox.select"), "");

		for (City city : cityList) {
			autoComplete.addItem(city.getCityName(), city.getId().toString());
		}

		return autoComplete.doAjax(request, response);
	}

	public Person setNullEntity(Person person) {
		if (person != null) {
			Address address = person.getAddress();

			if (address == null) {
				person.setAddress(null);
			}

			if ((address.getCountry() == null) || (address.getCountry().getId() == null)) {
				address.setCountry(null);
			}

			if ((address.getState() == null) || (address.getState().getId() == null)) {
				address.setState(null);
			}

			if ((address.getCity() == null) || (address.getCity().getId() == null)) {
				address.setCity(null);
			}
		}

		return person;
	}
	public Person initializePerson(Person person){
		if(person!=null){
		  Address address=person.getAddress();
		  if(address==null || person.getAddress().getId()==null){
			  person.setAddress(new Address());
		  }
		  if(address.getCountry()==null || address.getCountry().getId()==null){
			  address.setCountry(new Country());
			  			  
		  }
		  if(address.getState()==null || address.getState().getId()==null){
			  address.setState(new State());
		  }
		  if(address.getCity()==null || address.getCity().getId()==null){
			  address.setCity(new City());
		  }
		}
		return person;
		
	}
}
