package com.ihexa.common.admin.scheduler;

import java.util.Calendar;
import java.util.Map;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.calendar.HolidayCalendar;

public class CronScheduler {
	public static void main(String[] args) throws Exception {
		new CronScheduler().scheduleJob();

	}

	public void scheduleJob() throws Exception {
		Scheduler scheduler = new StdSchedulerFactory().getScheduler();
		scheduler.start();

		// create a calendar to exclude a particular date
		Calendar cal = Calendar.getInstance();
		cal.set(2007, Calendar.DECEMBER, 25);

		HolidayCalendar excludeCalendar = new HolidayCalendar();
		excludeCalendar.addExcludedDate(cal.getTime());

		// add to scheduler
		scheduler.addCalendar("xmasCalendar", excludeCalendar, true, false);

		JobDetail jobDetail = new JobDetail("messageJob", Scheduler.DEFAULT_GROUP, IHexaJob.class);

		Map map = jobDetail.getJobDataMap();
		map.put("message", "This is a message from Quartz");

		String cronExpression = "0/5 * * * * ?";

		Trigger trigger = new CronTrigger("cronTrigger", Scheduler.DEFAULT_GROUP, cronExpression);

		trigger.setCalendarName("xmasCalendar");

		scheduler.scheduleJob(jobDetail, trigger);
	}
}
