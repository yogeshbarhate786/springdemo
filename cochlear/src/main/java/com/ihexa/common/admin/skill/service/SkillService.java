package com.ihexa.common.admin.skill.service;

import com.prounify.framework.base.service.Service;

/**
 * Service layer interface for User entity.
 */

public interface SkillService<T> extends Service<T> {

}
