package com.ihexa.common.admin.resource;

public class CSResBundle_es extends CSResBundle {

	/**
	 * Constructor Call the parent constructor. Set the parent class to allow cascading resource
	 * bundle search.
	 */
	public CSResBundle_es() {
		super();
		this.setParent(new CSResBundle());
	}

}
