package com.ihexa.common.admin.dashboard;

import com.ihexa.common.admin.dashboard.base.BaseDoctor;



public class Doctor extends BaseDoctor {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Doctor () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Doctor (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}