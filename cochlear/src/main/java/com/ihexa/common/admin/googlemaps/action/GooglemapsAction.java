package com.ihexa.common.admin.googlemaps.action;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.ihexa.common.admin.googlemaps.Newsalon;
import com.ihexa.common.admin.googlemaps.service.GooglemapsService;
import com.ihexa.common.admin.login.Login;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.context.Context;

public class GooglemapsAction extends BaseCRUDAction {

	@Override
	protected boolean isHttpMethodAllowed(String httpMethod, String methodId) {
		if (httpMethod.equals(HTTP_METHOD_GET)) {

			if (methodId.equals("googler")) {
				return true;
			}
			if (methodId.equals("googlers")) {
				return true;
			}
			if (methodId.equals("saveArea")) {
				return true;
			}
			if (methodId.equals("updatearea")) {
				return true;
			}
			if (methodId.equals("updatepoint")) {
				return true;
			}
			if (methodId.equals("deletearea")) {
				return true;
			}
			if (methodId.equals("deleted")) {
				return true;
			}
		}

		return super.isHttpMethodAllowed(httpMethod, methodId);
	}

	public Object doGoogler(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		GooglemapsService service = (GooglemapsService) Context.getInstance().getBean("GooglemapsService");

		ArrayList<userdetail> usd = new ArrayList<userdetail>();
		ArrayList<double[]> arrList = new ArrayList<double[]>();
		ArrayList<String> Areaid = new ArrayList<String>();

		try {
			Class.forName("com.mysql.jdbc.Driver");

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/darling_16dec_8jan", "root", "root");
			Statement stmt1 = con.createStatement();
			ResultSet rs = stmt1.executeQuery("SELECT AsText(poly),ID FROM `bbc_area1`");

			while (rs.next()) {
				Areaid.add(rs.getString(2));
				String temp = rs.getString(1);
				temp = temp.replace("POLYGON", "");
				temp = temp.replaceAll("\\)", "");
				temp = temp.replaceAll("\\(", "");
				temp = temp.replaceAll(" ", ",");

				StringTokenizer tkn = new StringTokenizer(temp, ",");
				double[] farray = new double[tkn.countTokens()];

				for (int i = 0; i < farray.length; i++) {
					farray[i] = Double.parseDouble(tkn.nextToken());
				}

				arrList.add(farray);
				farray = null;
				temp = null;
			}

			// get cust list for custdispersion jsp.
			request.setAttribute("arrlist1", arrList);
			request.setAttribute("AREAID", Areaid);
			System.out.println("helloo");
			DetachedCriteria cris = DetachedCriteria.forClass(Login.class);
			// cris.add(Restrictions.eq(Newsalon.PROP_CREATEDBY,"34"));
			List<Login> list = service.findAll(cris);
			for (int i = 0; i < list.size(); i++) {
				if ("Salon Executive".equals(list.get(i).getUser().getRole().getRoleName())) {
					usd.add(new userdetail(list.get(i).getUser().getId().toString(), list.get(i).getLoginName()));
				}

			}

			DetachedCriteria cri = DetachedCriteria.forClass(Newsalon.class);
			cri.add(Restrictions.eq(Newsalon.PROP_CREATEDBY, "34"));
			List<Newsalon> customerList = service.findAll(cri);
			if (customerList != null && customerList.size() > 0)
				request.setAttribute("display", customerList);
			request.setAttribute("courseList", usd);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e + "error");
		}
		return SUCCESS;
	}

	public Object doGooglers(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		GooglemapsService service = (GooglemapsService) Context.getInstance().getBean("GooglemapsService");
		ArrayList<userdetail> usd = new ArrayList<userdetail>();
		String areaName = request.getParameter("ids");
		// Long ss=Long.parseLong(areaName);
		ArrayList<double[]> arrList = new ArrayList<double[]>();
		ArrayList<String> Areaid = new ArrayList<String>();

		try {
			Class.forName("com.mysql.jdbc.Driver");

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/darling_16dec_8jan", "root", "root");
			Statement stmt1 = con.createStatement();
			ResultSet rs = stmt1.executeQuery("SELECT AsText(poly),ID FROM `bbc_area1`");

			while (rs.next()) {
				Areaid.add(rs.getString(2));
				String temp = rs.getString(1);
				temp = temp.replace("POLYGON", "");
				temp = temp.replaceAll("\\)", "");
				temp = temp.replaceAll("\\(", "");
				temp = temp.replaceAll(" ", ",");

				StringTokenizer tkn = new StringTokenizer(temp, ",");
				double[] farray = new double[tkn.countTokens()];

				for (int i = 0; i < farray.length; i++) {
					farray[i] = Double.parseDouble(tkn.nextToken());
				}

				arrList.add(farray);
				farray = null;
				temp = null;
			}

			// get cust list for custdispersion jsp.
			request.setAttribute("arrlist1", arrList);
			request.setAttribute("AREAID", Areaid);
			System.out.println("helloo");
			DetachedCriteria cris = DetachedCriteria.forClass(Login.class);
			List<Login> list = service.findAll(cris);
			for (int i = 0; i < list.size(); i++) {
				if ("Salon Executive".equals(list.get(i).getUser().getRole().getRoleName())) {
					usd.add(new userdetail(list.get(i).getUser().getId().toString(), list.get(i).getLoginName()));
				}

			}
			DetachedCriteria cri = DetachedCriteria.forClass(Newsalon.class);
			cri.add(Restrictions.eq(Newsalon.PROP_CREATEDBY, areaName));
			List<Newsalon> customerList = service.findAll(cri);
			if (customerList != null && customerList.size() > 0)
				request.setAttribute("display", customerList);
			request.setAttribute("courseList", usd);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return SUCCESS;
	}

	public Object doSaveArea(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String[] lat = request.getParameterValues("lat");
		String[] lng = request.getParameterValues("lng");
		String cord = request.getParameter("cord");
		String areaName = request.getParameter("areaName");
		String areaCode = request.getParameter("areaCode");
		// String areaManager = request.getParameter("areaManager");
		if ("sdf".equals(cord)) {

		} else {
			StringBuffer statement = new StringBuffer(
					"INSERT INTO bbc_area1(poly,area_code,area_manager,area_name) VALUES(PolygonFromText('POLYGON((");

			/*
			 * for (int i = 0; i < lat.length; i++) { if ((lat[i] != null) &&
			 * !lat[i].equals("")) { statement.append(lat[i] + " " + lng[i] +
			 * ","); } }
			 */
			cord = cord.replace("(", "");
			cord = cord.replace(")", "");
			StringTokenizer st2 = new StringTokenizer(cord, ",");
			ArrayList<String> cordList = new ArrayList<String>();

			while (st2.hasMoreTokens()) {
				cordList.add(st2.nextToken());
			}

			for (int i = 0; i < cordList.size(); i += 2) {
				statement.append(cordList.get(i) + " " + cordList.get(i + 1) + ",");
			}
			statement.delete(statement.lastIndexOf(","), statement.lastIndexOf(",") + 1);

			// statement.append(lat[0] + " " + lng[0]);
			/* statement.append("))'))"); */
			statement.append("))'),'" + areaCode + "','" + "No AreaManager" + "','" + areaName + "')");
			System.out.println(statement);

			try {
				Class.forName("com.mysql.jdbc.Driver");

				Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/darling_16dec_8jan", "root", "root");
				Statement stmt1 = con.createStatement();
				stmt1.executeUpdate(statement.toString());
				statement = null;

				return SUCCESS;
			} catch (Exception e) {
				e.printStackTrace();

				return ERROR;
			}

		}
		return SUCCESS;
	}

	public Object doUpdatepoint(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String[] cord = request.getParameterValues("branchId");

		String[] da1 = cord[0].split(",");

		for (int i = 0; i < da1.length; i++) {
			String data[] = da1[i].split("#");
			String abc = data[0];
			String data2[] = abc.split("~");
			String[] da2 = data[1].split("%");
			String stat = "select id,lat,lng,areaid from areamapping where lat='" + da2[0] + "' and lng='" + da2[1]
					+ "' and areaid='" + data2[0] + "'";
			try {
				Class.forName("com.mysql.jdbc.Driver");

				Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/darling_16dec_8jan", "root", "root");
				Statement stmt1 = con.createStatement();
				ResultSet rs = stmt1.executeQuery(stat);
				int cnt = 0;
				String id = new String();
				while (rs.next()) {
					cnt++;
					id = rs.getString(1);
				}
				if (cnt > 0) {

				} else {

					String stat2 = "insert  into areamapping(lat,lng,areaid,salid) values(" + da2[0] + "," + da2[1]
							+ "," + data2[0] + "," + data2[1] + ")";
					stmt1.executeUpdate(stat2);
				}

				stat = new String();

			} catch (Exception e) {
				e.printStackTrace();

			}
			data = null;
			da2 = null;
		}

		return SUCCESS;
	}

	public Object doDeletearea(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ArrayList<listdetail> as = new ArrayList<listdetail>();

		String stat = "select * from bbc_area1";
		try {
			Class.forName("com.mysql.jdbc.Driver");

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/darling_16dec_8jan", "root", "root");
			Statement stmt1 = con.createStatement();
			Statement stmt2 = con.createStatement();
			Statement stmt3= con.createStatement();
			ResultSet rs = stmt1.executeQuery(stat);
			int cnt = 0;
			StringBuilder ab = new StringBuilder();
			String id = new String();
			int cn = 0;
			while (rs.next()) {
				cnt++;
				String ida = rs.getString(2);
				if (!"".equals(ida) && ida != null) {
					ResultSet rs1 = stmt2.executeQuery("select * from routeassign where Areaid='" + ida + "'");

					while (rs1.next()) {
						Long usid = Long.parseLong(rs1.getString(2));
						ResultSet rs2 = stmt3.executeQuery("select * from login where UserID='" + usid + "'");
						while (rs2.next()) {

							ab.append(rs2.getString("LoginName"));
							if (cn > 0) {
								ab.append(",");

							}

						}

						cn++;
					}
					if (cn == 0) {
						ab.append("NA");

					}
				}

				String areacode = rs.getString(3);
				String areaname = rs.getString(5);

				as.add(new listdetail(ida, areacode, areaname, ab.toString()));
			}

			System.out.println("hello");

		} catch (Exception e) {
			e.printStackTrace();

		}
		request.setAttribute("display", as);
		return SUCCESS;
	}

	
	public Object doDeleted(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		String id= request.getParameter("id");
		System.out.println(id);
		try {
			String statement="delete from bbc_area1 where ID='"+Long.parseLong(id)+"'";
			Class.forName("com.mysql.jdbc.Driver");

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/darling_16dec_8jan", "root", "root");
			Statement stmt1 = con.createStatement();
			stmt1.executeUpdate(statement.toString());
			statement = null;

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();

			return ERROR;
		}
		
	}
}
