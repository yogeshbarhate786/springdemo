package com.ihexa.common.admin.iHexaConstants;

public interface IHexaConstants {
    // /**
    // * to get Property value from .properties file
    // *
    // * @param key
    // * @return value of that key
    // */
    // public static String getProperty(String key) {
    // return ResourceBundle.getBundle("IHexaResources", Locale.getDefault()).getString(key);
    // }
    public static final long PRIORITY_HIGH = 1;
    public static final long PRIORITY_MEDIUM = 2;
    public static final long PRIORITY_NORMAL = 3;
    public static final long PRIORITY_LOW = 4;
    public static final int TASK_ASSIGNED = 1;
    public static final int TASK_COMPLETED = 2;
    public static final int REGISTRED = 2;
    public static final String YES = "Yes";
    public static final String NO = "No";
    public static final String[] ASSIGED_TYPE = {
            "Auto", "Manual", "Skill", "Role"
        };
    public static final String DOC_ROOT_LOCATION = "C:\\ihexa\\";
    public static final String[] YESNO = { "Yes", "No" };
    public static final String[] STATUS = { "Active", "Inactive" };
    public static final String[] GENDER = { "Female", "Male" };
    public static final String[] MARITALSTATUS = {
            "Married", "Single", "Divorce", "Widowed"
        };
    public static final String[] SALUTATION = { "Mr.", "Ms.", "Miss." };
    public static final String[] SOURCE = { "Email", "Web", "SMS", "Inbound" };
    public static final String[] MEDIA = {
            "Magazine", "Newspaper", "Internet", "Radio", "Family/Friends",
            "Outdoor Ads", "Mobile", "Others"
        };
    public static final String[] COMPLAINT_STATUS = {
            "New", "Resolved", "Feedback", "Close"
        };
    public static final String[] TASK_SEARCH_COMPLAINT_STATUS = {
            "New", "Feedback"
        };
    public static final long[] COMPLAINTSUBDISPOSITION = { 18, 19, 20, 21, 22 };
    public static final long[] COMPLAINTTYPE = { 1, 2, 3 };
    public static final String RETURN_SCRIPT_EXISTS = "EXISTS";
    public static final String RETURN_ENTITY_EXISTS = "ENTITY_EXISTS";
    public static final String RETURN_DEALER_EXISTS = "DEALER_EXISTS";
    public static final String RETURN_CUSTOMER_NOT_FOUND = "CUSTOMER_NOT_FOUND";
    public static final String RETURN_CUSTOMER_NEXT = "CUSTOMER_NEXT";
    public static final String RETURN_NEXT_CUSTOMER_TASK = "NEXT_CUSTOMER_TASK";
    public static final String RETURN_NEXT_DEALER_TASK = "NEXT_DEALER_TASK";
    public static final String RETURN_NEXT_COMPLAINT_TASK = "NEXT_COMPLAINT_TASK";
    public static final String RETURN_END_TASK = "END_TASK";
    public static final String RETURN_RE_ASSIGN = "RE_ASSIGN";
    public static final String RETURN_CUSTOMER_EXIST = "CUSTOMER_EXIST";
    public static final String RETURN_CUSTOMER_END = "CUSTOMER_END";
    public static final String RETURN_AGENT_CUSTOMER_NEXT = "AGENT_CUSTOMER_NEXT";

    /* Login roles */
    public static final long LOGINROLE_ADMINISTRATOR = 1;
    public static final long LOGINROLE_SUPERVISOR = 2;
    public static final long LOGINROLE_AGENT = 3;
    public static final long LOGINROLE_DEALER = 4;
    public static final long LOGINROLE_CUSTOMER = 5;
    public static final long LOGINROLE_DOCTOR = 7;
    public static final long LOGINROLE_TELECALLER = 8;

    // Xls upload constants
    /*
     * Customer sheet column count starting from 0. Ex. 0 to 11 count = 12
     */
    public final static int CUSTOMER_SHEET_COLUMN_LENGTH = 23;
    public final static int DEALER_SHEET_COLUMN_LENGTH = 29;

    // public static final String FILE_LOCATION = getProperty("file.location");
    public final static int FILE_UPLOAD_FAIL = 0;
    public final static int FILE_UPLOAD_DUPLICATE_RECORD = 1;
    public final static int FILE_UPLOAD_SUCCESS = 2;
    public final static int FILE_EMPTY = 3;
    public final static int FILE_UPLOAD_SMS_UNDELIVERED = 3;
    public final static int FILE_UPLOAD_SMS_DELIVERED = 4;
    public final static String[] DATA_UPLOAD = { "Customer", "Dealer" };
    public final static String[] FILE_UPLOAD_RESULT = {
            "File Upload failed", "Duplicate record", "Record added SuccessFuly",
            "Unable to deliver SMS", "SMS delivered"
        };

    // mail Status for handling
    public static final String[] MAILSTATUS = { "Fresh", "Inprocess", "Complete" };

    /**
     * Expression are separated with white-space, and represent: <br>
     * 1. Seconds <br>
     * 2. Minutes <br>
     * 3. Hours <br>
     * 4. Day-of-Month <br>
     * 5. Month <br>
     * 6. Day-of-Week <br>
     * 7. Year (optional field) <br>
     * <br>
     * Example 1 - an expression to create a trigger that simply fires every 5 minutes <br>
     * "0 0/5 * * * ?" <br>
     * <br>
     * Example 2 - an expression to create a trigger that fires every 5 minutes, at 10 seconds after the minute (i.e. 10:00:10 am, 10:05:10 am, etc.). <br>
     * "10 0/5 * * * ?" <br>
     * <br>
     * Example 3 - an expression to create a trigger that fires at 10:30, 11:30, 12:30, and 13:30, on every Wednesday and Friday. <br>
     * "0 30 10-13 ? * WED,FRI" <br>
     * <br>
     * Example 4 - an expression to create a trigger that fires every half hour between the hours of 8 am and 10 am on the 5th and 20th of every month. Note that the trigger will
     * NOT fire at 10:00 am, just at 8:00, 8:30, 9:00 and 9:30 <br>
     * "0 0/30 8-9 5,20 * ?" <br>
     * <br>
     * Source : http://www.opensymphony.com/quartz/wikidocs/TutorialLesson6.html
     */
    public final static String SCHEDULER_CRON_EXPRESSION = "0 0/5 * * * ?";
    public final static String[] YEARS = {
            "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997",
            "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005",
            "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013",
            "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021",
            "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029",
            "2030"
        };
    public final static String[] MONTHS = {
            "January", "February", "March", "April", "May", "June", "July",
            "August", "September", "October", "November", "December"
        };
    public final static String[] DATES = {
            "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13",
            "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24",
            "25", "26", "27", "28", "29", "30", "31"
        };
    public final static String[] DAYS = {
            "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
            "Saturday",
        };
    public static final byte STATUS_ACTIVE = 1;
    public static final byte STATUS_INACTIVE = 0;

    /**
     * Table Names for Unique code generation
     */
    public final static String TABLE_CUSTOMER = "customer";
}
