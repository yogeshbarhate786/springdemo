package com.ihexa.common.admin.person;

import com.ihexa.common.admin.person.base.BaseCountry;



public class Country extends BaseCountry {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Country () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Country (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}