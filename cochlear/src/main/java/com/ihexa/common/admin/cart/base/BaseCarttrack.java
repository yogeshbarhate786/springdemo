package com.ihexa.common.admin.cart.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the carttrack table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="carttrack"
 */

public abstract class BaseCarttrack  implements Serializable {

	public static String REF = "Carttrack";
	public static String PROP_STATUS = "Status";
	public static String PROP_UNIQUEID = "Uniqueid";
	public static String PROP_USERID = "Userid";
	public static String PROP_ID = "Id";
	public static String PROP_ORDERID = "Orderid";
	public static String PROP_ORDERDATE = "Orderdate";


	// constructors
	public BaseCarttrack () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseCarttrack (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String uniqueid;
	private java.lang.String orderid;
	private java.lang.String status;
	private java.util.Date orderdate;

	// many to one
	private com.ihexa.common.admin.doctor.Doctor userid;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="id"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: uniqueid
	 */
	public java.lang.String getUniqueid () {
		return uniqueid;
	}

	/**
	 * Set the value related to the column: uniqueid
	 * @param uniqueid the uniqueid value
	 */
	public void setUniqueid (java.lang.String uniqueid) {
		this.uniqueid = uniqueid;
	}



	/**
	 * Return the value associated with the column: orderid
	 */
	public java.lang.String getOrderid () {
		return orderid;
	}

	/**
	 * Set the value related to the column: orderid
	 * @param orderid the orderid value
	 */
	public void setOrderid (java.lang.String orderid) {
		this.orderid = orderid;
	}



	/**
	 * Return the value associated with the column: status
	 */
	public java.lang.String getStatus () {
		return status;
	}

	/**
	 * Set the value related to the column: status
	 * @param status the status value
	 */
	public void setStatus (java.lang.String status) {
		this.status = status;
	}



	/**
	 * Return the value associated with the column: orderdate
	 */
	public java.util.Date getOrderdate () {
		return orderdate;
	}

	/**
	 * Set the value related to the column: orderdate
	 * @param orderdate the orderdate value
	 */
	public void setOrderdate (java.util.Date orderdate) {
		this.orderdate = orderdate;
	}



	/**
	 * Return the value associated with the column: userid
	 */
	public com.ihexa.common.admin.doctor.Doctor getUserid () {
		return userid;
	}

	/**
	 * Set the value related to the column: userid
	 * @param userid the userid value
	 */
	public void setUserid (com.ihexa.common.admin.doctor.Doctor userid) {
		this.userid = userid;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.cart.Carttrack)) return false;
		else {
			com.ihexa.common.admin.cart.Carttrack carttrack = (com.ihexa.common.admin.cart.Carttrack) obj;
			if (null == this.getId() || null == carttrack.getId()) return false;
			else return (this.getId().equals(carttrack.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}