package com.ihexa.common.admin.resource;

public class CSResBundle_it extends CSResBundle {

	/**
	 * Constructor Call the parent constructor. Set the parent class to allow cascading resource
	 * bundle search.
	 */
	public CSResBundle_it() {
		super();
		this.setParent(new CSResBundle());
	}

}
