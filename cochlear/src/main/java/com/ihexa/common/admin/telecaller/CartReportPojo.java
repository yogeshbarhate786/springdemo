package com.ihexa.common.admin.telecaller;

import java.util.List;
import com.ihexa.common.admin.cart.Cartorderpojo;
import com.ihexa.common.admin.doctor.Doctor;

public class CartReportPojo
{
	private String orderid;
	private String placeddate;
	private String approstatus;
	
	public String getPlaceddate() {
		return placeddate;
	}
	public void setPlaceddate(String placeddate) {
		this.placeddate = placeddate;
	}
	public String getApprostatus() {
		return approstatus;
	}
	public void setApprostatus(String approstatus) {
		this.approstatus = approstatus;
	}
	private List<Doctor> doctor;
	private List<Cartorderpojo> cart;
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public List<Doctor> getDoctor() {
		return doctor;
	}
	public void setDoctor(List<Doctor> doctor) {
		this.doctor = doctor;
	}
	public List<Cartorderpojo> getCart() {
		return cart;
	}
	public void setCart(List<Cartorderpojo> cart) {
		this.cart = cart;
	}
	
	

}
