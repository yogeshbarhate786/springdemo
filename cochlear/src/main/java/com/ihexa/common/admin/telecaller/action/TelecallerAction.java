package com.ihexa.common.admin.telecaller.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.ActionForm;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.ihexa.common.admin.cart.Cart;
import com.ihexa.common.admin.cart.Cartorderpojo;
import com.ihexa.common.admin.cart.Carttrack;
import com.ihexa.common.admin.dashboard.User;
import com.ihexa.common.admin.doctor.Doctor;
import com.ihexa.common.admin.gift.service.GiftService;
import com.ihexa.common.admin.iHexaConstants.IHexaConstants;
import com.ihexa.common.admin.login.Login;

import com.ihexa.common.admin.order.OrderCart;
import com.ihexa.common.admin.order.Orderdetails;
import com.ihexa.common.admin.productsku.Category;
import com.ihexa.common.admin.productsku.Maincategory;
import com.ihexa.common.admin.productsku.Product;
import com.ihexa.common.admin.productsku.Subcategory;
import com.ihexa.common.admin.profile.Profile;

import com.ihexa.common.admin.telecaller.CartReportPojo;
import com.ihexa.common.admin.telecaller.Telecaller;
import com.ihexa.common.admin.ticket.Ticket;
import com.ihexa.common.admin.user.Address;
import com.ihexa.common.admin.user.City;
import com.ihexa.common.admin.user.Country;
import com.ihexa.common.admin.user.Person;
import com.ihexa.common.admin.user.Role;
import com.ihexa.common.admin.user.State;

import com.ihexa.common.admin.utility.DynamicIpPath;
import com.ihexa.common.admin.utility.SHAUtil;
import com.ihexa.common.admin.utility.SendMail;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.context.Context;
import com.prounify.framework.context.UserContext;
import com.prounify.framework.util.MD5Util;

public class TelecallerAction extends BaseCRUDAction {

	GiftService service = (GiftService) Context.getInstance().getBean("GiftService");
	protected boolean isHttpMethodAllowed(String httpMethod, String methodId) {

		if (httpMethod.equals(HTTP_METHOD_GET)) {

			if (methodId.equals("getorderlist"))
				return true;
			
			if (methodId.equals("ordervarify"))
				return true;
			if (methodId.equals("varifyorderlist"))
				return true;
			
		}
		return super.isHttpMethodAllowed(httpMethod, methodId);
	}

	@Override
	public Object doAdd(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {

			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}
	}

	@Override
	public Object doCreate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		try {
			String TelecallerFullName = request.getParameter("telecaller_full_name");
			String TelecallerEmailAddress = request.getParameter("telecaller_email_address");
			if (StringUtils.isNotBlank(TelecallerEmailAddress) && StringUtils.isNotBlank(TelecallerFullName)) {
				DetachedCriteria criteria = DetachedCriteria.forClass(Telecaller.class);
				criteria.add(Restrictions.eq(Telecaller.PROP_EMAIL, TelecallerEmailAddress));
				List<Telecaller> EmailAddress = service.findAll(criteria);
				if (EmailAddress.size() == 0) {

					String uid = "UID" + new Date().getTime() + "x" + new Random().nextInt(1000);
					Person person = new Person();
					Telecaller telecallerUser = new Telecaller();

					String[] parts = TelecallerFullName.split(" ");
					if (parts.length >= 1) {
						String firstName = parts[0];
						person.setFirstName(firstName);
						telecallerUser.setFirstName(firstName);
					}
					if (parts.length >= 2) {
						String lastName = "";
						for (int i = 1; i < parts.length; i++) {
							lastName = lastName + " " + parts[i];
						}
						person.setLastName(lastName);
						telecallerUser.setLastName(lastName);
					}
					person.setEmail(TelecallerEmailAddress);
					service.add(person, null);

					telecallerUser.setBio(null);
					// ideaUser.setDob(dob);
					// ideaUser.setFullAddress(fullAddress);
					// ideaUser.setGender(gender);
					// ideaUser.setMiddleName(middleName);
					// ideaUser.setMobile(mobile);
					telecallerUser.setEmail(TelecallerEmailAddress);
					telecallerUser.setRegisterOn(new Date());
					telecallerUser.setCreateddate(new Date());
					telecallerUser.setStatus("Active");
					telecallerUser.setUserName(TelecallerEmailAddress);
					telecallerUser.setIsEmailVerified(uid);
					
					
					service.add(telecallerUser, null);

					com.ihexa.common.admin.user.User user = new com.ihexa.common.admin.user.User();
					user.setPerson(person);
					user.setRole(new Role(Long.valueOf(IHexaConstants.LOGINROLE_TELECALLER)));
					user.setManager(new com.ihexa.common.admin.user.User(new Long(
							IHexaConstants.LOGINROLE_ADMINISTRATOR)));
					user.setTelecallerid(telecallerUser);
					service.add(user, null);

					String hash=MD5Util.hashPassword("telecaller");
					
					
					SHAUtil shaUtil = new SHAUtil();
					String salt = shaUtil.generateSalt(user.toString());

					Login login = new Login();
					login.setUser(user);
					login.setLoginName(TelecallerEmailAddress);
					login.setPassword(hash);
					//login.setVariant(salt);
					login.setLastLoginDate(new Date());
					login.setPwdExpirationDate(new Date());
					login.setStatusPasswordResetDate(new Date());
					login.setStatusActiveDate(new Date());
					// login.setProfile(new
					// Profile(IHexaConstants.LOGINROLE_DOCTOR));
					login.setProfileId((IHexaConstants.LOGINROLE_TELECALLER));
					service.add(login, null);

					String emailContents = "Hi "
							+ TelecallerFullName
							+ ", <br><br>Your account is created successfully. Please login to our site for further assistance.<br>Your UserName Is: "
							+ TelecallerEmailAddress + "<br> And Password Is:telecaller" + "<a href ='http://"
							+ DynamicIpPath.getEndPoints() + "/cochlear/" + "'>Click Here To Go</a>"
							+ "<br><br>Thanks<br>Cochlear Team";
					String emailSubject = "Cochlear - Your Account Is Created ";

					String emailID = TelecallerEmailAddress.trim();
					SendMail.sendEmail(emailID, emailSubject, emailContents, new ArrayList<String>());

				} else {
					request.setAttribute("TelecallerFullName", TelecallerFullName);
					request.setAttribute("TelecallerEmailAddress", TelecallerEmailAddress);

				}
			}
			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}
		
		

	}

	@Override
	public Object doEdit(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			
			Long userid = (Long) UserContext.getSessionScope().get("USERID");
			
			DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
			criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
			List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
			if(listobj.size()>0)
			{
			request.setAttribute("UserEditList", listobj.get(0));
			}
			if(listobj.get(0).getTelecallerid()!=null)
			{
			Long telecallerid = listobj.get(0).getTelecallerid().getId();
		    DetachedCriteria criteriaobj1 = DetachedCriteria.forClass(Telecaller.class);
			criteriaobj1.add(Restrictions.eq(Doctor.PROP_ID, new Long(telecallerid)));
			List<Telecaller> listobj1 = service.findAll(criteriaobj1);
			request.setAttribute("TelecallerEditList", listobj1.get(0));
			
			DetachedCriteria criteria = DetachedCriteria.forClass(Country.class);
			List<Country> CountryList = service.findAll(criteria);
			request.setAttribute("CountryList", CountryList);
			
			if(listobj1.get(0).getAddressid()!=null)
			{
			DetachedCriteria criteria2 = DetachedCriteria.forClass(State.class);
			criteria2.add(Restrictions.eq(State.PROP_COUNTRY+"."+Country.PROP_ID,listobj1.get(0).getAddressid().getCountry().getId()));
			List<State> StateList = service.findAll(criteria2);
			request.setAttribute("StateList", StateList);
			}
			
			if(listobj1.get(0).getAddressid()!=null)
			{
			DetachedCriteria criteria3 = DetachedCriteria.forClass(City.class);
			criteria3.add(Restrictions.eq(City.PROP_STATE+"."+State.PROP_ID,listobj1.get(0).getAddressid().getState().getId()));
			List<City> CityList = service.findAll(criteria3);
			request.setAttribute("CityList", CityList);
			}
			}
			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}
	}

	@Override
	public Object doUpdate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			
			String TelecallerFirstName = request.getParameter("telecaller_first_name");
			String TelecallerMiddleName = request.getParameter("telecaller_middle_name");
			String TelecallerLastName = request.getParameter("telecaller_last_name");
			String TelecallerEmailAddress = request.getParameter("telecaller_email_address");
			String TelecallerUserName = request.getParameter("telecaller_user_name");
			String TelecallerGender = request.getParameter("genderid");
			String TelecallerAge = request.getParameter("telecaller_age");
			String TelecallerBio = request.getParameter("telecaller_bio");
			String TelecallerMobileNumber = request.getParameter("telecaller_mobile_number");
			String TelecallerFullAddress = request.getParameter("telecaller_full_address");
			String TelecallerAddressStreet = request.getParameter("telecaller_address_street");
			String TelecallerZipCode = request.getParameter("telecaller_zip_code");
			String TelecallerCountry = request.getParameter("countryid");
			String TelecallerState = request.getParameter("stateid");
			String TelecallerCity = request.getParameter("cityid");
		
			
			String telecallerid = request.getParameter("telecallerid");
			
			DetachedCriteria criteriaobj1 = DetachedCriteria.forClass(Telecaller.class);
			criteriaobj1.add(Restrictions.eq(Telecaller.PROP_ID, new Long(telecallerid)));
			List<Telecaller> listobj1 = service.findAll(criteriaobj1);
			
			for (Telecaller telecallerlist : listobj1) {
				
				Address address = new Address();
				if (StringUtils.isNotBlank(TelecallerFullAddress)) {
					address.setAddressFull(TelecallerFullAddress);
				} else {
					address.setAddressFull(null);
				}
				
				if (StringUtils.isNotBlank(TelecallerAddressStreet)) {
					address.setAddressStreet(TelecallerAddressStreet);
				} else {
					address.setAddressStreet(null);
				}
				
				if (StringUtils.isNotBlank(TelecallerZipCode)) {
					address.setZipCode(new Long(TelecallerZipCode));
				} else {
					address.setZipCode(null);
				}
				
				if (StringUtils.isNotBlank(TelecallerCountry)) {
					address.setCountry(new Country(new Long(TelecallerCountry)));
				} else {
					address.setCountry(null);
				}
				
				if (StringUtils.isNotBlank(TelecallerState)) {
					address.setState(new State(new Long(TelecallerState)));
				} else {
					address.setState(null);
				}
				
				if (StringUtils.isNotBlank(TelecallerCity)) {
					address.setCity(new City(new Long(TelecallerCity)));
				} else {
					address.setCity(null);
				}
				
				service.add(address, null);
				telecallerlist.setAddressid(address);
				
				
				if (StringUtils.isNotBlank(TelecallerFirstName)) {
					telecallerlist.setFirstName(TelecallerFirstName);
				} else {
					telecallerlist.setFirstName(null);
				}
				
				if (StringUtils.isNotBlank(TelecallerMiddleName)) {
					telecallerlist.setMiddleName(TelecallerMiddleName);
				} else {
					telecallerlist.setMiddleName(null);
				}
				
				if (StringUtils.isNotBlank(TelecallerLastName)) {
					telecallerlist.setLastName(TelecallerLastName);
				} else {
					telecallerlist.setLastName(null);
				}
				
				if (StringUtils.isNotBlank(TelecallerEmailAddress)) {
					telecallerlist.setEmail(TelecallerEmailAddress);
				} else {
					telecallerlist.setEmail(null);
				}
				
				if (StringUtils.isNotBlank(TelecallerUserName)) {
					telecallerlist.setUserName(TelecallerUserName);
				} else {
					telecallerlist.setUserName(null);
				}
				
				if (StringUtils.isNotBlank(TelecallerGender)) {
					telecallerlist.setGender(TelecallerGender);
				} else {
					telecallerlist.setGender(null);
				}
				
				if (StringUtils.isNotBlank(TelecallerAge)) {
					telecallerlist.setAge(new Long(TelecallerAge));
				} else {
					telecallerlist.setAge(null);
				}
				
				if (StringUtils.isNotBlank(TelecallerBio)) {
					telecallerlist.setBio(TelecallerBio);
				} else {
					telecallerlist.setBio(null);
				}
				
				if (StringUtils.isNotBlank(TelecallerMobileNumber)) {
					telecallerlist.setMobile(TelecallerMobileNumber);
				} else {
					telecallerlist.setMobile(null);
				}
				
				telecallerlist.setUpdateddate(new Date());
				
				service.add(telecallerlist, null);
			
			}
			
			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}
	}
	
	@Override
	public Object doList(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			
			DetachedCriteria criteriaobj = DetachedCriteria.forClass(Telecaller.class);
			criteriaobj.add(Restrictions.eq(Telecaller.PROP_STATUS, "Active"));
			List<Telecaller> listobj = service.findAll(criteriaobj);
			request.setAttribute("TelecallerList", listobj);
			
			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}
	}

	@Override
	public Object doDelete(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			String telecallerid = request.getParameter("telecallerid");
			DetachedCriteria criteriaobj = DetachedCriteria.forClass(Telecaller.class);
			criteriaobj.add(Restrictions.eq(Doctor.PROP_ID, new Long(telecallerid)));
			List<Telecaller> listobj = service.findAll(criteriaobj);

			for (Telecaller telecallerlist : listobj) {

				telecallerlist.setUpdateddate(new Date());
				telecallerlist.setStatus("InActive");

				service.add(telecallerlist, null);

			}

			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	
	public Object doGetorderlist(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception 
			{
             try {
            	 
            	
            	ArrayList<CartReportPojo> cartreport=new ArrayList(); 
            	
            
            	
            	Long userid = (Long) UserContext.getSessionScope().get("USERID");
        			
        			DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
        			criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
        			List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
        			request.setAttribute("UserEditList", listobj.get(0));
        			
        			Long tellecallerid = listobj.get(0).getTelecallerid().getId();
        			

        			String orderid = request.getParameter("orderid");
                     if(orderid!=null)
                     {
        			DetachedCriteria cri=DetachedCriteria.forClass(Orderdetails.class);
        			cri.add(Restrictions.eq(Orderdetails.PROP_STATUS, "Success"));
        	     	cri.add(Restrictions.eq(Orderdetails.PROP_ORDERID,orderid ));
        	     	List<Orderdetails> list=service.findAll(cri);
        	     	
        	     	
        	     	for (Orderdetails orderdetails : list)
        	     	{
					    orderdetails.setApprovestatus("varify");
					    orderdetails.setApproveby(new Telecaller(tellecallerid));
					    orderdetails.setFeedback("successfully approved");
					    orderdetails.setApprovedate(new Date());
					    service.add(orderdetails, null);
					
        	     	}
        	     	}
        			
        		    DetachedCriteria criterod=DetachedCriteria.forClass(Orderdetails.class);
        		    criterod.add(Restrictions.eq(Orderdetails.PROP_STATUS, "Success"));
        		 /*   criterod.add(Restrictions.eq(Orderdetails.PROP_APPROVESTATUS, "Pendding"));;*/
        		    List<Orderdetails> list=service.findAll(criterod);
        		    for (Orderdetails odlist : list) 
        		    {
        		    	CartReportPojo carobj=new CartReportPojo();
        		    	carobj.setOrderid(odlist.getOrderid().toString());
        		    	carobj.setApprostatus(odlist.getApprovestatus());
        		    	carobj.setPlaceddate(odlist.getPlacedorderdate().toString());
        		    	
        		    DetachedCriteria criteriaobj1 = DetachedCriteria.forClass(Doctor.class);
        			criteriaobj1.add(Restrictions.eq(Doctor.PROP_STATUS, "Active"));
        			criteriaobj1.add(Restrictions.eq(Doctor.PROP_ID,new Long(odlist.getDoctorid().getId())));
        			List<Doctor> listobj1 = service.findAll(criteriaobj1);

        			carobj.setDoctor(listobj1);
        			
        			DetachedCriteria cirobj=DetachedCriteria.forClass(OrderCart.class);
        			cirobj.add(Restrictions.eq(OrderCart.PROP_UNIQUEID,odlist.getOrderid()));
        			List<OrderCart> listcartorder=service.findAll(cirobj);
        			
        			ArrayList<Cartorderpojo> cartlist=new ArrayList();
        			for (OrderCart orderCart : listcartorder) 
        			{
        				
        				DetachedCriteria criteria1 = DetachedCriteria.forClass(Cart.class);
            			criteria1.add(Restrictions.eq(Cart.PROP_DOCTORID+"."+Doctor.PROP_ID,new Long(listobj1.get(0).getId())));
            			criteria1.add(Restrictions.isNull(Cart.PROP_REMOVE_FROM_CART_TIME));
            			criteria1.add(Restrictions.eq(Cart.PROP_ID, new Long(orderCart.getCartid().getId())));
            			List<Cart> cartlist1 = service.findAll(criteria1);
            			
            			for (Cart cart : cartlist1)
            			{
						Cartorderpojo cartobj=new Cartorderpojo();
						cartobj.setCartid(cart.getId().intValue());
						cartobj.setBill(cart.getBill().doubleValue());
						cartobj.setTotalbill(cart.getTotalbill().doubleValue());
						cartobj.setProduct_name(cart.getProductid().getName());
						cartobj.setProduct_price(cart.getProductid().getPrice().toString());
						cartobj.setProduct_url(cart.getProductid().getPicurl());
						cartobj.setQuantity(cart.getQuantity());
						cartlist.add(cartobj);
						}
            		 }
        			carobj.setCart(cartlist);
        			 cartreport.add(carobj);
        			 }
        		    
        			request.setAttribute("orderlist", cartreport);	
        			
        			 DetachedCriteria cri=DetachedCriteria.forClass(Orderdetails.class);
  				     cri.add(Restrictions.eq(Orderdetails.PROP_STATUS, "Success"));
  				     cri.add(Restrictions.eq(Orderdetails.PROP_APPROVESTATUS, "Pendding"));
  				   List<Orderdetails> orderlist=service.findAll(cri);
  				
  				   DetachedCriteria criticket=DetachedCriteria.forClass(Ticket.class);
  				   criticket.add(Restrictions.eq(Ticket.PROP_STATUS, "Active"));
  				   List<Carttrack> ticketlist=service.findAll(criticket);
  				 
  				   request.setAttribute("ticketcount",ticketlist.size());
        			
        			} catch (Exception e) {
				e.printStackTrace();
			}		
 		return SUCCESS;
	}
	public Object doOrdervarify(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception 
			{
             try {
            	 
            		Long userid = (Long) UserContext.getSessionScope().get("USERID");
        			
        			DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
        			criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
        			List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
        			request.setAttribute("UserEditList", listobj.get(0));
        			
        			Long tellecallerid = listobj.get(0).getTelecallerid().getId();
        			
        			String orderid = request.getParameter("orderid");
        			
        			DetachedCriteria cri=DetachedCriteria.forClass(Orderdetails.class);
        	     	cri.add(Restrictions.eq(Orderdetails.PROP_ORDERID,orderid ));
        	     	List<Orderdetails> list=service.findAll(cri);
        	     	for (Orderdetails orderdetails : list)
        	     	{
					    orderdetails.setApprovestatus("varify");
					    orderdetails.setApproveby(new Telecaller(tellecallerid));
					    orderdetails.setFeedback("successfully approved");
					    orderdetails.setApprovedate(new Date());
					    service.add(orderdetails, null);
					
        	     	}
        	     	DetachedCriteria cri1=DetachedCriteria.forClass(Orderdetails.class);
        	        List<Orderdetails> list1=service.findAll(cri1);
        	     	
        	     	
        	     	request.setAttribute("orderlist", list1);
        			} catch (Exception e) {
				e.printStackTrace();
			}		
 		return SUCCESS;
	}

	
	public Object doVarifyorderlist(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
	try {
             DetachedCriteria criteria=DetachedCriteria.forClass(Orderdetails.class);
             criteria.add(Restrictions.eq(Orderdetails.PROP_APPROVESTATUS, "varify"));
             List<Orderdetails> varifyorder=service.findAll(criteria);
             
             request.setAttribute("orderlist", varifyorder);
             
             
           } catch (Exception e) {
		return ERROR;
	}
	return SUCCESS;

}
}
