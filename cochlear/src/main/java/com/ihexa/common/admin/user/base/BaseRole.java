package com.ihexa.common.admin.user.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the role table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="role"
 */

public abstract class BaseRole  implements Serializable {

	public static String REF = "Role";
	public static String PROP_ROLE_DESC = "roleDesc";
	public static String PROP_ROLE_NAME = "roleName";
	public static String PROP_ID = "id";


	// constructors
	public BaseRole () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseRole (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public BaseRole (
		java.lang.Long id,
		java.lang.String roleName) {

		this.setId(id);
		this.setRoleName(roleName);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String roleName;
	private java.lang.String roleDesc;

	// collections
	private java.util.Set<com.ihexa.common.admin.user.User> users;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: RoleName
	 */
	public java.lang.String getRoleName () {
		return roleName;
	}

	/**
	 * Set the value related to the column: RoleName
	 * @param roleName the RoleName value
	 */
	public void setRoleName (java.lang.String roleName) {
		this.roleName = roleName;
	}



	/**
	 * Return the value associated with the column: RoleDesc
	 */
	public java.lang.String getRoleDesc () {
		return roleDesc;
	}

	/**
	 * Set the value related to the column: RoleDesc
	 * @param roleDesc the RoleDesc value
	 */
	public void setRoleDesc (java.lang.String roleDesc) {
		this.roleDesc = roleDesc;
	}



	/**
	 * Return the value associated with the column: users
	 */
	public java.util.Set<com.ihexa.common.admin.user.User> getUsers () {
		return users;
	}

	/**
	 * Set the value related to the column: users
	 * @param users the users value
	 */
	public void setUsers (java.util.Set<com.ihexa.common.admin.user.User> users) {
		this.users = users;
	}

	public void addTousers (com.ihexa.common.admin.user.User user) {
		if (null == getUsers()) setUsers(new java.util.TreeSet<com.ihexa.common.admin.user.User>());
		getUsers().add(user);
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.user.Role)) return false;
		else {
			com.ihexa.common.admin.user.Role role = (com.ihexa.common.admin.user.Role) obj;
			if (null == this.getId() || null == role.getId()) return false;
			else return (this.getId().equals(role.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}