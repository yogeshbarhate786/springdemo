package com.ihexa.common.admin.user.util;

import java.util.HashSet;
import java.util.Set;

import com.ihexa.common.admin.iHexaConstants.IHexaConstants;
import com.ihexa.common.admin.login.Login;
import com.ihexa.common.admin.profile.Profile;
import com.ihexa.common.admin.profile.ProfileToAuthorization;
import com.ihexa.common.admin.profile.service.ProfileService;
import com.ihexa.common.admin.utility.RandomNumber;
import com.prounify.framework.context.Context;
import com.prounify.framework.context.RequestScope;
import com.prounify.framework.context.SessionScope;
import com.prounify.framework.context.UserContext;
import com.prounify.framework.entitlement.AuthoMap;

public class LoginUtility {

	// private static final Map<Integer, String> roleNameMap;
	//
	// static
	// {
	// Map<Integer, String> map = new HashMap<Integer, String>();
	// map.put(0, "REGULAR");
	// map.put(1, "FIRM");
	// map.put(2, "IMPLEMENTATION");
	// map.put(3, "GUEST");
	// roleNameMap = Collections.unmodifiableMap(map);
	// }

	/**
	 * Create a new session along with all the session data items required by the entitlement common service. When the session is created successfully, two elements are inserted in
	 * the HTTP Session: "AUTHO" and "ROLE. Use <code>getAuthorizations</code> to access the associated data for authorizations (this is required since the authorizations are
	 * serialized into a string). Use the <code>SessionHelper></code> to access the associated data for the role.
	 * 
	 * @param login
	 *            Login object to create the session for
	 */
	public void setSessionData(Login login) {

		// Create the session data for this session.
		createSessionData(login);

		// UserContext.getSessionScope().put(AdminConstants.FIRMHOMEPAGEURL,
		// login.getUser().getFirm().getHomePageURL());
	}

	/**
	 * Create the session data record for Autho, Login, User, Firm, Implementation and SessionTimeout
	 * 
	 * @param login
	 *            Login for which operation to be done
	 */
	private void createSessionData(Login login) {

		// Store the login name in HTTP Session
		createLoginSessionData(login);

		// Store the user information in HTTP Session
		createUserSessionData(login);

		// Store the firm information in HTTP Session
		// createFirmSessionData(login);

		// Store the firm information in HTTP Session
		createConstantSessionData();

	}

	/**
	 * Create the session data record for the Constants.
	 */
	private void createConstantSessionData() {
		UserContext.getSessionScope().put("LOGINROLE_ADMINISTRATOR", IHexaConstants.LOGINROLE_ADMINISTRATOR);
		UserContext.getSessionScope().put("LOGINROLE_SUPERVISOR", IHexaConstants.LOGINROLE_SUPERVISOR);
		UserContext.getSessionScope().put("LOGINROLE_AGENT", IHexaConstants.LOGINROLE_AGENT);
		UserContext.getSessionScope().put("LOGINROLE_DEALER", IHexaConstants.LOGINROLE_DEALER);
		UserContext.getSessionScope().put("LOGINROLE_CUSTOMER", IHexaConstants.LOGINROLE_CUSTOMER);
		UserContext.getSessionScope().put("LOGINROLE_DOCTOR", IHexaConstants.LOGINROLE_DOCTOR);
		UserContext.getSessionScope().put("LOGINROLE_TELECALLER", IHexaConstants.LOGINROLE_TELECALLER);
		
		//UserContext.getSessionScope().put("Yogesh_Conststant", RandomNumber.now());
		
	}

	/**
	 * Create the session data record for the current firm (Firm) record.
	 * 
	 * @param login
	 *            Login for which operation to be done
	 */

	// private void createFirmSessionData(Login login) {
	//
	// UserContext.getSessionScope().put("FIRMNAME", login.getUser().getFirm().getName());
	// UserContext.getSessionScope().put("FIRMID", login.getUser().getFirm().getId());
	// UserContext.getSessionScope().put(SessionScope.SELECTED_FIRM_NAME,
	// login.getUser().getFirm().getName());
	// UserContext.getSessionScope().put(SessionScope.SELECTED_FIRM_ID,
	// login.getUser().getFirm().getId());
	//
	// // UserContext.getSessionScope().put("FIRM_EXTERNAL_ID",
	// // login.getUser().getFirm().getExternalID());
	// UserContext.getSessionScope().put("FIRM_PORTAL_NAME",
	// login.getUser().getFirm().getPortalName());
	// // UserContext.getSessionScope().put("FIRM_HELP_URL",
	// // login.getUser().getFirm().getHelpPageURL());
	// }
	/**
	 * Create the session data record for the current user (User) record.
	 * 
	 * @param login
	 *            Login for which operation to be done
	 */
	private void createUserSessionData(Login login) {

		UserContext.getSessionScope().put("USERID", login.getUser().getId());
		UserContext.getSessionScope().put("USERLASTNAME", login.getUser().getPerson().getLastName());
		UserContext.getSessionScope().put("USERFIRSTNAME", login.getUser().getPerson().getFirstName());
		UserContext.getSessionScope().put("USERMIDDLENAME", login.getUser().getPerson().getMiddleName());
		UserContext.getSessionScope().put("ROLE", login.getUser().getRole().getId());
		UserContext.getSessionScope().put("ROLE_NAME", login.getUser().getRole().getRoleName());
		UserContext.getSessionScope().put(SessionScope.PROFILE_ID, login.getProfileId());

		UserContext.getSessionScope().put(RequestScope.AUTHO_MAP, getAuthorizationMap(true));

	}

	/**
	 * Create the session data record for login (Login) record.
	 * 
	 * @param login
	 *            Login for which operation to be done
	 */
	private void createLoginSessionData(Login login) {

		UserContext.getSessionScope().put("LOGINID", login.getId());
		UserContext.getSessionScope().put(SessionScope.LOGIN_NAME, login.getLoginName());
		UserContext.getSessionScope().put("LANGUAGE", login.getLanguage());
		UserContext.getSessionScope().put("COUNTRY", login.getCountry());
		UserContext.getSessionScope().put("VARIANT", login.getVariant());
		UserContext.getSessionScope().put("TIMEZONE", login.getTimezoneOffset());
	}
	/**
	 * Moved method from UserCintect to LoginUtility to make it generic
	 * 
	 * @param forceRefresh
	 * @return
	 */
	private static AuthoMap getAuthorizationMap(boolean forceRefresh) {
		AuthoMap map = (AuthoMap) UserContext.getSessionScope().get(RequestScope.AUTHO_MAP);

		if (forceRefresh || (map == null)) {
			Set<String> authoSet = new HashSet<String>();
			map = new AuthoMap();

			ProfileService service = (ProfileService) Context.getInstance().getBean("ProfileService");
			Profile profile = (Profile) service.findById(UserContext.getProfileId());

			for (ProfileToAuthorization profileToAuthorization : profile.getProfileToAuthorizations()) {
				authoSet.add(profileToAuthorization.getAuthorization().getName());

			}

			map = new AuthoMap(authoSet);

		}

		UserContext.getSessionScope().put(RequestScope.AUTHO_MAP, map);

		return map;
	}
}
