package com.ihexa.common.admin.googlemaps;

import com.ihexa.common.admin.googlemaps.base.BaseGooglemaps;



public class Googlemaps extends BaseGooglemaps {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Googlemaps () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Googlemaps (java.lang.Long id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public Googlemaps (
		java.lang.Long id,
		java.lang.String poly) {

		super (
			id,
			poly);
	}

/*[CONSTRUCTOR MARKER END]*/


}