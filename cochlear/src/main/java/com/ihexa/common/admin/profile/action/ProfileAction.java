package com.ihexa.common.admin.profile.action;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.validation.DataBinder;

import com.ihexa.common.admin.authorization.service.AuthorizationService;
import com.ihexa.common.admin.profile.Authorization;
import com.ihexa.common.admin.profile.Profile;
import com.ihexa.common.admin.profile.ProfileToAuthorization;
import com.ihexa.common.admin.profile.service.ProfileService;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.base.action.BindingActionForm;
import com.prounify.framework.base.service.Service;
import com.prounify.framework.context.Context;

public class ProfileAction extends BaseCRUDAction {

	/**
	 * to save new profile
	 * 
	 * @return
	 */
	@Override
	public Object doCreate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		BindingActionForm actionForm = (BindingActionForm) form;

		ActionMessages messages = new ActionMessages();
		messages.clear();
		ActionMessage msg = null;
		try {
			super.doCreate(mapping, form, request, response);
			msg = new ActionMessage("profile.SAVE_SUCCESS");
			messages.add("success", msg);
			saveMessages(request, messages);
			return RETURN_SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			// actionForm.expose(binder.getBindingResult(), request, e);
			msg = new ActionMessage("profile.SAVE_FAIL", e.getCause());
			messages.add("error", msg);
			saveMessages(request, messages);
			return ERROR;
		}
	}

	/**
	 * to redirect to profile add page
	 * 
	 * @return
	 */
	@Override
	public Object doAdd(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		ProfileService profileService = (ProfileService) getService(mapping);

		// TODO Auto-generated method stub
		request.setAttribute("roleList", profileService.getRoleList());
		return super.doAdd(mapping, form, request, response);
	}

	/**
	 * to redirect to profile edit page
	 * 
	 * @return
	 */
	@Override
	public Object doEdit(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		AuthorizationService authorizationService = (AuthorizationService) (Context.getInstance().getBean("AuthorizationService"));
		DetachedCriteria criteria = DetachedCriteria.forClass(com.ihexa.common.admin.authorization.Authorization.class);
		List<com.ihexa.common.admin.authorization.Authorization> authorization = authorizationService.findAll(criteria);

		Service service = getService(mapping);
		String ID = request.getParameter("id");
		Profile profile = (Profile) service.findById(new Long(ID));

		for (ProfileToAuthorization profileToAuthorization : profile.getProfileToAuthorizations()) {

			for (com.ihexa.common.admin.authorization.Authorization auth : authorization) {

				if (profileToAuthorization.getAuthorization().getId().longValue() == auth.getId().longValue()) {
					auth.setChecked(true);

					break;
				}
			}
		}

		request.setAttribute("AUTHO_LIST", authorization);
		request.setAttribute(mapping.getDataAttribute(), profile);

		return RETURN_SUCCESS;

	}

	/**
	 * to view profile
	 */
	public Object doView(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		AuthorizationService authorizationService = (AuthorizationService) (Context.getInstance().getBean("AuthorizationService"));
		DetachedCriteria criteria = DetachedCriteria.forClass(com.ihexa.common.admin.authorization.Authorization.class);
		List<com.ihexa.common.admin.authorization.Authorization> authorization = authorizationService.findAll(criteria);

		Service service = getService(mapping);
		String ID = request.getParameter("id");
		Profile profile = (Profile) service.findById(new Long(ID));

		for (ProfileToAuthorization profileToAuthorization : profile.getProfileToAuthorizations()) {

			for (com.ihexa.common.admin.authorization.Authorization auth : authorization) {

				if (profileToAuthorization.getAuthorization().getId().longValue() == auth.getId().longValue()) {
					auth.setChecked(true);

					break;
				}
			}
		}

		request.setAttribute("AUTHO_LIST", authorization);
		request.setAttribute(mapping.getDataAttribute(), profile);

		return RETURN_SUCCESS;
	}
	/**
	 * to update profile
	 * 
	 * @return
	 */
	@Override
	public Object doUpdate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		AuthorizationService authorizationService = (AuthorizationService) (Context.getInstance().getBean("AuthorizationService"));
		DetachedCriteria criteria = DetachedCriteria.forClass(com.ihexa.common.admin.authorization.Authorization.class);
		List<com.ihexa.common.admin.authorization.Authorization> authorizations = authorizationService.findAll(criteria);

		Service service = getService(mapping);
		String ID = request.getParameter("id");
		Profile profile = (Profile) service.findById(new Long(ID));
		DataBinder binder = getBinder(profile, mapping, request);

		Set<ProfileToAuthorization> newAuthoList = new HashSet<ProfileToAuthorization>();

		for (com.ihexa.common.admin.authorization.Authorization auth : authorizations) {
			String value = request.getParameter("PROFILE_AUTHO_" + auth.getId());

			if (value != null) {

				ProfileToAuthorization profileToAuthorization = new ProfileToAuthorization();
				Authorization authorization = new Authorization();

				authorization.setId(auth.getId());
				profileToAuthorization.setProfile(profile);
				profileToAuthorization.setAuthorization(authorization);

				newAuthoList.add(profileToAuthorization);
			}
		}

		profile.setProfileToAuthorizations(newAuthoList);

		BindingActionForm actionForm = (BindingActionForm) form;
		ActionMessages messages = new ActionMessages();
		messages.clear();
		ActionMessage msg = null;
		try {
			service.update(profile, null);
			msg = new ActionMessage("profile.UPDATE_SUCCESS");
			messages.add("success", msg);
			saveMessages(request, messages);
			return RETURN_SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			// actionForm.expose(binder.getBindingResult(), request, e);
			msg = new ActionMessage("profile.UPDATE_FAIL", e.getCause());
			messages.add("error", msg);
			saveMessages(request, messages);
			return ERROR;
		}
	}

	/**
	 * to delete Profile
	 * 
	 * @param
	 * @return
	 */
	@Override
	public Object doDelete(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		BindingActionForm actionForm = (BindingActionForm) form;

		ActionMessages messages = new ActionMessages();
		messages.clear();
		ActionMessage msg = null;
		try {
			super.doDelete(mapping, form, request, response);
			msg = new ActionMessage("profile.DELETE_SUCCESS");
			messages.add("success", msg);
			saveMessages(request, messages);
			return RETURN_SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			// actionForm.expose(binder.getBindingResult(), request, e);
			msg = new ActionMessage("profile.DELETE_FAIL", e.getCause());
			messages.add("error", msg);
			saveMessages(request, messages);
			return ERROR;
		}
	}
}
