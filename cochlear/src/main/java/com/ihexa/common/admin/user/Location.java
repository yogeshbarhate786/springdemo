package com.ihexa.common.admin.user;

import com.ihexa.common.admin.user.base.BaseLocation;

public class Location extends BaseLocation {
	private static final long serialVersionUID = 1L;

	/* [CONSTRUCTOR MARKER BEGIN] */
	public Location() {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Location(java.lang.Long id) {
		super(id);
	}

	/* [CONSTRUCTOR MARKER END] */

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ihexa.common.admin.user.base.BaseLocation#initialize()
	 */
	@Override
	protected void initialize() {
		// TODO Auto-generated method stub
		this.setCountry(new Country());
		this.setCity(new City());
		this.setState(new State());
		super.initialize();
	}

}