package com.ihexa.common.admin.dashboard.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.ihexa.common.admin.cart.Cart;
import com.ihexa.common.admin.cart.Carttrack;
import com.ihexa.common.admin.dashboard.Pricelist;
import com.ihexa.common.admin.gift.service.GiftService;
import com.ihexa.common.admin.iHexaConstants.IHexaConstants;
import com.ihexa.common.admin.order.OrderCart;
import com.ihexa.common.admin.order.Orderdetails;
import com.ihexa.common.admin.productsku.Category;
import com.ihexa.common.admin.productsku.Maincategory;
import com.ihexa.common.admin.productsku.Product;
import com.ihexa.common.admin.ticket.Ticket;
import com.ihexa.common.admin.ticket.action.TicketAction;
import com.ihexa.common.admin.user.Role;
import com.ihexa.common.admin.user.User;
import com.ihexa.common.admin.user.service.UserService;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.context.Context;
import com.prounify.framework.context.UserContext;

public class DashBoardAction extends BaseCRUDAction {
	
	/*com.ihexa.common.admin.order.action.OrderAction ord = new com.ihexa.common.admin.order.action.OrderAction();
	int m = ord.ranumber;*/
	
/**
	 * Method for allowing other .do action other than create,edit,update,list
	 */
	GiftService service = (GiftService) Context.getInstance().getBean("GiftService");
	protected boolean isHttpMethodAllowed(String httpMethod, String methodId) {

		if (httpMethod.equals(HTTP_METHOD_GET)) {

			if (methodId.equals("getMessages"))
				return true;
			if (methodId.equals("getCustomerTask"))
				return true;
		   if (methodId.equals("getComplaintTask"))
				return true;
			if (methodId.equals("getPendingTaskList"))
				return true;
			if (methodId.equals("getCompletedTaskList"))
				return true;
			if (methodId.equals("getComplaintList"))
				return true;
		}
		return super.isHttpMethodAllowed(httpMethod, methodId);
	}

	public Object doView(BaseActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		

	if ("true".equals(request.getParameter("ajax"))) {
			return RETURN_SUCCESS_AJAX;
		} else {
			
         String search=request.getParameter("txtsearch"); 
		
         String minprice=request.getParameter("select1"); 
         String maxprice=request.getParameter("select2"); 
         
         String sortid=request.getParameter("sortid");
           
           DetachedCriteria criobj=DetachedCriteria.forClass(Pricelist.class);
            criobj.addOrder(Order.asc(Pricelist.PROP_PRICE));
            List<Pricelist> listprice=service.findAll(criobj);
            request.setAttribute("PriceList", listprice);
			
			  DetachedCriteria criteria=DetachedCriteria.forClass(Maincategory.class);
			List<Maincategory> maincategorylist = service.findAll(criteria);
			request.setAttribute("categoryList", maincategorylist);
			
			 criteria=DetachedCriteria.forClass(Category.class);
			 List<Category>categorylist = service.findAll(criteria);
			 request.setAttribute("subCategoryList", categorylist);
			 if(sortid!=null)
			 {
			   if(sortid.equals("lowtohigh"))
			   {
				   criteria=DetachedCriteria.forClass(Product.class);
				   criteria.addOrder(Order.asc(Pricelist.PROP_PRICE));
				    List<Product> products = service.findAll(criteria);
					request.setAttribute("ProductsList", products);   
			   }else
			   if(sortid.equals("hightolow"))
			   {
				   criteria=DetachedCriteria.forClass(Product.class);
				   criteria.addOrder(Order.desc(Pricelist.PROP_PRICE));
				    List<Product> products = service.findAll(criteria);
					request.setAttribute("ProductsList", products);      
			   
			   }else
				   if(sortid.equals("newest"))
				   {
				   criteria=DetachedCriteria.forClass(Product.class);
				   criteria.addOrder(Order.desc(Pricelist.PROP_ID));
				    List<Product> products = service.findAll(criteria);
					request.setAttribute("ProductsList", products);        
			   }
			 }
			 else
			 {	 
			if(minprice!=null)
			 {
				 criteria=DetachedCriteria.forClass(Product.class);
					criteria.add(Restrictions.ge(Product.PROP_PRICE,Integer.parseInt(minprice)));
					criteria.add(Restrictions.le(Product.PROP_PRICE,Integer.parseInt(maxprice)));
				  List<Product> products = service.findAll(criteria);
					request.setAttribute("ProductsList", products); 
			 }
			 else
			 {
			 if(search!=null)
			 {
			  criteria=DetachedCriteria.forClass(Product.class);
				criteria.add(Restrictions.ilike(Product.PROP_NAME, search, MatchMode.ANYWHERE));
			  List<Product> products = service.findAll(criteria);
				request.setAttribute("ProductsList", products);
			 }
			 else 
			 {
				 criteria=DetachedCriteria.forClass(Product.class);
				 List<Product> products = service.findAll(criteria);
				 request.setAttribute("ProductsList", products); 
			 }
			 }
			
			 }
			 
			 DetachedCriteria criteria1 = DetachedCriteria.forClass(Cart.class);
			 criteria1.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
				criteria1.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
				List<Cart> cartlist = service.findAll(criteria1);
				
				
				request.setAttribute("CartList", cartlist);
				request.setAttribute("CartListCount", cartlist.size());
				
				 DetachedCriteria cri=DetachedCriteria.forClass(Orderdetails.class);
				   cri.add(Restrictions.eq(Orderdetails.PROP_STATUS, "Success"));
				/*   cri.add(Restrictions.eq(Orderdetails.PROP_APPROVESTATUS, "Pendding"));*/
				   List<Orderdetails> orderlist=service.findAll(cri);
				
				   DetachedCriteria criticket=DetachedCriteria.forClass(Ticket.class);
				   criticket.add(Restrictions.eq(Ticket.PROP_STATUS, "Active"));
				   List<Ticket> ticketlist=service.findAll(criticket);
				  
				   request.setAttribute("ordercount",orderlist.size());
				   request.setAttribute("ticketcount",ticketlist.size());
			
			return RETURN_SUCCESS;
		}
	}

	public Date getLargeDate(Date date1, Date date2) {

		if (date1 != null && date2 != null) {
			if (date1.after(date2))
				return date1;
			else
				return date2;
		} else if (date1 != null && date2 == null) {
			return date1;
		} else if (date1 == null && date2 != null) {
			return date2;
		} else
			return null;

	}
	
	public String getStringdemo(String name)
	{
		
		return name;
	}
	

	@Override
	public Object doList(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		  String search=request.getParameter("id");
		  List<Category>categorylist=null;
		  
		  String minprice=request.getParameter("select1"); 
	         String maxprice=request.getParameter("select2"); 
	           
	         DetachedCriteria criobj=DetachedCriteria.forClass(Pricelist.class);
	         criobj.addOrder(Order.asc(Pricelist.PROP_PRICE));
	         List<Pricelist> listprice=service.findAll(criobj);
	        request.setAttribute("PriceList", listprice);
	        
	        if(minprice!=null)
			 {
	        	DetachedCriteria criteria=DetachedCriteria.forClass(Product.class);
					criteria.add(Restrictions.ge(Product.PROP_PRICE,Integer.parseInt(minprice)));
					criteria.add(Restrictions.le(Product.PROP_PRICE,Integer.parseInt(maxprice)));
				  List<Product> products = service.findAll(criteria);
					request.setAttribute("ProductsList", products); 
			 }
	   
		
		  if(search!=null)
		  {
		       DetachedCriteria criteria=DetachedCriteria.forClass(Category.class);
		       criteria.add(Restrictions.eq(Category.PROP_ID, new Long(search)));
		       categorylist = service.findAll(criteria);
		  }
		  else
		  {
			   DetachedCriteria criteria=DetachedCriteria.forClass(Category.class);
			    criteria.add(Restrictions.eq(Category.PROP_ID, new Long(1)));
			    categorylist = service.findAll(criteria);  
		  }
		    
			
			DetachedCriteria criteria1=DetachedCriteria.forClass(Maincategory.class);
			List<Maincategory> maincategorylist = service.findAll(criteria1);
			request.setAttribute("categoryList", maincategorylist);
			
			 criteria1=DetachedCriteria.forClass(Category.class);
			 List<Category>categorylist1 = service.findAll(criteria1);
			 request.setAttribute("subCategoryList", categorylist1);
			 
			 
			 if(search!=null)
			 {
			  criteria1=DetachedCriteria.forClass(Product.class);
				criteria1.add(Restrictions.eq(Product.PROP_CATID+"."+Category.PROP_ID,new Long(categorylist.get(0).getId() )));
			  List<Product> products = service.findAll(criteria1);
				request.setAttribute("ProductsList", products);
			 }
			 else
			 {
				 criteria1=DetachedCriteria.forClass(Product.class);
				 List<Product> products = service.findAll(criteria1);
				 request.setAttribute("ProductsList", products); 
			 }
		   
			    DetachedCriteria criteria11 = DetachedCriteria.forClass(Cart.class);
			    criteria1.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
				criteria11.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
				List<Cart> cartlist = service.findAll(criteria11);
				
				request.setAttribute("CartList", cartlist);
				request.setAttribute("CartListCount", cartlist.size());
		    
		    
		  
		return SUCCESS;
	}

	

}
