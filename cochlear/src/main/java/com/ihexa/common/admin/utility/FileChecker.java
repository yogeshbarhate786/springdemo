package com.ihexa.common.admin.utility;

import java.util.Arrays;

public class FileChecker {

	private static String[] image_extension = { "jpg", "gif", "png", "jpeg", "bmp" };
	private static String[] audio_extension = { "mp3", "aac", "amr", "ogg", "wav" };
	private static String[] video_extension = { "mp4", "avi", "flv", "wmv", "mpeg" };
	private static String[] doc_extension = { "pdf", "txt", "doc", "ppt", "xls", "xps", "pub", "docx", "pptx", "xlsx" };
	private static String[] programing_extension = { "html", "htm", "css", "js", "java", "jsp", "sql", "xml" };

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(getFileType("TXT"));
	}

	public static String getFileType(String extension) {
		if (extension != null) {
			extension = extension.toLowerCase();
			if (Arrays.asList(image_extension).contains(extension)) {
				return "Image";
			} else if (Arrays.asList(audio_extension).contains(extension)) {
				return "Audio";
			} else if (Arrays.asList(video_extension).contains(extension)) {
				return "Video";
			} else if (Arrays.asList(doc_extension).contains(extension)) {
				return "Document";
			} else if (Arrays.asList(programing_extension).contains(extension)) {
				return "Document";
			}
		}

		return null;
	}

}
