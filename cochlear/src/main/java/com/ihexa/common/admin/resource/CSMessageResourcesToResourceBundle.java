package com.ihexa.common.admin.resource;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.struts.util.MessageResources;
import org.apache.struts.util.MessageResourcesFactory;

/**
 * 
 * This class allow a standard Resource Bundle (ResourceBundle class) to be used as a strut resource
 * bundle (MessageResource class).
 * 
 * This class is intanciated by the CSMessageResFactory class.
 * 
 * The getMessage method instantiate the configured standard resource bundle associated with the
 * received locale add redirect the call to this class.
 * 
 * TODO: Actually, the getMessage method uses a locale that correspond to user browser. It should
 * use the user locale configured in CSLogin table.
 */
public class CSMessageResourcesToResourceBundle extends MessageResources {

	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 3544958765888452149L;

	/**
	 * Version string (RCS Id)
	 */
	private static final String VERSION = "$Id: CSMessageResourcesToResourceBundle.java 40953 2007-10-16 13:19:18Z martin.goulet $";

	/**
	 * Constructor
	 * 
	 * @param factory
	 * @param config
	 */
	public CSMessageResourcesToResourceBundle(MessageResourcesFactory factory, String config) {
		super(factory, config);
	}

	/**
	 * Return the version number for this class.
	 * 
	 */
	public static String getVersion() {
		return VERSION;
	}

	/**
	 * This method instanciate the appropriate resource bundle class and get the associate message.
	 * This class is called by the strut validator in case of error when validating a form bean.
	 * 
	 * @param locale
	 * @param key
	 */
	public String getMessage(Locale locale, String key) {

		ResourceBundle r;

		r = ResourceBundle.getBundle(this.getConfig(), locale);

		try {
			return r.getString(key);
		} catch (Exception e) {

			if (this.returnNull) {
				return "";
			} else {
				return "???" + key + "???";
			}
		}
	}
}
