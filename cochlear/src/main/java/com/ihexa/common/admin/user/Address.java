package com.ihexa.common.admin.user;

import com.ihexa.common.admin.user.base.BaseAddress;

public class Address extends BaseAddress {
	private static final long serialVersionUID = 1L;

	/* [CONSTRUCTOR MARKER BEGIN] */
	public Address () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Address (java.lang.Long id) {
		super(id);
	}

	/* [CONSTRUCTOR MARKER END] */

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ihexa.common.admin.user.base.BaseAddress#initialize()
	 */
	@Override
	protected void initialize() {
		// TODO Auto-generated method stub
		this.setCity(new City());
		this.setState(new State());
		this.setCountry(new Country());
	//	this.setLocation(new Location());
		super.initialize();
	}

}