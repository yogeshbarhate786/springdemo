package com.ihexa.common.admin.person;

import com.ihexa.common.admin.person.base.BaseAddress;



public class Address extends BaseAddress {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Address () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Address (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/

	@Override
	protected void initialize() {
		// TODO Auto-generated method stub it is necessary to initialise it otherwise it throw null property exception
		super.initialize();
		this.setCountry(new Country());
		this.setState(new State());
		this.setCity(new City());
		
		

}
}