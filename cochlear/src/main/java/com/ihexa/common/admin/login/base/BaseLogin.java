package com.ihexa.common.admin.login.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the login table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="login"
 */

public abstract class BaseLogin  implements Serializable {

	public static String REF = "Login";
	public static String PROP_USER = "user";
	public static String PROP_STATUS_PASSWORD_RESET = "statusPasswordReset";
	public static String PROP_DESCRIPTION = "description";
	public static String PROP_LOGIN_ROLE = "loginRole";
	public static String PROP_NB_FAILED_LOGIN = "nbFailedLogin";
	public static String PROP_STATUS_ACTIVE = "statusActive";
	public static String PROP_LOGIN_NAME = "loginName";
	public static String PROP_PASSWORD = "password";
	public static String PROP_PWD_EXPIRATION_DATE = "pwdExpirationDate";
	public static String PROP_SESSION_TIMEOUT = "sessionTimeout";
	public static String PROP_PROFILE_ID = "profileId";
	public static String PROP_CHALLENGE_ANSWER = "challengeAnswer";
	public static String PROP_COUNTRY = "country";
	public static String PROP_TIMEZONE_OFFSET = "timezoneOffset";
	public static String PROP_LAST_LOGIN_DATE = "lastLoginDate";
	public static String PROP_CHALLENGE_QUESTION = "challengeQuestion";
	public static String PROP_SESSION_TIMEOUT_NOTIF = "sessionTimeoutNotif";
	public static String PROP_ID = "id";
	public static String PROP_STATUS_ACTIVE_DATE = "statusActiveDate";
	public static String PROP_LANGUAGE = "language";
	public static String PROP_VARIANT = "variant";
	public static String PROP_STATUS_PASSWORD_RESET_DATE = "statusPasswordResetDate";


	// constructors
	public BaseLogin () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseLogin (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public BaseLogin (
		java.lang.Long id,
		java.util.Date lastLoginDate,
		java.lang.String loginName,
		java.lang.String password,
		java.util.Date pwdExpirationDate,
		java.util.Date statusActiveDate,
		java.util.Date statusPasswordResetDate) {

		this.setId(id);
		this.setLastLoginDate(lastLoginDate);
		this.setLoginName(loginName);
		this.setPassword(password);
		this.setPwdExpirationDate(pwdExpirationDate);
		this.setStatusActiveDate(statusActiveDate);
		this.setStatusPasswordResetDate(statusPasswordResetDate);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String challengeAnswer;
	private java.lang.String challengeQuestion;
	private java.lang.String description;
	private java.lang.String language;
	private java.lang.String country;
	private java.lang.String variant;
	private java.util.Date lastLoginDate;
	private java.lang.String loginName;
	private java.lang.Long nbFailedLogin;
	private java.lang.String password;
	private java.util.Date pwdExpirationDate;
	private java.lang.Integer loginRole;
	private java.lang.Integer statusActive;
	private java.util.Date statusActiveDate;
	private java.lang.Integer statusPasswordReset;
	private java.util.Date statusPasswordResetDate;
	private java.lang.Long sessionTimeout;
	private java.lang.Long sessionTimeoutNotif;
	private java.lang.Double timezoneOffset;
	private java.lang.Long profileId;

	// many to one
	private com.ihexa.common.admin.user.User user;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: ChallengeAnswer
	 */
	public java.lang.String getChallengeAnswer () {
		return challengeAnswer;
	}

	/**
	 * Set the value related to the column: ChallengeAnswer
	 * @param challengeAnswer the ChallengeAnswer value
	 */
	public void setChallengeAnswer (java.lang.String challengeAnswer) {
		this.challengeAnswer = challengeAnswer;
	}



	/**
	 * Return the value associated with the column: ChallengeQuestion
	 */
	public java.lang.String getChallengeQuestion () {
		return challengeQuestion;
	}

	/**
	 * Set the value related to the column: ChallengeQuestion
	 * @param challengeQuestion the ChallengeQuestion value
	 */
	public void setChallengeQuestion (java.lang.String challengeQuestion) {
		this.challengeQuestion = challengeQuestion;
	}



	/**
	 * Return the value associated with the column: Description
	 */
	public java.lang.String getDescription () {
		return description;
	}

	/**
	 * Set the value related to the column: Description
	 * @param description the Description value
	 */
	public void setDescription (java.lang.String description) {
		this.description = description;
	}



	/**
	 * Return the value associated with the column: Language
	 */
	public java.lang.String getLanguage () {
		return language;
	}

	/**
	 * Set the value related to the column: Language
	 * @param language the Language value
	 */
	public void setLanguage (java.lang.String language) {
		this.language = language;
	}



	/**
	 * Return the value associated with the column: Country
	 */
	public java.lang.String getCountry () {
		return country;
	}

	/**
	 * Set the value related to the column: Country
	 * @param country the Country value
	 */
	public void setCountry (java.lang.String country) {
		this.country = country;
	}



	/**
	 * Return the value associated with the column: Variant
	 */
	public java.lang.String getVariant () {
		return variant;
	}

	/**
	 * Set the value related to the column: Variant
	 * @param variant the Variant value
	 */
	public void setVariant (java.lang.String variant) {
		this.variant = variant;
	}



	/**
	 * Return the value associated with the column: LastLoginDate
	 */
	public java.util.Date getLastLoginDate () {
		return lastLoginDate;
	}

	/**
	 * Set the value related to the column: LastLoginDate
	 * @param lastLoginDate the LastLoginDate value
	 */
	public void setLastLoginDate (java.util.Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}



	/**
	 * Return the value associated with the column: LoginName
	 */
	public java.lang.String getLoginName () {
		return loginName;
	}

	/**
	 * Set the value related to the column: LoginName
	 * @param loginName the LoginName value
	 */
	public void setLoginName (java.lang.String loginName) {
		this.loginName = loginName;
	}



	/**
	 * Return the value associated with the column: NbFailedLogin
	 */
	public java.lang.Long getNbFailedLogin () {
		return nbFailedLogin;
	}

	/**
	 * Set the value related to the column: NbFailedLogin
	 * @param nbFailedLogin the NbFailedLogin value
	 */
	public void setNbFailedLogin (java.lang.Long nbFailedLogin) {
		this.nbFailedLogin = nbFailedLogin;
	}



	/**
	 * Return the value associated with the column: Password
	 */
	public java.lang.String getPassword () {
		return password;
	}

	/**
	 * Set the value related to the column: Password
	 * @param password the Password value
	 */
	public void setPassword (java.lang.String password) {
		this.password = password;
	}



	/**
	 * Return the value associated with the column: PwdExpirationDate
	 */
	public java.util.Date getPwdExpirationDate () {
		return pwdExpirationDate;
	}

	/**
	 * Set the value related to the column: PwdExpirationDate
	 * @param pwdExpirationDate the PwdExpirationDate value
	 */
	public void setPwdExpirationDate (java.util.Date pwdExpirationDate) {
		this.pwdExpirationDate = pwdExpirationDate;
	}



	/**
	 * Return the value associated with the column: LoginRole
	 */
	public java.lang.Integer getLoginRole () {
		return loginRole;
	}

	/**
	 * Set the value related to the column: LoginRole
	 * @param loginRole the LoginRole value
	 */
	public void setLoginRole (java.lang.Integer loginRole) {
		this.loginRole = loginRole;
	}



	/**
	 * Return the value associated with the column: StatusActive
	 */
	public java.lang.Integer getStatusActive () {
		return statusActive;
	}

	/**
	 * Set the value related to the column: StatusActive
	 * @param statusActive the StatusActive value
	 */
	public void setStatusActive (java.lang.Integer statusActive) {
		this.statusActive = statusActive;
	}



	/**
	 * Return the value associated with the column: StatusActiveDate
	 */
	public java.util.Date getStatusActiveDate () {
		return statusActiveDate;
	}

	/**
	 * Set the value related to the column: StatusActiveDate
	 * @param statusActiveDate the StatusActiveDate value
	 */
	public void setStatusActiveDate (java.util.Date statusActiveDate) {
		this.statusActiveDate = statusActiveDate;
	}



	/**
	 * Return the value associated with the column: StatusPasswordReset
	 */
	public java.lang.Integer getStatusPasswordReset () {
		return statusPasswordReset;
	}

	/**
	 * Set the value related to the column: StatusPasswordReset
	 * @param statusPasswordReset the StatusPasswordReset value
	 */
	public void setStatusPasswordReset (java.lang.Integer statusPasswordReset) {
		this.statusPasswordReset = statusPasswordReset;
	}



	/**
	 * Return the value associated with the column: StatusPasswordResetDate
	 */
	public java.util.Date getStatusPasswordResetDate () {
		return statusPasswordResetDate;
	}

	/**
	 * Set the value related to the column: StatusPasswordResetDate
	 * @param statusPasswordResetDate the StatusPasswordResetDate value
	 */
	public void setStatusPasswordResetDate (java.util.Date statusPasswordResetDate) {
		this.statusPasswordResetDate = statusPasswordResetDate;
	}



	/**
	 * Return the value associated with the column: SessionTimeout
	 */
	public java.lang.Long getSessionTimeout () {
		return sessionTimeout;
	}

	/**
	 * Set the value related to the column: SessionTimeout
	 * @param sessionTimeout the SessionTimeout value
	 */
	public void setSessionTimeout (java.lang.Long sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}



	/**
	 * Return the value associated with the column: SessionTimeoutNotif
	 */
	public java.lang.Long getSessionTimeoutNotif () {
		return sessionTimeoutNotif;
	}

	/**
	 * Set the value related to the column: SessionTimeoutNotif
	 * @param sessionTimeoutNotif the SessionTimeoutNotif value
	 */
	public void setSessionTimeoutNotif (java.lang.Long sessionTimeoutNotif) {
		this.sessionTimeoutNotif = sessionTimeoutNotif;
	}



	/**
	 * Return the value associated with the column: TimezoneOffset
	 */
	public java.lang.Double getTimezoneOffset () {
		return timezoneOffset;
	}

	/**
	 * Set the value related to the column: TimezoneOffset
	 * @param timezoneOffset the TimezoneOffset value
	 */
	public void setTimezoneOffset (java.lang.Double timezoneOffset) {
		this.timezoneOffset = timezoneOffset;
	}



	/**
	 * Return the value associated with the column: ProfileID
	 */
	public java.lang.Long getProfileId () {
		return profileId;
	}

	/**
	 * Set the value related to the column: ProfileID
	 * @param profileId the ProfileID value
	 */
	public void setProfileId (java.lang.Long profileId) {
		this.profileId = profileId;
	}



	/**
	 * Return the value associated with the column: UserID
	 */
	public com.ihexa.common.admin.user.User getUser () {
		return user;
	}

	/**
	 * Set the value related to the column: UserID
	 * @param user the UserID value
	 */
	public void setUser (com.ihexa.common.admin.user.User user) {
		this.user = user;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.login.Login)) return false;
		else {
			com.ihexa.common.admin.login.Login login = (com.ihexa.common.admin.login.Login) obj;
			if (null == this.getId() || null == login.getId()) return false;
			else return (this.getId().equals(login.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}