package com.ihexa.common.admin.cart.action;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;



import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ihexa.common.admin.cart.Cart;
import com.ihexa.common.admin.cart.Carttrack;
import com.ihexa.common.admin.doctor.Doctor;
import com.ihexa.common.admin.gift.service.GiftService;
import com.ihexa.common.admin.order.OrderCart;
import com.ihexa.common.admin.productsku.Product;
import com.ihexa.common.admin.utility.RandomNumber;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.context.Context;
import com.prounify.framework.context.UserContext;

public class CartAction extends BaseCRUDAction {
	
/*	com.ihexa.common.admin.order.action.OrderAction ord = new com.ihexa.common.admin.order.action.OrderAction();
	int m = ord.ranumber;
*/
	public String ranumber=null;
	
	protected boolean isHttpMethodAllowed(String httpMethod, String methodId) {

		if (httpMethod.equals(HTTP_METHOD_GET)) {

			if (methodId.equals("create"))
				return true;
			if (methodId.equals("changecart"))
				return true;
		}
		return super.isHttpMethodAllowed(httpMethod, methodId);
	}

	GiftService service = (GiftService) Context.getInstance().getBean("GiftService");
      
	@Override
	public Object doAdd(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		try {

			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}

	}
          
	@Override
	public Object doCreate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		try {
            
			Long userid = (Long) UserContext.getSessionScope().get("USERID");

			DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
			criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
			List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
			request.setAttribute("UserEditList", listobj.get(0));
              if(listobj.get(0).getDoctorid()!=null)
              {
			Long doctorid = listobj.get(0).getDoctorid().getId();
			String productid = request.getParameter("productid");

			DetachedCriteria criteria = DetachedCriteria.forClass(Cart.class);
			criteria.add(Restrictions.eq(Cart.PROP_PRODUCTID + "." + Product.PROP_ID, new Long(productid)));
			criteria.add(Restrictions.eq(Cart.PROP_DOCTORID + "." + Doctor.PROP_ID, doctorid));
			criteria.add(Restrictions.isNull(Cart.PROP_BILL));
			criteria.add(Restrictions.isNull(Cart.PROP_REMOVE_FROM_CART_TIME));
			criteria.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
		    List<Cart> list = service.findAll(criteria);
			if (list.isEmpty()) {
				DetachedCriteria critobj1=DetachedCriteria.forClass(Carttrack.class);
				critobj1.add(Restrictions.eq(Carttrack.PROP_USERID+"."+Doctor.PROP_ID,doctorid ));
				critobj1.add(Restrictions.eq(Carttrack.PROP_STATUS, "Active"));
				List<Carttrack> track11=service.findAll(critobj1);
				if(track11.size()==0)
				{
					ranumber="CART"+RandomNumber.now();
					Carttrack trackobj=new Carttrack();
					trackobj.setOrderid(null);
				    trackobj.setUserid(new Doctor(doctorid));
					trackobj.setUniqueid(ranumber);
					trackobj.setStatus("Active");
					service.add(trackobj, null);
				
				}
				else
				{
					ranumber=track11.get(0).getUniqueid();
				}
				/*DetachedCriteria critob=DetachedCriteria.forClass(Product.class);
				critob.add(Restrictions.eq(propertyName, value))*/
				
				
				Cart cart = new Cart();
				cart.setProductid(new Product(new Long(productid)));
				cart.setDoctorid(new Doctor(new Long(doctorid)));
				cart.setAddedToCartTime(new Date());
				cart.setQuantity(new Integer(1));
				cart.setStatus("Active");
				cart.setCartsuccessstatus("Deactive");
				cart.setCartid(ranumber);
				/*cart.setGst(gst)*/
			   Date dt = new Date();
				Calendar c = Calendar.getInstance();
				c.setTime(dt);
				c.add(Calendar.DATE, 5);
				dt = c.getTime();
				cart.setProductDeliveryDate(dt);
				service.add(cart, null);
				
				} else {
				int quantity = 0;
				for (Cart cart : list) {
					if (cart.getQuantity() != null) {
						quantity = cart.getQuantity();
						quantity = quantity + 1;
						cart.setQuantity(quantity);
						service.add(cart, null);
					}

				}
			}
			DetachedCriteria criteria3 = DetachedCriteria.forClass(Cart.class);
			criteria3.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
			criteria3.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
			criteria3.add(Restrictions.eq(Cart.PROP_CARTID,ranumber));
			criteria3.add(Restrictions.eq(Cart.PROP_DOCTORID + "." + Doctor.PROP_ID, doctorid));
		    List<Cart> cartlist = service.findAll(criteria3);
          
			request.setAttribute("CartList", cartlist);
			request.setAttribute("CartListCount", cartlist.size());
			
			
			
			 PrintWriter out = response.getWriter();
             Gson gson = new Gson();
			JsonObject jsonObject = new JsonObject(); 
			JsonElement element = gson.toJsonTree(cartlist.size()); 
			
			jsonObject.addProperty("success", true); 
			jsonObject.add("cartlist", element);
			
			out.println(jsonObject.toString()); 
			out.close();
           }
			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}

	}

	@Override
	public Object doList(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		try {
			
			Long userid = (Long) UserContext.getSessionScope().get("USERID");

			DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
			criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
			List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
			request.setAttribute("UserEditList", listobj.get(0));

			Long doctorid = listobj.get(0).getDoctorid().getId();
			
			DetachedCriteria critobj1=DetachedCriteria.forClass(Carttrack.class);
			critobj1.add(Restrictions.eq(Carttrack.PROP_USERID+"."+Doctor.PROP_ID,doctorid ));
			critobj1.add(Restrictions.eq(Carttrack.PROP_STATUS, "Active"));
			List<Carttrack> list=service.findAll(critobj1);
           if(list.size()>0)
           {
        	if(list.get(0).getUniqueid()!=null)
        	{
			DetachedCriteria criteria = DetachedCriteria.forClass(Cart.class);
			criteria.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
			criteria.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
			criteria.add(Restrictions.eq(Cart.PROP_CARTID,list.get(0).getUniqueid()));
			criteria.add(Restrictions.isNull(Cart.PROP_REMOVE_FROM_CART_TIME));
			criteria.add(Restrictions.eq(Cart.PROP_DOCTORID + "." + Doctor.PROP_ID, doctorid));
			List<Cart> cartlist = service.findAll(criteria);
			request.setAttribute("CartList", cartlist);
			request.setAttribute("CartListCount", cartlist.size());
           }
           }	
           else
           {
        	   request.setAttribute("CartListCount", 0);
           }
			

			return "SUCCESS";

		} catch (Exception e) {
			return ERROR;
		}

	}

	@Override
	public Object doDelete(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		try {
			String cartid = request.getParameter("cartid");

			DetachedCriteria criteria = DetachedCriteria.forClass(Cart.class);
			criteria.add(Restrictions.eq(Cart.PROP_ID, new Long(cartid)));
			List<Cart> cartlist = service.findAll(criteria);

			for (Cart cart : cartlist) {
				cart.setRemoveFromCartTime(new Date());
				cart.setStatus("InActive");

				service.add(cart, null);
			}

			request.setAttribute("CartListCount", cartlist.size());

			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}

	}

	public Object doChangecart(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String cartid = request.getParameter("cartid");
		String symbol = request.getParameter("symbol");
		
	   Long userid = (Long) UserContext.getSessionScope().get("USERID");

		DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
		criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
		List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
		request.setAttribute("UserEditList", listobj.get(0));
         if(listobj.get(0).getDoctorid()!=null)
         {
		Long doctorid = listobj.get(0).getDoctorid().getId();
         DetachedCriteria critobj1=DetachedCriteria.forClass(Carttrack.class);
		critobj1.add(Restrictions.eq(Carttrack.PROP_USERID+"."+Doctor.PROP_ID,doctorid ));
		critobj1.add(Restrictions.eq(Carttrack.PROP_STATUS, "Active"));
		List<Carttrack> track11=service.findAll(critobj1);
		if(track11.size()==0)
		{
			ranumber="CART"+RandomNumber.now();
			Carttrack trackobj=new Carttrack();
			trackobj.setOrderid(null);
		    trackobj.setUserid(new Doctor(doctorid));
			trackobj.setUniqueid(ranumber);
			trackobj.setStatus("Active");
			service.add(trackobj, null);
		
		}
		else
		{
			ranumber=track11.get(0).getUniqueid();
		}
		
		DetachedCriteria criteria = DetachedCriteria.forClass(Cart.class);
		criteria.add(Restrictions.eq(Cart.PROP_ID, Long.parseLong(cartid)));
		criteria.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
		criteria.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
		criteria.add(Restrictions.eq(Cart.PROP_CARTID,ranumber));
		List<Cart> list = service.findAll(criteria);
		
		
		
		if (list != null) {
			int quantity = 0;
			if (symbol.equals("minus")) {
				for (Cart cart : list) {
					int qnt = cart.getQuantity();
					if (qnt != 1) {
						quantity = qnt - 1;
						cart.setQuantity(quantity);
					} else {
						cart.setQuantity(qnt);
					}
					service.add(cart, null);
				}
			} else {
				for (Cart cart : list) {
					int qnt = cart.getQuantity();
					quantity = qnt + 1;
					cart.setQuantity(quantity);

					service.add(cart, null);
				}
			}
		}
       }	
		return SUCCESS;
	}

}
