package com.ihexa.common.admin.user;

import com.ihexa.common.admin.user.base.BasePerson;



public class Person extends BasePerson {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Person () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Person (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}