package com.ihexa.common.admin.ticket;

import com.ihexa.common.admin.ticket.base.BaseTicket;



public class Ticket extends BaseTicket {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Ticket () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Ticket (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}