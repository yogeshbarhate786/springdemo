package com.ihexa.common.admin.profile.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the profiletoauthorization table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="profiletoauthorization"
 */

public abstract class BaseProfileToAuthorization  implements Serializable {

	public static String REF = "ProfileToAuthorization";
	public static String PROP_PROFILE = "profile";
	public static String PROP_AUTHORIZATION = "authorization";
	public static String PROP_ID = "id";


	// constructors
	public BaseProfileToAuthorization () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseProfileToAuthorization (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public BaseProfileToAuthorization (
		java.lang.Long id,
		com.ihexa.common.admin.profile.Profile profile,
		com.ihexa.common.admin.profile.Authorization authorization) {

		this.setId(id);
		this.setProfile(profile);
		this.setAuthorization(authorization);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// many to one
	private com.ihexa.common.admin.profile.Profile profile;
	private com.ihexa.common.admin.profile.Authorization authorization;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: ProfileID
	 */
	public com.ihexa.common.admin.profile.Profile getProfile () {
		return profile;
	}

	/**
	 * Set the value related to the column: ProfileID
	 * @param profile the ProfileID value
	 */
	public void setProfile (com.ihexa.common.admin.profile.Profile profile) {
		this.profile = profile;
	}



	/**
	 * Return the value associated with the column: AuthorizationID
	 */
	public com.ihexa.common.admin.profile.Authorization getAuthorization () {
		return authorization;
	}

	/**
	 * Set the value related to the column: AuthorizationID
	 * @param authorization the AuthorizationID value
	 */
	public void setAuthorization (com.ihexa.common.admin.profile.Authorization authorization) {
		this.authorization = authorization;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.profile.ProfileToAuthorization)) return false;
		else {
			com.ihexa.common.admin.profile.ProfileToAuthorization profileToAuthorization = (com.ihexa.common.admin.profile.ProfileToAuthorization) obj;
			if (null == this.getId() || null == profileToAuthorization.getId()) return false;
			else return (this.getId().equals(profileToAuthorization.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}