package com.ihexa.common.admin.user;

import com.ihexa.common.admin.user.base.BaseCity;

public class City extends BaseCity {
	private static final long serialVersionUID = 1L;

	/* [CONSTRUCTOR MARKER BEGIN] */
	public City() {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public City(java.lang.Long id) {
		super(id);
	}

	/* [CONSTRUCTOR MARKER END] */

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ihexa.common.admin.user.base.BaseCity#initialize()
	 */
	@Override
	protected void initialize() {
		// TODO Auto-generated method stub
		this.setCountry(new Country());
		this.setState(new State());
		super.initialize();
	}

}