package com.ihexa.common.admin.order.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the wishlist table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="wishlist"
 */

public abstract class BaseWishlist  implements Serializable {

	public static String REF = "Wishlist";
	public static String PROP_STATUS = "Status";
	public static String PROP_DOCTORID = "Doctorid";
	public static String PROP_PRODUCTID = "Productid";
	public static String PROP_ID = "Id";


	// constructors
	public BaseWishlist () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseWishlist (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String status;

	// many to one
	private com.ihexa.common.admin.productsku.Product productid;
	private com.ihexa.common.admin.doctor.Doctor doctorid;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="id"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: status
	 */
	public java.lang.String getStatus () {
		return status;
	}

	/**
	 * Set the value related to the column: status
	 * @param status the status value
	 */
	public void setStatus (java.lang.String status) {
		this.status = status;
	}



	/**
	 * Return the value associated with the column: productid
	 */
	public com.ihexa.common.admin.productsku.Product getProductid () {
		return productid;
	}

	/**
	 * Set the value related to the column: productid
	 * @param productid the productid value
	 */
	public void setProductid (com.ihexa.common.admin.productsku.Product productid) {
		this.productid = productid;
	}



	/**
	 * Return the value associated with the column: doctorid
	 */
	public com.ihexa.common.admin.doctor.Doctor getDoctorid () {
		return doctorid;
	}

	/**
	 * Set the value related to the column: doctorid
	 * @param doctorid the doctorid value
	 */
	public void setDoctorid (com.ihexa.common.admin.doctor.Doctor doctorid) {
		this.doctorid = doctorid;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.order.Wishlist)) return false;
		else {
			com.ihexa.common.admin.order.Wishlist wishlist = (com.ihexa.common.admin.order.Wishlist) obj;
			if (null == this.getId() || null == wishlist.getId()) return false;
			else return (this.getId().equals(wishlist.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}