package com.ihexa.common.admin.utility;

import com.ihexa.common.admin.iHexaConstants.IHexaConstants;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.MissingResourceException;
import java.util.StringTokenizer;


//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;

/**
 * Date Utility Class This is used to convert Strings to Dates and Timestamps
 *
 * <p>
 * <a href="DateUtil.java.html."><i>View Source</i></a>
 * </p>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by <a href="mailto:dan@getrolling.com">Dan
 *         Kibler </a> to correct time pattern. Minutes should be mm not MM (MM is month).
 * @version $Revision: 1.7 $ $Date: 2005/05/04 04:57:41 $
 */
public class DateUtil {
    // ~ Static fields/initializers
    // =============================================

    // private static Log log = LogFactory.getLog(DateUtil.class);

    /**
     * MM/dd/yyyy"
     */
    private static String defaultDatePattern = null;

    /**
     * HH:mm
     */
    private static String defaultTimePattern = "HH:mm";

    /**
     * date.format.dd_mm_yyyy
     */
    static String DATE_FORMAT = "dd_mm_yyyy";

    // number to word conversion
    private static String string;
    private static String[] a = {
            "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine",
        };
    private static String[] b = { "Hundred", "Thousand", "Lakh", "Crore" };
    private static String[] c = {
            "ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen",
            "Eighteen", "Ninteen",
        };
    private static String[] d = {
            "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninty"
        };

    // code added by n.mohata for number to word conversion(million style
    // conversion).
    private static final String[] majorNames = {
            "", " Thousand", " Million", " Billion", " Trillion", " Quadrillion", " Quintillion"
        };
    private static final String[] tensNames = {
            "", " Ten", " Twenty", " Thirty", " Fourty", " Fifty", " Sixty", " Seventy", " Eighty",
            " Ninety"
        };
    private static final String[] numNames = {
            "", " One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", " Nine",
            " Ten", " Eleven", " Twelve", " Thirteen", " Fourteen", " Fifteen", " Sixteen",
            " Seventeen", " Eighteen", " Nineteen"
        };

    // shakti
    public static int subtractDays(Date date1, Date date2) {
        GregorianCalendar gc1 = new GregorianCalendar();
        gc1.setTime(date1);

        GregorianCalendar gc2 = new GregorianCalendar();
        gc2.setTime(date2);

        int days1 = 0;
        int days2 = 0;
        int maxYear = Math.max(gc1.get(Calendar.YEAR), gc2.get(Calendar.YEAR));

        GregorianCalendar gctmp = (GregorianCalendar) gc1.clone();

        for (int f = gctmp.get(Calendar.YEAR); f < maxYear; f++) {
            days1 += gctmp.getActualMaximum(Calendar.DAY_OF_YEAR);
            gctmp.add(Calendar.YEAR, 1);
        }

        gctmp = (GregorianCalendar) gc2.clone();

        for (int f = gctmp.get(Calendar.YEAR); f < maxYear; f++) {
            days2 += gctmp.getActualMaximum(Calendar.DAY_OF_YEAR);
            gctmp.add(Calendar.YEAR, 1);
        }

        days1 += (gc1.get(Calendar.DAY_OF_YEAR) - 1);
        days2 += (gc2.get(Calendar.DAY_OF_YEAR) - 1);

        return (days1 - days2);
    }

    /*// ~ Methods
    // ================================================================
    
    public static String getBundle(String str) {
            ResourceBundle bundle = ResourceBundle.getBundle(
                            "org.sakaiproject.userManager.bundle.ApplicationResources");
            return bundle.getString(str);
    }*/

    /**
     * Convert a date string <br>
     * from a particular date format to <br>
     * another date format <br>
     * dateStr is date string to convert <br>
     * formatOld is format of date <br>
     * you are passing as string <br>
     * formatNew is format you want to change <br>
     * delimiter : which delimiter you are <br>
     * using in date format <br>
     *
     * @param dateStr --
     *            String
     * @param formatOld --
     *            String
     * @param formatNew --
     *            String
     * @param delimeter --
     *            String
     * @return
     *
     * public static String changeDateFormat(String dateStr, String formatOld, String delimiter, String formatNew) { try {
     * DateFormat dateformat = new SimpleDateFormat(formatOld); Date date = dateformat.parse(dateStr);
     *
     * String datePart[] = formatNew.split(delimiter);
     *
     * for(int ctr=0; ctr < datePart.length; ctr++){ } Calendar calendar = Calendar.getInstance(); calendar.clear();
     * calendar.set(Calendar.DAY_OF_MONTH, date.getDate()); calendar.set(Calendar.MONTH, date.getMonth());
     * calendar.set(Calendar.YEAR, date.getYear() + 1900); Date date1 = calendar.getTime(); DateFormat dateformatddMMyy =
     * new SimpleDateFormat("dd-MM-yy"); String dateDispatch = dateformatddMMyy.format(date1); } catch (Exception e) {
     * e.printStackTrace(); } }
     */
    public static String changeDateFormat(String dateStr, String formatOld) {
        // this method only convert to dd-MM-yy format
        String changedFormat = "";

        try {
            DateFormat dateformat = new SimpleDateFormat(formatOld);
            Date date = dateformat.parse(dateStr);

            Calendar calendar = Calendar.getInstance();
            calendar.clear();
            calendar.set(Calendar.DAY_OF_MONTH, date.getDate());
            calendar.set(Calendar.MONTH, date.getMonth());
            calendar.set(Calendar.YEAR, date.getYear() + 1900);

            Date date1 = calendar.getTime();
            DateFormat dateformatddMMyy = new SimpleDateFormat("dd-MM-yy");
            changedFormat = dateformatddMMyy.format(date1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return changedFormat;
    }

    /**
     * Add Day/Month/Year to a Date <br>
     * Send +ve values to add. <br>
     * Send 'null' values wherever applicable <br>
     * except date object.
     *
     * @param date --
     *            Date
     * @param years --
     *            Integer
     * @param months --
     *            Integer
     * @param days --
     *            Integer
     * @return
     */
    @SuppressWarnings("deprecation")
    public static Date addToDate(Date date, Integer years, Integer months, Integer days) {
        System.out.println("Entering 'addToDate' Method of DateUtil");

        Calendar calendar = null;

        try {
            java.text.SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat(DATE_FORMAT);
            calendar = Calendar.getInstance();
            calendar.set((1900 + date.getYear()), date.getMonth(), date.getDate());
            System.out.println("Date Format:" + DATE_FORMAT);
            System.out.println("Date Before addition: " +
                simpleDateFormat.format(calendar.getTime()));

            if ((years != null) && (years >= 0)) {
                calendar.add(Calendar.YEAR, years);
            }

            if ((months != null) && (months >= 0)) {
                calendar.add(Calendar.MONTH, months);
            }

            if ((days != null) && (days >= 0)) {
                calendar.add(Calendar.DATE, days);
            }

            System.out.println("Date After addition: " +
                simpleDateFormat.format(calendar.getTime()));

            return calendar.getTime();
        } catch (RuntimeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            return null;
        }
    }

    /**
     * Substract Day/Month/Year to a Date <br>
     * Send +ve values to substract. <br>
     * Send 'null' values wherever applicable <br>
     * except date object.
     *
     * @param date --
     *            Date
     * @param years --
     *            Integer
     * @param months --
     *            Integer
     * @param days --
     *            Integer
     * @return
     */
    @SuppressWarnings({"deprecation", "deprecation"})
    public static Date subFromDate(Date date, Integer years, Integer months, Integer days) {
        System.out.println("Entering 'subFromDate' Method of DateUtil");

        try {
            java.text.SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat(DATE_FORMAT);
            Calendar calendar = Calendar.getInstance();
            calendar.set(date.getYear(), date.getMonth(), date.getDate());
            System.out.println("Date Before Substraction: " +
                simpleDateFormat.format(calendar.getTime()));

            if ((years != null) && (years >= 0)) {
                calendar.add(Calendar.YEAR, -years);
            }

            if ((months != null) && (months >= 0)) {
                calendar.add(Calendar.MONTH, -months);
            }

            if ((days != null) && (days >= 0)) {
                calendar.add(Calendar.DATE, -days);
            }

            System.out.println("Date After Substraction: " +
                simpleDateFormat.format(calendar.getTime()));

            return calendar.getTime();
        } catch (RuntimeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            return null;
        }
    }

    /*public static void daysInMonth() {
            Calendar calendar = Calendar.getInstance(); //new GregorianCalendar();
            calendar.set(1999, 6, 20);
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            //            int days = calendar.get(Calendar.DATE);
            int[] daysInMonths = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
            daysInMonths[1] += DateUtility.isLeapYear(year) ? 1 : 0;
            System.out.println("Days in " + month + "th month for year " + year
                            + " is " + daysInMonths[calendar.get(Calendar.MONTH)]);
            System.out.println();
            System.out.println();
    }*/

    /**
     * Send a give date in yyyy-MM-dd or yyyy/MM/dd
     */
    public static boolean validateAGivenDate(String date) {
        System.out.println("Entering 'validateAGivenDate' method ");

        if ((date != null) && !"".equals(date.trim())) {
            date = date.replace("/", "");
            date = date.replace("-", "");
        } else {
            return false;
        }

        String dateformat = "ddMMyyyy";

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateformat);
            simpleDateFormat.setLenient(false);
            simpleDateFormat.parse(date.trim());

            return true;
        } catch (ParseException e) {
            System.out.println(e.getMessage());

            return false;
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid date");

            return false;
        }
    }

    /**
     * Method to get a day of the date
     *
     * @param date
     * @return String - day
     */
    public static String getDayofTheDate(Date date) {
        String day = null;
        DateFormat f = new SimpleDateFormat("EEEE");

        try {
            day = f.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("The dat for " + date + " is " + day);

        return day;
    }

    /**
     * Method to identify a leap year
     *
     * @param year
     * @return boolean
     */
    public static boolean isLeapYear(int year) {
        if (((year % 100) != 0) || ((year % 400) == 0)) {
            return true;
        }

        return false;
    }

    /**
     * Return default datePattern (MM/dd/yyyy)
     *
     * @return a string representing the date pattern on the UI
     */
    public static synchronized String getDatePattern() {
        try {
            defaultDatePattern = "dd/MM/yyyy";
        } catch (MissingResourceException mse) {
            defaultDatePattern = "dd/MM/yyyy";
        }

        return defaultDatePattern;
    }

    /**
     * This method attempts to convert an Oracle-formatted date in the form dd-MMM-yyyy to mm/dd/yyyy.
     *
     * @param aDate
     *            date from database as a string
     * @return formatted string for the ui
     */
    public static final String getDate(Date aDate) {
        SimpleDateFormat df = null;
        String returnValue = "";

        if (aDate != null) {
            df = new SimpleDateFormat(getDatePattern());
            returnValue = df.format(aDate);
        }

        return (returnValue);
    }

    /**
     * This method generates a string representation of a date/time in the format you specify on input
     *
     * @param aMask
     *            the date pattern the string is in
     * @param strDate
     *            a string representation of a date
     * @return a converted Date object
     * @see java.text.SimpleDateFormat
     * @throws ParseException
     */
    public static final Date convertStringToDate(String aMask, String strDate)
        throws ParseException {
        SimpleDateFormat df = null;
        Date date = null;
        df = new SimpleDateFormat(aMask);

        try {
            date = df.parse(strDate);
        } catch (ParseException pe) {
            // log.error("ParseException: " + pe);
            throw new ParseException(pe.getMessage(), pe.getErrorOffset());
        }

        return (date);
    }

    /**
     * This method returns the current date time in the format: MM/dd/yyyy HH:MM a
     *
     * @param theTime
     *            the current time
     * @return the current date/time
     */
    public static String getTimeNow(Date theTime) {
        return getDateTime(defaultTimePattern, theTime);
    }

    /**
     * This method returns the current date in the format: MM/dd/yyyy
     *
     * @return the current date
     * @throws ParseException
     */
    public static Calendar getToday() throws ParseException {
        Date today = new Date();
        SimpleDateFormat df = new SimpleDateFormat(getDatePattern());

        // This seems like quite a hack (date -> string -> date),
        // but it works ;-)
        String todayAsString = df.format(today);
        Calendar cal = new GregorianCalendar();
        cal.setTime(convertStringToDate(todayAsString));

        return cal;
    }

    /**
     * This method generates a string representation of a date's date/time in the format you specify on input
     *
     * @param aMask
     *            the date pattern the string is in
     * @param aDate
     *            a date object
     * @return a formatted string representation of the date
     *
     * @see java.text.SimpleDateFormat
     */
    public static final String getDateTime(String aMask, Date aDate) {
        SimpleDateFormat df = null;
        String returnValue = "";

        if (aDate == null) {
            // log.error("aDate is null!");
        } else {
            df = new SimpleDateFormat(aMask);
            returnValue = df.format(aDate);
        }

        return (returnValue);
    }

    /**
     * This method generates a string representation of a date based on the System Property 'dateFormat' in the format
     * you specify on input
     *
     * @param aDate
     *            A date to convert
     * @return a string representation of the date
     */
    public static final String convertDateToString(Date aDate) {
        return getDateTime("dd/MM/yyyy", aDate);
    }

    /**
     * This method converts a String to a date using the datePattern
     *
     * @param strDate
     *            the date to convert (in format MM/dd/yyyy)
     * @return a date object
     *
     * @throws ParseException
     */
    public static Date convertStringToDate(String strDate)
        throws ParseException {
        Date aDate = null;

        try {
            if (true) {
                // log.debug("converting date with pattern: " + getDatePattern());
            }

            aDate = convertStringToDate(getDatePattern(), strDate);
        } catch (ParseException pe) {
            // log.error("Could not convert '" + strDate+ "' to a date, throwing exception");
            pe.printStackTrace();
            throw new ParseException(pe.getMessage(), pe.getErrorOffset());
        }

        return aDate;
    }

    public static final String convertDateToStringinMMdd(Date aDate) {
        return getDateTime("MM/dd/yyyy", aDate);
    }

    public static Collection<Long> getLtaClaimYears() {
        Collection<Long> claimYears = new ArrayList<Long>();
        Calendar currentYear = new GregorianCalendar(); // Takes current year
        Calendar baseYear = new GregorianCalendar(1902, 0, 1);

        int yearDiff = (currentYear.get(Calendar.YEAR) - baseYear.get(Calendar.YEAR)) / 4;
        int ltaStartYear = baseYear.get(Calendar.YEAR) + (yearDiff * 4);

        claimYears.add(new Long(ltaStartYear));
        claimYears.add(new Long(ltaStartYear + 1));
        claimYears.add(new Long(ltaStartYear + 2));
        claimYears.add(new Long(ltaStartYear + 3));

        return claimYears;
    }

    /**
     * @author Gopal Patil
     * @param date1
     *            first date
     * @param date2
     *            second date
     * @return difference
     */
    public static int differenceDate(String date1, String date2) {
        ArrayList<Object> dates = new ArrayList<Object>();
        String[] splitStartDate = date1.split("/");
        GregorianCalendar startDate = new GregorianCalendar();
        startDate.set(Integer.parseInt(splitStartDate[2]), Integer.parseInt(splitStartDate[1]) - 1,
            Integer.parseInt(splitStartDate[0]));

        String[] splitEndDate = date2.split("/");
        GregorianCalendar endDate = new GregorianCalendar();
        endDate.set(Integer.parseInt(splitEndDate[2]), Integer.parseInt(splitEndDate[1]) - 1,
            Integer.parseInt(splitEndDate[0]));

        long differenceInMillis = endDate.getTimeInMillis() - startDate.getTimeInMillis();
        long differenceInDays = differenceInMillis / (24 * 60 * 60 * 1000);

        for (int i = 0; i <= differenceInDays; i++) {
            int month = startDate.get(Calendar.MONTH) + 1;
            String monString = String.valueOf(month);
            String year = (String.valueOf(startDate.get(Calendar.YEAR)));
            String day = (String.valueOf(startDate.get(Calendar.DATE)));

            if (monString.length() == 1) {
                monString = "0" + monString;
            }

            if (day.length() == 1) {
                day = "0" + day;
            }

            dates.add(monString + "/" + day + "/" + year);
            startDate.roll(Calendar.DAY_OF_YEAR, true);
        }

        // return difference between two dates
        return dates.size();
    }

    /**
     * @author Gopal Patil
     * @param amount
     *            first double
     * @param decimalPlaceForPrecision
     *            second int
     * @return string
     */
    public static String convertNumberToWord(double totalAmount, int decimalPlaceForPrecision) {
        if (totalAmount == 0.0d) {
            return "Zero";
        } else {
            DateUtil dateUtil = new DateUtil();

            // String
            // totalAmountInWords=dateUtil.convertNumberToWord((int)totalAmount);
            // By n.mohata for using million and billion wording style
            String totalAmountInWords = dateUtil.convert((int) totalAmount);

            /*
             * To disply number in words after decimal point Gives exact String
             * presentation of the double i.e gives o/p as 888,888,888,888.9 and
             * not as 8.888888888889E11
             */
            int numValAfterDecPoint = 0;
            NumberFormat numFormat = new DecimalFormat();

            numFormat.setMaximumFractionDigits(decimalPlaceForPrecision);
            numFormat.setMinimumFractionDigits(2);

            String exactTotal = numFormat.format(totalAmount);

            System.out.println("exactTotal" + exactTotal);

            if (exactTotal.indexOf(".") != -1) {
                String numAfterDecimal = exactTotal.substring(exactTotal.indexOf(".") + 1);
                System.out.println("numAfterDecimal" + numAfterDecimal);

                if (decimalPlaceForPrecision == 0) {
                    numAfterDecimal = "0";
                }

                System.out.println("numAfterDecimal" + numAfterDecimal);
                numValAfterDecPoint = Integer.parseInt(numAfterDecimal);
            }

            System.out.println("numAfterDecimal" + numValAfterDecPoint);

            if (numValAfterDecPoint != 0) {
                // totalAmountInWords+=" point
                // "+dateUtil.convertNumberToWord(numValAfterDecPoint);
                totalAmountInWords += (" and  " + dateUtil.convert(numValAfterDecPoint)); // By
                                                                                          // n.mohata
                                                                                          // for using
                                                                                          // million
                                                                                          // and
                                                                                          // billion
                                                                                          // wording
                                                                                          // style
            }

            // to add currency Symbol
            return totalAmountInWords;
        }
    } // End of Method convertAmountToWord

    /**
     * To Convert Number into word
     *
     * @author Gopal Patil
     * @param number
     *            first int
     *
     * @return string
     */
    private static String convertNumberToWord(int number) {
        int c = 1;
        int rm;

        string = " ";

        while (number != 0) {
            switch (c) {
            case 1:
                rm = number % 100;
                pass(rm);

                if ((number > 100) && ((number % 100) != 0)) {
                    display("And ");
                }

                number /= 100;

                break;

            case 2:
                rm = number % 10;

                if (rm != 0) {
                    display(" ");
                    display(b[0]);
                    display(" ");
                    pass(rm);
                }

                number /= 10;

                break;

            case 3:
                rm = number % 100;

                if (rm != 0) {
                    display(" ");
                    display(b[1]);
                    display(" ");
                    pass(rm);
                }

                number /= 100;

                break;

            case 4:
                rm = number % 100;

                if (rm != 0) {
                    display(" ");
                    display(b[2]);
                    display(" ");
                    pass(rm);
                }

                number /= 100;

                break;

            case 5:
                rm = number % 100;

                if (rm != 0) {
                    display(" ");
                    display(b[3]);
                    display(" ");
                    pass(rm);
                }

                number /= 100;

                break;
            }

            c++;
        }

        return string;
    }

    /**
     * To Convert Number into word
     *
     * @author Gopal Patil
     * @param number
     *            first int
     * @return void
     */
    private static void pass(int number) {
        int rm;
        int q;

        if (number < 10) {
            display(a[number]);
        }

        if ((number > 9) && (number < 20)) {
            display(c[number - 10]);
        }

        if (number > 19) {
            rm = number % 10;

            if (rm == 0) {
                q = number / 10;
                display(d[q - 2]);
            } else {
                q = number / 10;
                display(a[rm]);
                display(" ");
                display(d[q - 2]);
            }
        }
    }

    /**
     * To Convert Number into word
     *
     * @author Gopal Patil
     * @param number
     *            first int
     * @return void
     */
    private static void display(String s) {
        String t;
        t = string;
        string = s;
        string += t;
    }

    private String convertLessThanOneThousand(int number) {
        String soFar;

        if ((number % 100) < 20) {
            soFar = numNames[number % 100];
            number /= 100;
        } else {
            soFar = numNames[number % 10];
            number /= 10;

            soFar = tensNames[number % 10] + soFar;
            number /= 10;
        }

        if (number == 0) {
            return soFar;
        }

        return numNames[number] + " Hundred" + soFar;
    }

    public String convert(int number) {
        /* special case */
        if (number == 0) {
            return "zero";
        }

        String prefix = "";

        if (number < 0) {
            number = -number;
            prefix = "negative";
        }

        String soFar = "";
        int place = 0;

        do {
            int n = (int) number % 1000;

            if (n != 0) {
                String s = convertLessThanOneThousand(n);
                soFar = s + majorNames[place] + soFar;
            }

            place++;
            number /= 1000;
        } while (number > 0);

        return (prefix + soFar).trim();
    }

    /**
     * @author Gopal Patil
     * @param StrDate
     * @return Date
     * @throws ParseException
     */
    public static Date formatDate(String dateFormat, String StrDate)
        throws ParseException {
        String str = StrDate;

        StringTokenizer st = new StringTokenizer(str, "-");
        String[] finalStr = new String[3];
        int i = 0;

        while (st.hasMoreTokens()) {
            finalStr[i] = st.nextToken();
            i++;
        }

        String printString = finalStr[2] + "/" + finalStr[1] + "/" + finalStr[0];
        java.util.Date todate = null;

        if ((dateFormat != null) && !"".equals(dateFormat.trim())) {
            todate = DateUtil.convertStringToDate(dateFormat.trim(), printString);
        } else {
            todate = DateUtil.convertStringToDate("dd/MM/yyyy", printString);
        }

        return todate;
    }

    // code ends for number to word conversion.

    /**
     * Method to get diff in years. if your sending current date/date retrieved from db then send both dates in
     * following format: int date = new Date().getDate(); int month = new Date().getMonth(); int year = new
     * Date().getYear(); new Date((1900+year),month+1,date)
     *
     * OR if there is no change then send both dates directly
     *
     * @param smallDate
     * @param largeDate
     * @return
     */
    @SuppressWarnings("deprecation")
    public static Long getDiffYears(Date smallDate, Date largeDate) {
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.set(largeDate.getYear(), largeDate.getMonth(), largeDate.getDate());
        calendar2.set(smallDate.getYear(), smallDate.getMonth(), smallDate.getDate());

        long milliseconds1 = calendar1.getTimeInMillis();
        long milliseconds2 = calendar2.getTimeInMillis();
        long diff = milliseconds2 - milliseconds1;

        // long diffSeconds = diff / 1000;
        // long diffMinutes = diff / (60 * 1000);
        // long diffHours = diff / (60 * 60 * 1000);
        long diffDays = diff / (24 * 60 * 60 * 1000);
        long diffYears = (diffDays) / (365);
        System.out.println("Time in Years: " + diffYears + " Years.");

        return diffYears;
    }

    /**
     * Method to get diff in months
     *
     * @param smallDate
     * @param largeDate
     * @return
     */
    @SuppressWarnings("deprecation")
    public static Long getDiffDays(Date smallDate, Date largeDate) {
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.set(largeDate.getYear(), largeDate.getMonth(), largeDate.getDate());
        calendar2.set(smallDate.getYear(), smallDate.getMonth(), smallDate.getDate());

        long milliseconds1 = calendar1.getTimeInMillis();
        long milliseconds2 = calendar2.getTimeInMillis();
        long diff = milliseconds2 - milliseconds1;

        long diffDays = diff / (24 * 60 * 60 * 1000);
        System.out.println("Time in days: " + diffDays + " Days.");

        return diffDays;
    }

    /**
     * @return
     */
    public static List<Month> getMonthList() {
        List<Month> monthList = new ArrayList<Month>();
        Month month = null;
        int i = 0;

        for (String mth : IHexaConstants.MONTHS) {
            month = new Month();
            month.setName(mth);
            month.setValue(String.valueOf(i++));
            monthList.add(month);
        }

        return monthList;
    }

    /**
     * @param startYear
     * @param endYear
     * @return
     */
    public static List<Year> getYearList(Integer startYear, Integer endYear) {
        List<Year> yearList = new ArrayList<Year>();
        Year year = null;

        if ((startYear != null) && (endYear != null)) {
            Integer yearDiff = endYear - startYear;

            for (int i = 0; i < yearDiff; i++) {
                year = new Year();
                year.setName(String.valueOf(startYear));
                year.setValue(String.valueOf(startYear));
                yearList.add(year);
                ++startYear;
            }
        }

        return yearList;
    }

    public static int getDaysInMonth(int year, int month, int date) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, date);

        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static void main(String[] args) {
        System.out.println("" + DateUtil.convertNumberToWord(2));
    }

    /**
     * @return
     */
    public static List<Day> getDayList() {
        List<Day> dayList = new ArrayList<Day>();
        Day day = null;
        int i = 0;

        for (String dy : IHexaConstants.DAYS) {
            day = new Day();
            day.setName(dy);
            day.setValue(String.valueOf(i++));
            dayList.add(day);
        }

        return dayList;
    }
}
