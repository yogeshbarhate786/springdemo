package com.ihexa.common.admin.profile;

import com.ihexa.common.admin.profile.base.BaseAuthorization;



public class Authorization extends BaseAuthorization {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Authorization () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Authorization (java.lang.Long id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public Authorization (
		java.lang.Long id,
		java.lang.String name) {

		super (
			id,
			name);
	}

/*[CONSTRUCTOR MARKER END]*/


}