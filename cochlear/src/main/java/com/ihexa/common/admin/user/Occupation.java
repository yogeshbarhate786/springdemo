package com.ihexa.common.admin.user;

import com.ihexa.common.admin.user.base.BaseOccupation;



public class Occupation extends BaseOccupation {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Occupation () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Occupation (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}