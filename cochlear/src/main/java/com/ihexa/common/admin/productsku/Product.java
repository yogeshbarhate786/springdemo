package com.ihexa.common.admin.productsku;

import com.ihexa.common.admin.productsku.base.BaseProduct;



public class Product extends BaseProduct {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Product () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Product (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}