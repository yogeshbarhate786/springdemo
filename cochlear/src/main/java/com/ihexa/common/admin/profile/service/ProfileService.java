package com.ihexa.common.admin.profile.service;

import java.util.List;

import com.ihexa.common.admin.user.Role;
import com.prounify.framework.base.service.Service;

/**
 * Service layer interface for User entity.
 */

public interface ProfileService<T> extends Service<T> {

	public List<Role> getRoleList();
}
