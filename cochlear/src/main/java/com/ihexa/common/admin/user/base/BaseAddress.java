package com.ihexa.common.admin.user.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the address table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="address"
 */

public abstract class BaseAddress  implements Serializable {

	public static String REF = "Address";
	public static String PROP_ADDRESS_FULL = "addressFull";
	public static String PROP_STATE = "state";
	public static String PROP_ADDRESS_STREET = "addressStreet";
	public static String PROP_ZIP_CODE = "zipCode";
	public static String PROP_ID = "id";
	public static String PROP_COUNTRY = "country";
	public static String PROP_CITY = "city";


	// constructors
	public BaseAddress () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseAddress (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String addressFull;
	private java.lang.String addressStreet;
	private java.lang.Long zipCode;

	// many to one
	private com.ihexa.common.admin.user.State state;
	private com.ihexa.common.admin.user.Country country;
	private com.ihexa.common.admin.user.City city;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: AddressFull
	 */
	public java.lang.String getAddressFull () {
		return addressFull;
	}

	/**
	 * Set the value related to the column: AddressFull
	 * @param addressFull the AddressFull value
	 */
	public void setAddressFull (java.lang.String addressFull) {
		this.addressFull = addressFull;
	}



	/**
	 * Return the value associated with the column: AddressStreet
	 */
	public java.lang.String getAddressStreet () {
		return addressStreet;
	}

	/**
	 * Set the value related to the column: AddressStreet
	 * @param addressStreet the AddressStreet value
	 */
	public void setAddressStreet (java.lang.String addressStreet) {
		this.addressStreet = addressStreet;
	}
	
	public java.lang.Long getZipCode () {
		return zipCode;
	}
	
	public void setZipCode (java.lang.Long zipCode) {
		this.zipCode = zipCode;
		
	}

	/**
	 * Return the value associated with the column: StateID
	 */
	public com.ihexa.common.admin.user.State getState () {
		return state;
	}

	/**
	 * Set the value related to the column: StateID
	 * @param state the StateID value
	 */
	public void setState (com.ihexa.common.admin.user.State state) {
		this.state = state;
	}



	/**
	 * Return the value associated with the column: CountryID
	 */
	public com.ihexa.common.admin.user.Country getCountry () {
		return country;
	}

	/**
	 * Set the value related to the column: CountryID
	 * @param country the CountryID value
	 */
	public void setCountry (com.ihexa.common.admin.user.Country country) {
		this.country = country;
	}



	/**
	 * Return the value associated with the column: CityID
	 */
	public com.ihexa.common.admin.user.City getCity () {
		return city;
	}

	/**
	 * Set the value related to the column: CityID
	 * @param city the CityID value
	 */
	public void setCity (com.ihexa.common.admin.user.City city) {
		this.city = city;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.user.Address)) return false;
		else {
			com.ihexa.common.admin.user.Address address = (com.ihexa.common.admin.user.Address) obj;
			if (null == this.getId() || null == address.getId()) return false;
			else return (this.getId().equals(address.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}