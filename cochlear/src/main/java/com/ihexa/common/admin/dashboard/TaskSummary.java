/**
 * 
 */
package com.ihexa.common.admin.dashboard;

import java.util.Date;

import com.ihexa.common.admin.user.User;

/**
 * @author yogeshp
 * 
 */
public class TaskSummary {

	User user = null;
	String taskName = "";
	int totalTaskCount = 0;
	int pendingTaskCount = 0;
	int completedTaskCount = 0;
	Date lastTaskDate = null;

	/**
	 * @return the taskName
	 */
	public String getTaskName() {
		return taskName;
	}
	/**
	 * @param taskName
	 *            the taskName to set
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	/**
	 * @return the completedTaskCount
	 */
	public int getCompletedTaskCount() {
		return completedTaskCount;
	}
	/**
	 * @param completedTaskCount
	 *            the completedTaskCount to set
	 */
	public void setCompletedTaskCount(int completedTaskCount) {
		this.completedTaskCount = completedTaskCount;
	}
	/**
	 * @return the lastTaskDate
	 */
	public Date getLastTaskDate() {
		return lastTaskDate;
	}
	/**
	 * @param lastTaskDate
	 *            the lastTaskDate to set
	 */
	public void setLastTaskDate(Date lastTaskDate) {
		this.lastTaskDate = lastTaskDate;
	}
	/**
	 * @return the pendingTaskCount
	 */
	public int getPendingTaskCount() {
		return pendingTaskCount;
	}
	/**
	 * @param pendingTaskCount
	 *            the pendingTaskCount to set
	 */
	public void setPendingTaskCount(int pendingTaskCount) {
		this.pendingTaskCount = pendingTaskCount;
	}
	/**
	 * @return the totalTaskCount
	 */
	public int getTotalTaskCount() {
		return totalTaskCount;
	}
	/**
	 * @param totalTaskCount
	 *            the totalTaskCount to set
	 */
	public void setTotalTaskCount(int totalTaskCount) {
		this.totalTaskCount = totalTaskCount;
	}
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

}
