package com.ihexa.common.admin.person.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the person table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="person"
 */

public abstract class BasePerson  implements Serializable {

	public static String REF = "Person";
	public static String PROP_MIDDLE_NAME = "middleName";
	public static String PROP_ADDRESS = "address";
	public static String PROP_GENDER = "gender";
	public static String PROP_FIRST_NAME = "firstName";
	public static String PROP_SALUTATION = "salutation";
	public static String PROP_ID = "id";
	public static String PROP_MARITAL_STATUS = "maritalStatus";
	public static String PROP_LAST_NAME = "lastName";


	// constructors
	public BasePerson () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BasePerson (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String salutation;
	private java.lang.String firstName;
	private java.lang.String middleName;
	private java.lang.String lastName;
	private java.lang.String gender;
	private java.lang.String maritalStatus;

	// many to one
	private com.ihexa.common.admin.person.Address address;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="native"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: Salutation
	 */
	public java.lang.String getSalutation () {
		return salutation;
	}

	/**
	 * Set the value related to the column: Salutation
	 * @param salutation the Salutation value
	 */
	public void setSalutation (java.lang.String salutation) {
		this.salutation = salutation;
	}



	/**
	 * Return the value associated with the column: FirstName
	 */
	public java.lang.String getFirstName () {
		return firstName;
	}

	/**
	 * Set the value related to the column: FirstName
	 * @param firstName the FirstName value
	 */
	public void setFirstName (java.lang.String firstName) {
		this.firstName = firstName;
	}



	/**
	 * Return the value associated with the column: MiddleName
	 */
	public java.lang.String getMiddleName () {
		return middleName;
	}

	/**
	 * Set the value related to the column: MiddleName
	 * @param middleName the MiddleName value
	 */
	public void setMiddleName (java.lang.String middleName) {
		this.middleName = middleName;
	}



	/**
	 * Return the value associated with the column: LastName
	 */
	public java.lang.String getLastName () {
		return lastName;
	}

	/**
	 * Set the value related to the column: LastName
	 * @param lastName the LastName value
	 */
	public void setLastName (java.lang.String lastName) {
		this.lastName = lastName;
	}



	/**
	 * Return the value associated with the column: Gender
	 */
	public java.lang.String getGender () {
		return gender;
	}

	/**
	 * Set the value related to the column: Gender
	 * @param gender the Gender value
	 */
	public void setGender (java.lang.String gender) {
		this.gender = gender;
	}



	/**
	 * Return the value associated with the column: MaritalStatus
	 */
	public java.lang.String getMaritalStatus () {
		return maritalStatus;
	}

	/**
	 * Set the value related to the column: MaritalStatus
	 * @param maritalStatus the MaritalStatus value
	 */
	public void setMaritalStatus (java.lang.String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}



	/**
	 * Return the value associated with the column: AddressID
	 */
	public com.ihexa.common.admin.person.Address getAddress () {
		return address;
	}

	/**
	 * Set the value related to the column: AddressID
	 * @param address the AddressID value
	 */
	public void setAddress (com.ihexa.common.admin.person.Address address) {
		this.address = address;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.person.Person)) return false;
		else {
			com.ihexa.common.admin.person.Person person = (com.ihexa.common.admin.person.Person) obj;
			if (null == this.getId() || null == person.getId()) return false;
			else return (this.getId().equals(person.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}