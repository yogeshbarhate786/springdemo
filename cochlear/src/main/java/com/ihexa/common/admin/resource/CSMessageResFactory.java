/*
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.ihexa.common.admin.resource;

import org.apache.struts.util.MessageResources;
import org.apache.struts.util.MessageResourcesFactory;

/**
 * The main purpose of this class is to create a bridge between strut validator and the ProUnify
 * standard resource bundle.
 * 
 * This class is normally configured in strut-config.xml in Message Resource section, as the factory
 * class attribute.
 * 
 * @author marc.boudreault
 */
public class CSMessageResFactory extends MessageResourcesFactory {

	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 3976736960792703289L;

	/**
	 * Version string (RCS Id)
	 */
	private static final String VERSION = "$Id: CSMessageResFactory.java 40953 2007-10-16 13:19:18Z martin.goulet $";

	/**
	 * Return the version number for this class.
	 * 
	 * @return Version string
	 */
	public static String getVersion() {
		return VERSION;
	}

	/**
	 * Return an instance of CSMessageRes
	 * 
	 * @param config
	 *            a string configured in strut-config as a parameter this string must be a Resource
	 *            Bundle class i.e. ihexa.cs.control.resourcebundle.CSResBundle
	 */
	public MessageResources createResources(String config) {
		return new CSMessageResourcesToResourceBundle(this, config);
	}
}
