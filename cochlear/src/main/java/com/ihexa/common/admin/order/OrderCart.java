package com.ihexa.common.admin.order;

import com.ihexa.common.admin.order.base.BaseOrderCart;



public class OrderCart extends BaseOrderCart {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public OrderCart () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public OrderCart (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}