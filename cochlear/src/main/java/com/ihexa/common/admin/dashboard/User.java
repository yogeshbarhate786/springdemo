package com.ihexa.common.admin.dashboard;

import com.ihexa.common.admin.dashboard.base.BaseUser;



public class User extends BaseUser {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public User () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public User (java.lang.Long id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public User (
		java.lang.Long id,
		com.ihexa.common.admin.user.Person personID,
		com.ihexa.common.admin.user.Role roleID) {

		super (
			id,
			personID,
			roleID);
	}

/*[CONSTRUCTOR MARKER END]*/


}