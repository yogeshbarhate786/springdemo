package com.ihexa.common.admin.userskill.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the userskill table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="userskill"
 */

public abstract class BaseUserSkill  implements Serializable {

	public static String REF = "UserSkill";
	public static String PROP_SKILL = "skill";
	public static String PROP_USER = "user";
	public static String PROP_ID = "id";


	// constructors
	public BaseUserSkill () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseUserSkill (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// many to one
	private com.ihexa.common.admin.user.User user;
	private com.ihexa.common.admin.skill.Skill skill;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: UserID
	 */
	public com.ihexa.common.admin.user.User getUser () {
		return user;
	}

	/**
	 * Set the value related to the column: UserID
	 * @param user the UserID value
	 */
	public void setUser (com.ihexa.common.admin.user.User user) {
		this.user = user;
	}



	/**
	 * Return the value associated with the column: SkillID
	 */
	public com.ihexa.common.admin.skill.Skill getSkill () {
		return skill;
	}

	/**
	 * Set the value related to the column: SkillID
	 * @param skill the SkillID value
	 */
	public void setSkill (com.ihexa.common.admin.skill.Skill skill) {
		this.skill = skill;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.userskill.UserSkill)) return false;
		else {
			com.ihexa.common.admin.userskill.UserSkill userSkill = (com.ihexa.common.admin.userskill.UserSkill) obj;
			if (null == this.getId() || null == userSkill.getId()) return false;
			else return (this.getId().equals(userSkill.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}