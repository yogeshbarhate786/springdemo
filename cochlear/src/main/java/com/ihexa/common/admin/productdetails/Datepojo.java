package com.ihexa.common.admin.productdetails;

import java.util.Date;

public class Datepojo 
{
private String currentdate;
private String time;
private String mailid;
private String fullname;

public String getCurrentdate() {
	return currentdate;
}
public void setCurrentdate(String currentdate) {
	this.currentdate = currentdate;
}

public String getTime() {
	return time;
}
public void setTime(String time) {
	this.time = time;
}
public String getMailid() {
	return mailid;
}
public void setMailid(String mailid) {
	this.mailid = mailid;
}
public String getFullname() {
	return fullname;
}
public void setFullname(String fullname) {
	this.fullname = fullname;
}

}
