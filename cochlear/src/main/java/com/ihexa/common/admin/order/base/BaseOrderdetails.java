package com.ihexa.common.admin.order.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the orderdetails table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="orderdetails"
 */

public abstract class BaseOrderdetails  implements Serializable {

	public static String REF = "Orderdetails";
	public static String PROP_STATUS = "Status";
	public static String PROP_DOCTORID = "Doctorid";
	public static String PROP_FEEDBACK = "Feedback";
	public static String PROP_APPROVESTATUS = "Approvestatus";
	public static String PROP_PLACEDORDERDATE = "Placedorderdate";
	public static String PROP_ID = "Id";
	public static String PROP_ORDERID = "Orderid";
	public static String PROP_APPROVEDATE = "Approvedate";
	public static String PROP_APPROVEBY = "Approveby";


	// constructors
	public BaseOrderdetails () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseOrderdetails (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String orderid;
	private java.util.Date placedorderdate;
	private java.lang.String approvestatus;
	private java.lang.String status;
	private java.lang.String feedback;
	private java.util.Date approvedate;

	// many to one
	private com.ihexa.common.admin.telecaller.Telecaller approveby;
	private com.ihexa.common.admin.doctor.Doctor doctorid;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="id"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: orderid
	 */
	public java.lang.String getOrderid () {
		return orderid;
	}

	/**
	 * Set the value related to the column: orderid
	 * @param orderid the orderid value
	 */
	public void setOrderid (java.lang.String orderid) {
		this.orderid = orderid;
	}



	/**
	 * Return the value associated with the column: placedorderdate
	 */
	public java.util.Date getPlacedorderdate () {
		return placedorderdate;
	}

	/**
	 * Set the value related to the column: placedorderdate
	 * @param placedorderdate the placedorderdate value
	 */
	public void setPlacedorderdate (java.util.Date placedorderdate) {
		this.placedorderdate = placedorderdate;
	}



	/**
	 * Return the value associated with the column: approvestatus
	 */
	public java.lang.String getApprovestatus () {
		return approvestatus;
	}

	/**
	 * Set the value related to the column: approvestatus
	 * @param approvestatus the approvestatus value
	 */
	public void setApprovestatus (java.lang.String approvestatus) {
		this.approvestatus = approvestatus;
	}



	/**
	 * Return the value associated with the column: status
	 */
	public java.lang.String getStatus () {
		return status;
	}

	/**
	 * Set the value related to the column: status
	 * @param status the status value
	 */
	public void setStatus (java.lang.String status) {
		this.status = status;
	}



	/**
	 * Return the value associated with the column: feedback
	 */
	public java.lang.String getFeedback () {
		return feedback;
	}

	/**
	 * Set the value related to the column: feedback
	 * @param feedback the feedback value
	 */
	public void setFeedback (java.lang.String feedback) {
		this.feedback = feedback;
	}



	/**
	 * Return the value associated with the column: approvedate
	 */
	public java.util.Date getApprovedate () {
		return approvedate;
	}

	/**
	 * Set the value related to the column: approvedate
	 * @param approvedate the approvedate value
	 */
	public void setApprovedate (java.util.Date approvedate) {
		this.approvedate = approvedate;
	}



	/**
	 * Return the value associated with the column: approveby
	 */
	public com.ihexa.common.admin.telecaller.Telecaller getApproveby () {
		return approveby;
	}

	/**
	 * Set the value related to the column: approveby
	 * @param approveby the approveby value
	 */
	public void setApproveby (com.ihexa.common.admin.telecaller.Telecaller approveby) {
		this.approveby = approveby;
	}



	/**
	 * Return the value associated with the column: doctorid
	 */
	public com.ihexa.common.admin.doctor.Doctor getDoctorid () {
		return doctorid;
	}

	/**
	 * Set the value related to the column: doctorid
	 * @param doctorid the doctorid value
	 */
	public void setDoctorid (com.ihexa.common.admin.doctor.Doctor doctorid) {
		this.doctorid = doctorid;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.order.Orderdetails)) return false;
		else {
			com.ihexa.common.admin.order.Orderdetails orderdetails = (com.ihexa.common.admin.order.Orderdetails) obj;
			if (null == this.getId() || null == orderdetails.getId()) return false;
			else return (this.getId().equals(orderdetails.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}