package com.ihexa.common.admin.resource;

public class CSResBundle_en extends CSResBundle {

	/**
	 * Constructor Call the parent constructor. Set the parent class to allow cascading resource
	 * bundle search.
	 * 
	 */
	public CSResBundle_en() {
		super();
		this.setParent(new CSResBundle());
	}

}
