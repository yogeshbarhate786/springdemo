package com.ihexa.common.admin.dashboard.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the doctor table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="doctor"
 */

public abstract class BaseDoctor  implements Serializable {

	public static String REF = "Doctor";
	public static String PROP_STATUS = "status";
	public static String PROP_FULL_ADDRESS = "fullAddress";
	public static String PROP_USER_NAME = "userName";
	public static String PROP_EMAIL = "email";
	public static String PROP_FIRST_NAME = "firstName";
	public static String PROP_IS_OTP_VERIFIED = "isOtpVerified";
	public static String PROP_SPECIALIST = "specialist";
	public static String PROP_BIO = "bio";
	public static String PROP_GENDER = "gender";
	public static String PROP_CREATEDDATE = "createddate";
	public static String PROP_MIDDLE_NAME = "middleName";
	public static String PROP_MOBILE = "mobile";
	public static String PROP_MARITAL_STATUS = "maritalStatus";
	public static String PROP_UPDATEDDATE = "updateddate";
	public static String PROP_DOB = "dob";
	public static String PROP_PROFILE_IMAGE = "profileImage";
	public static String PROP_BELONG_TO_HOSPITAL = "belongToHospital";
	public static String PROP_LAST_NAME = "lastName";
	public static String PROP_ID = "id";
	public static String PROP_IS_EMAIL_VERIFIED = "isEmailVerified";
	public static String PROP_AGE = "age";
	public static String PROP_REGISTER_ON = "registerOn";


	// constructors
	public BaseDoctor () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseDoctor (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String firstName;
	private java.lang.String middleName;
	private java.lang.String lastName;
	private java.lang.String gender;
	private java.lang.Long age;
	private java.util.Date dob;
	private java.lang.String maritalStatus;
	private java.lang.String profileImage;
	private java.lang.String bio;
	private java.lang.String userName;
	private java.lang.String email;
	private java.lang.String mobile;
	private java.lang.String fullAddress;
	private java.util.Date registerOn;
	private java.lang.String status;
	private java.lang.String isEmailVerified;
	private java.lang.String isOtpVerified;
	private java.lang.String specialist;
	private java.lang.String belongToHospital;
	private java.util.Date createddate;
	private java.util.Date updateddate;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="id"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: first_name
	 */
	public java.lang.String getFirstName () {
		return firstName;
	}

	/**
	 * Set the value related to the column: first_name
	 * @param firstName the first_name value
	 */
	public void setFirstName (java.lang.String firstName) {
		this.firstName = firstName;
	}



	/**
	 * Return the value associated with the column: middle_name
	 */
	public java.lang.String getMiddleName () {
		return middleName;
	}

	/**
	 * Set the value related to the column: middle_name
	 * @param middleName the middle_name value
	 */
	public void setMiddleName (java.lang.String middleName) {
		this.middleName = middleName;
	}



	/**
	 * Return the value associated with the column: last_name
	 */
	public java.lang.String getLastName () {
		return lastName;
	}

	/**
	 * Set the value related to the column: last_name
	 * @param lastName the last_name value
	 */
	public void setLastName (java.lang.String lastName) {
		this.lastName = lastName;
	}



	/**
	 * Return the value associated with the column: gender
	 */
	public java.lang.String getGender () {
		return gender;
	}

	/**
	 * Set the value related to the column: gender
	 * @param gender the gender value
	 */
	public void setGender (java.lang.String gender) {
		this.gender = gender;
	}



	/**
	 * Return the value associated with the column: age
	 */
	public java.lang.Long getAge () {
		return age;
	}

	/**
	 * Set the value related to the column: age
	 * @param age the age value
	 */
	public void setAge (java.lang.Long age) {
		this.age = age;
	}



	/**
	 * Return the value associated with the column: dob
	 */
	public java.util.Date getDob () {
		return dob;
	}

	/**
	 * Set the value related to the column: dob
	 * @param dob the dob value
	 */
	public void setDob (java.util.Date dob) {
		this.dob = dob;
	}



	/**
	 * Return the value associated with the column: marital_status
	 */
	public java.lang.String getMaritalStatus () {
		return maritalStatus;
	}

	/**
	 * Set the value related to the column: marital_status
	 * @param maritalStatus the marital_status value
	 */
	public void setMaritalStatus (java.lang.String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}



	/**
	 * Return the value associated with the column: profile_image
	 */
	public java.lang.String getProfileImage () {
		return profileImage;
	}

	/**
	 * Set the value related to the column: profile_image
	 * @param profileImage the profile_image value
	 */
	public void setProfileImage (java.lang.String profileImage) {
		this.profileImage = profileImage;
	}



	/**
	 * Return the value associated with the column: bio
	 */
	public java.lang.String getBio () {
		return bio;
	}

	/**
	 * Set the value related to the column: bio
	 * @param bio the bio value
	 */
	public void setBio (java.lang.String bio) {
		this.bio = bio;
	}



	/**
	 * Return the value associated with the column: user_name
	 */
	public java.lang.String getUserName () {
		return userName;
	}

	/**
	 * Set the value related to the column: user_name
	 * @param userName the user_name value
	 */
	public void setUserName (java.lang.String userName) {
		this.userName = userName;
	}



	/**
	 * Return the value associated with the column: email
	 */
	public java.lang.String getEmail () {
		return email;
	}

	/**
	 * Set the value related to the column: email
	 * @param email the email value
	 */
	public void setEmail (java.lang.String email) {
		this.email = email;
	}



	/**
	 * Return the value associated with the column: mobile
	 */
	public java.lang.String getMobile () {
		return mobile;
	}

	/**
	 * Set the value related to the column: mobile
	 * @param mobile the mobile value
	 */
	public void setMobile (java.lang.String mobile) {
		this.mobile = mobile;
	}



	/**
	 * Return the value associated with the column: full_address
	 */
	public java.lang.String getFullAddress () {
		return fullAddress;
	}

	/**
	 * Set the value related to the column: full_address
	 * @param fullAddress the full_address value
	 */
	public void setFullAddress (java.lang.String fullAddress) {
		this.fullAddress = fullAddress;
	}



	/**
	 * Return the value associated with the column: register_on
	 */
	public java.util.Date getRegisterOn () {
		return registerOn;
	}

	/**
	 * Set the value related to the column: register_on
	 * @param registerOn the register_on value
	 */
	public void setRegisterOn (java.util.Date registerOn) {
		this.registerOn = registerOn;
	}



	/**
	 * Return the value associated with the column: status
	 */
	public java.lang.String getStatus () {
		return status;
	}

	/**
	 * Set the value related to the column: status
	 * @param status the status value
	 */
	public void setStatus (java.lang.String status) {
		this.status = status;
	}



	/**
	 * Return the value associated with the column: is_email_verified
	 */
	public java.lang.String getIsEmailVerified () {
		return isEmailVerified;
	}

	/**
	 * Set the value related to the column: is_email_verified
	 * @param isEmailVerified the is_email_verified value
	 */
	public void setIsEmailVerified (java.lang.String isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}



	/**
	 * Return the value associated with the column: is_otp_verified
	 */
	public java.lang.String getIsOtpVerified () {
		return isOtpVerified;
	}

	/**
	 * Set the value related to the column: is_otp_verified
	 * @param isOtpVerified the is_otp_verified value
	 */
	public void setIsOtpVerified (java.lang.String isOtpVerified) {
		this.isOtpVerified = isOtpVerified;
	}



	/**
	 * Return the value associated with the column: specialist
	 */
	public java.lang.String getSpecialist () {
		return specialist;
	}

	/**
	 * Set the value related to the column: specialist
	 * @param specialist the specialist value
	 */
	public void setSpecialist (java.lang.String specialist) {
		this.specialist = specialist;
	}



	/**
	 * Return the value associated with the column: belong_to_hospital
	 */
	public java.lang.String getBelongToHospital () {
		return belongToHospital;
	}

	/**
	 * Set the value related to the column: belong_to_hospital
	 * @param belongToHospital the belong_to_hospital value
	 */
	public void setBelongToHospital (java.lang.String belongToHospital) {
		this.belongToHospital = belongToHospital;
	}



	/**
	 * Return the value associated with the column: createddate
	 */
	public java.util.Date getCreateddate () {
		return createddate;
	}

	/**
	 * Set the value related to the column: createddate
	 * @param createddate the createddate value
	 */
	public void setCreateddate (java.util.Date createddate) {
		this.createddate = createddate;
	}



	/**
	 * Return the value associated with the column: updateddate
	 */
	public java.util.Date getUpdateddate () {
		return updateddate;
	}

	/**
	 * Set the value related to the column: updateddate
	 * @param updateddate the updateddate value
	 */
	public void setUpdateddate (java.util.Date updateddate) {
		this.updateddate = updateddate;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.dashboard.Doctor)) return false;
		else {
			com.ihexa.common.admin.dashboard.Doctor doctor = (com.ihexa.common.admin.dashboard.Doctor) obj;
			if (null == this.getId() || null == doctor.getId()) return false;
			else return (this.getId().equals(doctor.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}