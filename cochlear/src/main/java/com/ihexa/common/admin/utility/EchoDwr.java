package com.ihexa.common.admin.utility;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;


public class EchoDwr {
    public static String echo(String url) throws Exception {
        WebContext webContext = WebContextFactory.get();
        String html = webContext.forwardToString(url);

        return html;
    }
}
