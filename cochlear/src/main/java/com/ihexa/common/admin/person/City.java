package com.ihexa.common.admin.person;

import com.ihexa.common.admin.person.base.BaseCity;



public class City extends BaseCity {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public City () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public City (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}