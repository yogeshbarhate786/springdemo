package com.ihexa.common.admin.person;

import com.ihexa.common.admin.person.base.BasePerson;



public class Person extends BasePerson {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Person () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Person (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/
	@Override
	protected void initialize() {
		// TODO Auto-generated method stub it is necessary to initialise it otherwise it throw null property exception
		super.initialize();
		this.setAddress(new Address());

}
}