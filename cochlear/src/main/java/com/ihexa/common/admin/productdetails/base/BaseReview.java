package com.ihexa.common.admin.productdetails.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the review table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="review"
 */

public abstract class BaseReview  implements Serializable {

	public static String REF = "Review";
	public static String PROP_STATUS = "Status";
	public static String PROP_EMAIL = "Email";
	public static String PROP_MESSAGE = "Message";
	public static String PROP_USERID = "Userid";
	public static String PROP_TIME = "Time";
	public static String PROP_PRODUCTID = "Productid";
	public static String PROP_ID = "Id";
	public static String PROP_DATE = "Date";
	public static String PROP_NAME = "Name";


	// constructors
	public BaseReview () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseReview (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String name;
	private java.lang.String email;
	private java.lang.String message;
	private java.lang.String status;
	private java.lang.String date;
	private java.lang.String time;

	// many to one
	private com.ihexa.common.admin.productsku.Product productid;
	private com.ihexa.common.admin.doctor.Doctor userid;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="id"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: name
	 */
	public java.lang.String getName () {
		return name;
	}

	/**
	 * Set the value related to the column: name
	 * @param name the name value
	 */
	public void setName (java.lang.String name) {
		this.name = name;
	}



	/**
	 * Return the value associated with the column: email
	 */
	public java.lang.String getEmail () {
		return email;
	}

	/**
	 * Set the value related to the column: email
	 * @param email the email value
	 */
	public void setEmail (java.lang.String email) {
		this.email = email;
	}



	/**
	 * Return the value associated with the column: message
	 */
	public java.lang.String getMessage () {
		return message;
	}

	/**
	 * Set the value related to the column: message
	 * @param message the message value
	 */
	public void setMessage (java.lang.String message) {
		this.message = message;
	}



	/**
	 * Return the value associated with the column: status
	 */
	public java.lang.String getStatus () {
		return status;
	}

	/**
	 * Set the value related to the column: status
	 * @param status the status value
	 */
	public void setStatus (java.lang.String status) {
		this.status = status;
	}



	/**
	 * Return the value associated with the column: date
	 */
	public java.lang.String getDate () {
		return date;
	}

	/**
	 * Set the value related to the column: date
	 * @param date the date value
	 */
	public void setDate (java.lang.String date) {
		this.date = date;
	}



	/**
	 * Return the value associated with the column: time
	 */
	public java.lang.String getTime () {
		return time;
	}

	/**
	 * Set the value related to the column: time
	 * @param time the time value
	 */
	public void setTime (java.lang.String time) {
		this.time = time;
	}



	/**
	 * Return the value associated with the column: productid
	 */
	public com.ihexa.common.admin.productsku.Product getProductid () {
		return productid;
	}

	/**
	 * Set the value related to the column: productid
	 * @param productid the productid value
	 */
	public void setProductid (com.ihexa.common.admin.productsku.Product productid) {
		this.productid = productid;
	}



	/**
	 * Return the value associated with the column: userid
	 */
	public com.ihexa.common.admin.doctor.Doctor getUserid () {
		return userid;
	}

	/**
	 * Set the value related to the column: userid
	 * @param userid the userid value
	 */
	public void setUserid (com.ihexa.common.admin.doctor.Doctor userid) {
		this.userid = userid;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.productdetails.Review)) return false;
		else {
			com.ihexa.common.admin.productdetails.Review review = (com.ihexa.common.admin.productdetails.Review) obj;
			if (null == this.getId() || null == review.getId()) return false;
			else return (this.getId().equals(review.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}