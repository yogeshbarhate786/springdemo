package com.ihexa.common.admin.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.support.MultipartFilter;

/**
 * Give more flexibility to standard tomcat url-pattern to do servlet filtering on request for
 * Spring provided multipart filter.
 */
public class FileFilter extends MultipartFilter {

	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
			FilterChain filterChain) throws ServletException, IOException {

		String urlRegex = this.getFilterConfig().getInitParameter("url-regex");

		if ((urlRegex != null) && request.getRequestURI().matches(urlRegex)) {
			super.doFilterInternal(request, response, filterChain);
		} else {
			filterChain.doFilter(request, response);
		}
	}
}
