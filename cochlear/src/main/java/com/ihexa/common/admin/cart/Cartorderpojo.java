package com.ihexa.common.admin.cart;

public class Cartorderpojo 
{
private int quantity;
private double bill;
private double totalbill;
private String product_name;
private String product_price;
private String product_url;
private int cartid;
public int getQuantity() {
	return quantity;
}
public void setQuantity(int quantity) {
	this.quantity = quantity;
}
public double getBill() {
	return bill;
}
public void setBill(double bill) {
	this.bill = bill;
}
public double getTotalbill() {
	return totalbill;
}
public void setTotalbill(double totalbill) {
	this.totalbill = totalbill;
}
public String getProduct_name() {
	return product_name;
}
public void setProduct_name(String product_name) {
	this.product_name = product_name;
}
public String getProduct_price() {
	return product_price;
}
public void setProduct_price(String product_price) {
	this.product_price = product_price;
}
public String getProduct_url() {
	return product_url;
}
public void setProduct_url(String product_url) {
	this.product_url = product_url;
}
public int getCartid() {
	return cartid;
}
public void setCartid(int cartid) {
	this.cartid = cartid;
}


}
