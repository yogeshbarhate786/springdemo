package com.ihexa.common.admin.company;

import com.ihexa.common.admin.company.base.BaseCompany;



public class Company extends BaseCompany {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Company () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Company (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}