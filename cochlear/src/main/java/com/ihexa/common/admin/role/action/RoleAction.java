package com.ihexa.common.admin.role.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.springframework.validation.DataBinder;

import com.ihexa.common.admin.profile.Profile;
import com.ihexa.common.admin.profile.service.ProfileService;
import com.ihexa.common.admin.role.Role;
import com.ihexa.common.admin.role.service.RoleService;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.base.action.BindingActionForm;
import com.prounify.framework.context.Context;

public class RoleAction extends BaseCRUDAction {

	/**
	 * to save new Role
	 * 
	 * @param
	 * @return
	 */
	@Override
	public Object doCreate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub

		BindingActionForm actionForm = (BindingActionForm) form;

		ActionMessages messages = new ActionMessages();
		messages.clear();
		ActionMessage msg = null;

		try {
			RoleService roleService = (RoleService) getService(mapping);
			Role entity = (Role) getBean(mapping.getBaseName());
			DataBinder binder = getBinder(entity, mapping, request);
			roleService.add(entity, binder.getBindingResult());

			if (entity != null && entity.getId() != null) {

				// add respective role profile
				ProfileService profileService = (ProfileService) Context.getInstance().getBean("ProfileService");
				Profile profile = new Profile();
				profile.setRole(new com.ihexa.common.admin.profile.Role(entity.getId()));
				profile.setName(entity.getRoleName());
				profile.setDescription(entity.getRoleDesc());
				profileService.add(profile, null);

			}
			// super.doCreate(mapping, form, request, response);
			msg = new ActionMessage("role.SAVE_SUCCESS");
			messages.add("success", msg);
			saveMessages(request, messages);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			msg = new ActionMessage("role.SAVE_FAIL", e.getCause());
			messages.add("error", msg);
			saveMessages(request, messages);
			return ERROR;
		}
	}

	/**
	 * to update Role
	 * 
	 * @param
	 * @return
	 */
	@Override
	public Object doUpdate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		BindingActionForm actionForm = (BindingActionForm) form;

		ActionMessages messages = new ActionMessages();
		messages.clear();
		ActionMessage msg = null;

		try {
			super.doUpdate(mapping, form, request, response);
			msg = new ActionMessage("role.UPDATE_SUCCESS");
			messages.add("success", msg);
			saveMessages(request, messages);
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			msg = new ActionMessage("role.UPDATE_FAIL", e.getCause());
			messages.add("error", msg);
			saveMessages(request, messages);
			return ERROR;
		}
	}

	/**
	 * to delete Role
	 * 
	 * @param
	 * @return
	 */
	@Override
	public Object doDelete(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		BindingActionForm actionForm = (BindingActionForm) form;

		ActionMessages messages = new ActionMessages();
		messages.clear();
		ActionMessage msg = null;
		try {
			super.doDelete(mapping, form, request, response);
			msg = new ActionMessage("role.DELETE_SUCCESS");
			messages.add("success", msg);
			saveMessages(request, messages);
			return RETURN_SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			// actionForm.expose(binder.getBindingResult(), request, e);
			msg = new ActionMessage("role.DELETE_FAIL", e.getCause());
			messages.add("error", msg);
			saveMessages(request, messages);
			return ERROR;
		}
	}
}
