package com.ihexa.common.admin.authorization;

import com.ihexa.common.admin.authorization.base.BaseAuthorizationGroup;

public class AuthorizationGroup extends BaseAuthorizationGroup {
	private static final long serialVersionUID = 1L;

	/* [CONSTRUCTOR MARKER BEGIN] */
	public AuthorizationGroup() {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public AuthorizationGroup(java.lang.Long id) {
		super(id);
	}

	/* [CONSTRUCTOR MARKER END] */

}
