package com.ihexa.common.admin.utility;

import java.io.DataInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Random;

/**
 * Created by yogesh on 6/26/2017.
 */

public class GupShupSMS {

    public static int sendOTP(String mobile,String Orderid) {

        Random random = new Random();
        int otp = random.nextInt(8999) + 1000;
        

        StringBuffer sms = new StringBuffer();
        sms.append("Your order : " + Orderid+" is cancelled");
        if (!(mobile.isEmpty())) {
            try {
                URL smsURL = new URL("http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to="
                        + mobile + "&msg=" + URLEncoder.encode(sms.toString(), "UTF-8")
                        + "&msg_type=TEXT&userid=2000062157&auth_scheme=plain&password=abc1234&v=1.1&format=text");
                URLConnection tacConnection = smsURL.openConnection();
                DataInputStream dis = new DataInputStream(tacConnection.getInputStream());
                dis.close();
         } catch (Exception me) {
                return 0;
            } finally {
            }
        }
        return otp;
    }


    public static int updatedPts(String mobile,String pts) {



        StringBuffer sms = new StringBuffer();
        sms.append("Your Updated Points are : " + pts);
        if (!(mobile.isEmpty())) {
            try {
                URL smsURL = new URL("http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to="
                        + mobile + "&msg=" + URLEncoder.encode(sms.toString(), "UTF-8")
                        + "&msg_type=TEXT&userid=2000062157&auth_scheme=plain&password=abc1234&v=1.1&format=text");
                URLConnection tacConnection = smsURL.openConnection();
                DataInputStream dis = new DataInputStream(tacConnection.getInputStream());
                dis.close();
            } catch (Exception me) {
                return 0;
            } finally {
            }
        }
        return 1;
    }


    public static int readPts(String mobile,String pts,String uppts) {

        StringBuffer sms = new StringBuffer();
        sms.append("You Redeem  "+pts+" Points . Your Updated Points are : "+uppts+" ");
        if (!(mobile.isEmpty())) {
            try {
                URL smsURL = new URL("http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to="
                        + mobile + "&msg=" + URLEncoder.encode(sms.toString(), "UTF-8")
                        + "&msg_type=TEXT&userid=2000062157&auth_scheme=plain&password=abc1234&v=1.1&format=text");
                URLConnection tacConnection = smsURL.openConnection();
                DataInputStream dis = new DataInputStream(tacConnection.getInputStream());
                dis.close();
            } catch (Exception me) {
                return 0;
            } finally {
            }
        }
        return 1;
    }
}

