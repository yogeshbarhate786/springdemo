package com.ihexa.common.admin.cart.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the cart table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="cart"
 */

public abstract class BaseCart  implements Serializable {

	public static String REF = "Cart";
	public static String PROP_STATUS = "Status";
	public static String PROP_CARTSUCCESSSTATUS = "Cartsuccessstatus";
	public static String PROP_TOTALBILL = "Totalbill";
	public static String PROP_QUANTITY = "Quantity";
	public static String PROP_GST = "Gst";
	public static String PROP_PRODUCTID = "Productid";
	public static String PROP_DOCTORID = "Doctorid";
	public static String PROP_REMOVE_FROM_CART_TIME = "RemoveFromCartTime";
	public static String PROP_BILL = "Bill";
	public static String PROP_CARTID = "Cartid";
	public static String PROP_ID = "Id";
	public static String PROP_PRODUCT_DELIVERY_DATE = "ProductDeliveryDate";
	public static String PROP_ADDED_TO_CART_TIME = "AddedToCartTime";


	// constructors
	public BaseCart () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseCart (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String status;
	private java.util.Date addedToCartTime;
	private java.util.Date removeFromCartTime;
	private java.util.Date productDeliveryDate;
	private java.lang.Integer quantity;
	private java.math.BigDecimal bill;
	private java.math.BigDecimal totalbill;
	private java.lang.String cartsuccessstatus;
	private java.lang.String cartid;
	private java.math.BigDecimal gst;

	// many to one
	private com.ihexa.common.admin.productsku.Product productid;
	private com.ihexa.common.admin.doctor.Doctor doctorid;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="id"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: status
	 */
	public java.lang.String getStatus () {
		return status;
	}

	/**
	 * Set the value related to the column: status
	 * @param status the status value
	 */
	public void setStatus (java.lang.String status) {
		this.status = status;
	}



	/**
	 * Return the value associated with the column: added_to_cart_time
	 */
	public java.util.Date getAddedToCartTime () {
		return addedToCartTime;
	}

	/**
	 * Set the value related to the column: added_to_cart_time
	 * @param addedToCartTime the added_to_cart_time value
	 */
	public void setAddedToCartTime (java.util.Date addedToCartTime) {
		this.addedToCartTime = addedToCartTime;
	}



	/**
	 * Return the value associated with the column: remove_from_cart_time
	 */
	public java.util.Date getRemoveFromCartTime () {
		return removeFromCartTime;
	}

	/**
	 * Set the value related to the column: remove_from_cart_time
	 * @param removeFromCartTime the remove_from_cart_time value
	 */
	public void setRemoveFromCartTime (java.util.Date removeFromCartTime) {
		this.removeFromCartTime = removeFromCartTime;
	}



	/**
	 * Return the value associated with the column: product_delivery_date
	 */
	public java.util.Date getProductDeliveryDate () {
		return productDeliveryDate;
	}

	/**
	 * Set the value related to the column: product_delivery_date
	 * @param productDeliveryDate the product_delivery_date value
	 */
	public void setProductDeliveryDate (java.util.Date productDeliveryDate) {
		this.productDeliveryDate = productDeliveryDate;
	}



	/**
	 * Return the value associated with the column: quantity
	 */
	public java.lang.Integer getQuantity () {
		return quantity;
	}

	/**
	 * Set the value related to the column: quantity
	 * @param quantity the quantity value
	 */
	public void setQuantity (java.lang.Integer quantity) {
		this.quantity = quantity;
	}



	/**
	 * Return the value associated with the column: bill
	 */
	public java.math.BigDecimal getBill () {
		return bill;
	}

	/**
	 * Set the value related to the column: bill
	 * @param bill the bill value
	 */
	public void setBill (java.math.BigDecimal bill) {
		this.bill = bill;
	}



	/**
	 * Return the value associated with the column: totalbill
	 */
	public java.math.BigDecimal getTotalbill () {
		return totalbill;
	}

	/**
	 * Set the value related to the column: totalbill
	 * @param totalbill the totalbill value
	 */
	public void setTotalbill (java.math.BigDecimal totalbill) {
		this.totalbill = totalbill;
	}



	/**
	 * Return the value associated with the column: cartsuccessstatus
	 */
	public java.lang.String getCartsuccessstatus () {
		return cartsuccessstatus;
	}

	/**
	 * Set the value related to the column: cartsuccessstatus
	 * @param cartsuccessstatus the cartsuccessstatus value
	 */
	public void setCartsuccessstatus (java.lang.String cartsuccessstatus) {
		this.cartsuccessstatus = cartsuccessstatus;
	}



	/**
	 * Return the value associated with the column: cartid
	 */
	public java.lang.String getCartid () {
		return cartid;
	}

	/**
	 * Set the value related to the column: cartid
	 * @param cartid the cartid value
	 */
	public void setCartid (java.lang.String cartid) {
		this.cartid = cartid;
	}



	/**
	 * Return the value associated with the column: gst
	 */
	public java.math.BigDecimal getGst () {
		return gst;
	}

	/**
	 * Set the value related to the column: gst
	 * @param gst the gst value
	 */
	public void setGst (java.math.BigDecimal gst) {
		this.gst = gst;
	}



	/**
	 * Return the value associated with the column: productid
	 */
	public com.ihexa.common.admin.productsku.Product getProductid () {
		return productid;
	}

	/**
	 * Set the value related to the column: productid
	 * @param productid the productid value
	 */
	public void setProductid (com.ihexa.common.admin.productsku.Product productid) {
		this.productid = productid;
	}



	/**
	 * Return the value associated with the column: doctorid
	 */
	public com.ihexa.common.admin.doctor.Doctor getDoctorid () {
		return doctorid;
	}

	/**
	 * Set the value related to the column: doctorid
	 * @param doctorid the doctorid value
	 */
	public void setDoctorid (com.ihexa.common.admin.doctor.Doctor doctorid) {
		this.doctorid = doctorid;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.cart.Cart)) return false;
		else {
			com.ihexa.common.admin.cart.Cart cart = (com.ihexa.common.admin.cart.Cart) obj;
			if (null == this.getId() || null == cart.getId()) return false;
			else return (this.getId().equals(cart.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}