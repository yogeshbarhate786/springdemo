package com.ihexa.common.admin.userskill;

import com.ihexa.common.admin.skill.Skill;
import com.ihexa.common.admin.user.User;
import com.ihexa.common.admin.userskill.base.BaseUserSkill;

public class UserSkill extends BaseUserSkill {
	private static final long serialVersionUID = 1L;

	/* [CONSTRUCTOR MARKER BEGIN] */
	public UserSkill() {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public UserSkill(java.lang.Long id) {
		super(id);
	}

	/* [CONSTRUCTOR MARKER END] */

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ihexa.common.admin.userskill.base.BaseUserSkill#initialize()
	 */
	@Override
	protected void initialize() {
		// TODO Auto-generated method stub
		this.setUser(new User());
		this.setSkill(new Skill());
		super.initialize();
	}

}