package com.ihexa.common.admin.resource;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

public class CSResBundle extends ResourceBundle {
	private static final String UNKNOWN_KEY_DELIMITER = "???";

	public CSResBundle() {

	}

	public Enumeration<String> getKeys() {
		return null;
	}

	protected Object handleGetObject(String k) {

		// Not found in the database. Go check in the file implementation
		ResourceBundle fileBundle;
		fileBundle = ResourceBundle.getBundle("IHexaResources", Locale.getDefault());

		if (fileBundle != null) {
			return fileBundle.getString(k);
		}

		return UNKNOWN_KEY_DELIMITER + k + UNKNOWN_KEY_DELIMITER;

	}
}
