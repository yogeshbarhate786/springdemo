package com.ihexa.common.admin.productsku.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the maincategory table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="maincategory"
 */

public abstract class BaseMaincategory  implements Serializable {

	public static String REF = "Maincategory";
	public static String PROP_STATUS = "Status";
	public static String PROP_ID = "Id";
	public static String PROP_NAME = "Name";
	public static String PROP_CREATEDATE = "Createdate";


	// constructors
	public BaseMaincategory () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseMaincategory (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public BaseMaincategory (
		java.lang.Long id,
		java.util.Date createdate) {

		this.setId(id);
		this.setCreatedate(createdate);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String name;
	private java.util.Date createdate;
	private java.lang.String status;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="id"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: name
	 */
	public java.lang.String getName () {
		return name;
	}

	/**
	 * Set the value related to the column: name
	 * @param name the name value
	 */
	public void setName (java.lang.String name) {
		this.name = name;
	}



	/**
	 * Return the value associated with the column: createdate
	 */
	public java.util.Date getCreatedate () {
		return createdate;
	}

	/**
	 * Set the value related to the column: createdate
	 * @param createdate the createdate value
	 */
	public void setCreatedate (java.util.Date createdate) {
		this.createdate = createdate;
	}



	/**
	 * Return the value associated with the column: status
	 */
	public java.lang.String getStatus () {
		return status;
	}

	/**
	 * Set the value related to the column: status
	 * @param status the status value
	 */
	public void setStatus (java.lang.String status) {
		this.status = status;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.productsku.Maincategory)) return false;
		else {
			com.ihexa.common.admin.productsku.Maincategory maincategory = (com.ihexa.common.admin.productsku.Maincategory) obj;
			if (null == this.getId() || null == maincategory.getId()) return false;
			else return (this.getId().equals(maincategory.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}