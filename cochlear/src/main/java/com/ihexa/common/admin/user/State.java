package com.ihexa.common.admin.user;

import com.ihexa.common.admin.user.base.BaseState;

public class State extends BaseState {
	private static final long serialVersionUID = 1L;

	/* [CONSTRUCTOR MARKER BEGIN] */
	public State() {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public State(java.lang.Long id) {
		super(id);
	}

	/* [CONSTRUCTOR MARKER END] */

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ihexa.common.admin.user.base.BaseState#initialize()
	 */
	@Override
	protected void initialize() {
		// TODO Auto-generated method stub
		this.setCountry(new Country());
		super.initialize();
	}

}