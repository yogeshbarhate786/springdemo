package com.ihexa.common.admin.authorization.service;

import com.prounify.framework.base.service.Service;

/**
 * Service layer interface for Authorization entity.
 */

public interface AuthorizationService<Authorization> extends Service<Authorization> {

}
