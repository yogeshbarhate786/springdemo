package com.ihexa.common.admin.skill;

import com.ihexa.common.admin.skill.base.BaseSkill;

public class Skill extends BaseSkill {
	private static final long serialVersionUID = 1L;

	public boolean selected = false;

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/* [CONSTRUCTOR MARKER BEGIN] */
	public Skill() {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Skill(java.lang.Long id) {
		super(id);
	}

	/* [CONSTRUCTOR MARKER END] */

}