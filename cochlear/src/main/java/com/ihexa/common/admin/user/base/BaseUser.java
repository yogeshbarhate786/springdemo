package com.ihexa.common.admin.user.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the user table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="user"
 */

public abstract class BaseUser  implements Serializable {

	public static String REF = "User";
	public static String PROP_ROLE = "role";
	public static String PROP_DOCTORID = "doctorid";
	public static String PROP_LOGIN = "login";
	public static String PROP_ID = "id";
	public static String PROP_MANAGER = "manager";
	public static String PROP_PERSON = "person";
	public static String PROP_TELECALLERID = "telecallerid";


	// constructors
	public BaseUser () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseUser (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public BaseUser (
		java.lang.Long id,
		com.ihexa.common.admin.user.Person person,
		com.ihexa.common.admin.user.Role role) {

		this.setId(id);
		this.setPerson(person);
		this.setRole(role);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// one to one
	private com.ihexa.common.admin.user.Login login;

	// many to one
	private com.ihexa.common.admin.user.User manager;
	private com.ihexa.common.admin.user.Person person;
	private com.ihexa.common.admin.user.Role role;
	private com.ihexa.common.admin.doctor.Doctor doctorid;
	private com.ihexa.common.admin.telecaller.Telecaller telecallerid;

	// collections
	private java.util.Set<com.ihexa.common.admin.userskill.UserSkill> userSkills;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: login
	 */
	public com.ihexa.common.admin.user.Login getLogin () {
		return login;
	}

	/**
	 * Set the value related to the column: login
	 * @param login the login value
	 */
	public void setLogin (com.ihexa.common.admin.user.Login login) {
		this.login = login;
	}



	/**
	 * Return the value associated with the column: ManagerID
	 */
	public com.ihexa.common.admin.user.User getManager () {
		return manager;
	}

	/**
	 * Set the value related to the column: ManagerID
	 * @param manager the ManagerID value
	 */
	public void setManager (com.ihexa.common.admin.user.User manager) {
		this.manager = manager;
	}



	/**
	 * Return the value associated with the column: PersonID
	 */
	public com.ihexa.common.admin.user.Person getPerson () {
		return person;
	}

	/**
	 * Set the value related to the column: PersonID
	 * @param person the PersonID value
	 */
	public void setPerson (com.ihexa.common.admin.user.Person person) {
		this.person = person;
	}



	/**
	 * Return the value associated with the column: RoleID
	 */
	public com.ihexa.common.admin.user.Role getRole () {
		return role;
	}

	/**
	 * Set the value related to the column: RoleID
	 * @param role the RoleID value
	 */
	public void setRole (com.ihexa.common.admin.user.Role role) {
		this.role = role;
	}



	/**
	 * Return the value associated with the column: Doctorid
	 */
	public com.ihexa.common.admin.doctor.Doctor getDoctorid () {
		return doctorid;
	}

	/**
	 * Set the value related to the column: Doctorid
	 * @param doctorid the Doctorid value
	 */
	public void setDoctorid (com.ihexa.common.admin.doctor.Doctor doctorid) {
		this.doctorid = doctorid;
	}



	/**
	 * Return the value associated with the column: Telecallerid
	 */
	public com.ihexa.common.admin.telecaller.Telecaller getTelecallerid () {
		return telecallerid;
	}

	/**
	 * Set the value related to the column: Telecallerid
	 * @param telecallerid the Telecallerid value
	 */
	public void setTelecallerid (com.ihexa.common.admin.telecaller.Telecaller telecallerid) {
		this.telecallerid = telecallerid;
	}



	/**
	 * Return the value associated with the column: userSkills
	 */
	public java.util.Set<com.ihexa.common.admin.userskill.UserSkill> getUserSkills () {
		return userSkills;
	}

	/**
	 * Set the value related to the column: userSkills
	 * @param userSkills the userSkills value
	 */
	public void setUserSkills (java.util.Set<com.ihexa.common.admin.userskill.UserSkill> userSkills) {
		this.userSkills = userSkills;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.user.User)) return false;
		else {
			com.ihexa.common.admin.user.User user = (com.ihexa.common.admin.user.User) obj;
			if (null == this.getId() || null == user.getId()) return false;
			else return (this.getId().equals(user.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}