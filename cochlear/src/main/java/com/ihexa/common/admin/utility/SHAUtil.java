package com.ihexa.common.admin.utility;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class SHAUtil {

	public String generateHashPassword(String password, String salt)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(password.getBytes("UTF-8"));
		md.update(salt.getBytes("UTF-8"));

		byte byteData[] = md.digest();

		// convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
					.substring(1));
		}

		return sb.toString();
	}

	public String generateSalt(String UID) throws NoSuchAlgorithmException,
			UnsupportedEncodingException {

		Random rand = new Random();
		byte[] salt = new byte[12];
		rand.nextBytes(salt);

		MessageDigest m = MessageDigest.getInstance("MD5");
		m.update(salt);
		m.update(UID.getBytes("UTF8"));
		byte s[] = m.digest();
		String result = "";
		for (int i = 0; i < s.length; i++) {
			result += Integer.toHexString((0x000000ff & s[i]) | 0xffffff00)
					.substring(6);
		}

		return result;
	}
	
	public static void main(String[] args){
		
		SHAUtil shaUtil=new SHAUtil();
		try {
			String salt=shaUtil.generateSalt("6");
			System.out.println("Salt: "+salt);
			String password=shaUtil.generateHashPassword("bhushan.darandale@gmail.com", salt);
			System.out.println("password: "+password);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
