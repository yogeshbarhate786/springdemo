package com.ihexa.common.admin.order;

import java.util.List;

import com.ihexa.common.admin.cart.Cart;


public class OrderPojo 
{
private String orderid;
private String status;
private String addeddate;
private int   cartid;
public int getCartid() {
	return cartid;
}
public void setCartid(int cartid) {
	this.cartid = cartid;
}
private List<Cart> cart;

public List<Cart> getCart() {
	return cart;
}
public void setCart(List<Cart> cart) {
	this.cart = cart;
}
public String getOrderid() {
	return orderid;
}
public void setOrderid(String orderid) {
	this.orderid = orderid;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getAddeddate() {
	return addeddate;
}
public void setAddeddate(String addeddate) {
	this.addeddate = addeddate;
}

}





