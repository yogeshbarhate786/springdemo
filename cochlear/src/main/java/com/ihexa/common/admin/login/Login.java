package com.ihexa.common.admin.login;

import com.ihexa.common.admin.login.base.BaseLogin;



public class Login extends BaseLogin {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Login () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Login (java.lang.Long id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public Login (
		java.lang.Long id,
		java.util.Date lastLoginDate,
		java.lang.String loginName,
		java.lang.String password,
		java.util.Date pwdExpirationDate,
		java.util.Date statusActiveDate,
		java.util.Date statusPasswordResetDate) {

		super (
			id,
			lastLoginDate,
			loginName,
			password,
			pwdExpirationDate,
			statusActiveDate,
			statusPasswordResetDate);
	}

/*[CONSTRUCTOR MARKER END]*/


}