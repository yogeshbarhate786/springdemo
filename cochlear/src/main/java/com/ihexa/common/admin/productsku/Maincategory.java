package com.ihexa.common.admin.productsku;

import com.ihexa.common.admin.productsku.base.BaseMaincategory;



public class Maincategory extends BaseMaincategory {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Maincategory () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Maincategory (java.lang.Long id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public Maincategory (
		java.lang.Long id,
		java.util.Date createdate) {

		super (
			id,
			createdate);
	}

/*[CONSTRUCTOR MARKER END]*/


}