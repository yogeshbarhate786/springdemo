package com.ihexa.common.admin.order;

import com.ihexa.common.admin.order.base.BaseWishlist;



public class Wishlist extends BaseWishlist {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Wishlist () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Wishlist (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}