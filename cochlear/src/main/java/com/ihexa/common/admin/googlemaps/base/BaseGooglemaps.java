package com.ihexa.common.admin.googlemaps.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the googlemaps table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="googlemaps"
 */

public abstract class BaseGooglemaps  implements Serializable {

	public static String REF = "Googlemaps";
	public static String PROP_AREA_CODE = "AreaCode";
	public static String PROP_POLY = "Poly";
	public static String PROP_AREA_MANAGER = "AreaManager";
	public static String PROP_AREA_NAME = "AreaName";
	public static String PROP_ID = "Id";


	// constructors
	public BaseGooglemaps () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseGooglemaps (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public BaseGooglemaps (
		java.lang.Long id,
		java.lang.String poly) {

		this.setId(id);
		this.setPoly(poly);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String poly;
	private java.lang.String areaCode;
	private java.lang.String areaManager;
	private java.lang.String areaName;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: poly
	 */
	public java.lang.String getPoly () {
		return poly;
	}

	/**
	 * Set the value related to the column: poly
	 * @param poly the poly value
	 */
	public void setPoly (java.lang.String poly) {
		this.poly = poly;
	}



	/**
	 * Return the value associated with the column: area_code
	 */
	public java.lang.String getAreaCode () {
		return areaCode;
	}

	/**
	 * Set the value related to the column: area_code
	 * @param areaCode the area_code value
	 */
	public void setAreaCode (java.lang.String areaCode) {
		this.areaCode = areaCode;
	}



	/**
	 * Return the value associated with the column: area_manager
	 */
	public java.lang.String getAreaManager () {
		return areaManager;
	}

	/**
	 * Set the value related to the column: area_manager
	 * @param areaManager the area_manager value
	 */
	public void setAreaManager (java.lang.String areaManager) {
		this.areaManager = areaManager;
	}



	/**
	 * Return the value associated with the column: area_name
	 */
	public java.lang.String getAreaName () {
		return areaName;
	}

	/**
	 * Set the value related to the column: area_name
	 * @param areaName the area_name value
	 */
	public void setAreaName (java.lang.String areaName) {
		this.areaName = areaName;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.googlemaps.Googlemaps)) return false;
		else {
			com.ihexa.common.admin.googlemaps.Googlemaps googlemaps = (com.ihexa.common.admin.googlemaps.Googlemaps) obj;
			if (null == this.getId() || null == googlemaps.getId()) return false;
			else return (this.getId().equals(googlemaps.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}