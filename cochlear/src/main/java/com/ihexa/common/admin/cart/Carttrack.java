package com.ihexa.common.admin.cart;

import com.ihexa.common.admin.cart.base.BaseCarttrack;



public class Carttrack extends BaseCarttrack {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Carttrack () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Carttrack (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}