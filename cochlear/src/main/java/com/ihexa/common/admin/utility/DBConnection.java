package com.ihexa.common.admin.utility;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Locale;
import java.util.ResourceBundle;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

//DBConnection Class used for Database connectivity

public class DBConnection {

	java.sql.Connection cn = null;
	java.sql.Statement stmt = null;
	java.sql.PreparedStatement preparedStatement = null;
	ResultSet rs = null;

	public DBConnection() {
		try {
			Connection cn = null;
			Statement stmt = null;
			PreparedStatement preparedStatement = null;
			ResultSet rs = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// resource bundle
	// Locale locale=Locale.getDefault ();
	// ResourceBundle bundle = ResourceBundle.getBundle("IHexaResources", Locale.getDefault());
	/**
	 * to get Property value from .properties file
	 * 
	 * @param key
	 * @return value of that key
	 */
	public String getProperty(String key) {
		return ResourceBundle.getBundle("IHexaResources", Locale.getDefault()).getString(key);
	}

	// Login Function for Checking Successful Login in application
	public void setAutoCommit(boolean commit) {
		try {
			cn = DriverManager.getConnection(getProperty("DB_url"), getProperty("DB_UserName"), getProperty("DB_Password"));
			cn.setAutoCommit(false);
			cn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * private String "jdbc:mysql://localhost:3306/ihexademo"{ return bundle.getString ("db.connection.url"); } private String "root"{ String user = bundle.getString
	 * ("db.connection.user"); if(user !=null && !"".equals(user.trim())) return user.trim(); else{ user = "root"; return user; } }
	 * 
	 * private String "root"{ String pass = bundle.getString ("db.connection.pass"); if(pass !=null && !"".equals(pass.trim())) return pass.trim(); else{ pass = "root"; return
	 * pass; } }
	 */

	public boolean login(String username, String password) {
		try {
			Class.forName(getProperty("DB_Driver"));
			cn = DriverManager.getConnection(getProperty("DB_url"), getProperty("DB_UserName"), getProperty("DB_Password"));
			stmt = cn.createStatement();
			rs = stmt.executeQuery("select * from login where Username='" + username + "' and Password='" + password + "'");
			if (rs.next()) {
				cn.close();
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	// Select Function for getting data from Database

	public ResultSet select(String query) {
		try {
			Class.forName(getProperty("DB_Driver"));
			cn = DriverManager.getConnection(getProperty("DB_url"), getProperty("DB_UserName"), getProperty("DB_Password"));
			stmt = cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			System.out.println("QUERY fired for SELECT STATEMENT :: " + query);
			rs = stmt.executeQuery(query);
			return rs;
		} catch (Exception e) {
			System.out.println("*** Exception Occurred during SELECT QUERY ***");
			e.printStackTrace();
		} finally {

		}
		return null;
	}

	// Select Function for storing data in Database

	public boolean insert(String query) {
		try {
			Class.forName(getProperty("DB_Driver"));
			cn = DriverManager.getConnection(getProperty("DB_url"), getProperty("DB_UserName"), getProperty("DB_Password"));
			cn.setAutoCommit(false);
			stmt = cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			System.out.println("QUERY in INSERT:" + query);
			int i = stmt.executeUpdate(query);
			System.out.println("After update...!!!");
			cn.commit();
			cn.close();
			System.out.println(" i--------------->" + i);
			if (i == 0) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		finally {
			cn = null;
		}
	}

	// this function is used to insert escape sequens into the databse

	private String convertString(String str) {

		if (str.contains("'")) {
			str = str.replaceAll("'", "\'");
		}

		if (str.contains("=\'")) {
			str = str.replaceAll("=\'", "='");
		}

		if (str.contains("(\'")) {
			str = str.replaceAll("(\'", "('");
		}
		if (str.contains("\')")) {
			str = str.replaceAll("\')", "')");
		}
		if (str.contains(",\'")) {
			str = str.replaceAll(",\'", ",'");
		}
		if (str.contains("\',")) {
			str = str.replaceAll("\',", "',");
		}

		System.out.println(str);
		return str;

	}

	public boolean delete(String query) {
		try {
			Class.forName(getProperty("DB_Driver"));
			cn = DriverManager.getConnection(getProperty("DB_url"), getProperty("DB_UserName"), getProperty("DB_Password"));
			stmt = cn.createStatement();
			System.out.println("query : " + query);
			int i = stmt.executeUpdate(query);
			if (i == 0) {
				cn.commit();
				cn.close();
				return false;
			} else {
				cn.commit();
				cn.close();
				return true;
			}

			// cn.commit();

		} catch (Exception e) {
			System.out.println("*** Exception Occurred during SELECT QUERY ***");
			e.printStackTrace();
			return false;
		} finally {

		}

	}

	// Close Function for Closing connection with Database

	public void close() {
		try {
			if (cn != null)
				cn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// destroy all connections
	public void destroy() {
		try {
			cn = null;
			stmt = null;
			preparedStatement = null;
			rs = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
