package com.ihexa.common.admin.productsku;

import com.ihexa.common.admin.productsku.base.BaseSubcategory;



public class Subcategory extends BaseSubcategory {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Subcategory () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Subcategory (java.lang.Long id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public Subcategory (
		java.lang.Long id,
		java.util.Date createddate) {

		super (
			id,
			createddate);
	}

/*[CONSTRUCTOR MARKER END]*/


}