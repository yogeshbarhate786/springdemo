package com.ihexa.common.admin.user.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the occupation table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="occupation"
 */

public abstract class BaseOccupation  implements Serializable {

	public static String REF = "Occupation";
	public static String PROP_OCCUPATION_NAME = "occupationName";
	public static String PROP_ID = "id";


	// constructors
	public BaseOccupation () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseOccupation (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String occupationName;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: OccupationName
	 */
	public java.lang.String getOccupationName () {
		return occupationName;
	}

	/**
	 * Set the value related to the column: OccupationName
	 * @param occupationName the OccupationName value
	 */
	public void setOccupationName (java.lang.String occupationName) {
		this.occupationName = occupationName;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.user.Occupation)) return false;
		else {
			com.ihexa.common.admin.user.Occupation occupation = (com.ihexa.common.admin.user.Occupation) obj;
			if (null == this.getId() || null == occupation.getId()) return false;
			else return (this.getId().equals(occupation.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}