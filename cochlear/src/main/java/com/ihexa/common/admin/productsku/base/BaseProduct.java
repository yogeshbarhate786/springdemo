package com.ihexa.common.admin.productsku.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the product table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="product"
 */

public abstract class BaseProduct  implements Serializable {

	public static String REF = "Product";
	public static String PROP_STATUS = "Status";
	public static String PROP_WARRANTY = "Warranty";
	public static String PROP_SUBCATID = "Subcatid";
	public static String PROP_CREATEDDATE = "Createddate";
	public static String PROP_CATID = "Catid";
	public static String PROP_NAME = "Name";
	public static String PROP_DETAILS = "Details";
	public static String PROP_UPDATEDDATE = "Updateddate";
	public static String PROP_MAINCATID = "Maincatid";
	public static String PROP_VERSION = "Version";
	public static String PROP_PRICE = "Price";
	public static String PROP_ID = "Id";
	public static String PROP_PICURL = "Picurl";


	// constructors
	public BaseProduct () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseProduct (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String name;
	private java.math.BigDecimal version;
	private java.lang.Integer price;
	private java.lang.String warranty;
	private java.lang.String picurl;
	private java.lang.String details;
	private java.lang.String status;
	private java.util.Date createddate;
	private java.util.Date updateddate;

	// many to one
	private com.ihexa.common.admin.productsku.Maincategory maincatid;
	private com.ihexa.common.admin.productsku.Category catid;
	private com.ihexa.common.admin.productsku.Subcategory subcatid;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="id"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: name
	 */
	public java.lang.String getName () {
		return name;
	}

	/**
	 * Set the value related to the column: name
	 * @param name the name value
	 */
	public void setName (java.lang.String name) {
		this.name = name;
	}



	/**
	 * Return the value associated with the column: version
	 */
	public java.math.BigDecimal getVersion () {
		return version;
	}

	/**
	 * Set the value related to the column: version
	 * @param version the version value
	 */
	public void setVersion (java.math.BigDecimal version) {
		this.version = version;
	}



	/**
	 * Return the value associated with the column: price
	 */
	public java.lang.Integer getPrice () {
		return price;
	}

	/**
	 * Set the value related to the column: price
	 * @param price the price value
	 */
	public void setPrice (java.lang.Integer price) {
		this.price = price;
	}



	/**
	 * Return the value associated with the column: warranty
	 */
	public java.lang.String getWarranty () {
		return warranty;
	}

	/**
	 * Set the value related to the column: warranty
	 * @param warranty the warranty value
	 */
	public void setWarranty (java.lang.String warranty) {
		this.warranty = warranty;
	}



	/**
	 * Return the value associated with the column: picurl
	 */
	public java.lang.String getPicurl () {
		return picurl;
	}

	/**
	 * Set the value related to the column: picurl
	 * @param picurl the picurl value
	 */
	public void setPicurl (java.lang.String picurl) {
		this.picurl = picurl;
	}



	/**
	 * Return the value associated with the column: details
	 */
	public java.lang.String getDetails () {
		return details;
	}

	/**
	 * Set the value related to the column: details
	 * @param details the details value
	 */
	public void setDetails (java.lang.String details) {
		this.details = details;
	}



	/**
	 * Return the value associated with the column: status
	 */
	public java.lang.String getStatus () {
		return status;
	}

	/**
	 * Set the value related to the column: status
	 * @param status the status value
	 */
	public void setStatus (java.lang.String status) {
		this.status = status;
	}



	/**
	 * Return the value associated with the column: createddate
	 */
	public java.util.Date getCreateddate () {
		return createddate;
	}

	/**
	 * Set the value related to the column: createddate
	 * @param createddate the createddate value
	 */
	public void setCreateddate (java.util.Date createddate) {
		this.createddate = createddate;
	}



	/**
	 * Return the value associated with the column: updateddate
	 */
	public java.util.Date getUpdateddate () {
		return updateddate;
	}

	/**
	 * Set the value related to the column: updateddate
	 * @param updateddate the updateddate value
	 */
	public void setUpdateddate (java.util.Date updateddate) {
		this.updateddate = updateddate;
	}



	/**
	 * Return the value associated with the column: maincatid
	 */
	public com.ihexa.common.admin.productsku.Maincategory getMaincatid () {
		return maincatid;
	}

	/**
	 * Set the value related to the column: maincatid
	 * @param maincatid the maincatid value
	 */
	public void setMaincatid (com.ihexa.common.admin.productsku.Maincategory maincatid) {
		this.maincatid = maincatid;
	}



	/**
	 * Return the value associated with the column: catid
	 */
	public com.ihexa.common.admin.productsku.Category getCatid () {
		return catid;
	}

	/**
	 * Set the value related to the column: catid
	 * @param catid the catid value
	 */
	public void setCatid (com.ihexa.common.admin.productsku.Category catid) {
		this.catid = catid;
	}



	/**
	 * Return the value associated with the column: subcatid
	 */
	public com.ihexa.common.admin.productsku.Subcategory getSubcatid () {
		return subcatid;
	}

	/**
	 * Set the value related to the column: subcatid
	 * @param subcatid the subcatid value
	 */
	public void setSubcatid (com.ihexa.common.admin.productsku.Subcategory subcatid) {
		this.subcatid = subcatid;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.productsku.Product)) return false;
		else {
			com.ihexa.common.admin.productsku.Product product = (com.ihexa.common.admin.productsku.Product) obj;
			if (null == this.getId() || null == product.getId()) return false;
			else return (this.getId().equals(product.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}