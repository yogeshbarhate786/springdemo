package com.ihexa.common.admin.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.prounify.framework.base.action.ActionPath;
import com.prounify.framework.context.UserContext;
import com.prounify.framework.context.impl.HttpServletUserContext;

public class LoginFilter implements Filter {
	private FilterConfig filterConfig;

	public FilterConfig getFilterConfig() {
		return filterConfig;
	}

	public void setFilterConfig(FilterConfig filterConfig) {
		this.filterConfig = filterConfig;

	}

	public void init(FilterConfig arg0) throws ServletException {
		setFilterConfig(arg0);
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		ServletContext context = filterConfig.getServletContext();
		String skipURL = filterConfig.getInitParameter("SKIP_URL");

		// instanciate UserContext
		createContextInstance(context, req);

		if (UserContext.isAuthenticated() && UserContext.getAuthorizationMap().isEmpty()) {

			// User is deleted
			UserContext.invalidateAuthenticatedSession();

			return;
		}

		// code for maintain the selected tabs order for each request
		String tabId = request.getParameter("tabid");
		if (StringUtils.isNotBlank(tabId)) {

			if (UserContext.getSessionScope() != null)
				UserContext.getSessionScope().put("tabid", tabId);

		}

		ActionPath path = ActionPath.getInstanceFor(req, "");

		// if user is already authenticated or request URL is login then allow user to proceed
		// otherwise redirect to login page
		if (UserContext.isAuthenticated() || checkURLToSkip(skipURL, path)) {
			filterChain.doFilter(request, response);
		} else {
			((HttpServletResponse) res).sendError(404, "Invalid login");

			return;
		}

	}

	public void destroy() {
	}

	private UserContext createContextInstance(ServletContext ctx, HttpServletRequest request) {
		return new HttpServletUserContext(ctx, request);
	}

	/**
	 * method used to check url pattern with request path matching
	 * 
	 * @param pattern
	 * @param path
	 * @return
	 */
	private boolean checkURLToSkip(String pattern, ActionPath path) {

		if ((pattern != null)) {

			if (pattern.contains(",")) {
				String[] methods = pattern.split(",");

				for (String method : methods) {

					if (method.equals(path.getPath())) {
						return true;
					}
				}
			} else if (pattern.equals(path.getPath())) {
				return true;
			}

		}

		return false;

	}

}
