package com.ihexa.common.admin.role.service;

import com.prounify.framework.base.service.Service;

/**
 * Service layer interface for User entity.
 */

public interface RoleService<T> extends Service<T> {

}
