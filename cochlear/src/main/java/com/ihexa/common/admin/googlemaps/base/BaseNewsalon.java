package com.ihexa.common.admin.googlemaps.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the newsalon table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="newsalon"
 */

public abstract class BaseNewsalon  implements Serializable {

	public static String REF = "Newsalon";
	public static String PROP_MONTHLYINCOME = "Monthlyincome";
	public static String PROP_BUSNIESSYEAR = "Busniessyear";
	public static String PROP_DSA = "Dsa";
	public static String PROP_PHONE1 = "Phone1";
	public static String PROP_TYPEOF_LOCALITY = "TypeofLocality";
	public static String PROP_SALONID = "Salonid";
	public static String PROP_NOOFHAIR_P_M = "NoofhairPM";
	public static String PROP_PHONE2 = "Phone2";
	public static String PROP_PINCODE = "Pincode";
	public static String PROP_ANDROID_IMAGEPATH = "AndroidImagepath";
	public static String PROP_SERVICEOFFERED = "Serviceoffered";
	public static String PROP_INFRASTRUCTURE = "Infrastructure";
	public static String PROP_CUSSTYLEFREQUENCY_P_Y = "CusstylefrequencyPY";
	public static String PROP_SALONNAME = "Salonname";
	public static String PROP_LOCALITY_CLASS = "LocalityClass";
	public static String PROP_LANDMARK = "Landmark";
	public static String PROP_ADDRESS2 = "Address2";
	public static String PROP_PREFEREDBRAND = "Preferedbrand";
	public static String PROP_ADDRESS1 = "Address1";
	public static String PROP_LOANTAKEN = "Loantaken";
	public static String PROP_LATITUDE = "Latitude";
	public static String PROP_IMAGEPATH = "Imagepath";
	public static String PROP_NOOFSALONWITHSAMEBRAND = "Noofsalonwithsamebrand";
	public static String PROP_CUSTOMERFORTOUCHUP = "Customerfortouchup";
	public static String PROP_CREATEDBY = "Createdby";
	public static String PROP_SALONFOR = "Salonfor";
	public static String PROP_DATEOFADD = "Dateofadd";
	public static String PROP_NOOFCHAIR = "Noofchair";
	public static String PROP_CUSTOMERCATERED = "Customercatered";
	public static String PROP_AVERAGEPURCHASE = "Averagepurchase";
	public static String PROP_COUNTRY = "Country";
	public static String PROP_CITY = "City";
	public static String PROP_STATUS = "Status";
	public static String PROP_EMAIL = "Email";
	public static String PROP_STATE = "State";
	public static String PROP_NOOFFEMALESTYLISH = "Nooffemalestylish";
	public static String PROP_SALON_ASSOCIATION = "SalonAssociation";
	public static String PROP_ID = "Id";
	public static String PROP_LONGITUDE = "Longitude";
	public static String PROP_NOOFMALESTYLISH = "Noofmalestylish";
	public static String PROP_TRICYCLETAKEN = "Tricycletaken";
	public static String PROP_PURCHASEFROM = "Purchasefrom";
	public static String PROP_SERVICE_CHARGE = "ServiceCharge";
	public static String PROP_AVGCUSFOR_HAIRFIXING = "AvgcusforHairfixing";


	// constructors
	public BaseNewsalon () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseNewsalon (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String salonname;
	private java.lang.String address1;
	private java.lang.String address2;
	private java.lang.String city;
	private java.lang.String state;
	private java.lang.String country;
	private java.lang.String pincode;
	private java.lang.String landmark;
	private java.lang.String email;
	private java.lang.String typeofLocality;
	private java.lang.String localityClass;
	private java.lang.String phone1;
	private java.lang.String phone2;
	private java.lang.String noofchair;
	private java.lang.String noofmalestylish;
	private java.lang.String nooffemalestylish;
	private java.lang.String tricycletaken;
	private java.lang.String busniessyear;
	private java.lang.String salonAssociation;
	private java.lang.String infrastructure;
	private java.lang.String loantaken;
	private java.lang.String noofsalonwithsamebrand;
	private java.lang.String salonfor;
	private java.lang.String preferedbrand;
	private java.lang.String noofhairPM;
	private java.lang.String purchasefrom;
	private java.lang.String averagepurchase;
	private java.lang.String avgcusforHairfixing;
	private java.lang.String customerfortouchup;
	private java.lang.String cusstylefrequencyPY;
	private java.lang.String monthlyincome;
	private java.lang.String customercatered;
	private java.lang.String serviceoffered;
	private java.lang.String serviceCharge;
	private java.lang.String imagepath;
	private java.lang.String status;
	private java.lang.String latitude;
	private java.lang.String longitude;
	private java.lang.String salonid;
	private java.lang.String dsa;
	private java.lang.String createdby;
	private java.lang.String dateofadd;
	private java.lang.String androidImagepath;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: Salonname
	 */
	public java.lang.String getSalonname () {
		return salonname;
	}

	/**
	 * Set the value related to the column: Salonname
	 * @param salonname the Salonname value
	 */
	public void setSalonname (java.lang.String salonname) {
		this.salonname = salonname;
	}



	/**
	 * Return the value associated with the column: Address1
	 */
	public java.lang.String getAddress1 () {
		return address1;
	}

	/**
	 * Set the value related to the column: Address1
	 * @param address1 the Address1 value
	 */
	public void setAddress1 (java.lang.String address1) {
		this.address1 = address1;
	}



	/**
	 * Return the value associated with the column: Address2
	 */
	public java.lang.String getAddress2 () {
		return address2;
	}

	/**
	 * Set the value related to the column: Address2
	 * @param address2 the Address2 value
	 */
	public void setAddress2 (java.lang.String address2) {
		this.address2 = address2;
	}



	/**
	 * Return the value associated with the column: City
	 */
	public java.lang.String getCity () {
		return city;
	}

	/**
	 * Set the value related to the column: City
	 * @param city the City value
	 */
	public void setCity (java.lang.String city) {
		this.city = city;
	}



	/**
	 * Return the value associated with the column: State
	 */
	public java.lang.String getState () {
		return state;
	}

	/**
	 * Set the value related to the column: State
	 * @param state the State value
	 */
	public void setState (java.lang.String state) {
		this.state = state;
	}



	/**
	 * Return the value associated with the column: Country
	 */
	public java.lang.String getCountry () {
		return country;
	}

	/**
	 * Set the value related to the column: Country
	 * @param country the Country value
	 */
	public void setCountry (java.lang.String country) {
		this.country = country;
	}



	/**
	 * Return the value associated with the column: pincode
	 */
	public java.lang.String getPincode () {
		return pincode;
	}

	/**
	 * Set the value related to the column: pincode
	 * @param pincode the pincode value
	 */
	public void setPincode (java.lang.String pincode) {
		this.pincode = pincode;
	}



	/**
	 * Return the value associated with the column: Landmark
	 */
	public java.lang.String getLandmark () {
		return landmark;
	}

	/**
	 * Set the value related to the column: Landmark
	 * @param landmark the Landmark value
	 */
	public void setLandmark (java.lang.String landmark) {
		this.landmark = landmark;
	}



	/**
	 * Return the value associated with the column: Email
	 */
	public java.lang.String getEmail () {
		return email;
	}

	/**
	 * Set the value related to the column: Email
	 * @param email the Email value
	 */
	public void setEmail (java.lang.String email) {
		this.email = email;
	}



	/**
	 * Return the value associated with the column: TypeofLocality
	 */
	public java.lang.String getTypeofLocality () {
		return typeofLocality;
	}

	/**
	 * Set the value related to the column: TypeofLocality
	 * @param typeofLocality the TypeofLocality value
	 */
	public void setTypeofLocality (java.lang.String typeofLocality) {
		this.typeofLocality = typeofLocality;
	}



	/**
	 * Return the value associated with the column: LocalityClass
	 */
	public java.lang.String getLocalityClass () {
		return localityClass;
	}

	/**
	 * Set the value related to the column: LocalityClass
	 * @param localityClass the LocalityClass value
	 */
	public void setLocalityClass (java.lang.String localityClass) {
		this.localityClass = localityClass;
	}



	/**
	 * Return the value associated with the column: Phone1
	 */
	public java.lang.String getPhone1 () {
		return phone1;
	}

	/**
	 * Set the value related to the column: Phone1
	 * @param phone1 the Phone1 value
	 */
	public void setPhone1 (java.lang.String phone1) {
		this.phone1 = phone1;
	}



	/**
	 * Return the value associated with the column: Phone2
	 */
	public java.lang.String getPhone2 () {
		return phone2;
	}

	/**
	 * Set the value related to the column: Phone2
	 * @param phone2 the Phone2 value
	 */
	public void setPhone2 (java.lang.String phone2) {
		this.phone2 = phone2;
	}



	/**
	 * Return the value associated with the column: Noofchair
	 */
	public java.lang.String getNoofchair () {
		return noofchair;
	}

	/**
	 * Set the value related to the column: Noofchair
	 * @param noofchair the Noofchair value
	 */
	public void setNoofchair (java.lang.String noofchair) {
		this.noofchair = noofchair;
	}



	/**
	 * Return the value associated with the column: Noofmalestylish
	 */
	public java.lang.String getNoofmalestylish () {
		return noofmalestylish;
	}

	/**
	 * Set the value related to the column: Noofmalestylish
	 * @param noofmalestylish the Noofmalestylish value
	 */
	public void setNoofmalestylish (java.lang.String noofmalestylish) {
		this.noofmalestylish = noofmalestylish;
	}



	/**
	 * Return the value associated with the column: Nooffemalestylish
	 */
	public java.lang.String getNooffemalestylish () {
		return nooffemalestylish;
	}

	/**
	 * Set the value related to the column: Nooffemalestylish
	 * @param nooffemalestylish the Nooffemalestylish value
	 */
	public void setNooffemalestylish (java.lang.String nooffemalestylish) {
		this.nooffemalestylish = nooffemalestylish;
	}



	/**
	 * Return the value associated with the column: tricycletaken
	 */
	public java.lang.String getTricycletaken () {
		return tricycletaken;
	}

	/**
	 * Set the value related to the column: tricycletaken
	 * @param tricycletaken the tricycletaken value
	 */
	public void setTricycletaken (java.lang.String tricycletaken) {
		this.tricycletaken = tricycletaken;
	}



	/**
	 * Return the value associated with the column: Busniessyear
	 */
	public java.lang.String getBusniessyear () {
		return busniessyear;
	}

	/**
	 * Set the value related to the column: Busniessyear
	 * @param busniessyear the Busniessyear value
	 */
	public void setBusniessyear (java.lang.String busniessyear) {
		this.busniessyear = busniessyear;
	}



	/**
	 * Return the value associated with the column: SalonAssociation
	 */
	public java.lang.String getSalonAssociation () {
		return salonAssociation;
	}

	/**
	 * Set the value related to the column: SalonAssociation
	 * @param salonAssociation the SalonAssociation value
	 */
	public void setSalonAssociation (java.lang.String salonAssociation) {
		this.salonAssociation = salonAssociation;
	}



	/**
	 * Return the value associated with the column: Infrastructure
	 */
	public java.lang.String getInfrastructure () {
		return infrastructure;
	}

	/**
	 * Set the value related to the column: Infrastructure
	 * @param infrastructure the Infrastructure value
	 */
	public void setInfrastructure (java.lang.String infrastructure) {
		this.infrastructure = infrastructure;
	}



	/**
	 * Return the value associated with the column: loantaken
	 */
	public java.lang.String getLoantaken () {
		return loantaken;
	}

	/**
	 * Set the value related to the column: loantaken
	 * @param loantaken the loantaken value
	 */
	public void setLoantaken (java.lang.String loantaken) {
		this.loantaken = loantaken;
	}



	/**
	 * Return the value associated with the column: Noofsalonwithsamebrand
	 */
	public java.lang.String getNoofsalonwithsamebrand () {
		return noofsalonwithsamebrand;
	}

	/**
	 * Set the value related to the column: Noofsalonwithsamebrand
	 * @param noofsalonwithsamebrand the Noofsalonwithsamebrand value
	 */
	public void setNoofsalonwithsamebrand (java.lang.String noofsalonwithsamebrand) {
		this.noofsalonwithsamebrand = noofsalonwithsamebrand;
	}



	/**
	 * Return the value associated with the column: Salonfor
	 */
	public java.lang.String getSalonfor () {
		return salonfor;
	}

	/**
	 * Set the value related to the column: Salonfor
	 * @param salonfor the Salonfor value
	 */
	public void setSalonfor (java.lang.String salonfor) {
		this.salonfor = salonfor;
	}



	/**
	 * Return the value associated with the column: Preferedbrand
	 */
	public java.lang.String getPreferedbrand () {
		return preferedbrand;
	}

	/**
	 * Set the value related to the column: Preferedbrand
	 * @param preferedbrand the Preferedbrand value
	 */
	public void setPreferedbrand (java.lang.String preferedbrand) {
		this.preferedbrand = preferedbrand;
	}



	/**
	 * Return the value associated with the column: NoofhairPM
	 */
	public java.lang.String getNoofhairPM () {
		return noofhairPM;
	}

	/**
	 * Set the value related to the column: NoofhairPM
	 * @param noofhairPM the NoofhairPM value
	 */
	public void setNoofhairPM (java.lang.String noofhairPM) {
		this.noofhairPM = noofhairPM;
	}



	/**
	 * Return the value associated with the column: Purchasefrom
	 */
	public java.lang.String getPurchasefrom () {
		return purchasefrom;
	}

	/**
	 * Set the value related to the column: Purchasefrom
	 * @param purchasefrom the Purchasefrom value
	 */
	public void setPurchasefrom (java.lang.String purchasefrom) {
		this.purchasefrom = purchasefrom;
	}



	/**
	 * Return the value associated with the column: Averagepurchase
	 */
	public java.lang.String getAveragepurchase () {
		return averagepurchase;
	}

	/**
	 * Set the value related to the column: Averagepurchase
	 * @param averagepurchase the Averagepurchase value
	 */
	public void setAveragepurchase (java.lang.String averagepurchase) {
		this.averagepurchase = averagepurchase;
	}



	/**
	 * Return the value associated with the column: AvgcusforHairfixing
	 */
	public java.lang.String getAvgcusforHairfixing () {
		return avgcusforHairfixing;
	}

	/**
	 * Set the value related to the column: AvgcusforHairfixing
	 * @param avgcusforHairfixing the AvgcusforHairfixing value
	 */
	public void setAvgcusforHairfixing (java.lang.String avgcusforHairfixing) {
		this.avgcusforHairfixing = avgcusforHairfixing;
	}



	/**
	 * Return the value associated with the column: Customerfortouchup
	 */
	public java.lang.String getCustomerfortouchup () {
		return customerfortouchup;
	}

	/**
	 * Set the value related to the column: Customerfortouchup
	 * @param customerfortouchup the Customerfortouchup value
	 */
	public void setCustomerfortouchup (java.lang.String customerfortouchup) {
		this.customerfortouchup = customerfortouchup;
	}



	/**
	 * Return the value associated with the column: CusstylefrequencyPY
	 */
	public java.lang.String getCusstylefrequencyPY () {
		return cusstylefrequencyPY;
	}

	/**
	 * Set the value related to the column: CusstylefrequencyPY
	 * @param cusstylefrequencyPY the CusstylefrequencyPY value
	 */
	public void setCusstylefrequencyPY (java.lang.String cusstylefrequencyPY) {
		this.cusstylefrequencyPY = cusstylefrequencyPY;
	}



	/**
	 * Return the value associated with the column: Monthlyincome
	 */
	public java.lang.String getMonthlyincome () {
		return monthlyincome;
	}

	/**
	 * Set the value related to the column: Monthlyincome
	 * @param monthlyincome the Monthlyincome value
	 */
	public void setMonthlyincome (java.lang.String monthlyincome) {
		this.monthlyincome = monthlyincome;
	}



	/**
	 * Return the value associated with the column: customercatered
	 */
	public java.lang.String getCustomercatered () {
		return customercatered;
	}

	/**
	 * Set the value related to the column: customercatered
	 * @param customercatered the customercatered value
	 */
	public void setCustomercatered (java.lang.String customercatered) {
		this.customercatered = customercatered;
	}



	/**
	 * Return the value associated with the column: Serviceoffered
	 */
	public java.lang.String getServiceoffered () {
		return serviceoffered;
	}

	/**
	 * Set the value related to the column: Serviceoffered
	 * @param serviceoffered the Serviceoffered value
	 */
	public void setServiceoffered (java.lang.String serviceoffered) {
		this.serviceoffered = serviceoffered;
	}



	/**
	 * Return the value associated with the column: ServiceCharge
	 */
	public java.lang.String getServiceCharge () {
		return serviceCharge;
	}

	/**
	 * Set the value related to the column: ServiceCharge
	 * @param serviceCharge the ServiceCharge value
	 */
	public void setServiceCharge (java.lang.String serviceCharge) {
		this.serviceCharge = serviceCharge;
	}



	/**
	 * Return the value associated with the column: Imagepath
	 */
	public java.lang.String getImagepath () {
		return imagepath;
	}

	/**
	 * Set the value related to the column: Imagepath
	 * @param imagepath the Imagepath value
	 */
	public void setImagepath (java.lang.String imagepath) {
		this.imagepath = imagepath;
	}



	/**
	 * Return the value associated with the column: status
	 */
	public java.lang.String getStatus () {
		return status;
	}

	/**
	 * Set the value related to the column: status
	 * @param status the status value
	 */
	public void setStatus (java.lang.String status) {
		this.status = status;
	}



	/**
	 * Return the value associated with the column: latitude
	 */
	public java.lang.String getLatitude () {
		return latitude;
	}

	/**
	 * Set the value related to the column: latitude
	 * @param latitude the latitude value
	 */
	public void setLatitude (java.lang.String latitude) {
		this.latitude = latitude;
	}



	/**
	 * Return the value associated with the column: longitude
	 */
	public java.lang.String getLongitude () {
		return longitude;
	}

	/**
	 * Set the value related to the column: longitude
	 * @param longitude the longitude value
	 */
	public void setLongitude (java.lang.String longitude) {
		this.longitude = longitude;
	}



	/**
	 * Return the value associated with the column: salonid
	 */
	public java.lang.String getSalonid () {
		return salonid;
	}

	/**
	 * Set the value related to the column: salonid
	 * @param salonid the salonid value
	 */
	public void setSalonid (java.lang.String salonid) {
		this.salonid = salonid;
	}



	/**
	 * Return the value associated with the column: dsa
	 */
	public java.lang.String getDsa () {
		return dsa;
	}

	/**
	 * Set the value related to the column: dsa
	 * @param dsa the dsa value
	 */
	public void setDsa (java.lang.String dsa) {
		this.dsa = dsa;
	}



	/**
	 * Return the value associated with the column: createdby
	 */
	public java.lang.String getCreatedby () {
		return createdby;
	}

	/**
	 * Set the value related to the column: createdby
	 * @param createdby the createdby value
	 */
	public void setCreatedby (java.lang.String createdby) {
		this.createdby = createdby;
	}



	/**
	 * Return the value associated with the column: dateofadd
	 */
	public java.lang.String getDateofadd () {
		return dateofadd;
	}

	/**
	 * Set the value related to the column: dateofadd
	 * @param dateofadd the dateofadd value
	 */
	public void setDateofadd (java.lang.String dateofadd) {
		this.dateofadd = dateofadd;
	}



	/**
	 * Return the value associated with the column: AndroidImagepath
	 */
	public java.lang.String getAndroidImagepath () {
		return androidImagepath;
	}

	/**
	 * Set the value related to the column: AndroidImagepath
	 * @param androidImagepath the AndroidImagepath value
	 */
	public void setAndroidImagepath (java.lang.String androidImagepath) {
		this.androidImagepath = androidImagepath;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.googlemaps.Newsalon)) return false;
		else {
			com.ihexa.common.admin.googlemaps.Newsalon newsalon = (com.ihexa.common.admin.googlemaps.Newsalon) obj;
			if (null == this.getId() || null == newsalon.getId()) return false;
			else return (this.getId().equals(newsalon.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}