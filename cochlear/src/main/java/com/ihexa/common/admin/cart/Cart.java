package com.ihexa.common.admin.cart;

import com.ihexa.common.admin.cart.base.BaseCart;



public class Cart extends BaseCart {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Cart () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Cart (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}