package com.ihexa.common.admin.utility;

import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.ihexa.common.admin.utility.FileChecker;

public class FileUpload {
	public static String uploadAttachment(MultipartFile multipartFile) {
		String fileName = null;
		String aTempDir = null;
		String fileLocation = null;
		System.out.println(multipartFile.getName());
		String[] originalFileName = multipartFile.getOriginalFilename().split("\\.");
		String extension = originalFileName[originalFileName.length - 1];
		try {
			aTempDir = System.getProperty("catalina.base") + java.io.File.separator + "webapps"
					+ java.io.File.separator + "product" + java.io.File.separator + "attachment" + java.io.File.separator
					+ FileChecker.getFileType(extension);
			// File code
			String nameOfFIle;
			Calendar calendar = Calendar.getInstance();
			Date date = calendar.getTime();
			nameOfFIle = "ProductAttachment" + date.getTime();
			try {
				fileName = nameOfFIle + "." + extension;
			} catch (Exception e) {
				fileName = multipartFile.getOriginalFilename();
			}
			fileLocation = aTempDir + java.io.File.separator + fileName;
			new java.io.File(aTempDir).mkdirs();
			java.io.File file = new java.io.File(fileLocation);
			FileOutputStream output = new FileOutputStream(file);
			output.write(multipartFile.getBytes());
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/product/attachment/" + FileChecker.getFileType(extension) + "/" + fileName;
	}
}
