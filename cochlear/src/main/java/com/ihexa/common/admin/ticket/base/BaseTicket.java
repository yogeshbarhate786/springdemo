package com.ihexa.common.admin.ticket.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the ticket table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="ticket"
 */

public abstract class BaseTicket  implements Serializable {

	public static String REF = "Ticket";
	public static String PROP_STATUS = "Status";
	public static String PROP_CATEGORYID = "Categoryid";
	public static String PROP_UNIQUEID = "Uniqueid";
	public static String PROP_SUBCATEGORYID = "Subcategoryid";
	public static String PROP_CREATEDDATE = "Createddate";
	public static String PROP_DOCTORID = "Doctorid";
	public static String PROP_UPDATEDDATE = "Updateddate";
	public static String PROP_MAINCATID = "Maincatid";
	public static String PROP_PRODUCTNAME = "Productname";
	public static String PROP_QUESTION = "Question";
	public static String PROP_CARTID = "Cartid";
	public static String PROP_SOLVESTATUS = "Solvestatus";
	public static String PROP_ID = "Id";


	// constructors
	public BaseTicket () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseTicket (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String uniqueid;
	private java.lang.String question;
	private java.lang.String productname;
	private java.util.Date updateddate;
	private java.util.Date createddate;
	private java.lang.String status;
	private java.lang.String solvestatus;

	// many to one
	private com.ihexa.common.admin.productsku.Maincategory maincatid;
	private com.ihexa.common.admin.doctor.Doctor doctorid;
	private com.ihexa.common.admin.cart.Cart cartid;
	private com.ihexa.common.admin.productsku.Subcategory subcategoryid;
	private com.ihexa.common.admin.productsku.Category categoryid;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="id"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: uniqueid
	 */
	public java.lang.String getUniqueid () {
		return uniqueid;
	}

	/**
	 * Set the value related to the column: uniqueid
	 * @param uniqueid the uniqueid value
	 */
	public void setUniqueid (java.lang.String uniqueid) {
		this.uniqueid = uniqueid;
	}



	/**
	 * Return the value associated with the column: question
	 */
	public java.lang.String getQuestion () {
		return question;
	}

	/**
	 * Set the value related to the column: question
	 * @param question the question value
	 */
	public void setQuestion (java.lang.String question) {
		this.question = question;
	}



	/**
	 * Return the value associated with the column: productname
	 */
	public java.lang.String getProductname () {
		return productname;
	}

	/**
	 * Set the value related to the column: productname
	 * @param productname the productname value
	 */
	public void setProductname (java.lang.String productname) {
		this.productname = productname;
	}



	/**
	 * Return the value associated with the column: updateddate
	 */
	public java.util.Date getUpdateddate () {
		return updateddate;
	}

	/**
	 * Set the value related to the column: updateddate
	 * @param updateddate the updateddate value
	 */
	public void setUpdateddate (java.util.Date updateddate) {
		this.updateddate = updateddate;
	}



	/**
	 * Return the value associated with the column: createddate
	 */
	public java.util.Date getCreateddate () {
		return createddate;
	}

	/**
	 * Set the value related to the column: createddate
	 * @param createddate the createddate value
	 */
	public void setCreateddate (java.util.Date createddate) {
		this.createddate = createddate;
	}



	/**
	 * Return the value associated with the column: status
	 */
	public java.lang.String getStatus () {
		return status;
	}

	/**
	 * Set the value related to the column: status
	 * @param status the status value
	 */
	public void setStatus (java.lang.String status) {
		this.status = status;
	}



	/**
	 * Return the value associated with the column: solvestatus
	 */
	public java.lang.String getSolvestatus () {
		return solvestatus;
	}

	/**
	 * Set the value related to the column: solvestatus
	 * @param solvestatus the solvestatus value
	 */
	public void setSolvestatus (java.lang.String solvestatus) {
		this.solvestatus = solvestatus;
	}



	/**
	 * Return the value associated with the column: maincatid
	 */
	public com.ihexa.common.admin.productsku.Maincategory getMaincatid () {
		return maincatid;
	}

	/**
	 * Set the value related to the column: maincatid
	 * @param maincatid the maincatid value
	 */
	public void setMaincatid (com.ihexa.common.admin.productsku.Maincategory maincatid) {
		this.maincatid = maincatid;
	}



	/**
	 * Return the value associated with the column: doctorid
	 */
	public com.ihexa.common.admin.doctor.Doctor getDoctorid () {
		return doctorid;
	}

	/**
	 * Set the value related to the column: doctorid
	 * @param doctorid the doctorid value
	 */
	public void setDoctorid (com.ihexa.common.admin.doctor.Doctor doctorid) {
		this.doctorid = doctorid;
	}



	/**
	 * Return the value associated with the column: cartid
	 */
	public com.ihexa.common.admin.cart.Cart getCartid () {
		return cartid;
	}

	/**
	 * Set the value related to the column: cartid
	 * @param cartid the cartid value
	 */
	public void setCartid (com.ihexa.common.admin.cart.Cart cartid) {
		this.cartid = cartid;
	}



	/**
	 * Return the value associated with the column: subcategoryid
	 */
	public com.ihexa.common.admin.productsku.Subcategory getSubcategoryid () {
		return subcategoryid;
	}

	/**
	 * Set the value related to the column: subcategoryid
	 * @param subcategoryid the subcategoryid value
	 */
	public void setSubcategoryid (com.ihexa.common.admin.productsku.Subcategory subcategoryid) {
		this.subcategoryid = subcategoryid;
	}



	/**
	 * Return the value associated with the column: categoryid
	 */
	public com.ihexa.common.admin.productsku.Category getCategoryid () {
		return categoryid;
	}

	/**
	 * Set the value related to the column: categoryid
	 * @param categoryid the categoryid value
	 */
	public void setCategoryid (com.ihexa.common.admin.productsku.Category categoryid) {
		this.categoryid = categoryid;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.ticket.Ticket)) return false;
		else {
			com.ihexa.common.admin.ticket.Ticket ticket = (com.ihexa.common.admin.ticket.Ticket) obj;
			if (null == this.getId() || null == ticket.getId()) return false;
			else return (this.getId().equals(ticket.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}