package com.ihexa.common.admin.productsku.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the category table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="category"
 */

public abstract class BaseCategory  implements Serializable {

	public static String REF = "Category";
	public static String PROP_STATUS = "Status";
	public static String PROP_MAINCATEGORYID = "Maincategoryid";
	public static String PROP_CREATEDDATE = "Createddate";
	public static String PROP_ID = "Id";
	public static String PROP_NAME = "Name";


	// constructors
	public BaseCategory () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseCategory (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public BaseCategory (
		java.lang.Long id,
		java.util.Date createddate) {

		this.setId(id);
		this.setCreateddate(createddate);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String name;
	private java.util.Date createddate;
	private java.lang.String status;

	// many to one
	private com.ihexa.common.admin.productsku.Maincategory maincategoryid;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="id"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: name
	 */
	public java.lang.String getName () {
		return name;
	}

	/**
	 * Set the value related to the column: name
	 * @param name the name value
	 */
	public void setName (java.lang.String name) {
		this.name = name;
	}



	/**
	 * Return the value associated with the column: createddate
	 */
	public java.util.Date getCreateddate () {
		return createddate;
	}

	/**
	 * Set the value related to the column: createddate
	 * @param createddate the createddate value
	 */
	public void setCreateddate (java.util.Date createddate) {
		this.createddate = createddate;
	}



	/**
	 * Return the value associated with the column: status
	 */
	public java.lang.String getStatus () {
		return status;
	}

	/**
	 * Set the value related to the column: status
	 * @param status the status value
	 */
	public void setStatus (java.lang.String status) {
		this.status = status;
	}



	/**
	 * Return the value associated with the column: maincategoryid
	 */
	public com.ihexa.common.admin.productsku.Maincategory getMaincategoryid () {
		return maincategoryid;
	}

	/**
	 * Set the value related to the column: maincategoryid
	 * @param maincategoryid the maincategoryid value
	 */
	public void setMaincategoryid (com.ihexa.common.admin.productsku.Maincategory maincategoryid) {
		this.maincategoryid = maincategoryid;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.productsku.Category)) return false;
		else {
			com.ihexa.common.admin.productsku.Category category = (com.ihexa.common.admin.productsku.Category) obj;
			if (null == this.getId() || null == category.getId()) return false;
			else return (this.getId().equals(category.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}