package com.ihexa.common.admin.order.action;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ihexa.common.admin.cart.Cart;
import com.ihexa.common.admin.cart.Carttrack;
import com.ihexa.common.admin.dashboard.User;
import com.ihexa.common.admin.doctor.Doctor;
import com.ihexa.common.admin.gift.service.GiftService;
import com.ihexa.common.admin.order.CartPojo;
import com.ihexa.common.admin.order.MyorderPojo;
import com.ihexa.common.admin.order.OrderCart;
import com.ihexa.common.admin.order.OrderPojo;
import com.ihexa.common.admin.order.Orderdetails;
import com.ihexa.common.admin.order.TrackPojo;
import com.ihexa.common.admin.order.Wishlist;
import com.ihexa.common.admin.productsku.Product;
import com.ihexa.common.admin.productsku.ProductPojo;
import com.ihexa.common.admin.user.City;
import com.ihexa.common.admin.user.Country;
import com.ihexa.common.admin.user.State;
import com.ihexa.common.admin.utility.DynamicIpPath;
import com.ihexa.common.admin.utility.GupShupSMS;
import com.ihexa.common.admin.utility.RandomNumber;
import com.ihexa.common.admin.utility.SendMail;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.context.Context;
import com.prounify.framework.context.UserContext;

public class OrderAction extends BaseCRUDAction {
	
	
	
	GiftService service = (GiftService) Context.getInstance().getBean("GiftService");
	protected boolean isHttpMethodAllowed(String httpMethod, String methodId) {

		if (httpMethod.equals(HTTP_METHOD_GET)) {

			if (methodId.equals("trackorder"))
				return true;
			if (methodId.equals("myorder"))
				
				return true;
			if (methodId.equals("addwishlist"))
				return true;
			if (methodId.equals("wishlist"))
				return true;
		}
		return super.isHttpMethodAllowed(httpMethod, methodId);
	}

	@Override
	public Object doAdd(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try{
			 Long userid = (Long) UserContext.getSessionScope().get("USERID");
				
			DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
			criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
			List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
			request.setAttribute("UserEditList", listobj.get(0));
				
			Long doctorid = listobj.get(0).getDoctorid().getId();
			
			

			DetachedCriteria criteriaobj1 = DetachedCriteria.forClass(Doctor.class);
			criteriaobj1.add(Restrictions.eq(Doctor.PROP_ID, new Long(doctorid)));
			List<Doctor> listobj1 = service.findAll(criteriaobj1);
			request.setAttribute("DoctorEditList", listobj1.get(0));
			
			
			
			DetachedCriteria critobj=DetachedCriteria.forClass(Carttrack.class);
			critobj.add(Restrictions.eq(Carttrack.PROP_STATUS, "Active"));
			critobj.add(Restrictions.eq(Carttrack.PROP_USERID+"."+Doctor.PROP_ID,doctorid ));
			List<Carttrack> track=service.findAll(critobj);
			
			DetachedCriteria criteria = DetachedCriteria.forClass(Cart.class);
			criteria.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
			criteria.add(Restrictions.isNull(Cart.PROP_REMOVE_FROM_CART_TIME));
			criteria.add(Restrictions.isNull(Cart.PROP_BILL));
			criteria.add(Restrictions.eq(Cart.PROP_CARTID,track.get(0).getUniqueid()));
			List<Cart> cartlist = service.findAll(criteria);
			if(cartlist!=null)
			{
			String quantity[] = request.getParameterValues("quantity");
			int i=0;
				
				for (Cart cart : cartlist) {
					
					cart.setQuantity(Integer.parseInt(quantity[i]));
					i++;
					service.add(cart, null);
				}
			
			DetachedCriteria criteria1 = DetachedCriteria.forClass(Cart.class);
			criteria1.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
			criteria1.add(Restrictions.isNull(Cart.PROP_BILL));
			criteria1.add(Restrictions.isNull(Cart.PROP_REMOVE_FROM_CART_TIME));
			criteria1.add(Restrictions.eq(Cart.PROP_CARTID,track.get(0).getUniqueid()));
			List<Cart> cartlist1 = service.findAll(criteria1);
			
			int sum=0;
			double gstsum=0;
			double gst=0;
		/*	double productgst=0;*/
			for (Cart cart : cartlist1) {
				int price = cart.getProductid().getPrice();
				int quantity_int = cart.getQuantity();
				sum = sum + (price * quantity_int);
			}
			
			for (Cart cart : cartlist1)
			{
				double bill=cart.getProductid().getPrice() *cart.getQuantity();
				gst=sum*12/100;
				gstsum=sum+gst;
				
				if(bill<0)
				{
					cart.setBill(new BigDecimal(0));	
				}
				else
				{
				 cart.setBill(new BigDecimal(bill));	
				}
				if(gstsum<0)
				{
					cart.setTotalbill(new BigDecimal(0));	
				}
				else
				{
					cart.setTotalbill(new BigDecimal(gstsum));	
				}
				if(gst<0)
				{
					cart.setGst(new BigDecimal(0));
				}
				else
				{
					cart.setGst(new BigDecimal(gst));	
				}
				
				service.add(cart, null);
			}
	       int randomnumber=RandomNumber.now();
			
		
			
			DetachedCriteria critobj1=DetachedCriteria.forClass(Carttrack.class);
			critobj1.add(Restrictions.eq(Carttrack.PROP_STATUS, "Active"));
			critobj1.add(Restrictions.eq(Carttrack.PROP_USERID+"."+Doctor.PROP_ID,doctorid));
			List<Carttrack> track1=service.findAll(critobj1);
			
			DetachedCriteria critob=DetachedCriteria.forClass(Cart.class);
			critob.add(Restrictions.eq(Cart.PROP_DOCTORID+"."+Doctor.PROP_ID, doctorid));
			critob.add(Restrictions.eq(Cart.PROP_CARTID,track1.get(0).getUniqueid() ));
			critob.add(Restrictions.isNull(Cart.PROP_REMOVE_FROM_CART_TIME));
			critob.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
		    List<Cart> list=service.findAll(critob);
		
		    for (Cart cart : list) 
           {
				OrderCart obj=new OrderCart();
				 obj.setUniqueid("ORDER"+Integer.toString(randomnumber));
				 obj.setCartid(new Cart(new Long(cart.getId())));
				 obj.setOrderpaiddate(new Date());
				 obj.setOrderplacedate(new Date());
				 obj.setDoctorid(new Doctor(doctorid));
				 obj.setStatus("Active");
				 obj.setOrderstatus("Success");
				 
				  service.add(obj, null);
               } 
		  
                  int sum1=0;
                  double gstsum1=0;
      			  double gst1=0;
			
			for (Cart cart : list) {
				int price = cart.getProductid().getPrice();
				int quantity_int = cart.getQuantity();
				sum1 = sum1 + (price * quantity_int);
				gst1=sum1*12/100;
				gstsum1=sum1+gst1;
				cart.setCartsuccessstatus("Active");
				service.add(cart, null);
				
			}
			DetachedCriteria critobj11=DetachedCriteria.forClass(Carttrack.class);
			critobj11.add(Restrictions.eq(Carttrack.PROP_STATUS, "Active"));
			critobj11.add(Restrictions.eq(Carttrack.PROP_USERID+"."+Doctor.PROP_ID,doctorid ));
		    List<Carttrack> track11=service.findAll(critobj11);
			for (Carttrack carttrack : track11)
			{
				carttrack.setStatus("Deactive");
				carttrack.setOrderid("ORDER"+Integer.toString(randomnumber));
				carttrack.setOrderdate(new Date());
				service.update(carttrack, null);
				
				Orderdetails orobj=new Orderdetails();
				orobj.setApproveby(null);
				orobj.setApprovedate(null);
				orobj.setApprovestatus("Pendding");
				orobj.setDoctorid(new Doctor(new Long(doctorid)));
				orobj.setFeedback(null);
				orobj.setOrderid(carttrack.getOrderid());
				orobj.setPlacedorderdate(new Date());
				orobj.setStatus("Success");
				service.add(orobj, null);
			
			}
			request.setAttribute("CartList", list);
			request.setAttribute("CartListCount", list.size());
			request.setAttribute("TotalBillcount", gstsum1);
			request.setAttribute("gst", gst1);
			
			
			 
		/*	request.setAttribute("CartList", cartlist1);
			request.setAttribute("CartListCount", cartlist1.size());
			request.setAttribute("gst", gst);
			request.setAttribute("TotalBill", gstsum);*/
			
			}
			return "SUCCESS";
			
		}catch(Exception e)
		{
			return "ERROR";
		}
	}
	
	@Override
	public Object doCreate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try{
		/*	ranumber = RandomNumber.now();	*/
              Long userid = (Long) UserContext.getSessionScope().get("USERID");
			
			DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
			criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
			List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
			request.setAttribute("UserEditList", listobj.get(0));
			
			int randomnumber=RandomNumber.now();
			
			Long doctorid = listobj.get(0).getDoctorid().getId();
			
			DetachedCriteria critobj=DetachedCriteria.forClass(Carttrack.class);
			critobj.add(Restrictions.eq(Carttrack.PROP_STATUS, "Active"));
			critobj.add(Restrictions.eq(Carttrack.PROP_USERID+"."+Doctor.PROP_ID,doctorid));
			List<Carttrack> track=service.findAll(critobj);
			
			DetachedCriteria critob=DetachedCriteria.forClass(Cart.class);
			critob.add(Restrictions.eq(Cart.PROP_DOCTORID+"."+Doctor.PROP_ID, doctorid));
			critob.add(Restrictions.eq(Cart.PROP_CARTID,track.get(0).getUniqueid() ));
			critob.add(Restrictions.isNull(Cart.PROP_REMOVE_FROM_CART_TIME));
			critob.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
		    List<Cart> list=service.findAll(critob);
		
		    for (Cart cart : list) 
           {
				OrderCart obj=new OrderCart();
				 obj.setUniqueid("ORDER"+Integer.toString(randomnumber));
				 obj.setCartid(new Cart(new Long(cart.getId())));
				 obj.setOrderpaiddate(new Date());
				 obj.setOrderplacedate(new Date());
				 obj.setDoctorid(new Doctor(doctorid));
				 obj.setStatus("Active");
				 obj.setOrderstatus("Success");
				 
				  service.add(obj, null);
               } 
		  
                  int sum=0;
                  double gstsum=0;
      			  double gst=0;
			
			for (Cart cart : list) {
				int price = cart.getProductid().getPrice();
				int quantity_int = cart.getQuantity();
				sum = sum + (price * quantity_int);
				gst=sum*12/100;
				gstsum=sum+gst;
				cart.setCartsuccessstatus("Active");
				service.add(cart, null);
				
			}
			DetachedCriteria critobj1=DetachedCriteria.forClass(Carttrack.class);
			critobj1.add(Restrictions.eq(Carttrack.PROP_STATUS, "Active"));
			critobj1.add(Restrictions.eq(Carttrack.PROP_USERID+"."+Doctor.PROP_ID,doctorid ));
		    List<Carttrack> track1=service.findAll(critobj1);
			for (Carttrack carttrack : track1)
			{
				carttrack.setStatus("Deactive");
				carttrack.setOrderid("ORDER"+Integer.toString(randomnumber));
				carttrack.setOrderdate(new Date());
				service.update(carttrack, null);
				
				Orderdetails orobj=new Orderdetails();
				orobj.setApproveby(null);
				orobj.setApprovedate(null);
				orobj.setApprovestatus("Pendding");
				orobj.setDoctorid(new Doctor(new Long(doctorid)));
				orobj.setFeedback(null);
				orobj.setOrderid(carttrack.getOrderid());
				orobj.setPlacedorderdate(new Date());
				orobj.setStatus("Success");
				service.add(orobj, null);
			
			}
			request.setAttribute("CartList", list);
			request.setAttribute("CartListCount", list.size());
			request.setAttribute("TotalBillcount", gstsum);
			request.setAttribute("gst", gst);
			return "SUCCESS";
			
		}catch(Exception e)
		{
			return "ERROR";
		}
	}

	
	public Object doMyorder(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try{
			
		    Long userid = (Long) UserContext.getSessionScope().get("USERID");
			DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
			criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
			List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
			request.setAttribute("UserEditList", listobj.get(0));
			
			
			Long doctorid = listobj.get(0).getDoctorid().getId();
			Long cartid=Long.parseLong(request.getParameter("cartid"));
			
			
		    DetachedCriteria criteria1 = DetachedCriteria.forClass(Cart.class);
		    criteria1.add(Restrictions.eq(Cart.PROP_ID,cartid));
		    List<Cart> cartlist1 = service.findAll(criteria1);
			
			DetachedCriteria obj=DetachedCriteria.forClass(OrderCart.class);
			obj.add(Restrictions.eq(OrderCart.PROP_CARTID+"."+Cart.PROP_ID,cartid));
			List<OrderCart> listcart=service.findAll(obj);
	      for (OrderCart cart : listcart)
	      {
		   cart.setOrderstatus("Cancelled");
		   service.update(cart, null);
		   
		   DetachedCriteria crimm=DetachedCriteria.forClass(OrderCart.class);
		   crimm.add(Restrictions.eq(OrderCart.PROP_ORDERSTATUS, "Success"));
		   crimm.add(Restrictions.eq(OrderCart.PROP_UNIQUEID, cart.getUniqueid()));
		   crimm.add(Restrictions.eq(OrderCart.PROP_DOCTORID+"."+Doctor.PROP_ID,doctorid));
		   List<OrderCart> list=service.findAll(crimm);
		   if(list.size()==0)
		   {
			   DetachedCriteria cri=DetachedCriteria.forClass(Orderdetails.class);
			   cri.add(Restrictions.eq(Orderdetails.PROP_ORDERID, cart.getUniqueid()));
			   cri.add(Restrictions.eq(OrderCart.PROP_DOCTORID+"."+Doctor.PROP_ID,doctorid));
			   List<Orderdetails> orderdelist=service.findAll(cri);
			   for (Orderdetails orderdetails : orderdelist) 
			   {
				orderdetails.setStatus("Deactive");
				service.update(orderdetails, null);
			     
			   }
		   }
		   
		   
	       int otp=GupShupSMS.sendOTP(cart.getDoctorid().getMobile(),cart.getUniqueid());
	       
	       String emailContents = "Hi "
					+ cart.getDoctorid().getUserName()
					+ ", <br><br>Your order "+cart.getUniqueid()+"+ is cancelled successfully.<a href ='http://"
					+ DynamicIpPath.getEndPoints() + "/cochlear/" + "'>Click Here To Go</a>"
					+ "<br><br>Thanks<br>Cochlear Team";
			String emailSubject = "Cochlear - Your Order is canceled ";
	        String emailID= cart.getDoctorid().getEmail();
	       SendMail.sendEmail(emailID, emailSubject, emailContents, new ArrayList<String>());
		
	      }
	      DetachedCriteria criteria11 = DetachedCriteria.forClass(Cart.class);
	      criteria11.add(Restrictions.eq(Cart.PROP_DOCTORID+"."+Doctor.PROP_ID, doctorid));
	      criteria11.add(Restrictions.eq(Cart.PROP_CARTID,cartlist1.get(0).getCartid()));
	      criteria11.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
	      List<Cart> cartlist11 = service.findAll(criteria11);
		 
		for (Cart cart : cartlist11)
		{    
			double  totalbill=0;
			double prodprice=listcart.get(0).getCartid().getBill().doubleValue();
			double prodgst=prodprice*12/100;
			
			totalbill=(cart.getTotalbill().doubleValue())-(prodprice)-(prodgst);
			double gst=0;
			gst=(cart.getGst().doubleValue())-(prodgst);
			
			cart.setGst(new BigDecimal(gst));
			cart.setTotalbill(new BigDecimal(totalbill));
			service.update(cart, null);
		
		}
		 DetachedCriteria criteria111 = DetachedCriteria.forClass(Cart.class);
	      criteria111.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
	     criteria111.add(Restrictions.eq(Cart.PROP_DOCTORID+"."+Doctor.PROP_ID, doctorid));
	      criteria111.add(Restrictions.eq(Cart.PROP_CARTID,cartlist1.get(0).getCartid()));
	      
	     List<Cart> cartlist111 = service.findAll(criteria111);
		  
	
		if(cartlist11.size()>0)
		{
		request.setAttribute("TotalBillcount", cartlist111.get(0).getTotalbill());
		}
		else
		{
			request.setAttribute("TotalBillcount", 0);	
		}
		
		
		request.setAttribute("CartListCount", 0);
		 
		PrintWriter out = response.getWriter();
         Gson gson = new Gson();
         JsonElement element=null,gst=null;
		JsonObject jsonObject = new JsonObject(); 
		if(cartlist111.size()>0)
		{
		 element = gson.toJsonTree(cartlist111.get(0).getTotalbill()); 
		  gst=gson.toJsonTree(cartlist111.get(0).getGst());
		}
		else
		{
		 element = gson.toJsonTree(0); 
		}
		jsonObject.addProperty("success", true); 
		jsonObject.add("cartlist", element);
		jsonObject.add("gst", gst);
		
		out.println(jsonObject.toString()); 
		out.close();
		
		return "SUCCESS";
			
		}catch(Exception e)
		{
			return "ERROR";
		}
	}

	public Object doAddwishlist(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Long productid=Long.parseLong(request.getParameter("productid"));
		Long userid = (Long) UserContext.getSessionScope().get("USERID");
		
		DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
		criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
		List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
		request.setAttribute("UserEditList", listobj.get(0));
		
        
		
		Long doctorid = listobj.get(0).getDoctorid().getId();
		
		DetachedCriteria criteria=DetachedCriteria.forClass(Wishlist.class);
		criteria.add(Restrictions.eq(Wishlist.PROP_PRODUCTID+"."+Product.PROP_ID,new Long(productid)));
		criteria.add(Restrictions.eq(Wishlist.PROP_DOCTORID+"."+Doctor.PROP_ID,doctorid ));
		criteria.add(Restrictions.eq(Wishlist.PROP_STATUS,"Active"));
		
		List<Cart> list=service.findAll(criteria);
	     if(list.isEmpty())
	     {
		
		Wishlist wishobj=new Wishlist();
		wishobj.setDoctorid(new Doctor(doctorid));
		wishobj.setProductid(new Product(productid));
		wishobj.setStatus("Active");
		service.add(wishobj, null);
	     }
	     DetachedCriteria critobj1=DetachedCriteria.forClass(Carttrack.class);
			critobj1.add(Restrictions.eq(Carttrack.PROP_STATUS, "Active"));
			critobj1.add(Restrictions.eq(Carttrack.PROP_USERID+"."+Doctor.PROP_ID,doctorid ));
		    List<Carttrack> track1=service.findAll(critobj1);
		    if(track1.size()>0)
		    {
	  	DetachedCriteria criteria3 = DetachedCriteria.forClass(Cart.class);
	 	criteria3.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
	 	criteria3.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
	 	criteria3.add(Restrictions.eq(Cart.PROP_CARTID,track1.get(0).getUniqueid() ));
		List<Cart> cartlist = service.findAll(criteria3);
		     
			request.setAttribute("CartList", cartlist);
			request.setAttribute("CartListCount", cartlist.size());  
		    }
		    else
		    {
		    	request.setAttribute("CartListCount", 0);  	
		    }
		return SUCCESS;
	}

	
	public Object doTrackorder(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception 
			{
	      
		   ArrayList<OrderPojo> cartlist=new ArrayList<OrderPojo>(); 
		   Long userid = (Long) UserContext.getSessionScope().get("USERID");
			
		  String cartid=request.getParameter("cartid");
			
		   DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
			criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
			List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
			request.setAttribute("UserEditList", listobj.get(0));
			
			Long doctorid = listobj.get(0).getDoctorid().getId();     
			
	       if(cartid!=null)
	        {
			DetachedCriteria criteria1 = DetachedCriteria.forClass(Cart.class);
		    criteria1.add(Restrictions.eq(Cart.PROP_ID,Long.parseLong(cartid)));
		    List<Cart> cartlist1 = service.findAll(criteria1);
			
			DetachedCriteria obj=DetachedCriteria.forClass(OrderCart.class);
			obj.add(Restrictions.eq(OrderCart.PROP_CARTID+"."+Cart.PROP_ID,Long.parseLong(cartid)));
			List<OrderCart> listcart=service.findAll(obj);
	      for (OrderCart cart : listcart)
	      {
		   cart.setOrderstatus("Cancelled");
		  
		   service.update(cart, null);
		   
		   DetachedCriteria crimm=DetachedCriteria.forClass(OrderCart.class);
		   crimm.add(Restrictions.eq(OrderCart.PROP_ORDERSTATUS, "Success"));
		   crimm.add(Restrictions.eq(OrderCart.PROP_UNIQUEID, cart.getUniqueid()));
		   crimm.add(Restrictions.eq(OrderCart.PROP_DOCTORID+"."+Doctor.PROP_ID,doctorid));
		   List<OrderCart> list=service.findAll(crimm);
		   if(list.size()==0)
		   {
			   DetachedCriteria cri=DetachedCriteria.forClass(Orderdetails.class);
			   cri.add(Restrictions.eq(Orderdetails.PROP_ORDERID, cart.getUniqueid()));
			   cri.add(Restrictions.eq(Orderdetails.PROP_DOCTORID+"."+Doctor.PROP_ID,doctorid));
			   List<Orderdetails> orderdelist=service.findAll(cri);
			   for (Orderdetails orderdetails : orderdelist) 
			   {
				orderdetails.setStatus("Deactive");
				service.update(orderdetails, null);
			     
			   }
		   }
		   
	       int otp=GupShupSMS.sendOTP(cart.getDoctorid().getMobile(),cart.getUniqueid());
	       
	       String emailContents = "Hi "
					+ cart.getDoctorid().getUserName()
					+ ", <br><br>Your order "+cart.getUniqueid()+"+ is cancelled successfully.<a href ='http://"
					+ DynamicIpPath.getEndPoints() + "/cochlear/" + "'>Click Here To Go</a>"
					+ "<br><br>Thanks<br>Cochlear Team";
			String emailSubject = "Cochlear - Your Order is canceled ";
	        String emailID= cart.getDoctorid().getEmail();
	       SendMail.sendEmail(emailID, emailSubject, emailContents, new ArrayList<String>());
		
	      }
	      DetachedCriteria criteria11 = DetachedCriteria.forClass(Cart.class);
	      criteria11.add(Restrictions.eq(Cart.PROP_DOCTORID+"."+Doctor.PROP_ID, doctorid));
	      criteria11.add(Restrictions.eq(Cart.PROP_CARTID,cartlist1.get(0).getCartid()));
	      criteria11.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
	      List<Cart> cartlist11 = service.findAll(criteria11);
		 
		for (Cart cart : cartlist11)
		{    
			double  totalbill=0;
			double prodprice=listcart.get(0).getCartid().getBill().doubleValue();
			double prodgst=prodprice*12/100;
			
			totalbill=(cart.getTotalbill().doubleValue())-(prodprice)-(prodgst);
			double gst=0;
			gst=(cart.getGst().doubleValue())-(prodgst);
			
			cart.setGst(new BigDecimal(gst));
			cart.setTotalbill(new BigDecimal(totalbill));
			service.update(cart, null);
		
		  }
	    }
			DetachedCriteria critobj=DetachedCriteria.forClass(OrderCart.class);
		      critobj.add(Restrictions.eq(OrderCart.PROP_STATUS,"Active"));
		      critobj.addOrder(Order.desc(OrderCart.PROP_ID));
		      critobj.add(Restrictions.eq(OrderCart.PROP_DOCTORID+"."+Doctor.PROP_ID, doctorid));
		      List<OrderCart> list=service.findAll(critobj);
		     if(list.size()>0)
		     {
		    	 for (OrderCart orderCart : list) 
				        {
		    		 OrderPojo orderobj=new OrderPojo();
						orderobj.setOrderid(orderCart.getUniqueid().toString());
						orderobj.setStatus(orderCart.getOrderstatus());
						orderobj.setAddeddate(orderCart.getOrderplacedate().toString());
		    	        orderobj.setCartid(orderCart.getCartid().getId().intValue()); 
						
		    	        DetachedCriteria cartobj1=DetachedCriteria.forClass(Cart.class);
				     	cartobj1.add(Restrictions.eq(Cart.PROP_ID,new Long(orderCart.getCartid().getId())));
						List<Cart> listcart=service.findAll(cartobj1); 
			               if(listcart.size()>0)
			            
						orderobj.setCart(listcart);
							
			               cartlist.add(orderobj);
						}
			             request.setAttribute("CartList", cartlist); 
			             
			             DetachedCriteria critobj1=DetachedCriteria.forClass(Carttrack.class);
			 			critobj1.add(Restrictions.eq(Carttrack.PROP_STATUS, "Active"));
			 			critobj1.add(Restrictions.eq(Carttrack.PROP_USERID+"."+Doctor.PROP_ID,doctorid ));
			 		    List<Carttrack> track1=service.findAll(critobj1);
			         	if(track1.size()>0)
			         	{
			         		DetachedCriteria criteria3 = DetachedCriteria.forClass(Cart.class);
				    	 	criteria3.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
				    	 	criteria3.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
				    	 	criteria3.add(Restrictions.eq(Cart.PROP_CARTID,track1.get(0).getUniqueid() ));
				    		List<Cart> cartlist1 = service.findAll(criteria3);
				    		
				    		request.setAttribute("CartListCount", cartlist1.size());   
			         	}
			         	else
			         	{
			         		request.setAttribute("CartListCount", 0);   	
			         	}
			 		    
				       } 
		/*	   ArrayList<MyorderPojo> cartlist1=new ArrayList<MyorderPojo>(); 
		       DetachedCriteria critobj1=DetachedCriteria.forClass(Carttrack.class);
	 		   critobj1.add(Restrictions.eq(Carttrack.PROP_USERID+"."+Doctor.PROP_ID,doctorid ));
	 		    List<Carttrack> track1=service.findAll(critobj1);
	 		          MyorderPojo orderobj=new MyorderPojo();
		               for (Carttrack carttrack : track1)
		               {
		            	   
						DetachedCriteria criteria=DetachedCriteria.forClass(OrderCart.class);
						criteria.add(Restrictions.eq(OrderCart.PROP_UNIQUEID, carttrack.getOrderid()));
						List<OrderCart> list1=service.findAll(criteria);
						
						ArrayList<TrackPojo> tracklist=new ArrayList();
						
						for (OrderCart orderCart : list1) 
						{
						    orderobj.setOrderid(orderCart.getUniqueid());
			            	orderobj.setAddeddate(orderCart.getOrderplacedate());
			            	orderobj.setStatus(orderCart.getStatus());
							
			            	TrackPojo cartpojo=new TrackPojo();
							cartpojo.setStatus(orderCart.getOrderstatus());
							
							DetachedCriteria cricart=DetachedCriteria.forClass(Cart.class);
						    cricart.add(Restrictions.eq(Cart.PROP_ID, new Long(orderCart.getCartid().getId())));
						    List<Cart> cart_list=service.findAll(cricart);
					
						    cartpojo.setList(cart_list);
						    tracklist.add(cartpojo);
						}
						orderobj.setCart(tracklist);
						}
		               cartlist1.add(orderobj);
		               request.setAttribute("CartList",cartlist1);*/
		               
		     return SUCCESS;
	}


	public Object doWishlist(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception 
			{
		String prodid=request.getParameter("productid");
	
		
		Long userid = (Long) UserContext.getSessionScope().get("USERID");
		   ArrayList<ProductPojo> ProductList=new ArrayList();
			
		   DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
			criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
			List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
			request.setAttribute("UserEditList", listobj.get(0));
			
			Long doctorid = listobj.get(0).getDoctorid().getId();    
			
			if(prodid!=null)
			{
				DetachedCriteria criteria=DetachedCriteria.forClass(Wishlist.class);
			   criteria.add(Restrictions.eq(Wishlist.PROP_DOCTORID+"."+Doctor.PROP_ID,doctorid));
			   criteria.add(Restrictions.eq(Wishlist.PROP_PRODUCTID+"."+Product.PROP_ID,Long.parseLong(prodid)));
			     List<Wishlist> wishlist=service.findAll(criteria);
			     for (Wishlist wishlistobj : wishlist) 
			     {
					
			    	 wishlistobj.setStatus("Deactive");
			    	 service.add(wishlistobj, null);
			    	 
			     }
				
			}
	          	
		
		     DetachedCriteria criteria=DetachedCriteria.forClass(Wishlist.class);
		     criteria.add(Restrictions.eq(Wishlist.PROP_DOCTORID+"."+Doctor.PROP_ID,doctorid));
		     criteria.add(Restrictions.eq(Wishlist.PROP_STATUS, "Active"));
		     List<Wishlist> wishlist=service.findAll(criteria);
		     if(wishlist.size()>0)
		     {
		     for (Wishlist wishobj : wishlist)
		     {
				
			  DetachedCriteria criteria1=DetachedCriteria.forClass(Product.class);
			  criteria1.add(Restrictions.eq(Product.PROP_ID,wishobj.getProductid().getId()));
			  List<Product>  productlist=service.findAll(criteria1);
			  
			  for (Product product : productlist) 
			  {
				  ProductPojo proobj=new ProductPojo();
				       proobj.setProdid(product.getId().intValue());
				       proobj.setPicurl(product.getPicurl());
				       proobj.setProductname(product.getName());
				       proobj.setProductprice(product.getPrice().toString());
				       proobj.setWarranty(product.getWarranty());
				       ProductList.add(proobj);
				       
			  }
		      }
		     }
		     
		     DetachedCriteria critobj1=DetachedCriteria.forClass(Carttrack.class);
				critobj1.add(Restrictions.eq(Carttrack.PROP_STATUS, "Active"));
				critobj1.add(Restrictions.eq(Carttrack.PROP_USERID+"."+Doctor.PROP_ID,doctorid ));
			    List<Carttrack> track1=service.findAll(critobj1);
			    if(track1.size()>0)
			    {
		     DetachedCriteria criteria11 = DetachedCriteria.forClass(Cart.class);
		     criteria11.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
		     criteria11.add(Restrictions.eq(Cart.PROP_CARTID,track1.get(0).getUniqueid() ));
		      criteria11.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
			  List<Cart> cartlist11 = service.findAll(criteria11);
			    
			  if(cartlist11.size()>0)
			  {
			request.setAttribute("CartList", cartlist11);
			request.setAttribute("CartListCount", cartlist11.size());
			  }
			  }
			  else
			  {
				  request.setAttribute("CartListCount", 0);  
			  }
			request.setAttribute("ProductList", ProductList);
		     return SUCCESS;
	}

	/*@Override
	public Object doDelete(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Long prodid=Long.parseLong(request.getParameter("productid"));
		
		DetachedCriteria criteria=DetachedCriteria.
		
		
		
		return SUCCESS;
	}*/
	
	

}
