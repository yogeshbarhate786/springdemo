package com.ihexa.common.admin.profile;

import com.ihexa.common.admin.profile.base.BaseProfileToAuthorization;



public class ProfileToAuthorization extends BaseProfileToAuthorization {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public ProfileToAuthorization () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public ProfileToAuthorization (java.lang.Long id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public ProfileToAuthorization (
		java.lang.Long id,
		com.ihexa.common.admin.profile.Profile profile,
		com.ihexa.common.admin.profile.Authorization authorization) {

		super (
			id,
			profile,
			authorization);
	}

/*[CONSTRUCTOR MARKER END]*/


}