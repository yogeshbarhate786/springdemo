package com.ihexa.common.admin.productsku;

import com.ihexa.common.admin.productsku.base.BaseCategory;



public class Category extends BaseCategory {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Category () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Category (java.lang.Long id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public Category (
		java.lang.Long id,
		java.util.Date createddate) {

		super (
			id,
			createddate);
	}

/*[CONSTRUCTOR MARKER END]*/


}