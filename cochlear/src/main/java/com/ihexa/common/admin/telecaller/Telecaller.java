package com.ihexa.common.admin.telecaller;

import com.ihexa.common.admin.telecaller.base.BaseTelecaller;



public class Telecaller extends BaseTelecaller {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Telecaller () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Telecaller (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}