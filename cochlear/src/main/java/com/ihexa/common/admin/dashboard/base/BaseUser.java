package com.ihexa.common.admin.dashboard.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the user table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="user"
 */

public abstract class BaseUser  implements Serializable {

	public static String REF = "User";
	public static String PROP_DOCTORID = "doctorid";
	public static String PROP_PERSON_I_D = "personID";
	public static String PROP_ID = "id";
	public static String PROP_ROLE_I_D = "roleID";
	public static String PROP_MANAGER_I_D = "managerID";


	// constructors
	public BaseUser () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseUser (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public BaseUser (
		java.lang.Long id,
		com.ihexa.common.admin.user.Person personID,
		com.ihexa.common.admin.user.Role roleID) {

		this.setId(id);
		this.setPersonID(personID);
		this.setRoleID(roleID);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// many to one
	private com.ihexa.common.admin.doctor.Doctor doctorid;
	private com.ihexa.common.admin.user.Person personID;
	private com.ihexa.common.admin.user.Role roleID;
	private com.ihexa.common.admin.user.User managerID;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: Doctorid
	 */
	public com.ihexa.common.admin.doctor.Doctor getDoctorid () {
		return doctorid;
	}

	/**
	 * Set the value related to the column: Doctorid
	 * @param doctorid the Doctorid value
	 */
	public void setDoctorid (com.ihexa.common.admin.doctor.Doctor doctorid) {
		this.doctorid = doctorid;
	}



	/**
	 * Return the value associated with the column: PersonID
	 */
	public com.ihexa.common.admin.user.Person getPersonID () {
		return personID;
	}

	/**
	 * Set the value related to the column: PersonID
	 * @param personID the PersonID value
	 */
	public void setPersonID (com.ihexa.common.admin.user.Person personID) {
		this.personID = personID;
	}



	/**
	 * Return the value associated with the column: RoleID
	 */
	public com.ihexa.common.admin.user.Role getRoleID () {
		return roleID;
	}

	/**
	 * Set the value related to the column: RoleID
	 * @param roleID the RoleID value
	 */
	public void setRoleID (com.ihexa.common.admin.user.Role roleID) {
		this.roleID = roleID;
	}



	/**
	 * Return the value associated with the column: ManagerID
	 */
	public com.ihexa.common.admin.user.User getManagerID () {
		return managerID;
	}

	/**
	 * Set the value related to the column: ManagerID
	 * @param managerID the ManagerID value
	 */
	public void setManagerID (com.ihexa.common.admin.user.User managerID) {
		this.managerID = managerID;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.dashboard.User)) return false;
		else {
			com.ihexa.common.admin.dashboard.User user = (com.ihexa.common.admin.dashboard.User) obj;
			if (null == this.getId() || null == user.getId()) return false;
			else return (this.getId().equals(user.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}