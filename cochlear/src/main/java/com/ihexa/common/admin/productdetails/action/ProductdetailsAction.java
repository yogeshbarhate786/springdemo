package com.ihexa.common.admin.productdetails.action;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ihexa.common.admin.cart.Cart;
import com.ihexa.common.admin.cart.Carttrack;
import com.ihexa.common.admin.dashboard.Pricelist;
import com.ihexa.common.admin.doctor.Doctor;
import com.ihexa.common.admin.gift.service.GiftService;
import com.ihexa.common.admin.productdetails.Datepojo;
import com.ihexa.common.admin.productdetails.Review;
import com.ihexa.common.admin.productsku.Category;
import com.ihexa.common.admin.productsku.Maincategory;
import com.ihexa.common.admin.productsku.Product;
import com.ihexa.common.admin.productsku.Producthistroy;
import com.ihexa.common.admin.user.User;
import com.ihexa.common.admin.utility.RandomNumber;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.context.Context;
import com.prounify.framework.context.UserContext;

public class ProductdetailsAction extends BaseCRUDAction
{ 
	public String ranumber=null;
	GiftService service = (GiftService) Context.getInstance().getBean("GiftService");
	protected boolean isHttpMethodAllowed(String httpMethod, String methodId) {
		
		if (httpMethod.equals(HTTP_METHOD_GET)) {

			if (methodId.equals("prodetails"))
				return true;
			if (methodId.equals("addcart"))
				return true;
			if (methodId.equals("addreview"))
				return true;
			if (methodId.equals("updateprice"))
				return true;
			
			
		}
		return super.isHttpMethodAllowed(httpMethod, methodId);
	}
	
	public Object doProdetails(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception 
			{
		
		ArrayList curlist=new ArrayList();
		
		DateFormat df = new SimpleDateFormat("hh:mm a");
		SimpleDateFormat df1 = new SimpleDateFormat("dd/MM/yyyy");
		Long userid = (Long) UserContext.getSessionScope().get("USERID");

		DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
		criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
		List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
		request.setAttribute("UserEditList", listobj.get(0));
		Long doctorid=null;
		if(listobj.get(0).getDoctorid()!=null)
		{
		 doctorid = listobj.get(0).getDoctorid().getId();
		}
		
		String productid=request.getParameter("productid"); 
		 String search=request.getParameter("txtsearch"); 
		
        String minprice=request.getParameter("select1"); 
        String maxprice=request.getParameter("select2"); 
        
        String sortid=request.getParameter("sortid");
		
		DetachedCriteria criobj=DetachedCriteria.forClass(Product.class);
		criobj.add(Restrictions.eq(Product.PROP_ID,Long.parseLong(productid)));
		List<Product> proobj=service.findAll(criobj);
		
		request.setAttribute("listobj",proobj);
		
		
		DetachedCriteria critobj=DetachedCriteria.forClass(Review.class);
		critobj.add(Restrictions.eq(Review.PROP_PRODUCTID+"."+Product.PROP_ID,new Long(productid)));
		List<Review> reviewlist=service.findAll(critobj);
		
		request.setAttribute("reviewlist", reviewlist);
		request.setAttribute("reviewcount", reviewlist.size());
		
		Datepojo dateobj=new Datepojo();
		
		Date datecur=new Date();
		
		dateobj.setCurrentdate(df1.format(datecur));
		if(listobj.get(0).getDoctorid()!=null)
		 {
		dateobj.setFullname(listobj.get(0).getDoctorid().getFirstName()+" "+listobj.get(0).getDoctorid().getLastName());
		  
		dateobj.setMailid(listobj.get(0).getDoctorid().getEmail());
		dateobj.setTime(df.format(datecur));
		 }
		curlist.add(dateobj);
		request.setAttribute("dateobj", curlist);
		
		DetachedCriteria criobj1=DetachedCriteria.forClass(Pricelist.class);
         criobj1.addOrder(Order.asc(Pricelist.PROP_PRICE));
         List<Pricelist> listprice=service.findAll(criobj1);
         request.setAttribute("PriceList", listprice);
			
			  DetachedCriteria criteria=DetachedCriteria.forClass(Maincategory.class);
			List<Maincategory> maincategorylist = service.findAll(criteria);
			request.setAttribute("categoryList", maincategorylist);
			
			 criteria=DetachedCriteria.forClass(Category.class);
			 List<Category>categorylist = service.findAll(criteria);
			 request.setAttribute("subCategoryList", categorylist);
			
			 
			 
			 if(sortid!=null)
			 {
			   if(sortid.equals("lowtohigh"))
			   {
				   criteria=DetachedCriteria.forClass(Product.class);
				   criteria.addOrder(Order.asc(Pricelist.PROP_PRICE));
				    List<Product> products = service.findAll(criteria);
					request.setAttribute("ProductsList", products);   
			   }else
			   if(sortid.equals("hightolow"))
			   {
				   criteria=DetachedCriteria.forClass(Product.class);
				   criteria.addOrder(Order.desc(Pricelist.PROP_PRICE));
				    List<Product> products = service.findAll(criteria);
					request.setAttribute("ProductsList", products);      
			   
			   }else
				   if(sortid.equals("newest"))
				   {
				   criteria=DetachedCriteria.forClass(Product.class);
				   criteria.addOrder(Order.desc(Pricelist.PROP_ID));
				    List<Product> products = service.findAll(criteria);
					request.setAttribute("ProductsList", products);        
			   }
			 }
			 else
			 {	 
			if(minprice!=null)
			 {
				 criteria=DetachedCriteria.forClass(Product.class);
					criteria.add(Restrictions.ge(Product.PROP_PRICE,Integer.parseInt(minprice)));
					criteria.add(Restrictions.le(Product.PROP_PRICE,Integer.parseInt(maxprice)));
				  List<Product> products = service.findAll(criteria);
					request.setAttribute("ProductsList", products); 
			 }
			 else
			 {
			 if(search!=null)
			 {
			  criteria=DetachedCriteria.forClass(Product.class);
				criteria.add(Restrictions.ilike(Product.PROP_NAME, search, MatchMode.ANYWHERE));
			  List<Product> products = service.findAll(criteria);
				request.setAttribute("ProductsList", products);
			 }
			 else 
			 {
				 criteria=DetachedCriteria.forClass(Product.class);
				 List<Product> products = service.findAll(criteria);
				 request.setAttribute("ProductsList", products); 
			 }
			 }
			
			 }
		
		return SUCCESS;
	}

	

	public Object doAddreview(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception 
			{
		try {
			DateFormat df = new SimpleDateFormat("hh:mm a");
			SimpleDateFormat df1 = new SimpleDateFormat("dd/MM/yyyy");
			
			String message=request.getParameter("message");
			Long userid = (Long) UserContext.getSessionScope().get("USERID");

			DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
			criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
			List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
			request.setAttribute("UserEditList", listobj.get(0));

			Long doctorid = listobj.get(0).getDoctorid().getId();
			
			String productid=request.getParameter("productid");
			Date dateobj=new Date();
			
			Review reviewobj=new Review();
			reviewobj.setDate(df1.format(dateobj));
			reviewobj.setTime(df.format(dateobj));
			reviewobj.setEmail(listobj.get(0).getDoctorid().getEmail());
			reviewobj.setMessage(message);
			reviewobj.setProductid(new Product(new Long(productid)));
			reviewobj.setStatus("active");
			reviewobj.setUserid(new Doctor(new Long(doctorid)));
			reviewobj.setName(listobj.get(0).getDoctorid().getFirstName()+" "+listobj.get(0).getDoctorid().getLastName());
			service.add(reviewobj, null);
			
			
		} catch (Exception e) {
		   e.printStackTrace();
		   return ERROR;
		}
	    return SUCCESS;
	}

	public Object doUpdateprice(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception 
			{
		try {
		
			
			String productid=request.getParameter("productid");
			String price = request.getParameter("price");
            if(productid!=null)
            {
            Producthistroy prohiObj=new Producthistroy();
            prohiObj.setAddeddate(new Date());
            prohiObj.setProductid(new Product(new Long(productid)));
            prohiObj.setProductprice(new BigDecimal(price));
            service.add(prohiObj, null);
            
            DetachedCriteria crit=DetachedCriteria.forClass(Product.class);
			crit.add(Restrictions.eq(Product.PROP_ID,new Long(productid )));
			List<Product> product_list=service.findAll(crit);
			for (Product product : product_list)
             {
				product.setPrice(new Integer(price));
				service.update(product, null);
			}
            }
			} catch (Exception e) {
		   e.printStackTrace();
		   return ERROR;
		}
	    return SUCCESS;
	}
	public Object doAddcart(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception 
			{
            try {
				
        		Long userid = (Long) UserContext.getSessionScope().get("USERID");

    			DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
    			criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
    			List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
    			if(listobj.size()>0)
    			{
    			request.setAttribute("UserEditList", listobj.get(0));
                 if(listobj.get(0).getDoctorid()!=null)
                 {
    			Long doctorid = listobj.get(0).getDoctorid().getId();

    			String productid = request.getParameter("productid");

    			DetachedCriteria criteria = DetachedCriteria.forClass(Cart.class);
    			criteria.add(Restrictions.eq(Cart.PROP_PRODUCTID + "." + Product.PROP_ID, new Long(productid)));
    			criteria.add(Restrictions.eq(Cart.PROP_DOCTORID + "." + Doctor.PROP_ID, doctorid));
    			criteria.add(Restrictions.isNull(Cart.PROP_BILL));
    			criteria.add(Restrictions.isNull(Cart.PROP_REMOVE_FROM_CART_TIME));
    			criteria.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
    		    List<Cart> list = service.findAll(criteria);
    			if (list.isEmpty()) {
    				DetachedCriteria critobj1=DetachedCriteria.forClass(Carttrack.class);
    				critobj1.add(Restrictions.eq(Carttrack.PROP_USERID+"."+Doctor.PROP_ID,doctorid ));
    				critobj1.add(Restrictions.eq(Carttrack.PROP_STATUS, "Active"));
    				List<Carttrack> track11=service.findAll(critobj1);
    				if(track11.size()==0)
    				{
    					ranumber="CART"+RandomNumber.now();
    					Carttrack trackobj=new Carttrack();
    					trackobj.setOrderid(null);
    				    trackobj.setUserid(new Doctor(doctorid));
    					trackobj.setUniqueid(ranumber);
    					trackobj.setStatus("Active");
    					service.add(trackobj, null);
    				
    				}
    				else
    				{
    					ranumber=track11.get(0).getUniqueid();
    				}
    				/*DetachedCriteria critob=DetachedCriteria.forClass(Product.class);
    				critob.add(Restrictions.eq(propertyName, value))*/
    				
    				
    				Cart cart = new Cart();
    				cart.setProductid(new Product(new Long(productid)));
    				cart.setDoctorid(new Doctor(new Long(doctorid)));
    				cart.setAddedToCartTime(new Date());
    				cart.setQuantity(new Integer(1));
    				cart.setStatus("Active");
    				cart.setCartsuccessstatus("Deactive");
    				cart.setCartid(ranumber);
    				/*cart.setGst(gst)*/
    			   Date dt = new Date();
    				Calendar c = Calendar.getInstance();
    				c.setTime(dt);
    				c.add(Calendar.DATE, 5);
    				dt = c.getTime();
    				cart.setProductDeliveryDate(dt);
    				service.add(cart, null);
    				
    				} else {
    				int quantity = 0;
    				for (Cart cart : list) {
    					if (cart.getQuantity() != null) {
    						quantity = cart.getQuantity();
    						quantity = quantity + 1;
    						cart.setQuantity(quantity);
    						service.add(cart, null);
    					}

    				}
    			}
    			DetachedCriteria criteria3 = DetachedCriteria.forClass(Cart.class);
    			criteria3.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
    			criteria3.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
    			criteria3.add(Restrictions.eq(Cart.PROP_CARTID,ranumber));
    			criteria3.add(Restrictions.eq(Cart.PROP_DOCTORID + "." + Doctor.PROP_ID, doctorid));
    		    List<Cart> cartlist = service.findAll(criteria3);
              
    			request.setAttribute("CartList", cartlist);
    			request.setAttribute("CartListCount", cartlist.size());
    		  }
    		 }   
            } catch (Exception e) 
			{
				e.printStackTrace();
			}
             
		
		return SUCCESS;
	}


	
}
