package com.ihexa.common.admin.company.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the company table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="company"
 */

public abstract class BaseCompany  implements Serializable {

	public static String REF = "Company";
	public static String PROP_STATUS = "Status";
	public static String PROP_COMPANY_NAME = "CompanyName";
	public static String PROP_DESCRIPTION = "Description";
	public static String PROP_EMAIL = "Email";
	public static String PROP_PHONE = "Phone";
	public static String PROP_WEBSITE = "Website";
	public static String PROP_ID = "Id";
	public static String PROP_ADDRESSID = "Addressid";


	// constructors
	public BaseCompany () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseCompany (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String companyName;
	private java.lang.String description;
	private java.lang.String email;
	private java.lang.Long phone;
	private java.lang.String website;
	private java.lang.String status;

	// many to one
	private com.ihexa.common.admin.user.Address addressid;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="id"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: company_name
	 */
	public java.lang.String getCompanyName () {
		return companyName;
	}

	/**
	 * Set the value related to the column: company_name
	 * @param companyName the company_name value
	 */
	public void setCompanyName (java.lang.String companyName) {
		this.companyName = companyName;
	}



	/**
	 * Return the value associated with the column: description
	 */
	public java.lang.String getDescription () {
		return description;
	}

	/**
	 * Set the value related to the column: description
	 * @param description the description value
	 */
	public void setDescription (java.lang.String description) {
		this.description = description;
	}



	/**
	 * Return the value associated with the column: email
	 */
	public java.lang.String getEmail () {
		return email;
	}

	/**
	 * Set the value related to the column: email
	 * @param email the email value
	 */
	public void setEmail (java.lang.String email) {
		this.email = email;
	}



	/**
	 * Return the value associated with the column: phone
	 */
	public java.lang.Long getPhone () {
		return phone;
	}

	/**
	 * Set the value related to the column: phone
	 * @param phone the phone value
	 */
	public void setPhone (java.lang.Long phone) {
		this.phone = phone;
	}



	/**
	 * Return the value associated with the column: website
	 */
	public java.lang.String getWebsite () {
		return website;
	}

	/**
	 * Set the value related to the column: website
	 * @param website the website value
	 */
	public void setWebsite (java.lang.String website) {
		this.website = website;
	}



	/**
	 * Return the value associated with the column: status
	 */
	public java.lang.String getStatus () {
		return status;
	}

	/**
	 * Set the value related to the column: status
	 * @param status the status value
	 */
	public void setStatus (java.lang.String status) {
		this.status = status;
	}



	/**
	 * Return the value associated with the column: addressid
	 */
	public com.ihexa.common.admin.user.Address getAddressid () {
		return addressid;
	}

	/**
	 * Set the value related to the column: addressid
	 * @param addressid the addressid value
	 */
	public void setAddressid (com.ihexa.common.admin.user.Address addressid) {
		this.addressid = addressid;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.company.Company)) return false;
		else {
			com.ihexa.common.admin.company.Company company = (com.ihexa.common.admin.company.Company) obj;
			if (null == this.getId() || null == company.getId()) return false;
			else return (this.getId().equals(company.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}