package com.ihexa.common.admin.doctor.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.MultipartRequestWrapper;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ihexa.common.admin.cart.Cart;
import com.ihexa.common.admin.dashboard.User;
import com.ihexa.common.admin.doctor.Doctor;
import com.ihexa.common.admin.gift.service.GiftService;
import com.ihexa.common.admin.iHexaConstants.IHexaConstants;
import com.ihexa.common.admin.login.Login;

import com.ihexa.common.admin.productsku.Category;
import com.ihexa.common.admin.productsku.Maincategory;
import com.ihexa.common.admin.productsku.Product;
import com.ihexa.common.admin.productsku.Subcategory;
import com.ihexa.common.admin.profile.Profile;

import com.ihexa.common.admin.user.Address;
import com.ihexa.common.admin.user.City;
import com.ihexa.common.admin.user.Country;
import com.ihexa.common.admin.user.Person;
import com.ihexa.common.admin.user.Role;
import com.ihexa.common.admin.user.State;

import com.ihexa.common.admin.utility.DynamicIpPath;
import com.ihexa.common.admin.utility.FileUpload;
import com.ihexa.common.admin.utility.SHAUtil;
import com.ihexa.common.admin.utility.SendMail;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.context.Context;
import com.prounify.framework.context.UserContext;
import com.prounify.framework.util.MD5Util;

public class DoctorAction extends BaseCRUDAction {

	GiftService service = (GiftService) Context.getInstance().getBean("GiftService");

	@Override
	public Object doAdd(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {

			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}
	}

	@Override
	public Object doCreate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		try {
			String DoctorFullName = request.getParameter("doctor_full_name");
			String DoctorEmailAddress = request.getParameter("doctor_email_address");
			if (StringUtils.isNotBlank(DoctorEmailAddress) && StringUtils.isNotBlank(DoctorFullName)) {
				DetachedCriteria criteria = DetachedCriteria.forClass(Doctor.class);
				criteria.add(Restrictions.eq(Doctor.PROP_EMAIL, DoctorEmailAddress));
				List<Doctor> EmailAddress = service.findAll(criteria);
				if (EmailAddress.size() == 0) {

					String uid = "UID" + new Date().getTime() + "x" + new Random().nextInt(1000);
					Person person = new Person();
					Doctor doctorUser = new Doctor();

					String[] parts = DoctorFullName.split(" ");
					if (parts.length >= 1) {
						String firstName = parts[0];
						person.setFirstName(firstName);
						doctorUser.setFirstName(firstName);
					}
					if (parts.length >= 2) {
						String lastName = "";
						for (int i = 1; i < parts.length; i++) {
							lastName = lastName + " " + parts[i];
						}
						person.setLastName(lastName);
						doctorUser.setLastName(lastName);
					}
					person.setEmail(DoctorEmailAddress);
					service.add(person, null);

					doctorUser.setBio(null);
					// ideaUser.setDob(dob);
					// ideaUser.setFullAddress(fullAddress);
					// ideaUser.setGender(gender);
					// ideaUser.setMiddleName(middleName);
					// ideaUser.setMobile(mobile);
					doctorUser.setEmail(DoctorEmailAddress);
					doctorUser.setRegisterOn(new Date());
					doctorUser.setCreateddate(new Date());
					doctorUser.setStatus("Active");
					doctorUser.setUserName(DoctorEmailAddress);
					doctorUser.setIsEmailVerified(uid);
					
					
					service.add(doctorUser, null);

					com.ihexa.common.admin.user.User user = new com.ihexa.common.admin.user.User();
					user.setPerson(person);
					user.setRole(new Role(Long.valueOf(IHexaConstants.LOGINROLE_DOCTOR)));
					user.setManager(new com.ihexa.common.admin.user.User(new Long(
							IHexaConstants.LOGINROLE_ADMINISTRATOR)));
					user.setDoctorid(doctorUser);
					service.add(user, null);

					String hash=MD5Util.hashPassword("doctor");
					
					
					SHAUtil shaUtil = new SHAUtil();
					String salt = shaUtil.generateSalt(user.toString());

					Login login = new Login();
					login.setUser(user);
					login.setLoginName(DoctorEmailAddress);
					login.setPassword(hash);
					//login.setVariant(salt);
					login.setLastLoginDate(new Date());
					login.setPwdExpirationDate(new Date());
					login.setStatusPasswordResetDate(new Date());
					login.setStatusActiveDate(new Date());
					// login.setProfile(new
					// Profile(IHexaConstants.LOGINROLE_DOCTOR));
					login.setProfileId((IHexaConstants.LOGINROLE_DOCTOR));
					service.add(login, null);

					String emailContents = "Hi "
							+ DoctorFullName
							+ ", <br><br>Your account is created successfully. Please login to our site for further assistance.<br>Your UserName Is: "
							+ DoctorEmailAddress + "<br> And Password Is:doctor" + "<a href ='http://"
							+ DynamicIpPath.getEndPoints() + "/cochlear/" + "'>Click Here To Go</a>"
							+ "<br><br>Thanks<br>Cochlear Team";
					String emailSubject = "Cochlear - Your Account Is Created ";

					String emailID = DoctorEmailAddress.trim();
					SendMail.sendEmail(emailID, emailSubject, emailContents, new ArrayList<String>());

				} else {
					request.setAttribute("DoctorFullName", DoctorFullName);
					request.setAttribute("DoctorEmailAddress", DoctorEmailAddress);

				}
			}
		} catch (Exception e) {
			return ERROR;
		}
		DetachedCriteria criteria3 = DetachedCriteria.forClass(Cart.class);
	 	criteria3.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
	 	criteria3.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
		List<Cart> cartlist = service.findAll(criteria3);
		
			request.setAttribute("CartList", cartlist);
			request.setAttribute("CartListCount", cartlist.size());  
		return SUCCESS;

	}

	@Override
	public Object doEdit(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			
			Long userid = (Long) UserContext.getSessionScope().get("USERID");
			
			DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
			criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
			List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
			request.setAttribute("UserEditList", listobj.get(0));
			
			Long doctorid = listobj.get(0).getDoctorid().getId();
			

			
			DetachedCriteria criteriaobj1 = DetachedCriteria.forClass(Doctor.class);
			criteriaobj1.add(Restrictions.eq(Doctor.PROP_ID, new Long(doctorid)));
			List<Doctor> listobj1 = service.findAll(criteriaobj1);
			request.setAttribute("DoctorEditList", listobj1.get(0));
			
			DetachedCriteria criteria = DetachedCriteria.forClass(Country.class);
			List<Country> CountryList = service.findAll(criteria);
			request.setAttribute("CountryList", CountryList);
			
			if(listobj1.get(0).getAddressid()!=null)
			{
			DetachedCriteria criteria2 = DetachedCriteria.forClass(State.class);
			criteria2.add(Restrictions.eq(State.PROP_COUNTRY+"."+Country.PROP_ID,listobj1.get(0).getAddressid().getCountry().getId()));
			List<State> StateList = service.findAll(criteria2);
			request.setAttribute("StateList", StateList);
			}
			
			if(listobj1.get(0).getAddressid()!=null)
			{
			DetachedCriteria criteria3 = DetachedCriteria.forClass(City.class);
			criteria3.add(Restrictions.eq(City.PROP_STATE+"."+State.PROP_ID,listobj1.get(0).getAddressid().getState().getId()));
			List<City> CityList = service.findAll(criteria3);
			request.setAttribute("CityList", CityList);
			}
			DetachedCriteria criteria3 = DetachedCriteria.forClass(Cart.class);
			criteria3.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
		 	criteria3.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
			List<Cart> cartlist = service.findAll(criteria3);
			
				request.setAttribute("CartList", cartlist);
				request.setAttribute("CartListCount", cartlist.size());  

			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}
	}

	@Override
	public Object doUpdate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			
			String DoctorFirstName = request.getParameter("doctor_first_name");
			String DoctorMiddleName = request.getParameter("doctor_middle_name");
			String DoctorLastName = request.getParameter("doctor_last_name");
			String DoctorEmailAddress = request.getParameter("doctor_email_address");
			String DoctorUserName = request.getParameter("doctor_user_name");
			String DoctorGender = request.getParameter("doctor_gender");
			String DoctorAge = request.getParameter("doctor_age");
			String DoctorBio = request.getParameter("doctor_bio");
			String DoctorMobileNumber = request.getParameter("doctor_mobile_number");
			String DoctorFullAddress = request.getParameter("doctor_full_address");
			String DoctorAddressStreet = request.getParameter("doctor_address_street");
			String DoctorZipCode = request.getParameter("doctor_zip_code");
			String DoctorCountry = request.getParameter("countryid");
			String DoctorState = request.getParameter("stateid");
			String DoctorCity = request.getParameter("cityid");
			String DoctorSpecialist = request.getParameter("doctor_specialist");
			String DoctorHospitalName = request.getParameter("doctor_hospital_name");
			
			String doctorid = request.getParameter("doctorid");
			
			String path = null;
			if (request instanceof MultipartRequestWrapper) {
				MultipartRequestWrapper multipart = (MultipartRequestWrapper) request;
				MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) multipart.getRequest();
				Map files = multiRequest.getFileMap();
				for (Iterator iterator = files.values().iterator(); iterator.hasNext();) {
					MultipartFile multipartFile = (MultipartFile) iterator.next();
					if (!(multipartFile.isEmpty())) {
						path = FileUpload.uploadAttachment(multipartFile);
						String[] originalFileName = multipartFile.getOriginalFilename().split("\\.");
						String extension = originalFileName[originalFileName.length - 1];
					} else {
					}
				}
			}
			
			DetachedCriteria criteriaobj1 = DetachedCriteria.forClass(Doctor.class);
			criteriaobj1.add(Restrictions.eq(Doctor.PROP_ID, new Long(doctorid)));
			List<Doctor> listobj1 = service.findAll(criteriaobj1);
			
			for (Doctor doctorlist : listobj1) {
				
				Address address = new Address();
				if (StringUtils.isNotBlank(DoctorFullAddress)) {
					address.setAddressFull(DoctorFullAddress);
				} else {
					address.setAddressFull(null);
				}
				
				if (StringUtils.isNotBlank(DoctorAddressStreet)) {
					address.setAddressStreet(DoctorAddressStreet);
				} else {
					address.setAddressStreet(null);
				}
				
				if (StringUtils.isNotBlank(DoctorZipCode)) {
					address.setZipCode(new Long(DoctorZipCode));
				} else {
					address.setZipCode(null);
				}
				
				if (StringUtils.isNotBlank(DoctorCountry)) {
					address.setCountry(new Country(new Long(DoctorCountry)));
				} else {
					address.setCountry(null);
				}
				
				if (StringUtils.isNotBlank(DoctorState)) {
					address.setState(new State(new Long(DoctorState)));
				} else {
					address.setState(null);
				}
				
				if (StringUtils.isNotBlank(DoctorCity)) {
					address.setCity(new City(new Long(DoctorCity)));
				} else {
					address.setCity(null);
				}
				
				service.add(address, null);
				doctorlist.setAddressid(address);
				
				
				if (StringUtils.isNotBlank(DoctorFirstName)) {
					doctorlist.setFirstName(DoctorFirstName);
				} else {
					doctorlist.setFirstName(null);
				}
				
				if (StringUtils.isNotBlank(DoctorMiddleName)) {
					doctorlist.setMiddleName(DoctorMiddleName);
				} else {
					doctorlist.setMiddleName(null);
				}
				
				if (StringUtils.isNotBlank(DoctorLastName)) {
					doctorlist.setLastName(DoctorLastName);
				} else {
					doctorlist.setLastName(null);
				}
				
				if (StringUtils.isNotBlank(DoctorEmailAddress)) {
					doctorlist.setEmail(DoctorEmailAddress);
				} else {
					doctorlist.setEmail(null);
				}
				
				if (StringUtils.isNotBlank(DoctorUserName)) {
					doctorlist.setUserName(DoctorUserName);
				} else {
					doctorlist.setUserName(null);
				}
				
				if (StringUtils.isNotBlank(DoctorGender)) {
					doctorlist.setGender(DoctorGender);
				} else {
					doctorlist.setGender(null);
				}
				
				if (StringUtils.isNotBlank(DoctorAge)) {
					doctorlist.setAge(new Long(DoctorAge));
				} else {
					doctorlist.setAge(null);
				}
				
				if (StringUtils.isNotBlank(DoctorBio)) {
					doctorlist.setBio(DoctorBio);
				} else {
					doctorlist.setBio(null);
				}
				
				if (StringUtils.isNotBlank(DoctorMobileNumber)) {
					doctorlist.setMobile(DoctorMobileNumber);
				} else {
					doctorlist.setMobile(null);
				}
				
				if (StringUtils.isNotBlank(DoctorSpecialist)) {
					doctorlist.setSpecialist(DoctorSpecialist);
				} else {
					doctorlist.setSpecialist(null);
				}
				
				if (StringUtils.isNotBlank(DoctorHospitalName)) {
					doctorlist.setBelongToHospital(DoctorHospitalName);
				} else {
					doctorlist.setBelongToHospital(null);
				}
				doctorlist.setProfileImage(path);
				
				doctorlist.setUpdateddate(new Date());
				
				service.add(doctorlist, null);
			
			}
			DetachedCriteria criteria3 = DetachedCriteria.forClass(Cart.class);
		 	criteria3.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
			criteria3.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
			List<Cart> cartlist = service.findAll(criteria3);
			
				request.setAttribute("CartList", cartlist);
				request.setAttribute("CartListCount", cartlist.size());  
			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}
	}
	
	@Override
	public Object doList(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			
			DetachedCriteria criteriaobj = DetachedCriteria.forClass(Doctor.class);
			criteriaobj.add(Restrictions.eq(Doctor.PROP_STATUS, "Active"));
			List<Doctor> listobj = service.findAll(criteriaobj);

			request.setAttribute("DoctorList", listobj);
			
			
			DetachedCriteria criteria3 = DetachedCriteria.forClass(Cart.class);
		 	criteria3.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
			criteria3.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
			List<Cart> cartlist = service.findAll(criteria3);
			
				request.setAttribute("CartList", cartlist);
				request.setAttribute("CartListCount", cartlist.size());  
             
			return "SUCCESS";
		} catch (Exception e) {
			return ERROR;
		}
	}

	@Override
	public Object doDelete(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			String doctorid = request.getParameter("doctorid");
			DetachedCriteria criteriaobj = DetachedCriteria.forClass(Doctor.class);
			criteriaobj.add(Restrictions.eq(Doctor.PROP_ID, new Long(doctorid)));
			List<Doctor> listobj = service.findAll(criteriaobj);

			for (Doctor doctorlist : listobj) {

				doctorlist.setUpdateddate(new Date());
				doctorlist.setStatus("InActive");

				service.add(doctorlist, null);

			}

			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}
	
	public List<State> getState(String countryid)
	{
		List<State>statelist=new ArrayList<State>();
		try{
			if (StringUtils.isNotBlank(countryid) && GenericValidator.isLong(countryid)) {
				
				DetachedCriteria criteria=DetachedCriteria.forClass(State.class);
				criteria.add(Restrictions.eq(State.PROP_COUNTRY+"."+Country.PROP_ID, new Long(countryid)));
				statelist = service.findAll(criteria);
			}			
		}catch(Exception e)
		{
			e.printStackTrace();
		}		
		return statelist;
	}
	
	public List<City> getCity(String stateid)
	{
		List<City>citylist=new ArrayList<City>();
		try{
			if (StringUtils.isNotBlank(stateid) && GenericValidator.isLong(stateid)) {
				
				DetachedCriteria criteria=DetachedCriteria.forClass(City.class);
				criteria.add(Restrictions.eq(City.PROP_STATE+"."+State.PROP_ID, new Long(stateid)));
				citylist = service.findAll(criteria);
			}			
		}catch(Exception e)
		{
			e.printStackTrace();
		}		
		return citylist;
	}

	

}
