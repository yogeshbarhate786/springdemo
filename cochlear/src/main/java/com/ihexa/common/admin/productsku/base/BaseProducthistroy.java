package com.ihexa.common.admin.productsku.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the producthistroy table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="producthistroy"
 */

public abstract class BaseProducthistroy  implements Serializable {

	public static String REF = "Producthistroy";
	public static String PROP_ADDEDDATE = "Addeddate";
	public static String PROP_STATUS = "Status";
	public static String PROP_VERSION = "Version";
	public static String PROP_STOPDATE = "Stopdate";
	public static String PROP_PRODUCTPRICE = "Productprice";
	public static String PROP_PRODUCTID = "Productid";
	public static String PROP_ID = "Id";
	public static String PROP_PRODUCTURL = "Producturl";


	// constructors
	public BaseProducthistroy () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseProducthistroy (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.math.BigDecimal version;
	private java.util.Date addeddate;
	private java.util.Date stopdate;
	private java.lang.String status;
	private java.lang.String producturl;
	private java.math.BigDecimal productprice;

	// many to one
	private com.ihexa.common.admin.productsku.Product productid;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="id"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: version
	 */
	public java.math.BigDecimal getVersion () {
		return version;
	}

	/**
	 * Set the value related to the column: version
	 * @param version the version value
	 */
	public void setVersion (java.math.BigDecimal version) {
		this.version = version;
	}



	/**
	 * Return the value associated with the column: addeddate
	 */
	public java.util.Date getAddeddate () {
		return addeddate;
	}

	/**
	 * Set the value related to the column: addeddate
	 * @param addeddate the addeddate value
	 */
	public void setAddeddate (java.util.Date addeddate) {
		this.addeddate = addeddate;
	}



	/**
	 * Return the value associated with the column: stopdate
	 */
	public java.util.Date getStopdate () {
		return stopdate;
	}

	/**
	 * Set the value related to the column: stopdate
	 * @param stopdate the stopdate value
	 */
	public void setStopdate (java.util.Date stopdate) {
		this.stopdate = stopdate;
	}



	/**
	 * Return the value associated with the column: status
	 */
	public java.lang.String getStatus () {
		return status;
	}

	/**
	 * Set the value related to the column: status
	 * @param status the status value
	 */
	public void setStatus (java.lang.String status) {
		this.status = status;
	}



	/**
	 * Return the value associated with the column: producturl
	 */
	public java.lang.String getProducturl () {
		return producturl;
	}

	/**
	 * Set the value related to the column: producturl
	 * @param producturl the producturl value
	 */
	public void setProducturl (java.lang.String producturl) {
		this.producturl = producturl;
	}



	/**
	 * Return the value associated with the column: productprice
	 */
	public java.math.BigDecimal getProductprice () {
		return productprice;
	}

	/**
	 * Set the value related to the column: productprice
	 * @param productprice the productprice value
	 */
	public void setProductprice (java.math.BigDecimal productprice) {
		this.productprice = productprice;
	}



	/**
	 * Return the value associated with the column: productid
	 */
	public com.ihexa.common.admin.productsku.Product getProductid () {
		return productid;
	}

	/**
	 * Set the value related to the column: productid
	 * @param productid the productid value
	 */
	public void setProductid (com.ihexa.common.admin.productsku.Product productid) {
		this.productid = productid;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.productsku.Producthistroy)) return false;
		else {
			com.ihexa.common.admin.productsku.Producthistroy producthistroy = (com.ihexa.common.admin.productsku.Producthistroy) obj;
			if (null == this.getId() || null == producthistroy.getId()) return false;
			else return (this.getId().equals(producthistroy.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}