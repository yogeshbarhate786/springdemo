package com.ihexa.common.admin.authorization.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the authorization table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="authorization"
 */

public abstract class BaseAuthorization  implements Serializable {

	public static String REF = "Authorization";
	public static String PROP_TYPE = "type";
	public static String PROP_AUTHORIZATION_GROUP = "authorizationGroup";
	public static String PROP_DESCRIPTION = "description";
	public static String PROP_NAME = "name";
	public static String PROP_ID = "id";


	// constructors
	public BaseAuthorization () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseAuthorization (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public BaseAuthorization (
		java.lang.Long id,
		java.lang.String name) {

		this.setId(id);
		this.setName(name);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String name;
	private java.lang.String description;
	private java.lang.Integer type;

	// many to one
	private com.ihexa.common.admin.authorization.AuthorizationGroup authorizationGroup;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: Name
	 */
	public java.lang.String getName () {
		return name;
	}

	/**
	 * Set the value related to the column: Name
	 * @param name the Name value
	 */
	public void setName (java.lang.String name) {
		this.name = name;
	}



	/**
	 * Return the value associated with the column: Description
	 */
	public java.lang.String getDescription () {
		return description;
	}

	/**
	 * Set the value related to the column: Description
	 * @param description the Description value
	 */
	public void setDescription (java.lang.String description) {
		this.description = description;
	}



	/**
	 * Return the value associated with the column: Type
	 */
	public java.lang.Integer getType () {
		return type;
	}

	/**
	 * Set the value related to the column: Type
	 * @param type the Type value
	 */
	public void setType (java.lang.Integer type) {
		this.type = type;
	}



	/**
	 * Return the value associated with the column: AuthorizationGroupID
	 */
	public com.ihexa.common.admin.authorization.AuthorizationGroup getAuthorizationGroup () {
		return authorizationGroup;
	}

	/**
	 * Set the value related to the column: AuthorizationGroupID
	 * @param authorizationGroup the AuthorizationGroupID value
	 */
	public void setAuthorizationGroup (com.ihexa.common.admin.authorization.AuthorizationGroup authorizationGroup) {
		this.authorizationGroup = authorizationGroup;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.authorization.Authorization)) return false;
		else {
			com.ihexa.common.admin.authorization.Authorization authorization = (com.ihexa.common.admin.authorization.Authorization) obj;
			if (null == this.getId() || null == authorization.getId()) return false;
			else return (this.getId().equals(authorization.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}