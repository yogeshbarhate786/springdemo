package com.ihexa.common.admin.user.service;

import java.util.List;

import org.springframework.validation.Errors;

import com.prounify.framework.base.service.Service;

/**
 * Service layer interface for User entity.
 */

public interface UserService<T> extends Service<T> {
	public void register(T o, Errors validationErrors);

	public List getCountryList();
	public List getOccupationList();

	public List getStateList(Long countryId);
	public List getCityList(Long stateId);

	public List getRoleList();
	public List getManagerList(Long roleId);

}
