package com.ihexa.common.admin.person;

import com.ihexa.common.admin.person.base.BaseState;



public class State extends BaseState {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public State () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public State (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}