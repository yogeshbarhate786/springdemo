package com.ihexa.common.admin.googlemaps.action;

public class listdetail {
	private String id;
	private String Areacode;
	private String Areaname;
	private String Assignto;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAreacode() {
		return Areacode;
	}
	public void setAreacode(String areacode) {
		Areacode = areacode;
	}
	public String getAreaname() {
		return Areaname;
	}
	public void setAreaname(String areaname) {
		Areaname = areaname;
	}
	public String getAssignto() {
		return Assignto;
	}
	public void setAssignto(String assignto) {
		Assignto = assignto;
	}
	public listdetail(String id, String areacode, String areaname, String assignto) {
		super();
		this.id = id;
		Areacode = areacode;
		Areaname = areaname;
		Assignto = assignto;
	}
}
