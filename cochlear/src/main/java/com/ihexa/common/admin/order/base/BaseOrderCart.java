package com.ihexa.common.admin.order.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the order_cart table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="order_cart"
 */

public abstract class BaseOrderCart  implements Serializable {

	public static String REF = "OrderCart";
	public static String PROP_ORDERPLACEDATE = "Orderplacedate";
	public static String PROP_STATUS = "Status";
	public static String PROP_DOCTORID = "Doctorid";
	public static String PROP_UNIQUEID = "Uniqueid";
	public static String PROP_ORDERPAIDDATE = "Orderpaiddate";
	public static String PROP_CARTID = "Cartid";
	public static String PROP_ID = "Id";
	public static String PROP_ORDERSTATUS = "Orderstatus";


	// constructors
	public BaseOrderCart () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseOrderCart (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String uniqueid;
	private java.util.Date orderplacedate;
	private java.util.Date orderpaiddate;
	private java.lang.String status;
	private java.lang.String orderstatus;

	// many to one
	private com.ihexa.common.admin.doctor.Doctor doctorid;
	private com.ihexa.common.admin.cart.Cart cartid;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="identity"
     *  column="id"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: uniqueid
	 */
	public java.lang.String getUniqueid () {
		return uniqueid;
	}

	/**
	 * Set the value related to the column: uniqueid
	 * @param uniqueid the uniqueid value
	 */
	public void setUniqueid (java.lang.String uniqueid) {
		this.uniqueid = uniqueid;
	}



	/**
	 * Return the value associated with the column: orderplacedate
	 */
	public java.util.Date getOrderplacedate () {
		return orderplacedate;
	}

	/**
	 * Set the value related to the column: orderplacedate
	 * @param orderplacedate the orderplacedate value
	 */
	public void setOrderplacedate (java.util.Date orderplacedate) {
		this.orderplacedate = orderplacedate;
	}



	/**
	 * Return the value associated with the column: orderpaiddate
	 */
	public java.util.Date getOrderpaiddate () {
		return orderpaiddate;
	}

	/**
	 * Set the value related to the column: orderpaiddate
	 * @param orderpaiddate the orderpaiddate value
	 */
	public void setOrderpaiddate (java.util.Date orderpaiddate) {
		this.orderpaiddate = orderpaiddate;
	}



	/**
	 * Return the value associated with the column: status
	 */
	public java.lang.String getStatus () {
		return status;
	}

	/**
	 * Set the value related to the column: status
	 * @param status the status value
	 */
	public void setStatus (java.lang.String status) {
		this.status = status;
	}



	/**
	 * Return the value associated with the column: orderstatus
	 */
	public java.lang.String getOrderstatus () {
		return orderstatus;
	}

	/**
	 * Set the value related to the column: orderstatus
	 * @param orderstatus the orderstatus value
	 */
	public void setOrderstatus (java.lang.String orderstatus) {
		this.orderstatus = orderstatus;
	}



	/**
	 * Return the value associated with the column: doctorid
	 */
	public com.ihexa.common.admin.doctor.Doctor getDoctorid () {
		return doctorid;
	}

	/**
	 * Set the value related to the column: doctorid
	 * @param doctorid the doctorid value
	 */
	public void setDoctorid (com.ihexa.common.admin.doctor.Doctor doctorid) {
		this.doctorid = doctorid;
	}



	/**
	 * Return the value associated with the column: cartid
	 */
	public com.ihexa.common.admin.cart.Cart getCartid () {
		return cartid;
	}

	/**
	 * Set the value related to the column: cartid
	 * @param cartid the cartid value
	 */
	public void setCartid (com.ihexa.common.admin.cart.Cart cartid) {
		this.cartid = cartid;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.order.OrderCart)) return false;
		else {
			com.ihexa.common.admin.order.OrderCart orderCart = (com.ihexa.common.admin.order.OrderCart) obj;
			if (null == this.getId() || null == orderCart.getId()) return false;
			else return (this.getId().equals(orderCart.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}