package com.ihexa.common.admin.login.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.springframework.validation.DataBinder;

import com.ihexa.common.admin.login.Login;
import com.ihexa.common.admin.login.service.LoginService;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.base.action.BindingActionForm;
import com.prounify.framework.context.UserContext;

public class LoginAction extends BaseCRUDAction {

	// private static Log log = LogFactory.getLog(LoginAction.class);
	public Object doLogin(BaseActionMapping bam, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		BindingActionForm actionForm = (BindingActionForm) form;
		LoginService loginService = (LoginService) getService(bam);
		Login login = (Login) getBean(bam.getBaseName());
		DataBinder binder = getBinder(login, bam, request);

		// BindException errors = new BindException(binder.getBindingResult());
		try {
			System.out.println("--------------------UserContext.getUserId() == ");
			loginService.login(login, binder.getBindingResult());

			System.out.println("--------------------UserContext.getUserId() == "
					+ UserContext.getUserId());
			return SUCCESS;
		} catch (Exception e) {
			// log.debug("login failed");
			actionForm.expose(binder.getBindingResult(), request, e);
			return ERROR;
		}
	}

	public Object doLogout(BaseActionMapping bam, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		// LoginService loginService = (LoginService) getService(bam);
		// loginService.logout(o, e)
		UserContext.invalidateAuthenticatedSession();

		// loginService.logout();
		return "SUCCESS";
	}

	protected boolean isHttpMethodAllowed(String httpMethod, String methodId) {

		if (httpMethod.equals(HTTP_METHOD_POST)) {

			if (methodId.equals("login"))
				return true;
		}

		if (methodId.equals("logout")) {
			return true;
		}

		// return false;
		return super.isHttpMethodAllowed(httpMethod, methodId);
	}

}
