package com.ihexa.common.admin.user;

import com.ihexa.common.admin.user.base.BaseUser;



public class User extends BaseUser {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public User () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public User (java.lang.Long id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public User (
		java.lang.Long id,
		com.ihexa.common.admin.user.Person person,
		com.ihexa.common.admin.user.Role role) {

		super (
			id,
			person,
			role);
	}

/*[CONSTRUCTOR MARKER END]*/


}