package com.ihexa.common.admin.person.base;

import java.io.Serializable;


/**
 * This is an object that contains data related to the state table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="state"
 */

public abstract class BaseState  implements Serializable {

	public static String REF = "State";
	public static String PROP_ID = "id";
	public static String PROP_COUNTRY = "country";
	public static String PROP_STATE_NAME = "stateName";


	// constructors
	public BaseState () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseState (java.lang.Long id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Long id;

	// fields
	private java.lang.String stateName;

	// many to one
	private com.ihexa.common.admin.person.Country country;



	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="native"
     *  column="ID"
     */
	public java.lang.Long getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Long id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}




	/**
	 * Return the value associated with the column: StateName
	 */
	public java.lang.String getStateName () {
		return stateName;
	}

	/**
	 * Set the value related to the column: StateName
	 * @param stateName the StateName value
	 */
	public void setStateName (java.lang.String stateName) {
		this.stateName = stateName;
	}



	/**
	 * Return the value associated with the column: CountryID
	 */
	public com.ihexa.common.admin.person.Country getCountry () {
		return country;
	}

	/**
	 * Set the value related to the column: CountryID
	 * @param country the CountryID value
	 */
	public void setCountry (com.ihexa.common.admin.person.Country country) {
		this.country = country;
	}




	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof com.ihexa.common.admin.person.State)) return false;
		else {
			com.ihexa.common.admin.person.State state = (com.ihexa.common.admin.person.State) obj;
			if (null == this.getId() || null == state.getId()) return false;
			else return (this.getId().equals(state.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}


	public String toString () {
		return super.toString();
	}


}