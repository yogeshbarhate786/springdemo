package com.ihexa.common.admin.profile;

import com.ihexa.common.admin.profile.base.BaseProfile;

public class Profile extends BaseProfile {
	private static final long serialVersionUID = 1L;

	/* [CONSTRUCTOR MARKER BEGIN] */
	public Profile () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Profile (java.lang.Long id) {
		super(id);
	}

	/**
	 * Constructor for required fields
	 */
	public Profile (
		java.lang.Long id,
		com.ihexa.common.admin.profile.Role role) {

		super (
			id,
			role);
	}

	/* [CONSTRUCTOR MARKER END] */

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ihexa.common.admin.profile.base.BaseProfile#initialize()
	 */
	@Override
	protected void initialize() {
		// TODO Auto-generated method stub
		this.setRole(new Role());
		super.initialize();
	}

}