package com.ihexa.common.admin.ticket.action;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.ihexa.common.admin.cart.Cart;
import com.ihexa.common.admin.cart.Carttrack;
import com.ihexa.common.admin.doctor.Doctor;
import com.ihexa.common.admin.gift.service.GiftService;
import com.ihexa.common.admin.order.OrderCart;
import com.ihexa.common.admin.order.Orderdetails;
import com.ihexa.common.admin.productsku.Category;
import com.ihexa.common.admin.productsku.Maincategory;
import com.ihexa.common.admin.productsku.Product;
import com.ihexa.common.admin.productsku.Subcategory;
import com.ihexa.common.admin.ticket.Ticket;
import com.ihexa.common.admin.user.User;
import com.ihexa.common.admin.utility.RandomNumber;
import com.prounify.framework.base.action.BaseActionMapping;
import com.prounify.framework.base.action.BaseCRUDAction;
import com.prounify.framework.context.Context;
import com.prounify.framework.context.UserContext;

public class TicketAction extends BaseCRUDAction
{
	GiftService service = (GiftService) Context.getInstance().getBean("GiftService");
	@Override
	public Object doAdd(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {
			DetachedCriteria criteria = DetachedCriteria.forClass(Maincategory.class);
			List<Maincategory> maincategorylist = service.findAll(criteria);
			if(maincategorylist.size()>0)
			{
			request.setAttribute("maincategorylist", maincategorylist);
			}
		
			String cartid=request.getParameter("cartid");
			if(cartid!=null)
			{
			DetachedCriteria criobj=DetachedCriteria.forClass(Cart.class);
			criobj.add(Restrictions.eq(Cart.PROP_ID, new Long(cartid)));
			List<Cart> list=service.findAll(criobj);
			
			DetachedCriteria cri=DetachedCriteria.forClass(Product.class);
			cri.add(Restrictions.eq(Product.PROP_ID, list.get(0).getProductid().getId()));
		   List<Product> prolist=service.findAll(cri);
		      if(prolist.size()>0)
		      {
		     request.setAttribute("ProList", prolist);
		      }
		      }
			else
			{
				DetachedCriteria cri=DetachedCriteria.forClass(Product.class);
				List<Product> prolist=service.findAll(cri);
				  if(prolist.size()>0)
				  {
				   request.setAttribute("ProList", prolist);	
				  }
			}
			
			DetachedCriteria criteria1 = DetachedCriteria.forClass(Cart.class);
				criteria1.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
				criteria1.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
				List<Cart> cartlist = service.findAll(criteria1);
				
				request.setAttribute("CartList", cartlist);
				request.setAttribute("cartid",cartid);
				request.setAttribute("CartListCount", cartlist.size());
				
				DetachedCriteria prCriteria=DetachedCriteria.forClass(Product.class);
			    List<Product> products = service.findAll(prCriteria);
				request.setAttribute("ProductsList", products); 
			
		} catch (Exception e) {
			return ERROR;
		}
		return SUCCESS;
	}

	@Override
	public Object doCreate(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception 
			{
		String product = request.getParameter("product");
		String question = request.getParameter("question");
		
		String maincatid = request.getParameter("maincategoryid");
		String catid = request.getParameter("categoryid");
		String subcatid = request.getParameter("subcategoryid");
		String cartid=request.getParameter("cartid");
		 
		int randomnumber=RandomNumber.now();
		
		Long userid = (Long) UserContext.getSessionScope().get("USERID");
		if(userid!=null)
		{
		DetachedCriteria criteriaobj = DetachedCriteria.forClass(com.ihexa.common.admin.user.User.class);
		criteriaobj.add(Restrictions.eq(com.ihexa.common.admin.user.User.PROP_ID, new Long(userid)));
		List<com.ihexa.common.admin.user.User> listobj = service.findAll(criteriaobj);
		if(listobj.size()>0)
		{
		request.setAttribute("UserEditList", listobj.get(0));
		if(listobj.get(0).getDoctorid()!=null)
		{
		Long doctorid = listobj.get(0).getDoctorid().getId();
		Ticket ticket=new Ticket();
		        
		if(StringUtils.isBlank(product)) {
			ticket.setProductname(null);
		} else {
			ticket.setProductname(product);
		}
		
		if (StringUtils.isBlank(question)) {
			ticket.setQuestion(null);
		} else {
			ticket.setQuestion(question);
		}
		if (StringUtils.isBlank(maincatid)) {
			ticket.setMaincatid(null);
		} else {
			ticket.setMaincatid(new Maincategory(new Long(maincatid)));
		}
		if (StringUtils.isBlank(catid)) {
			ticket.setCategoryid(null);
		} else {
			ticket.setCategoryid(new Category(new Long(catid)));
		}
		
		if (StringUtils.isBlank(subcatid)) {
			ticket.setSubcategoryid(null);
		} else {
			ticket.setSubcategoryid(new Subcategory(new Long(subcatid)));
		}
		if(cartid!=null  || StringUtils.isBlank(cartid))
		{
		ticket.setCartid(null);	
		}
		else
		{
			ticket.setCartid(new Cart(new Long(cartid)));
		}
	  
		ticket.setUniqueid("TICKET"+Integer.toString(randomnumber));
		ticket.setCreateddate(new Date());
		ticket.setUpdateddate(new Date());
		ticket.setStatus("Active");
		ticket.setSolvestatus("pendding");
		ticket.setDoctorid(new Doctor(new Long(doctorid)));
		
		service.add(ticket, null);
		}
		DetachedCriteria criteria1 = DetachedCriteria.forClass(Cart.class);
		criteria1.add(Restrictions.eq(Cart.PROP_CARTSUCCESSSTATUS, "Deactive"));
			criteria1.add(Restrictions.eq(Cart.PROP_STATUS, "Active"));
			List<Cart> cartlist = service.findAll(criteria1);
			request.setAttribute("CartList", cartlist);
			request.setAttribute("CartListCount", cartlist.size());
		}
	   }
		return SUCCESS;
	}

	@Override
	public Object doList(BaseActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		   DetachedCriteria criticket=DetachedCriteria.forClass(Ticket.class);
		   criticket.add(Restrictions.eq(Ticket.PROP_STATUS, "Active"));
		   List<Carttrack> ticketlist=service.findAll(criticket);
		   request.setAttribute("Ticketlist",ticketlist);
		   
		   
		   request.setAttribute("ticketcount", ticketlist.size());
		   DetachedCriteria cri=DetachedCriteria.forClass(Orderdetails.class);
		   cri.add(Restrictions.eq(Orderdetails.PROP_STATUS, "Success"));
		   cri.add(Restrictions.eq(Orderdetails.PROP_APPROVESTATUS, "Pendding"));
		   List<Orderdetails> orderlist=service.findAll(cri);
		   
		   request.setAttribute("ordercount",orderlist.size());
		
		return SUCCESS;
	}
	
	
}
