package com.ihexa.common.admin.gift;

import com.ihexa.common.admin.gift.base.BaseGift;



public class Gift extends BaseGift {
	private static final long serialVersionUID = 1L;

/*[CONSTRUCTOR MARKER BEGIN]*/
	public Gift () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public Gift (java.lang.Long id) {
		super(id);
	}

/*[CONSTRUCTOR MARKER END]*/


}