package com.ihexa.common.admin.login.service;

import org.springframework.validation.Errors;

import com.ihexa.common.admin.login.Login;
import com.prounify.framework.base.service.Service;

/**
 * Service layer interface for User entity.
 */

public interface LoginService<T> extends Service<T> {
	public void login(Login o, Errors e) throws Exception;

	public void logout(Login o, Errors e);

}
