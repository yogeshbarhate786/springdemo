<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html" uri="http://jakarta.apache.org/struts/tags-html"%>
<%@ taglib prefix="logic" uri="http://jakarta.apache.org/struts/tags-logic"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>IHexa : Login</title>
<STYLE TYPE="text/css">
<!--
.button2 {
	background-color: #105A9F;
	border: none;
	font-weight: bold;
	cursor: pointer;
	color: #fff;
	border-width: 1px;
	border-style: solid;
	border-color: #fff #333 #333 #fff;
	font-size: 11px;
	padding: 3px
}
-->
</STYLE>

<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-size: 12px;
	font-family: Arial, Helvetica, sans-serif;
}
-->
</style>


<script language="javascript">

function Login(form) {
var username = form.loginName.value;
var password = form.password.value;

if (username && password ) 
	{
		form.submit();
	}
else 
   {
	alert("Please enter your username and password.\n");
   }
}

</script>
</head>

<table width="780" border="0" cellspacing="0" cellpadding="10"
	align="center">
	<tr align="center">

		<td width="100%" valign="top" bgcolor="#FFFFFF">
		<p><font size="4" face="Arial, Helvetica, sans-serif"><strong>IHexa</strong></font><br>
		</p>


		<table width="300" border="0" cellpadding="0" cellspacing="0"
			align="center">
			<logic:messagesPresent>
				<tr>
					<td>
					<div id="errorDiv">
					<table border="0" cellspacing="2" cellpadding="2" class="ebox">
						<tr>
							<td width="2%" align="right"><img src=../../images/alert.jpg
								alt="Error occured" width="16" height="16" /></td>
							<td width="92%" valign="middle" class="body" colspan="2">
							<html:messages id="error" message="false">
								<span class="style1"> <c:out value="${error}"
									escapeXml="false" /> </span><BR>
							</html:messages></td>
						</tr>
					</table>

					</div>
					</td>
				</tr>
			</logic:messagesPresent>
			<tr>
				<td>
				<table width="100%" border="0" cellspacing="1" cellpadding="5"
					align="center">
					<tr>
						<td bgcolor="#6699CC"><font color="#FFFFFF" size="2"
							face="Arial, Helvetica, sans-serif"><strong>Members
						Login</strong></font></td>
					</tr>
					<tr>
						<td bgcolor="#eeeeee">
						<form name=login action="<c:url value="/admin/login/login.do"/>"
							method="post">
						<table width="100%" border="0" cellspacing="0" cellpadding="2"
							align="center">
							<tr>
								<td align="right"><strong><font size="2"
									face="Arial, Helvetica, sans-serif">Username:</font></strong></td>
								<td><input name="loginName" type="text" size="11"></td>
							</tr>
							<tr>
								<td align="right"><font size="2"
									face="Arial, Helvetica, sans-serif"><strong>Password:</strong></font></td>
								<td><input name="password" type="password" size="11"></td>
							</tr>
							<tr>
								<td align="right">&nbsp;</td>
								<td><input type=button value="Login"
									onClick="Login(this.form)" name="button" class="button2"></td>
							</tr>

						</table>
						</form>
						</td>
					</tr>

				</table>
				</td>
			</tr>

		</table>


		</td>

	</tr>


</table>

<table border="0" cellpadding="0" cellspacing="0" align="center">
	<tr align="center">
		<td align="right">&nbsp;
		<div class="footer font10">&copy; 2008-2009&nbsp;&nbsp;
		IHexa. All Rights Reserved.</div>

		</td>
	</tr>

</table>
</html>