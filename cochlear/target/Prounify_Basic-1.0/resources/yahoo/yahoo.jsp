
<script type='text/javascript' src='resources/jmaki-min.js'></script>
<script type='text/javascript' src='resources/wfactory.js'></script>
<!-- autocomplete  -->
<span jmakiName="yahoo.autocomplete"
     jmakiValue="[
       {label : 'Alabama', value : 'AL'},
       {label : 'California', value : 'CA'},
       {label : 'New York', value : 'NY', selected : true},
       {label : 'Texas', value : 'TX'}	           
       ]"></span>

<!-- button  -->
<span jmakiName="yahoo.button" 
      jmakiValue="{ label : 'click me ' }"></span>
<!-- calendar  -->
<span jmakiName="yahoo.calendar"></span>
<!-- colorpicker  -->
<span jmakiName="yahoo.colorpicker"></span>
<!-- dataTable  -->
<div jmakiName="yahoo.dataTable"
     jmakiValue="{columns : [
      { label : 'Title', id : 'title'},
      { label :'Author', id : 'author'},
      { label : 'ISBN', id : 'isbn'},
      { label : 'Description', id : 'description'}
     ],
     rows : [
     { title : 'Book Title 1', author : 'Author 1', isbn: '4412', description : 'A Some long description'},
     { id : 'bar', title : 'Book Title 2', author : 'Author 2', isbn : '4412', description : 'A Some long description'}
     ]
     }"></div>

<!-- editor  -->
<div jmakiName="yahoo.editor"
     jmakiValue="Edit me"></div>
<!-- geocoder  -->
<div jmakiName="yahoo.geocoder"></div>
<!-- Note to developer. This widget requires the jMaki XmlHttpProxy
     to communicate with the Yahoo geocoding serivce.
     The proxy vailable both in PHP and Java and can be downloads from
     https://ajax.dev.java.net/downloads.html.
--> 

<!-- logger  -->
<div jmakiName="yahoo.logger"></div>                  

<!-- map  -->
<div jmakiName="yahoo.map"
     jmakiArgs="{ centerLat : 37.4041960114344, 
                  centerLon : -122.008194923401 }"></div>


<!-- menu  -->
<div jmakiName="yahoo.menu"
     jmakiValue="{menu: [ 
                { label :'Action',
                  action : { topic : '/foo', message : { value : 'test.jsp' } }
                },
                { label : 'Must Read',
                
                  menu: [
                   { label:'dev.java.net',
                     menu: [ 
                       { label : 'jMaki',
                         href :'http://ajax.dev.java.net'
                       },
                       { label : 'Glassfish',
                         href : 'http://glassfish.dev.java.net' 
                       }
                      ]  
                    }
                  ]                             
                }, 
                { label:'Click me for fun!',style:{strongemphasis:true} },
                { label:'Disabled!',style:{disabled:true} },
                { label:'Yahoo!', 
                  href :'http://www.yahoo.com' },
                { label:'Sun Microsystems',
                  href: 'http://www.sun.com',style:{checked:true} },
                { label:'Oracle', 
                  href: 'http://www.oracle.com' }
                ]}"></div>  

<!-- RGB Slider  -->
<div jmakiName="yahoo.rgbslider"></div>
<!-- search  -->
<span jmakiName="yahoo.search"></span>
<!-- Simple Dialog  -->

<div jmakiName="yahoo.simpledlg"></div>
<!--  Slider  -->

<span jmakiName="yahoo.slider"></span>
<!-- Tabbed View  -->
<div jmakiName="yahoo.tabbedview"
     jmakiValue="{items:[
           {label : 'My Tab', content : 'Some Content'},
           {id : 'bar', label : 'My Tab 2', include : 'test.jsp ', lazyLoad : true },
           {label : 'My Tab 3', content : 'More Content',  selected : true}
          ]
         }"></div>
         
<!-- tree  -->    
<span jmakiName="yahoo.tree"
      jmakiValue="{
          root : {
           label : 'Yahoo Tree Root Node',
           expanded : true,
           children : [
             { label : 'Node 1.1'},
             { label : 'Node 1.2',
                 expanded : true,
                 children : [
                   { label : 'Node 3.1',
                     action : { topic : '/foo/select', message : {targetId : 'bar'}}
                   }
                 ]
             }
            ]
          }
        }"></span>
<!-- weather  -->
<div jmakiName="yahoo.weather"></div>
<!-- Note to developer. This widget requires the jMaki XmlHttpProxy
     to communicate with the Yahoo geocoding serivce.
     The proxy vailable both in PHP and Java and can be downloads from
     https://ajax.dev.java.net/downloads.html.
--> 

     

