
<script type='text/javascript' src='resources/jmaki-min.js'></script>
<script type='text/javascript' src='resources/wfactory.js'></script>
<div jmakiName="jmaki.accordionMenu"
     jmakiValue="{menu : [
       {label: 'Links',
            menu: [
                { label : 'Sun.com',
                  href : 'http://www.sun.com'},
                { label : 'jMaki.com',
                  href : 'http://www.jmaki.com'}
                ]
       },
       {label: 'Actions',
            menu: [
                { label : 'Select',
                  action :{topic: '/foo/select',
                         message: { targetId : 'bar'}}
                },
                { label :'Set Content',
                  action :{topic: '/foo/setContent',
                         message: { value : 'test.jsp'}}
                }
                ]}
                ]
       }"></div>

       
       
<div jmakiName="jmaki.blockList"
     jmakiValue="[
        {title : 'jMaki Project Home', link : 'https://ajax.dev.java.net', description : 'Where to go for the latest jMaki.' },
        {title : 'jMaki Widgets Home', link : 'https://widgets.dev.java.net', description : 'The source for the latest jMaki widgets.' },
        {title : 'jMaki-Charting Home', link : 'https://jmaki-charting.dev.java.net', description : 'Enables complex charts rendered on the client in any modern browser.' }
    ]"></div>
    
<div jmakiName="jmaki.breadCrumbs"
     jmakiValue="{menu : [
               {label: 'Home',
                href : 'http://www.jmaki.com'
               },
               {label: 'Samples',
                action :{topic: '/foo/select',
                message: { targetId : 'bar'}}
               },
               {label: 'Charting',
                action :{topic: '/foo/select',
                message: { targetId : 'bar2'}}
               }               
               ]
             }"></div>



<div jmakiName="jmaki.carousel"
     jmakiArgs="{ feed : 'https://api.feedburner.com/format/1.0/JSONP?uri=TheAquarium_en' }"></div>




<div jmakiName="jmaki.dcontainer"></div>



<div jmakiName="jmaki.menu"
     jmakiValue="{menu : [
       {label: 'Links',
            menu: [
                { label : 'Sun.com',
                  href : 'http://www.sun.com'},
                { label : 'jMaki.com',
                  href : 'http://www.jmaki.com'}
                ]
       },
       {label: 'Actions',
            menu: [
                { label : 'Select',
                  action :{topic: '/foo/select',
                         message: { targetId : 'bar'}}
                },
                { label :'Set Content',
                  action :{topic: '/foo/setContent',
                         message: { value : 'test.jsp'}}
                }
                ]}
                ]
       }"></div>




<div jmakiName="jmaki.tabMenu"
     jmakiValue="{menu : [
               {label: 'Link',
                href : 'http://www.jmaki.com'
               },
               {label: 'Action',
                action :{topic: '/foo/select',
                message: { targetId : 'bar'}}
               }
               ]
             }"></div>


<div jmakiName="jmaki.tagcloud"
     jmakiValue="{
  items : [
      { label : 'jMaki', weight : 70},
      { label : 'Web2.0', weight : 150},
      { label : 'JSON', weight : 80},
      { label : 'JavaScript', weight : 145},
      { label : 'Java', weight : 100},
      { label : 'RSS', weight : 85},
      { label : 'Autocomplete', weight : 75},
      { label : 'Sun', weight : 65, href : 'http://www.sun.com'},
      { label : 'jMaki', weight : 150},
      { label : 'Web3.0', weight : 70},
      { label : 'Phobos', weight : 105},
      { label : 'Glassfish', weight : 120},
      { label : 'RSS2.0', weight : 75},
      { label : 'Web1.0', weight : 50},
      { label : 'JavaEE', weight : 75},
      { label : 'Jersey', weight : 115},
      { label : 'Roller', weight : 150},
      { label : 'CSS', weight : 105},
      { label : 'DHTML', weight : 65},
      { label : 'Netbeans', weight : 115, href : 'http://www.netbeans.com'}
  ]
}"></div>


