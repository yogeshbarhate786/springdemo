/* Copyright 2008 You may not modify, use, reproduce, or distribute this software except in compliance with the terms of the License at:
 http://developer.sun.com/berkeley_license.html
 $Id: component.js,v 1.0 2007/04/15 19:39:59 gmurray71 Exp $
*/
/**
 * jMaki Flash Video 
 *
 * Find out more documentation at :
 *
 * http://labs.adobe.com/wiki/index.php/Flash-Ajax_Video_Component0
 *
 * version 0.8
 *
 * Author : Greg Murray
 * gmurray71 (a) dev.java.com
 * 
 * 
 */
jmaki.namespace("jmaki.widgets.jmaki.flashplayer");

jmaki.widgets.jmaki.flashplayer.Widget = function(wargs) {
    
    var _widget = this;

    var publish = "/jmaki/flashplayer";
    var subscribe = ["/jmaki/flashplayer"];
    var isMuted = false;
    var videoRoot = "";
    var paused = false;
    var autohide = true;
    var defaultVideo;
    var muted = false;
    var volume;
    var unmuteVolume;
    var autoplay = false;
    var video;
    var bgColor;
    var previewImage;
    var startOffset;

    var skin = "ClearOverAll.swf";
    
    if (wargs.args) {     
        if (wargs.args.skin) {
            skin = wargs.args.skin + ".swf";            
        }
    }

    this.resize = function() {
        var dim = jmaki.getDimensions(_widget.container,55);
        try {     
            _widget.wrapper.setSize(dim.w, dim.h);
        } catch(e) {
            jmaki.log("Error resizing : " + e.message);
        }
    };
    
    this.play = function(_val) {  
        if (_val.message) _val = _val.message;
        if (_val.value) _val = _val.value;
        _widget.wrapper.play(videoRoot + _val);     
    };

    this.setVolume = function(_val) {
        if (_val.message) _val = _val.message;
        if (_val.value) _val = _val.value;
        _widget.wrapper.setVolume(_val);     
    };
    
    this.seek = function(_val) {
        if (_val.message) _val = _val.message;
        if (_val.value) _val = _val.value;
        if (_widget.wrapper.getPlayheadTime()) _val = _widget.wrapper.getPlayheadTime() + _val;
        _widget.wrapper.seek(_val);     
    };
    
    this.setPreviewImage = function(_val) {
        if (_val.message) _val = _val.message;
        if (_val.value) _val = _val.value;        
      _widget.wrapper.setPreviewImagePath(_val);  
    };
    
    this.mute = function() {        
        if (isMuted) isMuted = false;
        else isMuted = true;
        if (isMuted) {         
            unmuteVolume = volume;
            _widget.setVolume(0);
        } else {
            _widget.setVolume(unmuteVolume);
        }
    };
       
    this.load = function(_val) {       
        if (_val.message) _val = _val.message;
        if (_val.value) _val = _val.value;
        if (/http/i.test(_val)) video = _val;
        else video = videoRoot + _val
        _widget.wrapper.load(video);     
    };  
    
    this.stop = function() {
        _widget.wrapper.stop();
    };
    
    this.pause = function() {
        if (paused) paused = false;
        else paused = true;
        _widget.wrapper.pause(paused);
    };
    
    function doSubscribe(topic, handler) {
        var i = jmaki.subscribe(topic, handler);
        _widget.subs.push(i);     
    }
    
    this.postLoad = function() {       
        if (wargs.publish) publish = wargs.publish;
        if (wargs.args) {
            if (typeof wargs.args.autohide == "boolean") {
                autohide = wargs.args.autohide;
            }
            if (wargs.args.skin) {
                skin = wargs.args.skin;
            }
            if (wargs.args.volume) {
                volume = wargs.args.volume;
            }
            if (typeof wargs.args.muted == "boolean") {
                muted = wargs.args.muted;
            }
            if (typeof wargs.args.autoplay == "boolean") {
                autoplay = wargs.args.autoplay;
            }
            if (wargs.args.defaultVideo) {
                defaultVideo = wargs.args.defaultVideo;
            }
            if (wargs.args.previewImage) {
                previewImage = wargs.args.previewImage;
            }
            if (wargs.args.startOffset) {
                startOffset = wargs.args.startOffset;
            }
            if (wargs.args.background) {
                bgColor = wargs.args.background;
            }            
        }
        _widget.container = document.getElementById(wargs.uuid);
        if (bgColor) _widget.container.style.background = bgColor; 
        var dim = jmaki.getDimensions(_widget.container,55);
        var depth =0;
        var loc = top.window.location.href;
        if (loc.charAt(loc.length -1) == "/") {
        	videoRoot = loc;
        } else {
            var subdirs = loc.split("/");
            subdirs.pop();
            videoRoot = subdirs.join("/") + "/";
        }
        // set the path the swf file
        var options = {
            DEFAULT_SWF_PATH : wargs.widgetDir + "FAVideo"
        };
        if (volume) options.volume = volume;
        _widget.wrapper = new FAVideo(wargs.uuid, undefined, dim.w, dim.h, options);
        _widget.wrapper.setSkinAutoHide(autohide);    
        if (skin) _widget.wrapper.setSkinPath(wargs.widgetDir + "/skins/" + skin);
        if (defaultVideo) _widget.load(defaultVideo);
        _widget.subs = [];
        for (var _i=0; _i < subscribe.length; _i++) {
            doSubscribe(subscribe[_i] + "/play", _widget.play);
            doSubscribe(subscribe[_i] + "/seek", _widget.seek);
            doSubscribe(subscribe[_i] + "/load", _widget.play);
            doSubscribe(subscribe[_i] + "/stop", _widget.stop);
            doSubscribe(subscribe[_i] + "/pause", _widget.pause);
            doSubscribe(subscribe[_i] + "/mute", _widget.mute);
        }
		_widget.wrapper.addEventListener("complete", _widget, _widget.complete);
		_widget.wrapper.addEventListener("ready", _widget, _widget.initOptions);
	};
	
    this.initOptions = function() {
        if (autoplay && defaultVideo) _widget.play(defaultVideo);          
	    if (previewImage) _widget.setPreviewImage(videoRoot + previewImage);
        if (volume) _widget.setVolume(volume);
        if (muted) _widget.mute();
        if (startOffset) _widget.seek(startOffset);
    };
	
    this.complete = function() {
        jmaki.publish(publish + "/complete", { widgetId : wargs.uuid, value : video});
    };

    this.destroy = function(){
        _widget.wrapper.removeEventListener("complete", _widget, _widget.complete);
        _widget.wrapper.removeEventListener("ready", _widget, _widget.initOptions);
        for (var i=0; i < _widget.subs.length; i++) {
            jmaki.unsubscribe(_widget.subs[i]);
        }       
    };
};