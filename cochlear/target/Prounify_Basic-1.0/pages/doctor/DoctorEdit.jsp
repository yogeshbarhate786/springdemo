<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Doctor.js'></script>

</head>
 <style>
#blah{
  max-width:120px;
}
input[type=file]{
padding:10px;
background:#2d2d2d;
}
.button {
	background-color: #FE980F;
	border: none;
	color: black;
	padding: 10px 25px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 13px;
	margin: 4px 2px;
	cursor: pointer;
	border-radius: 5px;
}

</style> 
<script>
 function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>

<script type="text/javascript">

	function updateDoctor(form) {
		
		if(form.doctor_first_name.value == "") 
		  {
		    alert("First Name cannot be blank");
		    form.doctor_first_name.focus();
		    return false;
		  }
		
		if(form.doctor_last_name.value == "") 
		  {
		    alert("Last Name cannot be blank");
		    form.doctor_last_name.focus();
		    return false;
		  }
		
		if(form.doctor_email_address.value == "") 
		  {
		    alert("Email Address cannot be blank");
		    form.doctor_email_address.focus();
		    return false;
		  }
		
		if(form.doctor_user_name.value == "") 
		  {
		    alert("User Name cannot be blank");
		    form.doctor_user_name.focus();
		    return false;
		  }
		
		if(form.doctor_gender.value == "") 
		  {
		    alert("Gender cannot be blank");
		    form.doctor_gender.focus();
		    return false;
		  }
		
		if(form.doctor_age.value == "") 
		  {
		    alert("Age cannot be blank");
		    form.doctor_age.focus();
		    return false;
		  }
		
		if(form.doctor_bio.value == "") 
		  {
		    alert("About Yourself cannot be blank");
		    form.doctor_bio.focus();
		    return false;
		  }
		
		if(form.doctor_mobile_number.value == "") 
		  {
		    alert("Mobile Number cannot be blank");
		    form.doctor_mobile_number.focus();
		    return false;
		  }
		
		if(form.doctor_full_address.value == "") 
		  {
		    alert("Full Address cannot be blank");
		    form.doctor_full_address.focus();
		    return false;
		  }
		
		if(form.doctor_address_street.value == "") 
		  {
		    alert("Address Street cannot be blank");
		    form.doctor_address_street.focus();
		    return false;
		  }
		  
		  if(form.doctor_zip_code.value == "") 
		  {
		    alert("Zip Code cannot be blank");
		    form.doctor_zip_code.focus();
		    return false;
		  }
		  
		 	var e = document.getElementById("countryid");
		    var strUser = e.options[e.selectedIndex].value;
		    if(strUser==0)
		    {
		    alert("Please select a Country");
		    return false;
		    }
		    
		    var e = document.getElementById("stateid");
		    var strUser = e.options[e.selectedIndex].value;
		    if(strUser==0)
		    {
		    alert("Please select a State");
		    return false;
		    }
		    
		    var e = document.getElementById("cityid");
		    var strUser = e.options[e.selectedIndex].value;
		    if(strUser==0)
		    {
		    alert("Please select a City");
		    return false;
		    }
		
		if(form.doctor_specialist.value == "") 
		  {
		    alert("Specialist cannot be blank");
		    form.doctor_specialist.focus();
		    return false;
		  }
		
		if(form.doctor_hospital_name.value == "") 
		  {
		    alert("Hospital Name cannot be blank");
		    form.doctor_hospital_name.focus();
		    return false;
		  }
		
		form.submit();
	}
</script>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="login-form">
					<!--login form-->
					<form action="../../admin/doctor/update.do" method="post"
						id="doctorForm" name="doctorForm" enctype="multipart/form-data">
						
						<div class="col-sm-6">
						<div id="form-textarea">
						  <label class="required">Profile Image</label>
						    <c:if test="${empty DoctorEditList.profileImage}">&nbsp;&nbsp;
						   <img id="blah" src="../../template/images/blog/user.png" alt="your image" align="middle" class="img-circle" />
							</c:if>
							 <c:if test="${not empty DoctorEditList.profileImage}">
							<img id="blah" src="${DoctorEditList.profileImage}" alt="your image"  class="img-circle"/>
							</c:if>
							
							<input type='file' onchange="readURL(this);" name="attachment" />
                          </div>
						<div id="filebox"></div>
						<div>
							<p>
								<span class="form-description">Please choose a file(
									Image) less than 2 MB .</span>
							</p>
						</div>
						<div id="form-textarea">
							<label class="required">First Name<span>*</span></label> <input
								type="text" value="${DoctorEditList.firstName}" name="doctor_first_name" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Middle Name</label> <input
								type="text" value="${DoctorEditList.middleName}" name="doctor_middle_name" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Last Name<span>*</span></label> <input
								type="text" value="${DoctorEditList.lastName}" name="doctor_last_name" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Email Address<span>*</span></label> <input
								type="text" value="${DoctorEditList.email}" name="doctor_email_address" readonly/>
						</div>
						
						<div id="form-textarea">
							<label class="required">User Name</label> <input
								type="text" value="${DoctorEditList.userName}" name="doctor_user_name" readonly/>
						</div>
						
						<div id="form-textarea">
							<label class="required">Gender</label> <input
								type="text" value="${DoctorEditList.gender}" name="doctor_gender" />
						</div>
						
					
						<div id="form-textarea">
							<label class="required">Age</label> <input type="text" value="${DoctorEditList.age}"
								 onkeypress="return ValidateMobileNumber(event)" maxlength=2 name="doctor_age" />
						</div>
						
						
						
						</div>
						<div class="col-sm-6">
							
						<div id="form-textarea">
							<label class="required">About Yourself</label> <input
								type="text" value="${DoctorEditList.bio}" name="doctor_bio" />
						</div>
						<div id="form-textarea">
							<label class="required">Mobile Number</label> <input type="text" value="${DoctorEditList.mobile}"
								 onkeypress="return ValidateMobileNumber(event)" maxlength=10 name="doctor_mobile_number" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Full Address</label> <input
								type="text" value="${DoctorEditList.addressid.addressFull}" name="doctor_full_address" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Address Street</label> <input
								type="text" value="${DoctorEditList.addressid.addressStreet}" name="doctor_address_street" />
						</div>
						<div id="form-textarea">
							<label class="required">Zip Code</label> <input
								type="text" value="${DoctorEditList.addressid.zipCode}" name="doctor_zip_code" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Country</label>
							<select id="countryid" name="countryid" class="form-control"
								onchange="getState()">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
								<c:forEach items="${CountryList}" var="countrylistobj">
									<c:choose>
										<c:when
											test="${countrylistobj.id eq DoctorEditList.addressid.country.id}">
											<option value="${countrylistobj.id}" selected="selected">${countrylistobj.countryName}</option>
										</c:when>
										<c:otherwise>
											<option value="${countrylistobj.id}">${countrylistobj.countryName}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
					
						<div id="form-textarea">
							<label class="required">State</label>
							<select id="stateid" name="stateid" class="form-control"
								onchange="getCity()">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
								<c:forEach items="${StateList}" var="statelistobj">
									<c:choose>
										<c:when
											test="${statelistobj.id eq DoctorEditList.addressid.state.id }">
											<option value="${statelistobj.id}" selected="selected">${statelistobj.stateName}</option>
										</c:when>
										<c:otherwise>
											<option value="${statelistobj.id}">${statelistobj.stateName}</option>

										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
						
						<div id="form-textarea">
							<label class="required">City</label>
							<select id="cityid" name="cityid" class="form-control">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
								<c:forEach items="${CityList}" var="citylistobj">
									<c:choose>
										<c:when test="${citylistobj.id eq DoctorEditList.addressid.city.id}">
											<option value="${citylistobj.id}" selected="selected">${citylistobj.cityName}</option>
										</c:when>
										<c:otherwise>
											<option value="${citylistobj.id}">${citylistobj.cityName}</option>

										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
			
						<div id="form-textarea">
							<label class="required">Specialist</label> <input
								type="text" value="${DoctorEditList.specialist}" name="doctor_specialist" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Hospital Name</label> <input
								type="text" value="${DoctorEditList.belongToHospital}" name="doctor_hospital_name" />
						</div>
						
						
						</div>
						
						
							<input id="doctorid" name="doctorid"
									type="hidden" value="${DoctorEditList.id}" />
									
							<div style="display: inline;" align="center">
							<div style="float: center;">
								<button type="button" class="button" id="" value="Update" 
							onclick="updateDoctor(this.form)">UPDATE</button>
							</div>
						<c:if test="${ROLE eq LOGINROLE_ADMINISTRATOR }">
							<div style="margin-left:20px; float: left;">
								<button type="submit" id="" value="List" 
							 formaction="../../admin/doctor/list.do">LIST</button>
							</div>
							</c:if>
						</div>
						<br style="clear:both;">
							
							<!-- <button type="button" id="" value="Update" 
							onclick="updateDoctor(this.form)">UPDATE</button>
							
							<button type="submit" id="" value="List" 
							 formaction="../../admin/doctor/list.do">LIST</button> -->
							
							
							
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>