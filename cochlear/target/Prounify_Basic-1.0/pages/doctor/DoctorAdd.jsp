<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
<script type="text/javascript">

	function addDoctor(form) {
		
		if(form.doctor_full_name.value == "") 
		  {
		    alert("doctor full name cannot be blank");
		    form.doctor_full_name.focus();
		    return false;
		  }
		
		if(form.doctor_email_address.value == "") 
		  {
		    alert("doctor email address cannot be blank");
		    form.doctor_email_address.focus();
		    return false;
		  }
		
		if(form.doctor_email_address.value != "" && form.doctor_full_name.value != "") 
		  {
			form.submit();
		  }
		
		
	}
</script>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="login-form">
					<!--login form-->
					<form action="../../admin/doctor/create.do" method="post" 
						id="doctorForm" name="doctorForm" enctype="multipart/form-data">

						<div id="form-textarea">
							<label class="required">Full Name<span>*</span></label> <input
								type="text" placeholder="Name" name="doctor_full_name" />
						</div>
						
						<div id="form-textarea">
							<label class="required">Email Address<span>*</span></label> <input
								type="text" placeholder="Email" name="doctor_email_address" />
						</div>
						
						
							
							<div style="display: inline;" align="center">
							<div style="float: left;">
								<button type="button" id="" value="Add" 
							onclick="addDoctor(this.form)">ADD</button>
							</div>
							<div style="margin-left:20px; float: left;">
								<button type="submit" id="" value="List" 
							 formaction="../../admin/doctor/list.do">LIST</button>
							</div>
						</div>
						<br style="clear:both;">
							
							<!-- <button type="button" id="" value="Add" 
							onclick="addDoctor(this.form)">ADD</button>
							
							<button type="submit" id="" value="List" 
							 formaction="../../admin/doctor/list.do">LIST</button> -->
							
							<!-- <button type="submit" id="" value="List" 
							 formaction="../../admin/productsku/list.do">LIST</button> -->
							
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>