<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
	<style>
.button {
    background-color: #FE980F;
    border: none;
    color: black;
    padding: 10px 13px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 13px;
    margin: 13px 2px;
    cursor: pointer;
    border-radius: 5px;
    height: 35px;
    width: 100px;
}
a#mod {
	text-decoration: none !important;
	background-color: #2ECC71;
	color: #FFFFFF;
	font-weight: bold;
	padding: 5px;
	border-radius: 10%;
}
</style>
<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li class="active"><b>Ticket List</b></li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					
					<c:if test="${not empty Ticketlist }">
					<thead>
						<tr class="cart_menu">
							<td class="image">Ticket Id</td>
							<td class="description">User Id</td>
						    <td class="description">Generating Date</td>
							   <td class="price">Status</td>
							<td class="description">Ticket Details</td>
							<td class="description">Action</td>
						</tr>
					</thead>
					<tbody>
					  
					<c:forEach items="${Ticketlist}" var="ticketlist" varStatus="loop">
					  
						<tr>
						
							<td class="cart_description">
								<p>${ticketlist.uniqueid}</p>
							</td>
							<td class="cart_price">
								<p>${ticketlist.doctorid.id}</p>
							</td>
						   <td class="cart_quantity">
								<p>${ticketlist.createddate}</p>
							</td>
							  <td class="cart_description">
								<p>${ticketlist.solvestatus}</p>
							</td>
							<td class="cart_price">
							<a href='#' data-toggle="modal" data-target='#sss${loop.index}' id='mod'>Show</a>
							</td>
							<c:if test="${ticketlist.solvestatus eq 'pendding' }">
											<td>
												<button class="button" type="button"
													onclick="window.open('../../admin/ticket/list.do?ticketid=<c:out value="${ticketlist.uniqueid}"/>','_parent')">
													Resolve</button>
											</td>
										</c:if>
										<c:if test="${ticketlist.solvestatus eq 'varify' }">
											<td>
												<button class="button1" type="button">Success</button>
											</td>
										</c:if>	
							</tr>
						</c:forEach>
							</c:if>
							<c:if test="${empty Ticketlist }">
							<h4 align="center">There is No Ticket Available </h4>
							
							</c:if>
				
					</tbody>
				</table>
				<c:forEach items="${Ticketlist}" var="ticketlist" varStatus="loop">
			<div class="modal fade" id="sss${loop.index}" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Ticket Details</h4>
						</div>
						<div class="modal-body">
							<table width="100%"
								class="table table-striped table-bordered table-hover">

								<tr>
									<td width="40%" align="left" valign="top" class="leftColon">User FirstName</td>
									<td width="60%">${ticketlist.doctorid.firstName}</td>
								</tr>

								<tr>
									<td width="40%" align="left" valign="top" class="leftColon">User LastName</td>
									<td width="60%">${ticketlist.doctorid.lastName}</td>
								</tr>
								<tr>
									<td width="40%" align="left" valign="top" class="leftColon">Mobile No</td>
									<td width="60%">${ticketlist.doctorid.mobile}</td>
								</tr>

								<tr>
									<td width="40%" align="left" valign="top" class="leftColon">Email</td>
									<td width="60%">${ticketlist.doctorid.email}</td>
								</tr>
								<tr>
									<td width="40%" align="left" valign="top" class="leftColon">Address</td>
									<td width="60%">${ticketlist.doctorid.fullAddress}</td>
								</tr>
								  <tr>
									<td width="40%" align="left" valign="top" class="leftColon">Product Name</td>
									<td width="60%">${ticketlist.maincatid.name}</td>
								</tr>
                                  <tr>
									<td width="40%" align="left" valign="top" class="leftColon">MainCategory Name</td>
									<td width="60%">${ticketlist.maincatid.name}</td>
								</tr>
								  <tr>
									<td width="40%" align="left" valign="top" class="leftColon">Category Name</td>
									<td width="60%">${ticketlist.categoryid.name}</td>
								</tr>
								  <tr>
									<td width="40%" align="left" valign="top" class="leftColon">SubCategory Name</td>
									<td width="60%">${ticketlist.subcategoryid.name}</td>
								</tr>
								
								  <tr>
									<td width="40%" align="left" valign="top" class="leftColon">Query</td>
									<td width="60%"><textarea id="question-details" cols="30" rows="5" name="question">${ticketlist.question}
									</textarea></td>
								</tr>
							  
						</table>
						</div>

					</div>

				</div>
			</div>
		</c:forEach>
			</div>
		</div>
	</section>
	<section id="cart_items">
		<div class="container">
		</div>
		</section>
</html>