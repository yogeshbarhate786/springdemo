<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html"%>
<%@ taglib prefix="logic"
	uri="http://jakarta.apache.org/struts/tags-logic"%>

<logic:messagesPresent>
	<div id="errorDiv">
	<table align=center>
		<tr>
			<td class="error" width="2%" align="right"><img
				src="../../images/alert.jpg" width="16" height="16"
				alt="Error occured" /></td>
			<td width="92%" valign="middle" class="body" colspan="2"
				style="font: Arial, Helvetica, sans-serif; font-size: 12px"><span
				class="style1"> <html:messages id="error" message="false">
				<c:out value="${error}" escapeXml="false" />
				<BR>
			</html:messages> </span></td>
		</tr>
	</table>
	</div>
</logic:messagesPresent>

<html:messages id="msg" message="true" property="success">
	<table border="0" cellspacing="0" cellpadding="0" class="success"
		align="center">
		<tr>
			<td width="5" align="left" valign="top" class="tl"></td>
			<td align="left" valign="top"></td>
			<td width="5" align="right" valign="top" class="tr"></td>
		</tr>
		<tr>
			<td align="left" valign="top"></td>
			<td align="left" valign="top" class="msCC"><c:out value="${msg}"
				escapeXml="false" /></td>
			<td align="left" valign="top"></td>
		</tr>
		<tr>
			<td align="left" valign="top" class="bl"></td>
			<td align="left" valign="top"></td>
			<td align="right" valign="top" class="br"></td>
		</tr>
	</table>
</html:messages>


<html:messages id="msg" message="true" property="error">
	<table border="0" cellspacing="0" cellpadding="0" class="error"
		align="center">
		<tr>
			<td width="5" align="left" valign="top" class="tl"></td>
			<td align="left" valign="top"></td>
			<td width="5" align="right" valign="top" class="tr"></td>
		</tr>
		<tr>
			<td align="left" valign="top"></td>
			<td align="left" valign="top" class="msCC"><c:out value="${msg}"
				escapeXml="false" /></td>
			<td align="left" valign="top"></td>
		</tr>
		<tr>
			<td align="left" valign="top" class="bl"></td>
			<td align="left" valign="top"></td>
			<td align="right" valign="top" class="br"></td>
		</tr>
	</table>
</html:messages>


<html:messages id="msg" message="true" property="warn">
	<table border="0" cellspacing="0" cellpadding="0" class="warn"
		align="center">
		<tr>
			<td width="5" align="left" valign="top" class="tl"></td>
			<td align="left" valign="top"></td>
			<td width="5" align="right" valign="top" class="tr"></td>
		</tr>
		<tr>
			<td align="left" valign="top"></td>
			<td align="left" valign="top" class="msCC"><c:out value="${msg}"
				escapeXml="false" /></td>
			<td align="left" valign="top"></td>
		</tr>
		<tr>
			<td align="left" valign="top" class="bl"></td>
			<td align="left" valign="top"></td>
			<td align="right" valign="top" class="br"></td>
		</tr>
	</table>
</html:messages>

<html:messages id="msg" message="true" property="note">
	<table border="0" cellspacing="0" cellpadding="0" class="note"
		align="center">
		<tr>
			<td width="5" align="left" valign="top" class="tl"></td>
			<td align="left" valign="top"></td>
			<td width="5" align="right" valign="top" class="tr"></td>
		</tr>
		<tr>
			<td align="left" valign="top"></td>
			<td align="left" valign="top" class="msCC"><c:out value="${msg}"
				escapeXml="false" /></td>
			<td align="left" valign="top"></td>
		</tr>
		<tr>
			<td align="left" valign="top" class="bl"></td>
			<td align="left" valign="top"></td>
			<td align="right" valign="top" class="br"></td>
		</tr>
	</table>
</html:messages>
