<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags"
	prefix="ajax"%>

<div align="right"><input  type="button" class="buttonBg"  name='<fmt:message key="add" />' 
	value='<fmt:message key="add" />' onclick="window.open('add.do','_parent')" />
</div>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td class="boxBg">
			<fieldset>
			<legend class="dashTitle">Search Gift</legend>
				<form name='giftForm' action='<c:url value="/admin/gift/list.do"/>' method="post">
					<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
						<tr>
							<td >
								<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
									<tr>
										<td width="12%"  align="left" class="leftColon">Gift Name</td>
										<td><input id="gift" name="gift" type="text" value="${gift}"  style="width: 182px" />
											<span id="giftNameindicator" class="indicator" style="display: none;"></span>
										</td>
										
									</tr>
									
								</table>
								<div align="right">
									<input type="submit" class="buttonBg" name='Search' value='Search' /> 
									<input type="button" class="buttonBg" name="Clear Search" value="Clear Search"
												onclick="window.open('list.do','_parent')" />
								</div>
							</td>
						</tr>
					</table>
				</form>
			</fieldset>
	
<br>
			<ajax:displayTag id="displayTagFrame">
			
				<display:table name="valueList" export="true" sort="list" pagesize="10"
					cellpadding="2" cellspacing="0" class="its" id="currentRowObject"
					requestURI="list.do?ajax=true">
			
					<display:setProperty name="export.excel.filename" value="Gift List.xls" />
					<display:setProperty name="export.pdf.filename" value="Gift List.pdf" />
					<display:setProperty name="export.xml.filename" value="Gift List.xml" />
					<display:setProperty name="export.csv.filename" value="Gift List.csv" />
					<display:setProperty name="export.rtf.filename" value="Gift List.rtf" />
					
					<display:column title="SNO"  class="colCenter"> <c:out value="${currentRowObject_rowNum}"/> </display:column>
						
					<display:column media="html" title="GIFT NAME" sortable="true"
						headerClass="sortable" sortProperty="giftName" class="colCenter">
						<a href='view.do?giftID=${currentRowObject.id}'>${currentRowObject.giftName}</a>
					</display:column>
					
					<display:column media="csv excel pdf xml rtf" property="giftName" title="GIFT NAME" class="colCenter" />
					
					<display:column media="html csv excel pdf xml rtf" property="giftPrice" title="GIFT PRICE" class="colCenter" />
					
					<display:column media="html" title="EDIT" class="colCenter">
						<a href="#" onclick="window.open('edit.do?id=<c:out value="${currentRowObject.id}"/>','_parent')">
						<img src='<c:url value="/img/edit-icon.gif"/>' /></a>
					</display:column>
					
					<display:column media="html" title="STATUS" class="colCenter">
						<c:choose>
							<c:when test="${currentRowObject.status == 1}">
								<a href="#" onclick="window.open('changeStatus.do?id=<c:out value="${currentRowObject.id}"/>','_parent')">									
								<img src='<c:url value="/img/alert_msg_green.gif"/>' alt="De-activate Gift" title="De-activate Gift"/>ACTIVE </a>		
							</c:when>
							<c:otherwise>
								<a href="#" onclick="window.open('changeStatus.do?id=<c:out value="${currentRowObject.id}"/>','_parent')">
								<img src='<c:url value="/img/alert_msg_red.gif"/>' alt="Activate  Gift" title="Activate Gift" />IN-ACTIVE  </a>	
							</c:otherwise>
						</c:choose>
					</display:column>
					
					<display:column media="html" title="DELETE" class="colCenter">
						<a href="#" onclick="window.open('delete.do?id=<c:out value="${currentRowObject.id}"/>','_parent')">
						<img src='<c:url value="/img/icn_delete.gif"/>' /></a>
					</display:column>
					
				</display:table>
			</ajax:displayTag>
		</td>
	</tr>
</table>

<c:url var="urlTogetGiftName" value="/admin/gift/getGiftName.do" />
<ajax:autocomplete parameters="gift={gift}" source="gift" 
	target="gift" baseUrl="${urlTogetGiftName}" 
	className="autocomplete" indicator="giftNameindicator" minimumCharacters="1" />
	
</body>