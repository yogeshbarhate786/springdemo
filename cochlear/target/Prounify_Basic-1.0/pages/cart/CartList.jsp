<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
<style>
.button {
    background-color: #FE980F;
    border: none;
    color: black;
    padding: 1px 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 13px;
    margin: 4px 2px;
    cursor: pointer;
    border-radius: 5px;
    height: 44px;
    width: 170px;
}
</style>
<!-- <script type="text/javascript">
  
    function onload() { 
    	  var quanid = document.getElementById('quanid');
    	  if(Number(quanid.value)==1)
    		  {
    		  alert("minimum one record is requird in cart");
    		  }
    }
 
</script> -->
<body >
<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li class="active"><b>My Cart (${CartListCount})</b></li>
				</ol>
			</div>
			<form action="" method="post" 
						id="doctorForm" name="doctorForm" enctype="multipart/form-data">
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
			 	<thead>
						<!-- <tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="description"></td>
							<td class="price" style="width:50px;">Quantity</td>
							<td class="price"></td>
						</tr> -->
					</thead> 
					<tbody>
					<c:if test="${not empty CartList}">
					<c:forEach items="${CartList}" var="cartlistvar">
						<tr>
							<td class="cart_description" >
								<a href=""><img src=${cartlistvar.productid.picurl} height="120" width="120" alt=""></a>
							</td>
							
							<td class="cart_description">
								<p>${cartlistvar.productid.name}</p>
								<p>Warranty: ${cartlistvar.productid.warranty}</p>
							
							</td>
							<td class="cart_description">
							
								<center><p>Price</p><p><i class="fa fa-rupee" style="font-size:18px">  ${cartlistvar.productid.price * cartlistvar.quantity }</i></p></center>
								
							
							</td>
							
							<td>
							<div class="row">
								  <div class="col-lg-offset-3 col-lg-6">
								    <div class="input-group">
								      <span class="input-group-btn">
								        <button class="btn btn-danger btn-number" type="button" onclick="window.open('../../admin/cart/changecart.do?cartid=<c:out value="${cartlistvar.id}"/>&symbol=minus','_parent')" style="width: 40px;height:34px" ><i class="fa fa-minus" ></i></button>
								      </span>
								       <input type="text" class="form-control" id="quanid" value="${cartlistvar.quantity}" name="quantity" style="width: 50px;height:34px;margin-bottom: 9px; margin-top: 9px;">
								       <span class="input-group-btn">
								        <button class="btn btn-success btn-number" type="button"  onclick="window.open('../../admin/cart/changecart.do?cartid=<c:out value="${cartlistvar.id}"/>&symbol=plus','_parent')" style="width: 40px;height:34px"><i class="fa fa-plus"></i></button>
								      </span>
								    </div>
								  </div>
								</div>
							</td>
							<td>
					<h7>Free Delivery on</h7> 
							<h7><fmt:formatDate value="${cartlistvar.productDeliveryDate}" pattern="dd-MM-yyyy" /></h7>
							
							</td>
							<td>
							<button class="button" type="button"
							onclick="window.open('../../admin/cart/delete.do?cartid=<c:out value="${cartlistvar.id}"/>','_parent')">
								Remove
								</button>
							</td>
						</tr>
					</c:forEach>
			
					<tr>
						<td></td>
						<td></td>
						<td>
							<button type="button" id="" class="button" value="Continue Shopping" style="margin-top: 22px"
								onclick="window.open('../../admin/dashboard/view.do','_parent')">
								Continue Shopping</button>
						</td>
						<td>
							<button type="submit" class="button" id="" value="Place Order" style="margin-top: 22px"
							formaction="../../admin/order/add.do">Place Order</button>
						</td>
					</tr>
				</c:if>
				<c:if test="${empty CartList}">
				
					<tr align="center">
						
						<td>
						    <h4>Your Shopping cart is empty please do shopping</h4>
							<button type="button"  class="button" class="btn btn-default" value="Continue Shopping" style="margin-top: 22px"
								onclick="window.open('../../admin/dashboard/view.do','_parent')" >
						    		 Go To Home Page</button>
						</td>
					
					</tr>
				
				</c:if>

				<!-- <div style="display: inline;" align="center">
					<div style="float: left;">
						<button type="button" id="" value="Continue Shopping"
							onclick="addDoctor(this.form)">Continue Shopping</button>
					</div>
					<div style="margin-left: 20px; float: left;">
						<button type="submit" id="" value="Place Order">Place Order</button>
					</div>
				</div>
				<br style="clear: both;"> -->

			</tbody>
				</table>
			</div>
			</form>
		</div>
	</section>
	
	</body>
</html>

