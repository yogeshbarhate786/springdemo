<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags"
	prefix="ajax"%>

<c:if test="${autho['USER_ADD'] == 'Yes'}">
	<div align="right"><input  type="button" class="buttonBg" 
		name='<fmt:message key="add" />' value='<fmt:message key="add" />'
		onclick="window.open('add.do','_parent')" /></div>
</c:if>
<br>
<ajax:displayTag id="displayTagFrame">

	<display:table name="valueList" export="true" sort="list" pagesize="10"
		cellpadding="2" cellspacing="0" class="its" id="currentRowObject"
		requestURI="list.do?ajax=true">

		<display:setProperty name="export.excel.filename"
			value="User List.xls" />
		<display:setProperty name="export.pdf.filename" value="User List.pdf" />
		<display:setProperty name="export.xml.filename" value="User List.xml" />
		<display:setProperty name="export.csv.filename" value="User List.csv" />
		<display:setProperty name="export.rtf.filename" value="User List.rtf" />

		<display:column media="html" title="ID" sortable="true"
			headerClass="sortable" sortProperty="id" class="colCenter">
			<a href='view.do?id=${currentRowObject.id}'>${currentRowObject.id}</a>
		</display:column>
		<display:column media="csv excel pdf xml rtf" property="id" title="ID"
			class="colCenter" />

		<display:column media="html" title="NAME" sortable="true"
			headerClass="sortable" sortProperty="person.firstName">
			<a href='view.do?id=${currentRowObject.id}'>${currentRowObject.person.firstName}
			${currentRowObject.person.lastName}</a>
		</display:column>
		<display:column media="csv excel pdf xml rtf" title="NAME">
			${currentRowObject.person.firstName} ${currentRowObject.person.lastName}
		</display:column>

		<display:column media="html csv excel pdf xml rtf"
			property="role.roleName" title="USER TYPE" />
		<display:column media="html csv excel pdf xml rtf"
			property="manager.person.firstName" title="MANAGER" />


		<c:if test="${autho['USER_EDIT'] == 'Yes'}">
			<display:column media="html" title="EDIT" class="colCenter">
				<a href="#"
					onclick="window.open('edit.do?id=<c:out value="${currentRowObject.id}"/>','_parent')"><img
					src='<c:url value="/img/edit-icon.gif"/>' /></a>
			</display:column>
		</c:if>

		<c:if test="${autho['USER_DELETE'] == 'Yes'}">
			<display:column media="html" title="DELETE" class="colCenter">
				<a href="#"
					onclick="window.open('delete.do?id=<c:out value="${currentRowObject.id}"/>','_parent')"><img
					src='<c:url value="/img/icn_delete.gif"/>' /></a>
			</display:column>
		</c:if>

	</display:table>

</ajax:displayTag>