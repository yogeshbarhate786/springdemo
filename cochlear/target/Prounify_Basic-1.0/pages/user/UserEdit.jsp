<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags"
	prefix="ajax"%>

<body>
<form name='userForm' id='userForm'
	action='<c:url value="/admin/user/update.do"/>' method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" valign="top" class="dashTitle" colspan="2">User
		Details</td>
	</tr>
	<tr>
		<td width="100%" align="left" valign="top" class="boxBg">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="50%" align="left" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="common.salutation" /></td>
						<td width="60%"><select id="personsalutation"
							name="person.salutation">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${salutationList}" var="Salutation">
								<c:if test="${userData.person.salutation == Salutation}">
									<option value="${Salutation}" selected="selected">
									${Salutation}</option>
								</c:if>
								<c:if test="${userData.person.salutation != Salutation}">
									<option value="${Salutation}">${Salutation}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.firstname" /><span class="mandtry">
						* </span></td>
						<td width="60%"><input id="personfirstName"
							name="person.firstName" type="text"
							value="${userData.person.firstName}" maxlength="50" /></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.middlename" /></td>
						<td width="60%"><input id="personmiddleName"
							name="person.middleName" type="text"
							value="${userData.person.middleName}" maxlength="50" /></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.lastname" /><span class="mandtry">
						* </span></td>
						<td width="60%"><input id="personlastName"
							name="person.lastName" type="text"
							value="${userData.person.lastName}" maxlength="50" /></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.gender" /></td>
						<td width="60%"><select id="persongender"
							name="person.gender">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${genderList}" var="Gender">
								<c:if test="${userData.person.gender == Gender}">
									<option value="${Gender}" selected="selected">
									${Gender}</option>
								</c:if>
								<c:if test="${userData.person.gender != Gender}">
									<option value="${Gender}">${Gender}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.maritalstatus" /></td>
						<td width="60%"><select id="personmaritalStatus"
							name="person.maritalStatus">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${maritalStatusList}" var="MaritalStatus">
								<c:if test="${userData.person.maritalStatus == MaritalStatus}">
									<option value="${MaritalStatus}" selected="selected">
									${MaritalStatus}</option>
								</c:if>
								<c:if test="${userData.person.maritalStatus != MaritalStatus}">
									<option value="${MaritalStatus}">${MaritalStatus}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.nickname" /></td>
						<td width="60%"><input id="personshortName"
							name="person.shortName" type="text"
							value="${userData.person.shortName}" maxlength="20" /></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="common.role" /><span class="mandtry"> *
						</span></td>
						<td width="60%"><select id="roleId" name="role.id">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${roleList}" var="Role">
								<c:if test="${userData.role.id == Role.id}">
									<option value="${Role.id}" selected="selected">
									${Role.roleName}</option>
								</c:if>
								<c:if test="${userData.role.id != Role.id}">
									<option value="${Role.id}">${Role.roleName}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="common.manager" /> <fmt:message
							key="common.name" /></td>
						<td width="60%"><select id="managerId" name="manager.id">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${managerList}" var="Manager">
								<c:if test="${userData.manager.id == Manager.id}">
									<option value="${Manager.id}" selected="selected">
									${Manager.person.firstName} ${Manager.person.firstName} -
									${Manager.role.roleName}</option>
								</c:if>
								<c:if test="${userData.manager.id != Manager.id}">
									<option value="${Manager.id}">
									${Manager.person.firstName} ${Manager.person.lastName} -
									${Manager.role.roleName}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="common.login" /> <fmt:message key="common.name" />
						<span class="mandtry"> * </span></td>
						<td width="60%"><input id="loginloginName"
							name="login.loginName" type="text"
							value="${userData.login.loginName}" maxlength="20" /></td>
					</tr>
				</table>
				</td>
				<td width="50%" align="left" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.address.fulladdress" /></td>
						<td width="60%"><input id="personaddressaddressFull"
							name="person.address.addressFull" type="text"
							value="${userData.person.address.addressFull}" maxlength="100" />
						</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.address.streetaddress" /></td>
						<td width="60%"><input id="personaddressaddressStreet"
							name="person.address.addressStreet" type="text"
							value="${userData.person.address.addressStreet}" maxlength="100" />
						</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.address.country" /></td>
						<td width="60%"><select id="personaddresscountryid"
							name="person.address.country.id">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${countryList}" var="Country">
								<c:if test="${userData.person.address.country.id==Country.id}">
									<option value="${Country.id}" selected="selected">
									${Country.countryName}</option>
								</c:if>
								<c:if test="${userData.person.address.country.id != Country.id}">
									<option value="${Country.id}">${Country.countryName}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.address.state" /></td>
						<td width="60%"><select id="personaddressstateid"
							name="person.address.state.id">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${stateList}" var="State">
								<c:if test="${userData.person.address.state.id == State.id}">
									<option value="${State.id}" selected="selected">
									${State.stateName}</option>
								</c:if>
								<c:if test="${userData.person.address.state.id != State.id}">
									<option value="${State.id}">${State.stateName}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.address.city" /></td>
						<td width="60%"><select id="personaddresscityid"
							name="person.address.city.id">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${cityList}" var="City">
								<c:if test="${userData.person.address.city.id == City.id}">
									<option value="${City.id}" selected="selected">
									${City.cityName}</option>
								</c:if>
								<c:if test="${userData.person.address.city.id != City.id}">
									<option value="${City.id}">${City.cityName}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.occupation" /></td>
						<td width="60%"><select id="personoccupationid"
							name="person.occupation.id">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${occupationList}" var="Occupation">
								<c:if test="${userData.person.occupation.id == Occupation.id}">
									<option value="${Occupation.id}" selected="selected">
									${Occupation.occupationName}</option>
								</c:if>
								<c:if test="${userData.person.occupation.id != Occupation.id}">
									<option value="${Occupation.id}">
									${Occupation.occupationName}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.birthdate" /></td>

						<td width="60%"><input id="personbirthDate"
							name="person.birthDate"
							value='<fmt:formatDate pattern="dd/MM/yyyy" value="${userData.person.birthDate}"></fmt:formatDate>'
							readonly="readonly" size="11" /> <img
							src="../../images/icon/calender.gif" alt="click" id="cal-img-1"
							title="Click" /> <script type="text/javascript">
                                                            Calendar.setup({
                                                                inputField: "personbirthDate",
                                                                button: "cal-img-1",
                                                                align: "Tr"
                                                            });
                                                        </script></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.nativelanguage" /></td>
						<td width="60%"><input id="personnativeLanguage"
							name="person.nativeLanguage" type="text"
							value="${userData.person.nativeLanguage}" maxlength="20" /></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="common.skills" /> <fmt:message
							key="common.name" /></td>
						<td width="60%">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="45%" align="right"><select name="Skills"
									id="skillId" multiple="true"
									style="width: 100px; height: 75px;">
									<c:forEach items="${Skills}" var="skill">
										<c:if test="${skill.selected==false}">
											<option value="${skill.id}">${skill.name}</option>
										</c:if>
									</c:forEach>
								</select></td>
								<td width="10%" align="center"><img alt="Click"
									title="Click" src="../../images/skill_btn1.gif"
									onClick="addSkillToUserSkill()" name="addToUserSkill" /> <br>
								<img alt="Click" title="Click" src="../../images/skill_btn2.gif"
									onClick="addAllSkillToUserSkill()" name="addAllToUserSkill" />
								<br>
								<img alt="Click" title="Click" src="../../images/skill_btn3.gif"
									onClick="addAllUserSkillToSkill()" name="addAllToSkill" /> <br>
								<img alt="Click" title="Click" src="../../images/skill_btn4.gif"
									onClick="addUserSkillToSkill()" name="addToSkill" /></td>
								<td width="45%" align="left"><select name="skill.id"
									multiple="true" id="userSkillId"
									style="width: 100px; height: 75px;">
									<c:forEach items="${Skills}" var="skill">
										<c:if test="${skill.selected==true}">
											<option value="${skill.id}" selected="selected">
											${skill.name}</option>
										</c:if>
									</c:forEach>
								</select></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td align="right" valign="top">&nbsp;</td>
						<td align="left" valign="top"><c:if
							test="${autho['USER_ADD'] == 'Yes'}">
							<input type="submit" class="buttonBg"
								name='<fmt:message
                                                            key="update" />'
								value='<fmt:message
                                                            key="update" />' />
						</c:if> <c:if test="${autho['USER_LIST'] == 'Yes'}">
							<input type="button" class="buttonBg"
								name='<fmt:message
                                                            key="goToList" />'
								value='<fmt:message
                                                            key="goToList" />'
								onclick="window.open('list.do','_parent')" />
						</c:if></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<input name="id" type="hidden" value="${userData.id}" /></form>
<c:url var="urlTogetStateList" value="/admin/customer/getStateList.do" />
<ajax:select source="personaddresscountryid"
	target="personaddressstateid" baseUrl="${urlTogetStateList}"
	parameters="countryId={personaddresscountryid}">
</ajax:select>
<c:url var="urlTogetCityList" value="/admin/customer/getCityList.do" />
<ajax:select source="personaddressstateid" target="personaddresscityid"
	baseUrl="${urlTogetCityList}"
	parameters="stateId={personaddressstateid}">
</ajax:select>
<c:url var="urlTogetManagerList" value="/admin/user/getManagerList.do" />
<ajax:select source="roleId" target="managerId"
	baseUrl="${urlTogetManagerList}" parameters="roleId={roleId}">
</ajax:select>
<script type="text/javascript">
   function addSkillToUserSkill() {
       var len1 = document.getElementById("skillId").options.length;
       for (i = len1 - 1; i >= 0; i--) {
           if (document.getElementById("skillId").options[i].selected) {
               opt = document.createElement("option");
               opt.text = document.getElementById("skillId").options[i].text;
               opt.value = document.getElementById("skillId").options[i].value;
               opt.selected = true;
               document.getElementById("userSkillId").options.add(opt);
               document.getElementById("skillId").remove(i);
           }
       }
   }

   function addUserSkillToSkill() {
       var len1 = document.getElementById("userSkillId").options.length;
       for (i = len1 - 1; i >= 0; i--) {
           if (document.getElementById("userSkillId").options[i].selected) {
               opt = document.createElement("option");
               opt.text = document.getElementById("userSkillId").options[i].text;
               opt.value = document.getElementById("userSkillId").options[i].value;
               document.getElementById("skillId").options.add(opt);
               document.getElementById("userSkillId").remove(i);
           }
       }
   }

   function addAllSkillToUserSkill() {
       var len1 = document.getElementById("skillId").options.length;
       for (i = len1 - 1; i >= 0; i--)

       {
           opt = document.createElement("option");
           opt.text = document.getElementById("skillId").options[i].text;
           opt.value = document.getElementById("skillId").options[i].value;
           opt.selected = true;
           document.getElementById("userSkillId").options.add(opt);
           document.getElementById("skillId").remove(i);

       }
   }

   function addAllUserSkillToSkill() {
       var len1 = document.getElementById("userSkillId").options.length;
       for (i = len1 - 1; i >= 0; i--) {
           opt = document.createElement("option");
           opt.text = document.getElementById("userSkillId").options[i].text;
           opt.value = document.getElementById("userSkillId").options[i].value;
           document.getElementById("skillId").options.add(opt);
           document.getElementById("userSkillId").remove(i);
       }
   }
</script>
<script language="JavaScript" type="text/javascript">
    var frmvalidator = new Validator("userForm");
    frmvalidator.addValidation("personfirstName","req","'First Name' Is Mandatory");
	frmvalidator.addValidation("personlastName","req","'Last Name' Is Mandatory");
	frmvalidator.addValidation("roleId","dontselect=0","'Role' Is Mandatory");
    frmvalidator.addValidation("loginloginName", "req", "'Login Name' Is Mandatory");
    frmvalidator.addValidation("loginloginName", "maxlen=20", "Max length For 'Login Name' is 20");
    frmvalidator.addValidation("loginloginName", "alphanumeric_space", "Only Alphanumberic Characters With Space Are Allowed For 'Login Name'");
</script>
</body>