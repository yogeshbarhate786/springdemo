<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags"
	prefix="ajax"%>

<body>
<form name='userForm' id='userForm'
	action='<c:url value="/admin/user/profileUpdate.do"/>' method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" valign="top" class="dashTitle" colspan="2">User
		Details</td>
	</tr>
	<tr>
		<td width="100%" align="left" valign="top" class="boxBg">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="50%" align="left" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="common.salutation" /></td>
						<td width="60%"><select id="personsalutation"
							name="person.salutation">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${salutationList}" var="Salutation">
								<c:if test="${userData.person.salutation == Salutation}">
									<option value="${Salutation}" selected="selected">
									${Salutation}</option>
								</c:if>
								<c:if test="${userData.person.salutation != Salutation}">
									<option value="${Salutation}">${Salutation}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.firstname" /><span class="mandtry">
						* </span></td>
						<td width="60%"><input id="personfirstName"
							name="person.firstName" type="text"
							value="${userData.person.firstName}" maxlength="50" /></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.middlename" /></td>
						<td width="60%"><input id="personmiddleName"
							name="person.middleName" type="text"
							value="${userData.person.middleName}" maxlength="50" /></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.lastname" /><span class="mandtry">
						* </span></td>
						<td width="60%"><input id="personlastName"
							name="person.lastName" type="text"
							value="${userData.person.lastName}" maxlength="50" /></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.gender" /></td>
						<td width="60%"><select id="persongender"
							name="person.gender">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${genderList}" var="Gender">
								<c:if test="${userData.person.gender == Gender}">
									<option value="${Gender}" selected="selected">
									${Gender}</option>
								</c:if>
								<c:if test="${userData.person.gender != Gender}">
									<option value="${Gender}">${Gender}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.maritalstatus" /></td>
						<td width="60%"><select id="personmaritalStatus"
							name="person.maritalStatus">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${maritalStatusList}" var="MaritalStatus">
								<c:if test="${userData.person.maritalStatus == MaritalStatus}">
									<option value="${MaritalStatus}" selected="selected">
									${MaritalStatus}</option>
								</c:if>
								<c:if test="${userData.person.maritalStatus != MaritalStatus}">
									<option value="${MaritalStatus}">${MaritalStatus}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.nickname" /></td>
						<td width="60%"><input id="personshortName"
							name="person.shortName" type="text"
							value="${userData.person.shortName}" maxlength="20" /></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="common.login" /> <fmt:message key="common.name" />
						</td>
						<td width="60%">${userData.login.loginName}</td>
					</tr>
				</table>
				</td>
				<td width="50%" align="left" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.address.fulladdress" /></td>
						<td width="60%"><input id="personaddressaddressFull"
							name="person.address.addressFull" type="text"
							value="${userData.person.address.addressFull}" maxlength="100" />
						</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.address.streetaddress" /></td>
						<td width="60%"><input id="personaddressaddressStreet"
							name="person.address.addressStreet" type="text"
							value="${userData.person.address.addressStreet}" maxlength="100" />
						</td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.address.country" /></td>
						<td width="60%"><select id="personaddresscountryid"
							name="person.address.country.id">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${countryList}" var="Country">
								<c:if test="${userData.person.address.country.id==Country.id}">
									<option value="${Country.id}" selected="selected">
									${Country.countryName}</option>
								</c:if>
								<c:if test="${userData.person.address.country.id != Country.id}">
									<option value="${Country.id}">${Country.countryName}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.address.state" /></td>
						<td width="60%"><select id="personaddressstateid"
							name="person.address.state.id">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${stateList}" var="State">
								<c:if test="${userData.person.address.state.id == State.id}">
									<option value="${State.id}" selected="selected">
									${State.stateName}</option>
								</c:if>
								<c:if test="${userData.person.address.state.id != State.id}">
									<option value="${State.id}">${State.stateName}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.address.city" /></td>
						<td width="60%"><select id="personaddresscityid"
							name="person.address.city.id">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${cityList}" var="City">
								<c:if test="${userData.person.address.city.id == City.id}">
									<option value="${City.id}" selected="selected">
									${City.cityName}</option>
								</c:if>
								<c:if test="${userData.person.address.city.id != City.id}">
									<option value="${City.id}">${City.cityName}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.occupation" /></td>
						<td width="60%"><select id="personoccupationid"
							name="person.occupation.id">
							<option value=""><fmt:message
								key="common.listbox.select" /></option>
							<c:forEach items="${occupationList}" var="Occupation">
								<c:if test="${userData.person.occupation.id == Occupation.id}">
									<option value="${Occupation.id}" selected="selected">
									${Occupation.occupationName}</option>
								</c:if>
								<c:if test="${userData.person.occupation.id != Occupation.id}">
									<option value="${Occupation.id}">
									${Occupation.occupationName}</option>
								</c:if>
							</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.birthdate" /></td>
						<td width="60%"><input id="personbirthDate"
							name="person.birthDate"
							value='<fmt:formatDate pattern="dd/MM/yyyy" value="${userData.person.birthDate}"></fmt:formatDate>'
							readonly="readonly" size="11" /> <img
							src="../../images/icon/calender.gif" alt="click" id="cal-img-1"
							title="Click" /> <script type="text/javascript">
                                                            Calendar.setup({
                                                                inputField: "personbirthDate",
                                                                button: "cal-img-1",
                                                                align: "Tr"
                                                            });
                                                        </script></td>
					</tr>
					<tr>
						<td width="40%" align="left" valign="top" class="leftColon">
						<fmt:message key="person.nativelanguage" /></td>
						<td width="60%"><input id="personnativeLanguage"
							name="person.nativeLanguage" type="text"
							value="${userData.person.nativeLanguage}" maxlength="20" /></td>
					</tr>
					<tr>
						<td align="right" valign="top">&nbsp;</td>
						<td align="left" valign="top"><c:if
							test="${autho['USER_PROFILE_EDIT'] == 'Yes'}">
							<input type="submit" class="buttonBg"
								name='<fmt:message key="update" />'
								value='<fmt:message key="update" />' />
						</c:if> <input type="button" class="buttonBg"
							name='<fmt:message key="back" />'
							value='<fmt:message key="back" />'
							onclick="window.open('profileView.do','_parent')" /></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<input name="id" type="hidden" value="${userData.id}" /></form>
<c:url var="urlTogetStateList" value="/admin/customer/getStateList.do" />
<ajax:select source="personaddresscountryid"
	target="personaddressstateid" baseUrl="${urlTogetStateList}"
	parameters="countryId={personaddresscountryid}">
</ajax:select>
<c:url var="urlTogetCityList" value="/admin/customer/getCityList.do" />
<ajax:select source="personaddressstateid" target="personaddresscityid"
	baseUrl="${urlTogetCityList}"
	parameters="stateId={personaddressstateid}">
</ajax:select>
<c:url var="urlTogetManagerList" value="/admin/user/getManagerList.do" />
<ajax:select source="roleId" target="managerId"
	baseUrl="${urlTogetManagerList}" parameters="roleId={roleId}">
</ajax:select>

<script language="JavaScript" type="text/javascript">
    var frmvalidator = new Validator("userForm");
    frmvalidator.addValidation("personfirstName","req","'First Name' Is Mandatory");
	frmvalidator.addValidation("personlastName","req","'Last Name' Is Mandatory");
	
</script>
</body>