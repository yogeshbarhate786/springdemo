<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="html" uri="http://jakarta.apache.org/struts/tags-html"%>
<%@ taglib prefix="bean" uri="http://jakarta.apache.org/struts/tags-bean"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags" prefix="ajax"%>
	
<body>


<table width="100%" border="0" cellspacing="0" cellpadding="0" class="boxBg">
	<tr>
		<td>
			<fieldset>
			<legend class="dashTitle">Person Details</legend>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="55%" align="left" valign="top">
						<table width="100%" border="0" cellspacing="5" cellpadding="5">
							<tr>
								<td align="left" class="leftColon"><fmt:message key="common.salutation" /></td>
								<td>${personData.salutation} </td>
							</tr>
							<tr>
								<td align="left" class="leftColon"><fmt:message key="person.firstname" /></td>
								<td>${personData.firstName} </td>
							</tr>
							<tr>
								<td align="left" class="leftColon"><fmt:message key="person.middlename" /></td>
								<td>${personData.middleName} </td>
							</tr>
							<tr>
								<td align="left" class="leftColon"><fmt:message key="person.lastname" /></td>
								<td>${personData.lastName} </td>
							</tr>
							<tr>
								<td align="left" class="leftColon"><fmt:message key="person.gender" /></td>
								<td>${personData.gender} </td>
							</tr>
							<tr>
								<td align="left" class="leftColon"><fmt:message key="person.maritalstatus" /></td>
								<td>${personData.maritalStatus} </td>
							</tr>
							<tr>
								<td align="left" class="leftColon"><fmt:message key="person.maritalstatus" /></td>
								<td>${personData.maritalStatus} </td>
							</tr>
						</table>
						</td>
						<td width="55%" align="left" valign="top">
						   <table width="100%" border="0" cellspacing="0" cellpadding="5">
						   <tr>
								<td align="left" class="leftColon"><fmt:message key="person.address.fulladdress" /></td>
								<td>${personData.address.addressFull} </td>
							</tr>
							<tr>
								<td align="left" class="leftColon"><fmt:message key="person.address.streetaddress" /></td>
								<td>${personData.address.addressStreet} </td>
							</tr>
							<tr>
								<td align="left" class="leftColon"><fmt:message key="person.address.country" /></td>
								<td>${personData.address.country.countryName} </td>
							</tr>
							<tr>
								<td align="left" class="leftColon"><fmt:message key="person.address.state" /></td>
								<td>${personData.address.state.stateName} </td>
							</tr>
							</table>
					</tr>
					<tr>
						<td align="left" valign="top">&nbsp;</td>
						<td align="right" valign="top"><input type="button" class="buttonBg" name='<fmt:message key="edit" />'
							value='<fmt:message key="edit" />' onclick="window.open('edit.do?personid=<c:out value="${personData.id}"/>','_parent')" />
						<input type="button" class="buttonBg" name='<fmt:message key="goToList" />'
							value='<fmt:message key="goToList" />' onclick="window.open('list.do','_parent')" /></td>
					</tr>
				</table>
			</fieldset>
		</td>
	</tr>
</table>

</body>
