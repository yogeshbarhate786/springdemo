<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html"%>
<%@ taglib prefix="bean"
	uri="http://jakarta.apache.org/struts/tags-bean"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags"
	prefix="ajax"%>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<form id='personForm' name='personForm'
		action='<c:url value="/admin/person/update.do"/>' method="post">
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			class="boxBg">
			<tr>
				<td>
					<fieldset>
						<legend>Person Details</legend>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="55%" align="left" valign="top">
									<table width="100%" border="0" cellspacing="5" cellpadding="5">

										<tr>
											<td align="left" class="leftColon"><fmt:message
													key="common.salutation" />
											<td width="60%"><select id="salutation"
												name="salutation" style="width: 182px">
													<option value="">
														<fmt:message key="common.listbox.select" />
													</option>
													<c:forEach items="${salutationList}" var="Person">
														<c:if test="${personData.salutation == Person}">
															<option value="${Person}" selected="selected">
																${Person}</option>
														</c:if>
														<c:if test="${personData.salutation != Person}">
															<option value="${Person}">${Person}</option>
														</c:if>
														
													</c:forEach>
											</select>
										</tr>

										<tr>
											<td align="left" class="leftColon"><fmt:message
													key="person.firstname" /><span class="mandtry">*</span></td>
											<td><input id="firstName" name="firstName"
												value="${personData.firstName}" type="text"
												style="width: 182px" maxlength="50" /></td>
										</tr>
										<tr>
											<td align="left" class="leftColon"><fmt:message
													key="person.middlename" /></td>
											<td><input id="middleName" name="middleName"
												value="${personData.middleName}" type="text"
												style="width: 182px" maxlength="50" /></td>
										</tr>
										<tr>
											<td align="left" class="leftColon"><fmt:message
													key="person.lastname" /><span class="mandtry">*</span></td>
											<td><input id="lastName" name="lastName"
												value="${personData.lastName}" type="text"
												style="width: 182px" maxlength="50" /></td>
										</tr>
										<tr>
											<td align="left" class="leftColon"><fmt:message
													key="person.gender" />
											<td width="60%"><select id="gender" name="gender"
												style="width: 182px">
													<option value="">
														<fmt:message key="common.listbox.select" />
													</option>
													<c:forEach items="${genderList}" var="Person">
														<c:if test="${personData.gender == Person}">
															<option value="${Person}" selected="selected">${Person}</option>
														</c:if>
														<c:if test="${personData.gender != Person}">
															<option value="${Person}">${Person}</option>
														</c:if>
													</c:forEach>

											</select>
										</tr>
										<tr>
											<td align="left" class="leftColon"><fmt:message
													key="person.maritalstatus" />
											<td width="60%"><select id="maritalStatus"
												name="maritalStatus" style="width: 182px">
													<option value="">
														<fmt:message key="common.listbox.select" />
													</option>
													<c:forEach items="${maritalStatusList}" var="Person">
														<c:if test="${personData.maritalStatus == Person}">
															<option value="${Person}" selected="selected">${Person}</option>
														</c:if>
														<c:if test="${personData.maritalStatus != Person}">
															<option value="${Person}">${Person}</option>
														</c:if>
													</c:forEach>
											</select>
										</tr>
									</table>
								</td>

								<td>
									<table width="100%" align="left" valign="top">
										<tr>
											<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
													key="person.address.fulladdress" /></td>
											<td width="60%"><input id="addressFull"
												name="address.addressFull" type="text"
												value="${personData.address.addressFull}" maxlength="100"
												style="width: 182px" /></td>
										</tr>
										<tr>
											<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
													key="person.address.streetaddress" /></td>
											<td width="60%"><input id="addressStreet"
												name="address.addressStreet" type="text"
												value="${personData.address.addressStreet}" maxlength="100"
												style="width: 182px" /></td>
										</tr>
										<tr>
											<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
													key="person.address.country" /><span class="mandtry">*</span></td>
											<td width="60%"><select id="countryID"
												name="address.country.id" style="width: 182px">
													<option value="">
														<fmt:message key="common.listbox.select" />
													</option>
													<c:forEach items="${CountryList}" var="Country">
														<c:choose>
															<c:when
																test="${personData.address.country.id == Country.id}">
																<option value="${Country.id}" selected="selected">${Country.countryName}</option>
															</c:when>
															<c:otherwise>
																<option value="${Country.id}">${Country.countryName}</option>
															</c:otherwise>
														</c:choose>
													</c:forEach>

													<!--  
													<c:forEach items="${countryList}" var="Country">
														<c:if
															test="${personData.address.country.id == Country.id}">
															<option value="${Country.id}" selected="selected">${Country.countryName}</option>
														</c:if>
														<c:if
															test="${personData.address.country.id != Country.id}">
															<option value="${Country.id}">${Country.countryName}</option>
														</c:if>
													</c:forEach>
											-->
											</select></td>
										</tr>
										<tr>
											<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
													key="person.address.state" /><span class="mandtry">*</span></td>
											<td width="60%"><select id="stateID"
												name="address.state.id" style="width: 182px">
													<option value="">
														<fmt:message key="common.listbox.select" />
													</option>

													<c:forEach items="${StateList}" var="State">
														<c:choose>
															<c:when
																test="${personData.address.state.id == State.id}">
																<option value="${State.id}" selected="selected">${State.stateName}</option>
															</c:when>
															<c:otherwise>
																<option value="${State.id}">${State.stateName}</option>
															</c:otherwise>
														</c:choose>
													</c:forEach>
                                                                              <!--  
													<c:forEach items="${stateList}" var="State">
														<c:if test="${personData.address.state.id == State.id}">
															<option value="${State.id}" selected="selected">${State.stateName}</option>
														</c:if>
														<c:if test="${personData.address.state.id != State.id}">
															<option value="${State.id}">${State.stateName}</option>
														</c:if>
													</c:forEach>
													
													-->
											</select>
										</tr>
										<tr>
								<td width="40%" align="left" valign="top" class="leftColon">
									<fmt:message key="person.address.city" />&nbsp;<span class="mandtry">*</span></td>
								<td width="60%"><select id="cityID" name="address.city.id" style="width: 182px">
									<option value=""><fmt:message key="common.listbox.select" /></option>
									<c:forEach items="${CityList}" var="City">
										<c:choose>
											<c:when test="${personData.address.city.id == City.id}">
												<option value="${City.id}" selected="selected">${City.cityName}</option>
											</c:when>
											<c:otherwise>
												<option value="${City.id}">${City.cityName}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select></td>
							</tr>
										

									</table>
							</tr>
							<tr>
								<td align="left" valign="top"><input id="id" name="id"
									type="hidden" value="${personData.id }" /></td>

								<td align="right" valign="top"><input type="submit"
									class="buttonBg" name='<fmt:message key="update" />'
									value='<fmt:message key="update" />' /> <input type="button"
									class="buttonBg" name='<fmt:message key="goToList" />'
									value='<fmt:message key="goToList" />'
									onclick="window.open('list.do','_parent')" /></td>
							</tr>

						</table>

					</fieldset>
				</td>
			</tr>

		</table>
	</form>
	
<c:url var="urlTogetStateList" value="/admin/person/getCountryWiseStateList.do" />
<ajax:select source="countryID" target="stateID" 
	baseUrl="${urlTogetStateList}" parameters="countryID={countryID}">
</ajax:select>

<c:url var="urlTogetCityList" value="/admin/person/getStateWiseCityList.do" />
<ajax:select source="stateID" target="cityID"
	baseUrl="${urlTogetCityList}" parameters="stateID={stateID}">
</ajax:select>
	
</body>
