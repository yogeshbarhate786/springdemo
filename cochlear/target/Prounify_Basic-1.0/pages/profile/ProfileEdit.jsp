<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>

<body>

<form name='profileForm' id='profileForm'
	action='<c:url value="/admin/profile/update.do"/>' method="post">

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="left" valign="top" class="dashTitle" colspan="2">Profile
		Details</td>
	</tr>
	<tr>
		<td width="60%" align="left" valign="top" class="boxBg">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="25%" align="left" valign="top" class="leftColon"><fmt:message
					key="common.name" /><span class="mandtry">*</span></td>
				<td width="75%"><input name="name" id="name" type="text"
					value="${profileData.name }" /></td>
			</tr>
			<tr>
				<td width="25%" align="left" valign="top" class="leftColon"><fmt:message
					key="common.role" /></td>
				<td width="75%"><input name="roleName" id="roleName"
					type="text" readonly="readonly"
					value="${profileData.role.roleName }" /></td>
			</tr>
			<tr>
				<td width="25%" align="left" valign="top" class="leftColon"><fmt:message
					key="common.desc" /></td>
				<td width="75%"><textarea name="description" id="description"
					rows="2" cols="25" maxlength="254"
					onkeyup="return ismaxlength(this)">${profileData.description}</textarea>
				</td>
			</tr>
			<tr>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top"><c:if
					test="${autho['PROFILE_EDIT'] == 'Yes'}">
					<input  type="submit" class="buttonBg" 
						name='<fmt:message
											key="update" />'
						value='<fmt:message
											key="update" />' />
				</c:if> <c:if test="${autho['PROFILE_LIST'] == 'Yes'}">
					<input  type="button" class="buttonBg" 
						name='<fmt:message
											key="goToList" />'
						value='<fmt:message
											key="goToList" />'
						onclick="window.open('list.do','_parent')" />
				</c:if></td>
			</tr>




		</table>
		</td>
		<td width="40%" align="left" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="40%" align="left" valign="top"></td>
				<td width="60%"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>


<div align="left" class="dashTitle">Profile Authorization</div>
<display:table name="AUTHO_LIST" class="its" id="currentRowObject"
	requestURI="edit.do">
	<display:column media="html" class="colCenter">
		<input name="PROFILE_AUTHO_${currentRowObject.id}" type="checkbox"
			value="${currentRowObject.id}"
			<c:if test="${currentRowObject.checked==true}">
					checked="checked" 
					</c:if> />
	</display:column>
	<display:column media="html" title="NAME" group="1">
		<c:out value="${currentRowObject.name}" />
	</display:column>
	<display:column media="html" title="DESCRIPTION">
		<c:out value="${currentRowObject.description}" />
	</display:column>
</display:table> <input name="id" type="hidden" value="${profileData.id }"></form>
<script language="JavaScript" type="text/javascript">	 
	 var frmvalidator  = new Validator("profileForm");
	  frmvalidator.addValidation("name","req","'Name' Is Mandatory");
	  frmvalidator.addValidation("name","maxlen=40","Max length For 'Name' is 40");  
	  frmvalidator.addValidation("name","alphanumeric_space","Only Alphanumberic Characters With Space Are Allowed For 'Name'");
	  frmvalidator.addValidation("description","maxlen=254","Max Length For Description Is 254");	  	
</script>

</body>