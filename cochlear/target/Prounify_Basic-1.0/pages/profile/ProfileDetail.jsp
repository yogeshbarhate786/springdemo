<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>

<body>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="left" valign="top" class="dashTitle" colspan="2">Profile
		Details</td>
	</tr>
	<tr>
		<td width="60%" align="left" valign="top" class="boxBg">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="25%" align="left" valign="top" class="leftColon"><fmt:message
					key="common.name" /></td>
				<td width="75%">${profileData.name }</td>
			</tr>
			<tr>
				<td width="25%" align="left" valign="top" class="leftColon"><fmt:message
					key="common.role" /></td>
				<td width="75%">${profileData.role.roleName}</td>
			</tr>
			<tr>
				<td width="25%" align="left" valign="top" class="leftColon"><fmt:message
					key="common.desc" /></td>
				<td width="75%">${profileData.description}</td>
			</tr>
			<tr>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top"><c:if
					test="${autho['PROFILE_EDIT'] == 'Yes'}">
					<input  type="button" class="buttonBg"  name='<fmt:message
					key="edit" />'
						value='<fmt:message
					key="edit" />'
						onclick="window.open('edit.do?id=<c:out value="${profileData.id}"/>','_parent')" />
				</c:if> <c:if test="${autho['PROFILE_LIST'] == 'Yes'}">
					<input  type="button" class="buttonBg"  name='<fmt:message
					key="goToList" />'
						value='<fmt:message
					key="goToList" />'
						onclick="window.open('list.do','_parent')" />
				</c:if></td>
			</tr>


		</table>
		</td>
		<td width="40%" align="left" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="40%" align="left" valign="top"></td>
				<td width="60%"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>


<div align="left" class="dashTitle">Profile Authorization</div>
<display:table name="AUTHO_LIST" class="its" id="currentRowObject"
	requestURI="edit.do">

	<display:column media="html" class="colCenter">
		<c:if test="${currentRowObject.checked==true}">
			<img alt="Yes" src="../../images/available.png" width="20"
				height="20" />
		</c:if>
		<c:if test="${currentRowObject.checked==false}">
			<img alt="Yes" src="../../images/unavailable.png" width="20"
				height="20" />
		</c:if>
	</display:column>

	<display:column media="html" title="NAME" group="1">
		<c:out value="${currentRowObject.name}" />
	</display:column>
	<display:column media="html" title="DESCRIPTION">
		<c:out value="${currentRowObject.description}" />
	</display:column>
</display:table>

</body>