<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags"
	prefix="ajax"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<body>
<form name='profileForm' id='profileForm'
	action='<c:url value="/admin/profile/create.do"/>' method="post">


<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="left" valign="top" class="dashTitle" colspan="2">Profile
		Details</td>
	</tr>
	<tr>
		<td width="60%" align="left" valign="top" class="boxBg">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
					key="common.name" /><span class="mandtry">*</span></td>
				<td width="60%"><input name="name" id="name" type="text"
					value="" /></td>
			</tr>
			<tr>
				<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
					key="common.role" /><span class="mandtry">*</span></td>
				<td width="60%"><select id="roleid" name="role.id">
					<option value=""><fmt:message key="common.listbox.select" /></option>
					<c:forEach items="${roleList}" var="Role">
						<option value="${Role.id}">${Role.roleName}</option>
					</c:forEach>
				</select> <c:if test="${fn:length(roleList) == 0 }">
				No role exist to create profile click <a
						href="../../admin/role/add.do" style="color: blue;">here</a> to create new role.</c:if>
				</td>
			</tr>
			<tr>
				<td width="40%" align="left" valign="top" class="leftColon"><fmt:message
					key="common.desc" /></td>
				<td width="60%"><textarea name="description" id="description"
					rows="2" cols="25" maxlength="254"
					onkeyup="return ismaxlength(this)"></textarea></td>
			</tr>
			<tr>
				<td align="left" valign="top">&nbsp;</td>
				<td align="left" valign="top"><c:if
					test="${autho['PROFILE_ADD'] == 'Yes'}">
					<input  type="submit" class="buttonBg"  name='<fmt:message
						key="save" />'
						value='<fmt:message
						key="save" />' />
				</c:if> <c:if test="${autho['PROFILE_LIST'] == 'Yes'}">
					<input  type="button" class="buttonBg"  name='<fmt:message
						key="goToList" />'
						value='<fmt:message
						key="goToList" />'
						onclick="window.open('list.do','_parent')" />
				</c:if></td>
			</tr>

		</table>
		</td>
		<td width="40%" align="left" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td width="40%" align="left" valign="top"></td>
				<td width="60%"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>

</form>
<script language="JavaScript" type="text/javascript">	 
var frmvalidator  = new Validator("profileForm");
 frmvalidator.addValidation("name","req","'Name' Is Mandatory");
 frmvalidator.addValidation("name","maxlen=40","Max length For 'Name' is 40");  
 frmvalidator.addValidation("name","alphanumeric_space","Only Alphanumberic Characters With Space Are Allowed For 'Name'");
 frmvalidator.addValidation("roleid","dontselect=0","'Role' Is Mandatory");
 frmvalidator.addValidation("description","maxlen=254","Max Length For Description Is 254");	  	
</script>
</body>