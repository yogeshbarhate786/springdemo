<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
<style>
.button {
    background-color: #FE980F;
    border: none;
    color: black;
    padding: 10px 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
</style>
<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li class="active"><b>Your   Wishlist </b></li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					
					<c:if test="${not empty ProductList }">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description">Product Name</td>
							<td class="price">Price</td>
							<td class="description">Warranty</td>
							<td></td>
							<td></td>
						</tr>
					</thead>
					<tbody>
					<c:forEach items="${ProductList}" var="productlistvar">
						<tr>
							<td class="cart_product">
								<a href=""><img src=${productlistvar.picurl} height="150" width="150" alt=""></a>
							</td>
							<td class="cart_description">
								<p>${productlistvar.productname}</p>
							</td>
							<td class="cart_price">
								<p> <i class="fa fa-rupee" style="font-size:18px">    ${productlistvar.productprice}</i></p>
							</td>
							<td class="cart_description">
								<p>${productlistvar.warranty}</p>
							</td>
							
							<td class="cart_delete">
								<button class="button" type="button"
							onclick="window.open('../../admin/order/wishlist.do?productid=<c:out value="${productlistvar.prodid}"/>','_parent')">
								Remove
								</button>
							</td>
							<td>
							<button class="button" type="button"  onclick="window.open('../../admin/productdetails/addcart.do?productid=<c:out value="${productlistvar.prodid}"/>','_parent')"><i class="fa fa-shopping-cart"> Add To Cart</i></button>
						</td>
						</tr>
						
					</c:forEach>
					</tbody>
					</c:if>
						<c:if test="${empty ProductList}">
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td>
						    <h4>Oops Your WishList is empty please Add Product In Your Wishlist</h4>
							<button type="button"  class="button" class="btn btn-default"  style="margin-top: 22px"
								onclick="window.open('../../admin/dashboard/view.do','_parent')" >
						    		 Go To Home Page</button>
						</td>
					  
					</tr>
				
				</c:if>
                      
						
					</tbody>
				</table>
			</div>
		</div>
	</section>
</html>