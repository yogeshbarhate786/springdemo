<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Dashbord.js'></script>	
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
   <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
 <style>
.button {
    background-color: #FE980F;
    border: none;
    color: black;
    padding: 10px 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    border-radius: 5px;
}
</style>
<section id="cart_items">
<div class="container">
	<div class="breadcrumbs">
		<ol class="breadcrumb">
			<li class="active"><b>Order Confirmation</b></li>
		</ol>
	</div>
	<form action="" method="post"
		id="doctorForm" name="doctorForm" enctype="multipart/form-data">
		<h3 align="center">Thanks For Shopping</h3>
		<div class="table-responsive cart_info">
		
			<table class="table table-condensed">
			<tbody>

					<c:forEach items="${CartList}" var="cartlistvar">
						<tr>
							<td class="cart_product"><center><a href=""><img
									src=${cartlistvar.productid.picurl } height="100" width="100"
									alt=""></a></center></td>
							<td class="cart_description"><center>
								<p>${cartlistvar.productid.name}</p>
								<p>Rs.${cartlistvar.productid.price}</p>
								<p>Warranty: ${cartlistvar.productid.warranty}</p></center>
							</td>
							<td><input class="cart_quantity_input" type="text"
								name="quantity" value="${cartlistvar.quantity}" readonly
								onkeypress="return ValidateMobileNumber(event)" maxlength=2
								style="width: 50px;"></td>
							<td><center><i class="fa fa-rupee"></i>
							<c:out value="${cartlistvar.productid.price * cartlistvar.quantity}"/>
								</center>
							</td>
							<td><center>
							<button type="button"  class="button" id="btn${cartlistvar.id}" class="button" value="${cartlistvar.id}" onclick="Myorder(this.value)" >cancel order </button>
								</center>
							</td>
						</tr>
						<tr>
						
						
					</c:forEach>

					<tr>
					  <td colspan="3">&nbsp;</td>
						<td colspan="3">
							<table class="table table-condensed total-result">
								<tr class="shipping-cost">
									<td>Shipping Cost</td>
									<td>Free</td>
								</tr>
								<tr class="shipping-cost">
									<td>Gst </td>
									<td ><span id="gst"> ${gst}</span>  <i class="fa fa-rupee" style="font-size:15px"></i> </td>  
								</tr>
								<tr>
									<td>Total Bill</td>
									<td ><span id="totalbill">  ${TotalBillcount}</span> <i class="fa fa-rupee" style="font-size:15px"></i></td>
								</tr>
							</table>
						</td>
						
					</tr>
			 </tbody>
			</table>
		</div>
	</form>
</div>
</section>

<script>
function Myorder(cartid) {
  document.getElementById("btn"+cartid).innerHTML = "Order Cancelled";
  document.getElementById("btn"+cartid).disabled = 'disabled';
/*   xhttp.open("GET", "../../admin/order/myorder.do?cartid="+cartid, true);
  xhttp.send(); */
  $.ajax({
      type: "post",
      url:"../../admin/order/myorder.do",
  data : {
	   cartid : cartid,
      },
      
		dataType: "json",
      success: function(data){
      var cartsize = document.getElementById("totalbill").innerHTML;
      var gst = document.getElementById("gst").innerHTML;
	   document.getElementById("totalbill").innerHTML = data.cartlist;
	   document.getElementById("gst").innerHTML = data.gst;
      },
      error:function(e)
      {
         
      }
  });
  
}

</script>
</html>

