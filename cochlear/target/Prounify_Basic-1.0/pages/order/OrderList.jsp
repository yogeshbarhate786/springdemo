<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
 <script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Doctor.js'></script>
 
 <style>
 
.button {
    background-color: #FE980F;
    border: none;
    color: black;
    padding: 10px 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
   border-radius: 5px;
}
</style>
<section id="cart_items">
<div class="container">
    <div class="shopper-informations">
				<div class="row">
					<div class="col-sm-5 clearfix">
						<div class="bill-to">
							<div class="breadcrumbs">
							<ol class="breadcrumb">
								<li class="active"><b>Bill To</b></li>
							</ol>
						</div>
	                  
							<div class="form-one" >
								<form>
									<input type="text" value="${DoctorEditList.firstName}"  placeholder="First Name *">
									<input type="text" value="${DoctorEditList.middleName}" placeholder="Middle Name">
									<input type="text" value="${DoctorEditList.lastName}" placeholder="Last Name *">
									<input type="text" value="${DoctorEditList.email}"  placeholder="Email*">
									<input type="text" value="${DoctorEditList.addressid.addressFull}" placeholder="Address 1 *">
								    <input type="text" value="${DoctorEditList.addressid.addressStreet}" placeholder="Address 1 *">
								</form>
							</div>
							<div class="form-two">
								<form>
									<input type="text" value="${DoctorEditList.addressid.zipCode}" placeholder="Zip / Postal Code *">
									<select id="countryid" name="countryid" class="form-control"
								onchange="getState()">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
								<c:forEach items="${CountryList}" var="countrylistobj">
									<c:choose>
										<c:when
											test="${countrylistobj.id eq DoctorEditList.addressid.country.id}">
											<option value="${countrylistobj.id}" selected="selected">${countrylistobj.countryName}</option>
										</c:when>
										<c:otherwise>
											<option value="${countrylistobj.id}">${countrylistobj.countryName}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							    </select>
									<select id="stateid" name="stateid" class="form-control"
								onchange="getCity()">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
								<c:forEach items="${StateList}" var="statelistobj">
									<c:choose>
										<c:when
											test="${statelistobj.id eq DoctorEditList.addressid.state.id }">
											<option value="${statelistobj.id}" selected="selected">${statelistobj.stateName}</option>
										</c:when>
										<c:otherwise>
											<option value="${statelistobj.id}">${statelistobj.stateName}</option>

										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
							<select id="cityid" name="cityid" class="form-control">
								<option value="">
									<fmt:message key="common.listbox.select" />
								</option>
								<c:forEach items="${CityList}" var="citylistobj">
									<c:choose>
										<c:when test="${citylistobj.id eq DoctorEditList.addressid.city.id}">
											<option value="${citylistobj.id}" selected="selected">${citylistobj.cityName}</option>
										</c:when>
										<c:otherwise>
											<option value="${citylistobj.id}">${citylistobj.cityName}</option>

										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
									
									<input type="text" value="${DoctorEditList.mobile}" placeholder="Phone *">
								</form>
							</div>
						
						</div>
					</div>
					<!-- <div class="col-sm-4">
						<div class="order-message">
							<p>Shipping Order</p>
							<textarea name="message"  placeholder="Notes about your order, Special Notes for Delivery" rows="16"></textarea>
							<label><input type="checkbox"> Shipping to bill address</label>
						</div>	
					</div>			 -->		
				</div>
			</div>
	<div class="breadcrumbs">
		<ol class="breadcrumb">
			<li class="active"><b>Order Details</b></li>
		</ol>
	</div>
	
	<form action="../../admin/doctor/create.do" method="post"
		id="doctorForm" name="doctorForm" enctype="multipart/form-data">
		<div class="table-responsive cart_info">
			<table class="table table-condensed">
				<%-- <thead>
					<tr class="cart_menu">
						<td class="image"><center>Item</center></td>
						<td class="description"></td>
						<td class="price" style="width: 50px;">Quantity</td>
						<td class="price"><center>Total Price</center></td>
					</tr>
				</thead> --%>
				<tbody>

					<c:forEach items="${CartList}" var="cartlistvar">
						<tr>
							<td class="cart_product"><center><a href=""><img
									src=${cartlistvar.productid.picurl } height="100" width="100"
									alt=""></a></center></td>
							<td class="cart_description"><center>
								<p>${cartlistvar.productid.name}</p>
								<p>&#8377; ${cartlistvar.productid.price}</p>
								<p>Warranty: ${cartlistvar.productid.warranty}</p></center>
							</td>
							
							<td><h5>Quantity</h5><input class="cart_quantity_input" type="text"
								name="quantity" value="${cartlistvar.quantity}" readonly
								onkeypress="return ValidateMobileNumber(event)" maxlength=2
								style="width: 50px;"></td>
							<td><center><i class="fa fa-rupee"></i>
							<c:out value="${cartlistvar.productid.price * cartlistvar.quantity}"/>
								</center>
							</td>
						</tr>
					</c:forEach>

					<tr>
					  
						<td colspan="2">&nbsp;</td>
						<td colspan="2">
							<table class="table table-condensed total-result">
								<tr class="shipping-cost">
									<td>Shipping Cost</td>
									<td>Free</td>
							   </tr>
							   <tr>
							       <td>Gst</td>
									<td><c:out value=" ${gst}" /> <i class="fa fa-rupee" style="font-size:15px"></i></td>
							   </tr>
								<tr>
									<td>Total Bill</td>
									<td><c:out value=" ${TotalBill}" />  <i class="fa fa-rupee" style="font-size:15px"></i></td>
								</tr>
							</table>
						</td>
						
					</tr>
					<tr>
						
						<td colspan="2">&nbsp;</td>
						<td colspan="2">
							<table class="table table-condensed total-result">
								<tr class="shipping-cost">
										<td>
										
											<button type="submit" class="button" id="" value="Place Order" 
											formaction="../../admin/order/create.do">Confirm Order</button>
										</td>
								</tr>
								
							</table>
						</td>
					
					</tr>

				</tbody>
			</table>
		</div>
	</form>
</div>
</section>
</html>