<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://ajaxtags.sourceforge.net/tags/ajaxtags"
	prefix="ajax"%>

<c:if test="${autho['ROLE_ADD'] == 'Yes'}">
	<div align="right"><input  type="button" class="buttonBg" 
		name='<fmt:message key="add" />' value='<fmt:message key="add" />'
		onclick="window.open('add.do','_parent')" /></div>
</c:if>
<br>
<body>
<ajax:displayTag id="displayTagFrame">

	<display:table name="valueList" export="true" sort="list" pagesize="10"
		cellpadding="2" cellspacing="0" class="its" id="currentRowObject"
		requestURI="list.do?ajax=true">

		<display:setProperty name="export.excel.filename"
			value="Role List.xls" />
		<display:setProperty name="export.pdf.filename" value="Role List.pdf" />
		<display:setProperty name="export.xml.filename" value="Role List.xml" />
		<display:setProperty name="export.csv.filename" value="Role List.csv" />
		<display:setProperty name="export.rtf.filename" value="Role List.rtf" />

		<display:column media="html" title="ID" sortable="true"
			headerClass="sortable" sortProperty="id" class="colCenter">
			<a href='view.do?id=${currentRowObject.id}'>${currentRowObject.id}</a>
		</display:column>
		<display:column media="csv excel pdf xml rtf" property="id" title="ID"
			class="colCenter" />

		<display:column media="html" title="ROLE NAME" sortable="true"
			headerClass="sortable" sortProperty="roleName">
			<a href='view.do?id=${currentRowObject.id}'>${currentRowObject.roleName}</a>
		</display:column>
		<display:column media="csv excel pdf xml rtf" property="roleName"
			title="ROLE NAME" />

		<display:column media="html csv excel pdf xml rtf" property="roleDesc"
			title="DESCRIPTION" />

		<c:if test="${autho['ROLE_EDIT'] == 'Yes'}">
			<display:column media="html" title="EDIT" class="colCenter">
				<a href="#"
					onclick="window.open('edit.do?id=<c:out value="${currentRowObject.id}"/>','_parent')">
				<img src='<c:url value="/img/edit-icon.gif"/>' /></a>
			</display:column>
		</c:if>

		<c:if test="${autho['ROLE_DELETE'] == 'Yes'}">
			<display:column media="html" title="DELETE" class="colCenter">
				<a href="#"
					onclick="window.open('delete.do?id=<c:out value="${currentRowObject.id}"/>','_parent')">
				<img src='<c:url value="/img/icn_delete.gif"/>' /></a>
			</display:column>
		</c:if>

	</display:table>

</ajax:displayTag>

</body>
