<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tiles"
	uri="http://jakarta.apache.org/struts/tags-tiles-el"%>
<%@ taglib prefix="html"
	uri="http://jakarta.apache.org/struts/tags-html-el"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <!-- <link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/engine.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/util.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/dwr/interface/Product.js'></script>
<script type='text/javascript'
	src='${pageContext.request.contextPath}/js/cochlear.js'></script>
<style>
.button {
   background-color: #FE980F;
    border: none;
    color: black;
    padding: 13px 15px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 13px;
    margin: 8px 2px;
    cursor: pointer;
    border-radius: 23px;
    height: 42px;
    width: 100px;
}
.button1 {
   background-color: #11f13f;
    border: none;
    color: black;
    padding: 13px 15px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 13px;
    margin: 8px 2px;
    cursor: pointer;
    border-radius: 23px;
    height: 42px;
    width: 100px;
   
}
.button2 {
   background-color: #2da4e6;
    border: none;
    color: #f5f5f5;
    padding: 13px 15px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    margin: 8px 2px;
    cursor: pointer;
    border-radius: 23px;
    height: 46px;
    width: 100px;
   
}
.button3 {
   background-color: #11dcd0;
    border: none;
    color: #f5f5f5;
    padding: 13px 15px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    margin: 8px 2px;
    cursor: pointer;
    border-radius: 23px;
    height: 46px;
    width: 150px;
   
}
</style>
<div class="container">
	<c:if test="${not empty orderlist}">
		<c:forEach items="${orderlist}" var="odlistvar" varStatus="loop">
			<!-- Modal -->
			<div class="modal fade" id="myModal${loop.count }" role="dialog">
				<div class="modal-dialog modal-lg">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Order</h4>
							</div>
							<div class="modal-body">
								<c:forEach items="${odlistvar.cart}" var="cartlistvar"
									varStatus="loop1">
									<table width="100%"
										class="table table-striped table-bordered table-hover">
										<tr>
											<td width="40%" align="left" valign="top" class="leftColon">Cart
												Id</td>
											<td width="60%">${cartlistvar.cartid}</td>
										</tr>
										<tr>
											<td width="40%"><a href=""><img
													src=${cartlistvar.product_url } height="120" width="120"
													alt=""></a></td>
											<td width="60%">${cartlistvar.product_name}</td>
										</tr>

										<tr>
											<td width="40%" align="left" valign="top" class="leftColon">Quantity</td>
											<td width="60%">${cartlistvar.quantity}</td>
										</tr>

										<tr>
											<td width="40%" align="left" valign="top" class="leftColon">Product
												Price</td>
											<td width="60%">${cartlistvar.product_price}</td>
										</tr>

										<tr>
											<td width="40%" align="left" valign="top" class="leftColon">Bill</td>
											<td width="60%">${cartlistvar.bill}</td>
										</tr>

										<tr>
											<td width="40%" align="left" valign="top" class="leftColon">Total
												Bill</td>
											<td width="60%">${cartlistvar.totalbill}</td>
										</tr>



									</table>
								</c:forEach>
							</div>

						</div>
					</div>

				</div>
			</div>
				<div class="modal fade" id="ss${loop.count }"
							role="dialog">
							
                              <div class="modal-dialog modal-lg">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Profile</h4>
									</div>
									<div class="modal-body">
									<c:forEach items="${odlistvar.doctor}" var="doctorlistvar"
						                   varStatus="loop1">
										<table width="100%"
											class="table table-striped table-bordered table-hover">
											<tr>
												<td width="40%" align="left" valign="top" class="leftColon">First
													Name</td>
												<td width="60%">${doctorlistvar.firstName}</td>
											</tr>

											<tr>
												<td width="40%" align="left" valign="top" class="leftColon">Middle
													Name</td>
												<td width="60%">${doctorlistvar.middleName}</td>
											</tr>

											<tr>
												<td width="40%" align="left" valign="top" class="leftColon">Last
													Name</td>
												<td width="60%">${doctorlistvar.lastName}</td>
											</tr>

											<tr>
												<td width="40%" align="left" valign="top" class="leftColon">Gender</td>
												<td width="60%">${doctorlistvar.gender}</td>
											</tr>

											<tr>
												<td width="40%" align="left" valign="top" class="leftColon">Age</td>
												<td width="60%">${doctorlistvar.age}</td>
											</tr>

											<tr>
												<td width="40%" align="left" valign="top" class="leftColon">About
													Doctor</td>
												<td width="60%">${doctorlistvar.bio}</td>
											</tr>

											<tr>
												<td width="40%" align="left" valign="top" class="leftColon">Mobile
													Number</td>
												<td width="60%">${doctorlistvar.mobile}</td>
											</tr>

											<tr>
												<td width="40%" align="left" valign="top" class="leftColon">Full
													Address1</td>
												<td width="60%">${doctorlistvar.addressid.addressFull},
													${doctorlistvar.addressid.addressStreet}</td>
											</tr>

											<tr>
												<td width="40%" align="left" valign="top" class="leftColon">Full
													Address2</td>
												<td width="60%">${doctorlistvar.addressid.country.countryName},
													${doctorlistvar.addressid.state.stateName},
													${doctorlistvar.addressid.city.cityName},
													${doctorlistvar.addressid.zipCode}</td>
											</tr>

											<tr>
												<td width="40%" align="left" valign="top" class="leftColon">Specialist</td>
												<td width="60%">${doctorlistvar.specialist}</td>
											</tr>

											<tr>
												<td width="40%" align="left" valign="top" class="leftColon">Belong
													To Hospital</td>
												<td width="60%">${doctorlistvar.belongToHospital}</td>
											</tr>

										</table>
										</c:forEach>
									</div>

								</div>

							</div>
						
						</div>
		</c:forEach>
	
	</c:if>

</div>

<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
			</ol>
		</div>
		<form action="" method="post" id="doctorForm" name="doctorForm"
			enctype="multipart/form-data">
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<c:if test="${not empty orderlist}">
					<thead>
						<tr class="cart_menu">
							<td class="description">Sr.No</td>
							<td class="description">Order Id</td>
							<td class="description">Placed Order Date</td>
							<td class="description">Approve Status</td>
							<td class="description">Customer Details</td>
							<td></td>
							<td class="description">Action</td>

						</tr>
					</thead>
					<tbody>
					
							<c:forEach items="${orderlist}" var="odlistvar" varStatus="loop">
								<c:forEach items="${odlistvar.doctor}" var="doctorlist"
									varStatus="loop1">
									<tr>
										<td>${loop.count }</td>
										<td class="cart_description">
											<button type="button" class="button3"
												data-toggle="modal" data-target="#myModal${loop.count }">${odlistvar.orderid}</button>
										</td>

										<td class="cart_description">
											<%-- <p> ${odlistvar.placedorderdate}</p> --%> <%--  <h6><fmt:formatDate value="${odlistvar.placeddate}" pattern="dd-MM-yyyy" /></h6>  --%>
											<p>${odlistvar.placeddate}</p>
										</td>
										<td class="cart_description">
											<p>${odlistvar.approstatus}</p>
										</td>

										<%-- <td class="cart_description">
							<p>${odlistvar.doctorid.id}</p>
							</td> --%>
										<td class="cart_price"><button
												class="button2" data-toggle="modal"
												data-target="#ss${loop.count}" >Show</button></td>

										<td></td>
										<c:if test="${odlistvar.approstatus eq 'Pendding' }">
											<td>
												<button class="button" type="button"
													onclick="window.open('../../admin/telecaller/getorderlist.do?orderid=<c:out value="${odlistvar.orderid}"/>','_parent')">
													Approve</button>
											</td>
										</c:if>
										<c:if test="${odlistvar.approstatus eq 'varify' }">
											<td>
												<button class="button1" type="button">Verified</button>
											</td>
										</c:if>
									</tr>
								</c:forEach>
							</c:forEach>


						</c:if>
						<c:if test="${empty orderlist}">

							<tr align="center">

								<td>
									<h4>Still Order List Is Empty</h4>

								</td>

							</tr>

						</c:if>
					</tbody>
				</table>

				
			</div>
		</form>
	</div>
</section>