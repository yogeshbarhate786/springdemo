/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.75
 * Generated at: 2019-02-15 10:52:19 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.pages.ticket;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Success_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(3);
    _jspx_dependants.put("/WEB-INF/tlds/fn.tld", Long.valueOf(1323412722338L));
    _jspx_dependants.put("/WEB-INF/tlds/fmt.tld", Long.valueOf(1323412722338L));
    _jspx_dependants.put("/WEB-INF/tlds/c.tld", Long.valueOf(1323412722338L));
  }

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<script src=\"../../template/js/jquery.js\"></script>\r\n");
      out.write("\t<script src=\"../../template/js/bootstrap.min.js\"></script>\r\n");
      out.write("\t<script src=\"../../template/js/jquery.scrollUp.min.js\"></script>\r\n");
      out.write("\t<script src=\"../../template/js/price-range.js\"></script>\r\n");
      out.write("    <script src=\"../../template/js/jquery.prettyPhoto.js\"></script>\r\n");
      out.write("    <script src=\"../../template/js/main.js\"></script>\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
      out.write("<title>Insert title here</title>\r\n");
      out.write("<style>\r\n");
      out.write("/* CSS used here will be applied after bootstrap.css */\r\n");
      out.write(".qlt-confirmation {\r\n");
      out.write("    width: 30%;\r\n");
      out.write("    margin: 10px auto;\r\n");
      out.write(" }\r\n");
      out.write("  \r\n");
      out.write(".qlt-confirmation .panel-body {\r\n");
      out.write("  \twidth: 99%;\r\n");
      out.write(" \tmargin: 0 auto;      \r\n");
      out.write("        padding: 40px 10px;\r\n");
      out.write(" }\r\n");
      out.write("      .qlt-confirmation .panel-body .desc{\r\n");
      out.write("             \tmargin: 10px auto; \r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write(".qlt-confirmation .panel-body .notice {\r\n");
      out.write("       padding: 0px 20px; \r\n");
      out.write("        margin-top: 50px;\r\n");
      out.write("        text-align: left;\r\n");
      out.write("        font-style:italic;\r\n");
      out.write("        color: gray;\r\n");
      out.write("      }\r\n");
      out.write("\r\n");
      out.write("</style>\r\n");
      out.write("</head>\r\n");
      out.write("<script type='text/javascript'\r\n");
      out.write("\tsrc='");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("/dwr/engine.js'></script>\r\n");
      out.write("<script type='text/javascript'\r\n");
      out.write("\tsrc='");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.proprietaryEvaluate("${pageContext.request.contextPath}", java.lang.String.class, (javax.servlet.jsp.PageContext)_jspx_page_context, null, false));
      out.write("/dwr/util.js'></script>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<body>\r\n");
      out.write("\t<div class=\"qlt-confirmation\">\r\n");
      out.write("  \t<div class=\"panel panel-default\">\r\n");
      out.write("      <div class=\"panel-body\">\r\n");
      out.write("        <center>\r\n");
      out.write("        <img src=\"https://cdn4.iconfinder.com/data/icons/social-communication/142/open_mail_letter-512.png\" style=\"width:30px; height: 30px;\">\r\n");
      out.write("          <p class=\"desc\">Thank you for cmmunicate with us!<br>We've sent a confirmation link on your email.<br>Please visit your Email Box.</p>\r\n");
      out.write("        </center>\r\n");
      out.write("        \r\n");
      out.write("      </div>\r\n");
      out.write("      \r\n");
      out.write("\t</div>\r\n");
      out.write("</div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
